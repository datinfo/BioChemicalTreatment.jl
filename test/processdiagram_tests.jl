@testset "Simple ProcessDiagram" begin
    # Build simple system: one CSTR with influent
    @named process = OzoneDisinfection(; kO3=0, kd=1)
    @named react = CSTR(1, unknowns(states(process)), initial_states=[0.5, 0])
    @named inf = Influent([0.5, 1.0], unknowns(states(process)), flowrate=1)

    eqns = [
        connect(states(process), states(react))
        connect(rates(process), rates(react))
        connect(outflows(inf, 1), inflows(react, 1))
    ]

    # Test with only flow
    pd = ProcessDiagram([process, react, inf], eqns)
    @test pd.systems == [process, react, inf]
    @test pd.equations == eqns
    @test pd.chains == [[inf, react]]
    @test pd.shifts == [0]

    # Test with all systems
    pd = ProcessDiagram([process, react, inf], eqns; only_flow=false)
    @test pd.systems == [process, react, inf]
    @test pd.equations == eqns
    @test pd.chains == [[process], [inf, react]] || pd.chains == [[inf, react], [process]]
    @test pd.shifts == [1, 0] || pd.shifts == [0, 1]

    # Test invalid
    @test_throws ArgumentError ProcessDiagram([process, react], eqns[1:end-1])
    @test_logs (:warn, "Not all systems used in the connections are in the systems vector. Missing systems are:\n\tinf") min_level=Test.Logging.Warn ProcessDiagram([process, react], eqns)
    @test_logs (:warn, "Not all systems in the systems vector are used in the connections. Missing systems are:\n\tinf") min_level=Test.Logging.Warn ProcessDiagram([process, react, inf], eqns[1:end-1], only_flow=false)

    # Test setters
    set_linewidth!(pd, "4mm")
    @test pd.printing_options[:linewidth] == "4mm"
    set_nodedistance!(pd, "4mm")
    @test pd.printing_options[:nodedistance] == "4mm"
    set_blockwidth!(pd, "4mm")
    @test pd.printing_options[:blockwidth] == "4mm"
    set_blockheight!(pd, "4mm")
    @test pd.printing_options[:blockheight] == "4mm"
    set_innersep!(pd, "4mm")
    @test pd.printing_options[:innersep] == "4mm"
    set_background!(pd, "white")
    @test pd.printing_options[:background] == "white"
    print_connector_names!(pd)
    @test pd.printing_options[:print_connector_names]

    # Test functions for manipulating the ordering
    chains_old = copy.(pd.chains)
    shifts_old = copy(pd.shifts)
    switch_chains!(pd, 1, 2)
    @test pd.chains == [chains_old[2], chains_old[1]]
    @test pd.shifts == [shifts_old[2], shifts_old[1]]
    move_chain_vertically!(pd, 2, 1)
    @test pd.chains == chains_old
    @test pd.shifts == shifts_old
    switch_columns!(pd, 1, 2)
    @test pd.chains == reverse.(chains_old)
    @test pd.shifts == [0, 0]
    switch_columns!(pd, 2, 1)
    cleanup!(pd)
    @test pd.chains == chains_old
    @test pd.shifts == shifts_old
    move_column!(pd, 1, 2)
    @test pd.chains == reverse.(chains_old)
    @test pd.shifts == [0, 0]
    move_column!(pd, 2, 1)
    cleanup!(pd)
    @test pd.chains == chains_old
    @test pd.shifts == shifts_old
    insert_empty_columns!(pd, 1)
    insert!(chains_old[1], 2, missing)
    @test all(pd.chains[1] .=== chains_old[1])
    @test all(pd.chains[2] .=== chains_old[2])
    @test pd.shifts == shifts_old.*2
    cleanup!(pd)
    deleteat!(chains_old[1], 2)
    @test pd.chains == chains_old
    @test pd.shifts == shifts_old
    switch_elem!(pd, (findfirst(length(chains_old).==2), 1), (findfirst(length(chains_old).==2), 2))
    @test pd.chains == reverse.(chains_old)
    @test pd.shifts == shifts_old
    switch_elem!(pd, (findfirst(length(chains_old).==2), 1), (findfirst(length(chains_old).==2), 2))
    @test pd.chains == chains_old
    @test pd.shifts == shifts_old
    # try Merging the chains (should not work due to overlappings)
    @test_throws ArgumentError merge_chains!(pd, 1, 2)
    # Shift the first chain by 2
    move_chain_horizontally!(pd, 1, 2)
    @test pd.chains == chains_old
    @test pd.shifts == shifts_old .+ [2, 0]
    # Now merging should work
    merge_chains!(pd, 1, 2)
    @test length(pd.chains) == 1
    @test length(pd.shifts) == 1
    @test (length(pd.chains[1]) == 3 && all(pd.chains[1] .=== [chains_old[2]..., chains_old[1]...]) && pd.shifts == [1]) ||
        (all(pd.chains[1] .=== [chains_old[1]..., missing, chains_old[2]...]) && pd.shifts == [0])

    # Test getting latex and tikzpicture and if the tikzpicture is in the latex
    tikzpic = get_tikzpicture(pd)
    tex = get_latex(pd)
    @test occursin(tikzpic, tex)

    # Test show functions
    @test_nowarn show(devnull, MIME("application/x-latex"), pd)
    @test_nowarn show(devnull, MIME("text/latex"), pd)
end

@testset "With External systems" begin
    sys = []
    eqs = Equation[]
    @named asm = Process("ASM1") # Use the matrix-defined version with default parameters
    @named aer = Aeration() # The aeration process
    append!(sys, [asm, aer]) # Bookkeeping

    @named react = CSTR(1000, unknowns(states(asm)); initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)
    push!(sys, react) # Bookkeeping

    @named reactor_state_to_single_ports = StateInputToRealOutput(unknowns(states(react))) # note the use of the states of the reactor here as the reactor state port is to be split
    push!(sys, reactor_state_to_single_ports)

    @named single_ports_to_aeration_states = RealInputToStateOutput(unknowns(states(aer))) # Similarly here, aeration state is to be built -> use unknowns(states(aer))
    push!(sys, single_ports_to_aeration_states)

    @named asm_rates_to_single_ports = ReactionInputToRealOutput(unknowns(rates(asm))) # asm rates is to be split -> unknowns(rates(asm))
    @named aeration_rates_to_single_ports = ReactionInputToRealOutput(unknowns(rates(aer))) # aeration rates is to be split -> unknowns(rates(aer))
    append!(sys, [asm_rates_to_single_ports, aeration_rates_to_single_ports])

    @named add_aeration_rate = ModelingToolkitStandardLibrary.Blocks.Add()
    push!(sys, add_aeration_rate)

    @named single_ports_to_reaction_output = RealInputToReactionOutput(unknowns(rates(react))) # reactor rates is to be built -> unknowns(rates(react))
    push!(sys, single_ports_to_reaction_output)

    @named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], unknowns(states(asm)), flowrate = 18446) # This is the stabilization influent from BSM1
    push!(sys, influent)

    @named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)
    push!(sys, aeration_ctrl)

    push!(eqs, connect(states(react), states(asm)))
    push!(eqs, connect(states(react), states(reactor_state_to_single_ports))) # First the reactor states to the splitting system
    push!(eqs, connect(exogenous_outputs(reactor_state_to_single_ports, :S_O2), exogenous_inputs(single_ports_to_aeration_states, :S_O))) # Then the split up dissolved oxygen to the reassembly system
    push!(eqs, connect(states(single_ports_to_aeration_states), states(aer))) # Then the reassembly system to the aeration states
    push!(eqs, connect(rates(asm), rates(asm_rates_to_single_ports)))
    push!(eqs, connect(rates(aer), rates(aeration_rates_to_single_ports)))
    push!(eqs, connect(exogenous_outputs(asm_rates_to_single_ports, :S_O2), add_aeration_rate.input1))
    push!(eqs, connect(exogenous_outputs(aeration_rates_to_single_ports, :S_O), add_aeration_rate.input2))
    append!(eqs, [
        # First the summed up aeration rate
        connect(add_aeration_rate.output, exogenous_inputs(single_ports_to_reaction_output, :S_O2))
        # Then all others directly from the splitted asm rates 
        connect(exogenous_outputs(asm_rates_to_single_ports, :S_Alk), exogenous_inputs(single_ports_to_reaction_output, :S_Alk))
        connect(exogenous_outputs(asm_rates_to_single_ports, :S_B), exogenous_inputs(single_ports_to_reaction_output, :S_B))
        connect(exogenous_outputs(asm_rates_to_single_ports, :S_BN), exogenous_inputs(single_ports_to_reaction_output, :S_BN))
        connect(exogenous_outputs(asm_rates_to_single_ports, :S_N2), exogenous_inputs(single_ports_to_reaction_output, :S_N2))
        connect(exogenous_outputs(asm_rates_to_single_ports, :S_NHx), exogenous_inputs(single_ports_to_reaction_output, :S_NHx))
        connect(exogenous_outputs(asm_rates_to_single_ports, :S_NOx), exogenous_inputs(single_ports_to_reaction_output, :S_NOx))
        connect(exogenous_outputs(asm_rates_to_single_ports, :S_U), exogenous_inputs(single_ports_to_reaction_output, :S_U))
        connect(exogenous_outputs(asm_rates_to_single_ports, :XC_B), exogenous_inputs(single_ports_to_reaction_output, :XC_B))
        connect(exogenous_outputs(asm_rates_to_single_ports, :XC_BN), exogenous_inputs(single_ports_to_reaction_output, :XC_BN))
        connect(exogenous_outputs(asm_rates_to_single_ports, :X_ANO), exogenous_inputs(single_ports_to_reaction_output, :X_ANO))
        connect(exogenous_outputs(asm_rates_to_single_ports, :X_OHO), exogenous_inputs(single_ports_to_reaction_output, :X_OHO))
        connect(exogenous_outputs(asm_rates_to_single_ports, :X_UE), exogenous_inputs(single_ports_to_reaction_output, :X_UE))
        connect(exogenous_outputs(asm_rates_to_single_ports, :X_UInf), exogenous_inputs(single_ports_to_reaction_output, :X_UInf))
    ])
    push!(eqs, connect(rates(single_ports_to_reaction_output), rates(react)))
    push!(eqs, connect(outflows(influent, 1), inflows(react, 1)))
    push!(eqs, connect(exogenous_inputs(aer, :k_La), aeration_ctrl.output))

    # Getting the process diagram (only flow)
    pd = ProcessDiagram(sys, eqs)
    @test pd.chains == [[influent, react]]
    @test pd.shifts == [0]
    # Getting process diagramm (also non-flow)
    pd = ProcessDiagram(sys, eqs; only_flow=false)
    print_connector_names!(pd)
    @test Set(skipmissing(vcat(pd.chains...))) == Set(sys)
end
