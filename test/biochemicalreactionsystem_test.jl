
@testset "Rates with Function" begin
    rsys = BioChemicalReactionSystem(joinpath(@__DIR__, "test_models", "ASM1_with_sin"), "ASM1")
    @test check_composition(rsys; params_substitution=:values)
    @test_throws ErrorException BioChemicalReactionSystem(joinpath(@__DIR__, "test_models", "ASM1_with_unknown_fcn"), "ASM1")
    @test_throws ErrorException BioChemicalReactionSystem(joinpath(@__DIR__, "test_models", "ASM1_with_ratecall"), "ASM1")
end

@testset "Invalid symbols" begin
    rsys = BioChemicalReactionSystem(joinpath(@__DIR__, "test_models", "ASM1_with_symbol_from_context"), "ASM1")
    @test check_composition(rsys; params_substitution=:expressions)
    @test check_composition(rsys; params_substitution=:values)
    @test_throws ErrorException BioChemicalReactionSystem(joinpath(@__DIR__, "test_models", "ASM1_with_invalid_symbol"), "ASM1")
end

@testset "Circular parameter dependency" begin
    rsys = BioChemicalReactionSystem(joinpath(@__DIR__, "test_models", "ASM1_circular_param"), "ASM1")
    @test_throws ArgumentError Reactions.parameter_values(rsys)
    @test check_composition(rsys)
    @test_throws ArgumentError check_composition(rsys; params_substitution=:values)
    @test_throws ArgumentError check_composition(rsys; params_substitution=:whatever)
end
