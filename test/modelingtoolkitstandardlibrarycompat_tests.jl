
@variables q(t) = 31.23 q2(t) = 1 S_S(t) = 12.5 [particulate = false, colloidal = false, soluble = true] X_S(t) = 2 [particulate = true, colloidal = false, soluble = false]

@testset "ConnectorsCompatibility tests" begin
    # Test inflow
    @testset "RealInputToOutflowPort tests" begin
        # Test with dict input
        @named inflow = RealInputToOutflowPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true),]))
        # Test if it has the right connectors
        @test !hasstates(inflow)
        @test num_states(inflow) == 0
        @test !hasrates(inflow)
        @test num_rates(inflow) == 0
        @test !hasinflows(inflow)
        @test num_inflows(inflow) == 0
        @test hasoutflows(inflow)
        @test num_outflows(inflow) == 1
        @test hasexogenous_inputs(inflow)
        @test num_exogenous_inputs(inflow) == 3
        @test !hasexogenous_outputs(inflow)
        @test num_exogenous_outputs(inflow) == 0
        # Test for name
        @test nameof(inflow) == :inflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(inflow)) == :t
        # Test inflows 
        @test_throws BoundsError inflows(inflow)[1]
        @test_throws BoundsError inflows(inflow, 1)
        @test_throws ArgumentError inflows(inflow, :inflow)
        # Test outflows
        @test length(outflows(inflow)) == 1
        @test nameof(outflows(inflow, :outflow)) == :inflow₊outflow
        @test_throws BoundsError outflows(inflow, 2)
        @test_throws ArgumentError outflows(inflow, :inflow)
        # Test states
        @test isnothing(states(inflow))
        # Test rates
        @test isnothing(rates(inflow))
        # Test exogenous inputs
        @test length(exogenous_inputs(inflow)) == 3
        @test issetequal(exogenous_input_names(inflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_inputs(inflow, [1, 3])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        # Test exogenous outputs
        @test isempty(exogenous_outputs(inflow))
        @test isempty(exogenous_output_names(inflow))
        @test_throws BoundsError exogenous_outputs(inflow, [1, 3])
        @test_throws ArgumentError exogenous_outputs(inflow, [:q, :X_S])
        # Test equations
        @test length(equations(inflow)) == 3
        # Test that none has defaults
        for unk in unknowns(inflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :S_S))) .== [0])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :X_S))) .== [0])
        # Test with symbolic input
        @named inflow = RealInputToOutflowPort([X_S, S_S])
        # Test if it has the right connectors
        @test !hasstates(inflow)
        @test num_states(inflow) == 0
        @test !hasrates(inflow)
        @test num_rates(inflow) == 0
        @test !hasinflows(inflow)
        @test num_inflows(inflow) == 0
        @test hasoutflows(inflow)
        @test num_outflows(inflow) == 1
        @test hasexogenous_inputs(inflow)
        @test num_exogenous_inputs(inflow) == 3
        @test !hasexogenous_outputs(inflow)
        @test num_exogenous_outputs(inflow) == 0
        # Test for name
        @test nameof(inflow) == :inflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(inflow)) == :t
        # Test inflows 
        @test_throws BoundsError inflows(inflow)[1]
        @test_throws BoundsError inflows(inflow, 1)
        @test_throws ArgumentError inflows(inflow, :inflow)
        # Test outflows
        @test length(outflows(inflow)) == 1
        @test nameof(outflows(inflow, :outflow)) == :inflow₊outflow
        @test_throws BoundsError outflows(inflow, 2)
        @test_throws ArgumentError outflows(inflow, :inflow)
        # Test states
        @test isnothing(states(inflow))
        # Test rates
        @test isnothing(rates(inflow))
        # Test exogenous inputs
        @test length(exogenous_inputs(inflow)) == 3
        @test issetequal(exogenous_input_names(inflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_inputs(inflow, [1, 3])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        # Test exogenous outputs
        @test isempty(exogenous_outputs(inflow))
        @test isempty(exogenous_output_names(inflow))
        @test_throws BoundsError exogenous_outputs(inflow, [1, 3])
        @test_throws ArgumentError exogenous_outputs(inflow, [:q, :X_S])
        # Test equations
        @test length(equations(inflow)) == 3
        # Test that none has defaults
        for unk in unknowns(inflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :X_S))) .== [2])
        # Test with q input
        @named inflow = RealInputToOutflowPort([q, X_S, S_S])
        # Test if it has the right connectors
        @test !hasstates(inflow)
        @test num_states(inflow) == 0
        @test !hasrates(inflow)
        @test num_rates(inflow) == 0
        @test !hasinflows(inflow)
        @test num_inflows(inflow) == 0
        @test hasoutflows(inflow)
        @test num_outflows(inflow) == 1
        @test hasexogenous_inputs(inflow)
        @test num_exogenous_inputs(inflow) == 3
        @test !hasexogenous_outputs(inflow)
        @test num_exogenous_outputs(inflow) == 0
        # Test for name
        @test nameof(inflow) == :inflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(inflow)) == :t
        # Test inflows 
        @test_throws BoundsError inflows(inflow)[1]
        @test_throws BoundsError inflows(inflow, 1)
        @test_throws ArgumentError inflows(inflow, :inflow)
        # Test outflows
        @test length(outflows(inflow)) == 1
        @test nameof(outflows(inflow, :outflow)) == :inflow₊outflow
        @test_throws BoundsError outflows(inflow, 2)
        @test_throws ArgumentError outflows(inflow, :inflow)
        # Test states
        @test isnothing(states(inflow))
        # Test rates
        @test isnothing(rates(inflow))
        # Test exogenous inputs
        @test length(exogenous_inputs(inflow)) == 3
        @test issetequal(exogenous_input_names(inflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_inputs(inflow, [1, 3])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        # Test exogenous outputs
        @test isempty(exogenous_outputs(inflow))
        @test isempty(exogenous_output_names(inflow))
        @test_throws BoundsError exogenous_outputs(inflow, [1, 3])
        @test_throws ArgumentError exogenous_outputs(inflow, [:q, :X_S])
        # Test equations
        @test length(equations(inflow)) == 3
        # Test that none has defaults
        for unk in unknowns(inflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :q))) .== [31.23])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :X_S))) .== [2])
        # Test with q input at different location
        @named inflow = RealInputToOutflowPort([X_S, q, S_S])
        # Test if it has the right connectors
        @test !hasstates(inflow)
        @test num_states(inflow) == 0
        @test !hasrates(inflow)
        @test num_rates(inflow) == 0
        @test !hasinflows(inflow)
        @test num_inflows(inflow) == 0
        @test hasoutflows(inflow)
        @test num_outflows(inflow) == 1
        @test hasexogenous_inputs(inflow)
        @test num_exogenous_inputs(inflow) == 3
        @test !hasexogenous_outputs(inflow)
        @test num_exogenous_outputs(inflow) == 0
        # Test for name
        @test nameof(inflow) == :inflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(inflow)) == :t
        # Test inflows 
        @test_throws BoundsError inflows(inflow)[1]
        @test_throws BoundsError inflows(inflow, 1)
        @test_throws ArgumentError inflows(inflow, :inflow)
        # Test outflows
        @test length(outflows(inflow)) == 1
        @test nameof(outflows(inflow, :outflow)) == :inflow₊outflow
        @test_throws BoundsError outflows(inflow, 2)
        @test_throws ArgumentError outflows(inflow, :inflow)
        # Test states
        @test isnothing(states(inflow))
        # Test rates
        @test isnothing(rates(inflow))
        # Test exogenous inputs
        @test length(exogenous_inputs(inflow)) == 3
        @test issetequal(exogenous_input_names(inflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_inputs(inflow, [1, 3])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        # Test exogenous outputs
        @test isempty(exogenous_outputs(inflow))
        @test isempty(exogenous_output_names(inflow))
        @test_throws BoundsError exogenous_outputs(inflow, [1, 3])
        @test_throws ArgumentError exogenous_outputs(inflow, [:q, :X_S])
        # Test equations
        @test length(equations(inflow)) == 3
        # Test that none has defaults
        for unk in unknowns(inflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :q))) .== [31.23])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :X_S))) .== [2])
        # Test initial flow
        @named inflow = RealInputToOutflowPort([X_S, S_S]; initial_flow = 35.6)
        # Test if it has the right connectors
        @test !hasstates(inflow)
        @test num_states(inflow) == 0
        @test !hasrates(inflow)
        @test num_rates(inflow) == 0
        @test !hasinflows(inflow)
        @test num_inflows(inflow) == 0
        @test hasoutflows(inflow)
        @test num_outflows(inflow) == 1
        @test hasexogenous_inputs(inflow)
        @test num_exogenous_inputs(inflow) == 3
        @test !hasexogenous_outputs(inflow)
        @test num_exogenous_outputs(inflow) == 0
        # Test for name
        @test nameof(inflow) == :inflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(inflow)) == :t
        # Test inflows 
        @test_throws BoundsError inflows(inflow)[1]
        @test_throws BoundsError inflows(inflow, 1)
        @test_throws ArgumentError inflows(inflow, :inflow)
        # Test outflows
        @test length(outflows(inflow)) == 1
        @test nameof(outflows(inflow, :outflow)) == :inflow₊outflow
        @test_throws BoundsError outflows(inflow, 2)
        @test_throws ArgumentError outflows(inflow, :inflow)
        # Test states
        @test isnothing(states(inflow))
        # Test rates
        @test isnothing(rates(inflow))
        # Test exogenous inputs
        @test length(exogenous_inputs(inflow)) == 3
        @test issetequal(exogenous_input_names(inflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_inputs(inflow, [1, 3])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        # Test exogenous outputs
        @test isempty(exogenous_outputs(inflow))
        @test isempty(exogenous_output_names(inflow))
        @test_throws BoundsError exogenous_outputs(inflow, [1, 3])
        @test_throws ArgumentError exogenous_outputs(inflow, [:q, :X_S])
        # Test equations
        @test length(equations(inflow)) == 3
        # Test that none has defaults
        for unk in unknowns(inflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :q))) .== [35.6])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :X_S))) .== [2])
        # Test initial concentrations with array
        @named inflow = RealInputToOutflowPort([X_S, S_S]; initial_concentrations = [5, 3])
        # Test if it has the right connectors
        @test !hasstates(inflow)
        @test num_states(inflow) == 0
        @test !hasrates(inflow)
        @test num_rates(inflow) == 0
        @test !hasinflows(inflow)
        @test num_inflows(inflow) == 0
        @test hasoutflows(inflow)
        @test num_outflows(inflow) == 1
        @test hasexogenous_inputs(inflow)
        @test num_exogenous_inputs(inflow) == 3
        @test !hasexogenous_outputs(inflow)
        @test num_exogenous_outputs(inflow) == 0
        # Test for name
        @test nameof(inflow) == :inflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(inflow)) == :t
        # Test inflows 
        @test_throws BoundsError inflows(inflow)[1]
        @test_throws BoundsError inflows(inflow, 1)
        @test_throws ArgumentError inflows(inflow, :inflow)
        # Test outflows
        @test length(outflows(inflow)) == 1
        @test nameof(outflows(inflow, :outflow)) == :inflow₊outflow
        @test_throws BoundsError outflows(inflow, 2)
        @test_throws ArgumentError outflows(inflow, :inflow)
        # Test states
        @test isnothing(states(inflow))
        # Test rates
        @test isnothing(rates(inflow))
        # Test exogenous inputs
        @test length(exogenous_inputs(inflow)) == 3
        @test issetequal(exogenous_input_names(inflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_inputs(inflow, [1, 3])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        # Test exogenous outputs
        @test isempty(exogenous_outputs(inflow))
        @test isempty(exogenous_output_names(inflow))
        @test_throws BoundsError exogenous_outputs(inflow, [1, 3])
        @test_throws ArgumentError exogenous_outputs(inflow, [:q, :X_S])
        # Test equations
        @test length(equations(inflow)) == 3
        # Test that none has defaults
        for unk in unknowns(inflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :S_S))) .== [3])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :X_S))) .== [5])
        # Test initial concentrations with map
        @named inflow = RealInputToOutflowPort([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]))
        # Test if it has the right connectors
        @test !hasstates(inflow)
        @test num_states(inflow) == 0
        @test !hasrates(inflow)
        @test num_rates(inflow) == 0
        @test !hasinflows(inflow)
        @test num_inflows(inflow) == 0
        @test hasoutflows(inflow)
        @test num_outflows(inflow) == 1
        @test hasexogenous_inputs(inflow)
        @test num_exogenous_inputs(inflow) == 3
        @test !hasexogenous_outputs(inflow)
        @test num_exogenous_outputs(inflow) == 0
        # Test for name
        @test nameof(inflow) == :inflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(inflow)) == :t
        # Test inflows 
        @test_throws BoundsError inflows(inflow)[1]
        @test_throws BoundsError inflows(inflow, 1)
        @test_throws ArgumentError inflows(inflow, :inflow)
        # Test outflows
        @test length(outflows(inflow)) == 1
        @test nameof(outflows(inflow, :outflow)) == :inflow₊outflow
        @test_throws BoundsError outflows(inflow, 2)
        @test_throws ArgumentError outflows(inflow, :inflow)
        # Test states
        @test isnothing(states(inflow))
        # Test rates
        @test isnothing(rates(inflow))
        # Test exogenous inputs
        @test length(exogenous_inputs(inflow)) == 3
        @test issetequal(exogenous_input_names(inflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_inputs(inflow, [1, 3])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        @test nameof.(exogenous_inputs(inflow, [:q, :X_S])) == [:inflow₊q, :inflow₊X_S]
        # Test exogenous outputs
        @test isempty(exogenous_outputs(inflow))
        @test isempty(exogenous_output_names(inflow))
        @test_throws BoundsError exogenous_outputs(inflow, [1, 3])
        @test_throws ArgumentError exogenous_outputs(inflow, [:q, :X_S])
        # Test equations
        @test length(equations(inflow)) == 3
        # Test that none has defaults
        for unk in unknowns(inflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :S_S))) .== [5.25])
#        @test all(getguess.(unknowns(exogenous_inputs(inflow, :X_S))) .== [123.5])
    end

    # Test InflowPortToRealOutput
    @testset "InflowPortToRealOutput tests" begin
        # Test with dict input
        @named outflow = InflowPortToRealOutput(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true),]))
        # Test if it has the right connectors
        @test !hasstates(outflow)
        @test num_states(outflow) == 0
        @test !hasrates(outflow)
        @test num_rates(outflow) == 0
        @test hasinflows(outflow)
        @test num_inflows(outflow) == 1
        @test !hasoutflows(outflow)
        @test num_outflows(outflow) == 0
        @test !hasexogenous_inputs(outflow)
        @test num_exogenous_inputs(outflow) == 0
        @test hasexogenous_outputs(outflow)
        @test num_exogenous_outputs(outflow) == 3
        # Test for name
        @test nameof(outflow) == :outflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(outflow)) == :t
        # Test inflows
        @test length(inflows(outflow)) == 1
        @test nameof(inflows(outflow, :inflow)) == :outflow₊inflow
        @test_throws BoundsError inflows(outflow, 2)
        @test_throws ArgumentError inflows(outflow, :outflow)
        # Test outflows 
        @test_throws BoundsError outflows(outflow)[1]
        @test_throws BoundsError outflows(outflow, 1)
        @test_throws ArgumentError outflows(outflow, :outflow)
        # Test states
        @test isnothing(states(outflow))
        # Test rates
        @test isnothing(rates(outflow))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(outflow))
        @test isempty(exogenous_input_names(outflow))
        @test_throws BoundsError exogenous_inputs(outflow, [1, 3])
        @test_throws ArgumentError exogenous_inputs(outflow, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(outflow)) == 3
        @test issetequal(exogenous_output_names(outflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_outputs(outflow, [1, 3])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        # Test equations
        @test length(equations(outflow)) == 3
        # Test that none has defaults
        for unk in unknowns(outflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :S_S))) .== [0])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :X_S))) .== [0])
        # Test with symbolic input
        @named outflow = InflowPortToRealOutput([X_S, S_S])
        # Test if it has the right connectors
        @test !hasstates(outflow)
        @test num_states(outflow) == 0
        @test !hasrates(outflow)
        @test num_rates(outflow) == 0
        @test hasinflows(outflow)
        @test num_inflows(outflow) == 1
        @test !hasoutflows(outflow)
        @test num_outflows(outflow) == 0
        @test !hasexogenous_inputs(outflow)
        @test num_exogenous_inputs(outflow) == 0
        @test hasexogenous_outputs(outflow)
        @test num_exogenous_outputs(outflow) == 3
        # Test for name
        @test nameof(outflow) == :outflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(outflow)) == :t
        # Test inflows
        @test length(inflows(outflow)) == 1
        @test nameof(inflows(outflow, :inflow)) == :outflow₊inflow
        @test_throws BoundsError inflows(outflow, 2)
        @test_throws ArgumentError inflows(outflow, :outflow)
        # Test outflows 
        @test_throws BoundsError outflows(outflow)[1]
        @test_throws BoundsError outflows(outflow, 1)
        @test_throws ArgumentError outflows(outflow, :outflow)
        # Test states
        @test isnothing(states(outflow))
        # Test rates
        @test isnothing(rates(outflow))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(outflow))
        @test isempty(exogenous_input_names(outflow))
        @test_throws BoundsError exogenous_inputs(outflow, [1, 3])
        @test_throws ArgumentError exogenous_inputs(outflow, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(outflow)) == 3
        @test issetequal(exogenous_output_names(outflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_outputs(outflow, [1, 3])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        # Test equations
        @test length(equations(outflow)) == 3
        # Test that none has defaults
        for unk in unknowns(outflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :X_S))) .== [2])
        # Test with q input
        @named outflow = InflowPortToRealOutput([q, X_S, S_S])
        # Test if it has the right connectors
        @test !hasstates(outflow)
        @test num_states(outflow) == 0
        @test !hasrates(outflow)
        @test num_rates(outflow) == 0
        @test hasinflows(outflow)
        @test num_inflows(outflow) == 1
        @test !hasoutflows(outflow)
        @test num_outflows(outflow) == 0
        @test !hasexogenous_inputs(outflow)
        @test num_exogenous_inputs(outflow) == 0
        @test hasexogenous_outputs(outflow)
        @test num_exogenous_outputs(outflow) == 3
        # Test for name
        @test nameof(outflow) == :outflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(outflow)) == :t
        # Test inflows
        @test length(inflows(outflow)) == 1
        @test nameof(inflows(outflow, :inflow)) == :outflow₊inflow
        @test_throws BoundsError inflows(outflow, 2)
        @test_throws ArgumentError inflows(outflow, :outflow)
        # Test outflows 
        @test_throws BoundsError outflows(outflow)[1]
        @test_throws BoundsError outflows(outflow, 1)
        @test_throws ArgumentError outflows(outflow, :outflow)
        # Test states
        @test isnothing(states(outflow))
        # Test rates
        @test isnothing(rates(outflow))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(outflow))
        @test isempty(exogenous_input_names(outflow))
        @test_throws BoundsError exogenous_inputs(outflow, [1, 3])
        @test_throws ArgumentError exogenous_inputs(outflow, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(outflow)) == 3
        @test issetequal(exogenous_output_names(outflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_outputs(outflow, [1, 3])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        # Test equations
        @test length(equations(outflow)) == 3
        # Test that none has defaults
        for unk in unknowns(outflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :q))) .== [31.23])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :X_S))) .== [2])
        # Test with q input at different location
        @named outflow = InflowPortToRealOutput([X_S, q, S_S])
        # Test if it has the right connectors
        @test !hasstates(outflow)
        @test num_states(outflow) == 0
        @test !hasrates(outflow)
        @test num_rates(outflow) == 0
        @test hasinflows(outflow)
        @test num_inflows(outflow) == 1
        @test !hasoutflows(outflow)
        @test num_outflows(outflow) == 0
        @test !hasexogenous_inputs(outflow)
        @test num_exogenous_inputs(outflow) == 0
        @test hasexogenous_outputs(outflow)
        @test num_exogenous_outputs(outflow) == 3
        # Test for name
        @test nameof(outflow) == :outflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(outflow)) == :t
        # Test inflows
        @test length(inflows(outflow)) == 1
        @test nameof(inflows(outflow, :inflow)) == :outflow₊inflow
        @test_throws BoundsError inflows(outflow, 2)
        @test_throws ArgumentError inflows(outflow, :outflow)
        # Test outflows 
        @test_throws BoundsError outflows(outflow)[1]
        @test_throws BoundsError outflows(outflow, 1)
        @test_throws ArgumentError outflows(outflow, :outflow)
        # Test states
        @test isnothing(states(outflow))
        # Test rates
        @test isnothing(rates(outflow))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(outflow))
        @test isempty(exogenous_input_names(outflow))
        @test_throws BoundsError exogenous_inputs(outflow, [1, 3])
        @test_throws ArgumentError exogenous_inputs(outflow, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(outflow)) == 3
        @test issetequal(exogenous_output_names(outflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_outputs(outflow, [1, 3])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        # Test equations
        @test length(equations(outflow)) == 3
        # Test that none has defaults
        for unk in unknowns(outflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :q))) .== [31.23])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :X_S))) .== [2])
        # Test initial flow
        @named outflow = InflowPortToRealOutput([X_S, S_S]; initial_flow = 35.6)
        # Test if it has the right connectors
        @test !hasstates(outflow)
        @test num_states(outflow) == 0
        @test !hasrates(outflow)
        @test num_rates(outflow) == 0
        @test hasinflows(outflow)
        @test num_inflows(outflow) == 1
        @test !hasoutflows(outflow)
        @test num_outflows(outflow) == 0
        @test !hasexogenous_inputs(outflow)
        @test num_exogenous_inputs(outflow) == 0
        @test hasexogenous_outputs(outflow)
        @test num_exogenous_outputs(outflow) == 3
        # Test for name
        @test nameof(outflow) == :outflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(outflow)) == :t
        # Test inflows
        @test length(inflows(outflow)) == 1
        @test nameof(inflows(outflow, :inflow)) == :outflow₊inflow
        @test_throws BoundsError inflows(outflow, 2)
        @test_throws ArgumentError inflows(outflow, :outflow)
        # Test outflows 
        @test_throws BoundsError outflows(outflow)[1]
        @test_throws BoundsError outflows(outflow, 1)
        @test_throws ArgumentError outflows(outflow, :outflow)
        # Test states
        @test isnothing(states(outflow))
        # Test rates
        @test isnothing(rates(outflow))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(outflow))
        @test isempty(exogenous_input_names(outflow))
        @test_throws BoundsError exogenous_inputs(outflow, [1, 3])
        @test_throws ArgumentError exogenous_inputs(outflow, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(outflow)) == 3
        @test issetequal(exogenous_output_names(outflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_outputs(outflow, [1, 3])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        # Test equations
        @test length(equations(outflow)) == 3
        # Test that none has defaults
        for unk in unknowns(outflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :q))) .== [35.6])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :X_S))) .== [2])
        # Test initial concentrations with array
        @named outflow = InflowPortToRealOutput([X_S, S_S]; initial_concentrations = [5, 3])
        # Test if it has the right connectors
        @test !hasstates(outflow)
        @test num_states(outflow) == 0
        @test !hasrates(outflow)
        @test num_rates(outflow) == 0
        @test hasinflows(outflow)
        @test num_inflows(outflow) == 1
        @test !hasoutflows(outflow)
        @test num_outflows(outflow) == 0
        @test !hasexogenous_inputs(outflow)
        @test num_exogenous_inputs(outflow) == 0
        @test hasexogenous_outputs(outflow)
        @test num_exogenous_outputs(outflow) == 3
        # Test for name
        @test nameof(outflow) == :outflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(outflow)) == :t
        # Test inflows
        @test length(inflows(outflow)) == 1
        @test nameof(inflows(outflow, :inflow)) == :outflow₊inflow
        @test_throws BoundsError inflows(outflow, 2)
        @test_throws ArgumentError inflows(outflow, :outflow)
        # Test outflows 
        @test_throws BoundsError outflows(outflow)[1]
        @test_throws BoundsError outflows(outflow, 1)
        @test_throws ArgumentError outflows(outflow, :outflow)
        # Test states
        @test isnothing(states(outflow))
        # Test rates
        @test isnothing(rates(outflow))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(outflow))
        @test isempty(exogenous_input_names(outflow))
        @test_throws BoundsError exogenous_inputs(outflow, [1, 3])
        @test_throws ArgumentError exogenous_inputs(outflow, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(outflow)) == 3
        @test issetequal(exogenous_output_names(outflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_outputs(outflow, [1, 3])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        # Test equations
        @test length(equations(outflow)) == 3
        # Test that none has defaults
        for unk in unknowns(outflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :S_S))) .== [3])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :X_S))) .== [5])
        # Test initial concentrations with map
        @named outflow = InflowPortToRealOutput([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]))
        # Test if it has the right connectors
        @test !hasstates(outflow)
        @test num_states(outflow) == 0
        @test !hasrates(outflow)
        @test num_rates(outflow) == 0
        @test hasinflows(outflow)
        @test num_inflows(outflow) == 1
        @test !hasoutflows(outflow)
        @test num_outflows(outflow) == 0
        @test !hasexogenous_inputs(outflow)
        @test num_exogenous_inputs(outflow) == 0
        @test hasexogenous_outputs(outflow)
        @test num_exogenous_outputs(outflow) == 3
        # Test for name
        @test nameof(outflow) == :outflow
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(outflow)) == :t
        # Test inflows
        @test length(inflows(outflow)) == 1
        @test nameof(inflows(outflow, :inflow)) == :outflow₊inflow
        @test_throws BoundsError inflows(outflow, 2)
        @test_throws ArgumentError inflows(outflow, :outflow)
        # Test outflows 
        @test_throws BoundsError outflows(outflow)[1]
        @test_throws BoundsError outflows(outflow, 1)
        @test_throws ArgumentError outflows(outflow, :outflow)
        # Test states
        @test isnothing(states(outflow))
        # Test rates
        @test isnothing(rates(outflow))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(outflow))
        @test isempty(exogenous_input_names(outflow))
        @test_throws BoundsError exogenous_inputs(outflow, [1, 3])
        @test_throws ArgumentError exogenous_inputs(outflow, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(outflow)) == 3
        @test issetequal(exogenous_output_names(outflow), [:q, :X_S, :S_S])
        @test nameof.(exogenous_outputs(outflow, [1, 3])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        @test nameof.(exogenous_outputs(outflow, [:q, :X_S])) == [:outflow₊q, :outflow₊X_S]
        # Test equations
        @test length(equations(outflow)) == 3
        # Test that none has defaults
        for unk in unknowns(outflow)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :q))) .== [0])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :S_S))) .== [3])
#        @test all(getguess.(unknowns(exogenous_outputs(outflow, :X_S))) .== [5])
    end

    # Test RealInputToReactionOutput
    @testset "RealInputToReactionOutput tests" begin
        # Test with array input
        @named reactionoutput = RealInputToReactionOutput(["X_S", "S_S"])
        # Test if it has the right connectors
        @test !hasstates(reactionoutput)
        @test num_states(reactionoutput) == 0
        @test hasrates(reactionoutput)
        @test num_rates(reactionoutput) == 1
        @test !hasinflows(reactionoutput)
        @test num_inflows(reactionoutput) == 0
        @test !hasoutflows(reactionoutput)
        @test num_outflows(reactionoutput) == 0
        @test hasexogenous_inputs(reactionoutput)
        @test num_exogenous_inputs(reactionoutput) == 2
        @test !hasexogenous_outputs(reactionoutput)
        @test num_exogenous_outputs(reactionoutput) == 0
        # Test for name
        @test nameof(reactionoutput) == :reactionoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactionoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactionoutput)[1]
        @test_throws BoundsError inflows(reactionoutput, 1)
        @test_throws ArgumentError inflows(reactionoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactionoutput)[1]
        @test_throws BoundsError outflows(reactionoutput, 1)
        @test_throws ArgumentError outflows(reactionoutput, :outflow)
        # Test states
        @test isnothing(states(reactionoutput))
        # Test rates
        @test nameof(rates(reactionoutput)) == :reactionoutput₊reactionoutput
        # Test exogenous inputs
        @test length(exogenous_inputs(reactionoutput)) == 2
        @test issetequal(exogenous_input_names(reactionoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(reactionoutput, 1)) == :reactionoutput₊S_S
        @test nameof(exogenous_inputs(reactionoutput, :X_S)) == :reactionoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(reactionoutput))
        @test isempty(exogenous_output_names(reactionoutput))
        @test_throws BoundsError exogenous_outputs(reactionoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(reactionoutput, [:q, :X_S])
        # Test equations
        @test length(equations(reactionoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactionoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :X_S))) .== [2])
        # Test with symbolic input
        @named reactionoutput = RealInputToReactionOutput([X_S, S_S])
        # Test if it has the right connectors
        @test !hasstates(reactionoutput)
        @test num_states(reactionoutput) == 0
        @test hasrates(reactionoutput)
        @test num_rates(reactionoutput) == 1
        @test !hasinflows(reactionoutput)
        @test num_inflows(reactionoutput) == 0
        @test !hasoutflows(reactionoutput)
        @test num_outflows(reactionoutput) == 0
        @test hasexogenous_inputs(reactionoutput)
        @test num_exogenous_inputs(reactionoutput) == 2
        @test !hasexogenous_outputs(reactionoutput)
        @test num_exogenous_outputs(reactionoutput) == 0
        # Test for name
        @test nameof(reactionoutput) == :reactionoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactionoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactionoutput)[1]
        @test_throws BoundsError inflows(reactionoutput, 1)
        @test_throws ArgumentError inflows(reactionoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactionoutput)[1]
        @test_throws BoundsError outflows(reactionoutput, 1)
        @test_throws ArgumentError outflows(reactionoutput, :outflow)
        # Test states
        @test isnothing(states(reactionoutput))
        # Test rates
        @test nameof(rates(reactionoutput)) == :reactionoutput₊reactionoutput
        # Test exogenous inputs
        @test length(exogenous_inputs(reactionoutput)) == 2
        @test issetequal(exogenous_input_names(reactionoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(reactionoutput, 1)) == :reactionoutput₊S_S
        @test nameof(exogenous_inputs(reactionoutput, :X_S)) == :reactionoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(reactionoutput))
        @test isempty(exogenous_output_names(reactionoutput))
        @test_throws BoundsError exogenous_outputs(reactionoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(reactionoutput, [:q, :X_S])
        # Test equations
        @test length(equations(reactionoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactionoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :X_S))) .== [2])
        # Test initial concentrations with array
        @named reactionoutput = RealInputToReactionOutput([X_S, S_S]; initial_rates = [5, 3])
        # Test if it has the right connectors
        @test !hasstates(reactionoutput)
        @test num_states(reactionoutput) == 0
        @test hasrates(reactionoutput)
        @test num_rates(reactionoutput) == 1
        @test !hasinflows(reactionoutput)
        @test num_inflows(reactionoutput) == 0
        @test !hasoutflows(reactionoutput)
        @test num_outflows(reactionoutput) == 0
        @test hasexogenous_inputs(reactionoutput)
        @test num_exogenous_inputs(reactionoutput) == 2
        @test !hasexogenous_outputs(reactionoutput)
        @test num_exogenous_outputs(reactionoutput) == 0
        # Test for name
        @test nameof(reactionoutput) == :reactionoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactionoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactionoutput)[1]
        @test_throws BoundsError inflows(reactionoutput, 1)
        @test_throws ArgumentError inflows(reactionoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactionoutput)[1]
        @test_throws BoundsError outflows(reactionoutput, 1)
        @test_throws ArgumentError outflows(reactionoutput, :outflow)
        # Test states
        @test isnothing(states(reactionoutput))
        # Test rates
        @test nameof(rates(reactionoutput)) == :reactionoutput₊reactionoutput
        # Test exogenous inputs
        @test length(exogenous_inputs(reactionoutput)) == 2
        @test issetequal(exogenous_input_names(reactionoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(reactionoutput, 1)) == :reactionoutput₊S_S
        @test nameof(exogenous_inputs(reactionoutput, :X_S)) == :reactionoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(reactionoutput))
        @test isempty(exogenous_output_names(reactionoutput))
        @test_throws BoundsError exogenous_outputs(reactionoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(reactionoutput, [:q, :X_S])
        # Test equations
        @test length(equations(reactionoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactionoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :S_S))) .== [3])
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :X_S))) .== [5])
        # Test initial concentrations with map
        @named reactionoutput = RealInputToReactionOutput([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
        # Test if it has the right connectors
        @test !hasstates(reactionoutput)
        @test num_states(reactionoutput) == 0
        @test hasrates(reactionoutput)
        @test num_rates(reactionoutput) == 1
        @test !hasinflows(reactionoutput)
        @test num_inflows(reactionoutput) == 0
        @test !hasoutflows(reactionoutput)
        @test num_outflows(reactionoutput) == 0
        @test hasexogenous_inputs(reactionoutput)
        @test num_exogenous_inputs(reactionoutput) == 2
        @test !hasexogenous_outputs(reactionoutput)
        @test num_exogenous_outputs(reactionoutput) == 0
        # Test for name
        @test nameof(reactionoutput) == :reactionoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactionoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactionoutput)[1]
        @test_throws BoundsError inflows(reactionoutput, 1)
        @test_throws ArgumentError inflows(reactionoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactionoutput)[1]
        @test_throws BoundsError outflows(reactionoutput, 1)
        @test_throws ArgumentError outflows(reactionoutput, :outflow)
        # Test states
        @test isnothing(states(reactionoutput))
        # Test rates
        @test nameof(rates(reactionoutput)) == :reactionoutput₊reactionoutput
        # Test exogenous inputs
        @test length(exogenous_inputs(reactionoutput)) == 2
        @test issetequal(exogenous_input_names(reactionoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(reactionoutput, 1)) == :reactionoutput₊S_S
        @test nameof(exogenous_inputs(reactionoutput, :X_S)) == :reactionoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(reactionoutput))
        @test isempty(exogenous_output_names(reactionoutput))
        @test_throws BoundsError exogenous_outputs(reactionoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(reactionoutput, [:q, :X_S])
        # Test equations
        @test length(equations(reactionoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactionoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :S_S))) .== [5.25])
#        @test all(getguess.(unknowns(exogenous_inputs(reactionoutput, :X_S))) .== [123.5])
    end

    # Test ReactionInputToRealOutput
    @testset "ReactionInputToRealOutput tests" begin
        # Test with array input
        @named reactioninput = ReactionInputToRealOutput(["X_S", "S_S"])
        # Test if it has the right connectors
        @test !hasstates(reactioninput)
        @test num_states(reactioninput) == 0
        @test hasrates(reactioninput)
        @test num_rates(reactioninput) == 1
        @test !hasinflows(reactioninput)
        @test num_inflows(reactioninput) == 0
        @test !hasoutflows(reactioninput)
        @test num_outflows(reactioninput) == 0
        @test !hasexogenous_inputs(reactioninput)
        @test num_exogenous_inputs(reactioninput) == 0
        @test hasexogenous_outputs(reactioninput)
        @test num_exogenous_outputs(reactioninput) == 2
        # Test for name
        @test nameof(reactioninput) == :reactioninput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactioninput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactioninput)[1]
        @test_throws BoundsError inflows(reactioninput, 1)
        @test_throws ArgumentError inflows(reactioninput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactioninput)[1]
        @test_throws BoundsError outflows(reactioninput, 1)
        @test_throws ArgumentError outflows(reactioninput, :outflow)
        # Test states
        @test isnothing(states(reactioninput))
        # Test rates
        @test nameof(rates(reactioninput)) == :reactioninput₊reactioninput
        # Test exogenous inputs
        @test isempty(exogenous_inputs(reactioninput))
        @test isempty(exogenous_input_names(reactioninput))
        @test_throws BoundsError exogenous_inputs(reactioninput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(reactioninput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(reactioninput)) == 2
        @test issetequal(exogenous_output_names(reactioninput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(reactioninput, 1)) == :reactioninput₊S_S
        @test nameof(exogenous_outputs(reactioninput, :X_S)) == :reactioninput₊X_S
        # Test equations
        @test length(equations(reactioninput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactioninput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :S_S))) .== [0])
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :X_S))) .== [0])
        # Test with symbolic input
        @named reactioninput = ReactionInputToRealOutput([X_S, S_S])
        # Test if it has the right connectors
        @test !hasstates(reactioninput)
        @test num_states(reactioninput) == 0
        @test hasrates(reactioninput)
        @test num_rates(reactioninput) == 1
        @test !hasinflows(reactioninput)
        @test num_inflows(reactioninput) == 0
        @test !hasoutflows(reactioninput)
        @test num_outflows(reactioninput) == 0
        @test !hasexogenous_inputs(reactioninput)
        @test num_exogenous_inputs(reactioninput) == 0
        @test hasexogenous_outputs(reactioninput)
        @test num_exogenous_outputs(reactioninput) == 2
        # Test for name
        @test nameof(reactioninput) == :reactioninput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactioninput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactioninput)[1]
        @test_throws BoundsError inflows(reactioninput, 1)
        @test_throws ArgumentError inflows(reactioninput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactioninput)[1]
        @test_throws BoundsError outflows(reactioninput, 1)
        @test_throws ArgumentError outflows(reactioninput, :outflow)
        # Test states
        @test isnothing(states(reactioninput))
        # Test rates
        @test nameof(rates(reactioninput)) == :reactioninput₊reactioninput
        # Test exogenous inputs
        @test isempty(exogenous_inputs(reactioninput))
        @test isempty(exogenous_input_names(reactioninput))
        @test_throws BoundsError exogenous_inputs(reactioninput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(reactioninput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(reactioninput)) == 2
        @test issetequal(exogenous_output_names(reactioninput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(reactioninput, 1)) == :reactioninput₊S_S
        @test nameof(exogenous_outputs(reactioninput, :X_S)) == :reactioninput₊X_S
        # Test equations
        @test length(equations(reactioninput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactioninput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :X_S))) .== [2])
        # Test initial concentrations with array
        @named reactioninput = ReactionInputToRealOutput([X_S, S_S]; initial_rates = [5, 3])
        # Test if it has the right connectors
        @test !hasstates(reactioninput)
        @test num_states(reactioninput) == 0
        @test hasrates(reactioninput)
        @test num_rates(reactioninput) == 1
        @test !hasinflows(reactioninput)
        @test num_inflows(reactioninput) == 0
        @test !hasoutflows(reactioninput)
        @test num_outflows(reactioninput) == 0
        @test !hasexogenous_inputs(reactioninput)
        @test num_exogenous_inputs(reactioninput) == 0
        @test hasexogenous_outputs(reactioninput)
        @test num_exogenous_outputs(reactioninput) == 2
        # Test for name
        @test nameof(reactioninput) == :reactioninput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactioninput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactioninput)[1]
        @test_throws BoundsError inflows(reactioninput, 1)
        @test_throws ArgumentError inflows(reactioninput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactioninput)[1]
        @test_throws BoundsError outflows(reactioninput, 1)
        @test_throws ArgumentError outflows(reactioninput, :outflow)
        # Test states
        @test isnothing(states(reactioninput))
        # Test rates
        @test nameof(rates(reactioninput)) == :reactioninput₊reactioninput
        # Test exogenous inputs
        @test isempty(exogenous_inputs(reactioninput))
        @test isempty(exogenous_input_names(reactioninput))
        @test_throws BoundsError exogenous_inputs(reactioninput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(reactioninput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(reactioninput)) == 2
        @test issetequal(exogenous_output_names(reactioninput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(reactioninput, 1)) == :reactioninput₊S_S
        @test nameof(exogenous_outputs(reactioninput, :X_S)) == :reactioninput₊X_S
        # Test equations
        @test length(equations(reactioninput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactioninput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :S_S))) .== [3])
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :X_S))) .== [5])
        # Test initial concentrations with map
        @named reactioninput = ReactionInputToRealOutput([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
        # Test if it has the right connectors
        @test !hasstates(reactioninput)
        @test num_states(reactioninput) == 0
        @test hasrates(reactioninput)
        @test num_rates(reactioninput) == 1
        @test !hasinflows(reactioninput)
        @test num_inflows(reactioninput) == 0
        @test !hasoutflows(reactioninput)
        @test num_outflows(reactioninput) == 0
        @test !hasexogenous_inputs(reactioninput)
        @test num_exogenous_inputs(reactioninput) == 0
        @test hasexogenous_outputs(reactioninput)
        @test num_exogenous_outputs(reactioninput) == 2
        # Test for name
        @test nameof(reactioninput) == :reactioninput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(reactioninput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(reactioninput)[1]
        @test_throws BoundsError inflows(reactioninput, 1)
        @test_throws ArgumentError inflows(reactioninput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(reactioninput)[1]
        @test_throws BoundsError outflows(reactioninput, 1)
        @test_throws ArgumentError outflows(reactioninput, :outflow)
        # Test states
        @test isnothing(states(reactioninput))
        # Test rates
        @test nameof(rates(reactioninput)) == :reactioninput₊reactioninput
        # Test exogenous inputs
        @test isempty(exogenous_inputs(reactioninput))
        @test isempty(exogenous_input_names(reactioninput))
        @test_throws BoundsError exogenous_inputs(reactioninput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(reactioninput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(reactioninput)) == 2
        @test issetequal(exogenous_output_names(reactioninput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(reactioninput, 1)) == :reactioninput₊S_S
        @test nameof(exogenous_outputs(reactioninput, :X_S)) == :reactioninput₊X_S
        # Test equations
        @test length(equations(reactioninput)) == 2
        # Test that none has defaults
        for unk in unknowns(reactioninput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :S_S))) .== [5.25])
#        @test all(getguess.(unknowns(exogenous_outputs(reactioninput, :X_S))) .== [123.5])
    end

    # Test RealInputToStateOutput
    @testset "RealInputToStateOutput tests" begin
        # Test with dict input
        @named stateoutput = RealInputToStateOutput(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true)]))
        # Test if it has the right connectors
        @test hasstates(stateoutput)
        @test num_states(stateoutput) == 1
        @test !hasrates(stateoutput)
        @test num_rates(stateoutput) == 0
        @test !hasinflows(stateoutput)
        @test num_inflows(stateoutput) == 0
        @test !hasoutflows(stateoutput)
        @test num_outflows(stateoutput) == 0
        @test hasexogenous_inputs(stateoutput)
        @test num_exogenous_inputs(stateoutput) == 2
        @test !hasexogenous_outputs(stateoutput)
        @test num_exogenous_outputs(stateoutput) == 0
        # Test for name
        @test nameof(stateoutput) == :stateoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateoutput)[1]
        @test_throws BoundsError inflows(stateoutput, 1)
        @test_throws ArgumentError inflows(stateoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateoutput)[1]
        @test_throws BoundsError outflows(stateoutput, 1)
        @test_throws ArgumentError outflows(stateoutput, :outflow)
        # Test states
        @test nameof(states(stateoutput)) == :stateoutput₊stateoutput
        # Test rates
        @test isnothing(rates(stateoutput))
        # Test exogenous inputs
        @test length(exogenous_inputs(stateoutput)) == 2
        @test issetequal(exogenous_input_names(stateoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(stateoutput, 1)) == :stateoutput₊S_S
        @test nameof(exogenous_inputs(stateoutput, :X_S)) == :stateoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(stateoutput))
        @test isempty(exogenous_output_names(stateoutput))
        @test_throws BoundsError exogenous_outputs(stateoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(stateoutput, [:q, :X_S])
        # Test equations
        @test length(equations(stateoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :S_S))) .== [0])
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :X_S))) .== [0])
        # Test with symbolic input
        @named stateoutput = RealInputToStateOutput([X_S, S_S])
        # Test if it has the right connectors
        @test hasstates(stateoutput)
        @test num_states(stateoutput) == 1
        @test !hasrates(stateoutput)
        @test num_rates(stateoutput) == 0
        @test !hasinflows(stateoutput)
        @test num_inflows(stateoutput) == 0
        @test !hasoutflows(stateoutput)
        @test num_outflows(stateoutput) == 0
        @test hasexogenous_inputs(stateoutput)
        @test num_exogenous_inputs(stateoutput) == 2
        @test !hasexogenous_outputs(stateoutput)
        @test num_exogenous_outputs(stateoutput) == 0
        # Test for name
        @test nameof(stateoutput) == :stateoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateoutput)[1]
        @test_throws BoundsError inflows(stateoutput, 1)
        @test_throws ArgumentError inflows(stateoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateoutput)[1]
        @test_throws BoundsError outflows(stateoutput, 1)
        @test_throws ArgumentError outflows(stateoutput, :outflow)
        # Test states
        @test nameof(states(stateoutput)) == :stateoutput₊stateoutput
        # Test rates
        @test isnothing(rates(stateoutput))
        # Test exogenous inputs
        @test length(exogenous_inputs(stateoutput)) == 2
        @test issetequal(exogenous_input_names(stateoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(stateoutput, 1)) == :stateoutput₊S_S
        @test nameof(exogenous_inputs(stateoutput, :X_S)) == :stateoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(stateoutput))
        @test isempty(exogenous_output_names(stateoutput))
        @test_throws BoundsError exogenous_outputs(stateoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(stateoutput, [:q, :X_S])
        # Test equations
        @test length(equations(stateoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :X_S))) .== [2])
        # Test initial concentrations with array
        @named stateoutput = RealInputToStateOutput([X_S, S_S]; initial_states = [5, 3])
        # Test if it has the right connectors
        @test hasstates(stateoutput)
        @test num_states(stateoutput) == 1
        @test !hasrates(stateoutput)
        @test num_rates(stateoutput) == 0
        @test !hasinflows(stateoutput)
        @test num_inflows(stateoutput) == 0
        @test !hasoutflows(stateoutput)
        @test num_outflows(stateoutput) == 0
        @test hasexogenous_inputs(stateoutput)
        @test num_exogenous_inputs(stateoutput) == 2
        @test !hasexogenous_outputs(stateoutput)
        @test num_exogenous_outputs(stateoutput) == 0
        # Test for name
        @test nameof(stateoutput) == :stateoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateoutput)[1]
        @test_throws BoundsError inflows(stateoutput, 1)
        @test_throws ArgumentError inflows(stateoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateoutput)[1]
        @test_throws BoundsError outflows(stateoutput, 1)
        @test_throws ArgumentError outflows(stateoutput, :outflow)
        # Test states
        @test nameof(states(stateoutput)) == :stateoutput₊stateoutput
        # Test rates
        @test isnothing(rates(stateoutput))
        # Test exogenous inputs
        @test length(exogenous_inputs(stateoutput)) == 2
        @test issetequal(exogenous_input_names(stateoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(stateoutput, 1)) == :stateoutput₊S_S
        @test nameof(exogenous_inputs(stateoutput, :X_S)) == :stateoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(stateoutput))
        @test isempty(exogenous_output_names(stateoutput))
        @test_throws BoundsError exogenous_outputs(stateoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(stateoutput, [:q, :X_S])
        # Test equations
        @test length(equations(stateoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :S_S))) .== [3])
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :X_S))) .== [5])
        # Test initial concentrations with map
        @named stateoutput = RealInputToStateOutput([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
        # Test if it has the right connectors
        @test hasstates(stateoutput)
        @test num_states(stateoutput) == 1
        @test !hasrates(stateoutput)
        @test num_rates(stateoutput) == 0
        @test !hasinflows(stateoutput)
        @test num_inflows(stateoutput) == 0
        @test !hasoutflows(stateoutput)
        @test num_outflows(stateoutput) == 0
        @test hasexogenous_inputs(stateoutput)
        @test num_exogenous_inputs(stateoutput) == 2
        @test !hasexogenous_outputs(stateoutput)
        @test num_exogenous_outputs(stateoutput) == 0
        # Test for name
        @test nameof(stateoutput) == :stateoutput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateoutput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateoutput)[1]
        @test_throws BoundsError inflows(stateoutput, 1)
        @test_throws ArgumentError inflows(stateoutput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateoutput)[1]
        @test_throws BoundsError outflows(stateoutput, 1)
        @test_throws ArgumentError outflows(stateoutput, :outflow)
        # Test states
        @test nameof(states(stateoutput)) == :stateoutput₊stateoutput
        # Test rates
        @test isnothing(rates(stateoutput))
        # Test exogenous inputs
        @test length(exogenous_inputs(stateoutput)) == 2
        @test issetequal(exogenous_input_names(stateoutput), [:X_S, :S_S])
        @test nameof(exogenous_inputs(stateoutput, 1)) == :stateoutput₊S_S
        @test nameof(exogenous_inputs(stateoutput, :X_S)) == :stateoutput₊X_S
        # Test exogenous outputs
        @test isempty(exogenous_outputs(stateoutput))
        @test isempty(exogenous_output_names(stateoutput))
        @test_throws BoundsError exogenous_outputs(stateoutput, [1, 3])
        @test_throws ArgumentError exogenous_outputs(stateoutput, [:q, :X_S])
        # Test equations
        @test length(equations(stateoutput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateoutput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :S_S))) .== [5.25])
#        @test all(getguess.(unknowns(exogenous_inputs(stateoutput, :X_S))) .== [123.5])
    end

    # Test StateInputToRealOutput
    @testset "StateInputToRealOutput tests" begin
        # Test with dict input
        @named stateinput = StateInputToRealOutput(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true)]))
        # Test if it has the right connectors
        @test hasstates(stateinput)
        @test num_states(stateinput) == 1
        @test !hasrates(stateinput)
        @test num_rates(stateinput) == 0
        @test !hasinflows(stateinput)
        @test num_inflows(stateinput) == 0
        @test !hasoutflows(stateinput)
        @test num_outflows(stateinput) == 0
        @test !hasexogenous_inputs(stateinput)
        @test num_exogenous_inputs(stateinput) == 0
        @test hasexogenous_outputs(stateinput)
        @test num_exogenous_outputs(stateinput) == 2
        # Test for name
        @test nameof(stateinput) == :stateinput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateinput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateinput)[1]
        @test_throws BoundsError inflows(stateinput, 1)
        @test_throws ArgumentError inflows(stateinput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateinput)[1]
        @test_throws BoundsError outflows(stateinput, 1)
        @test_throws ArgumentError outflows(stateinput, :outflow)
        # Test states
        @test nameof(states(stateinput)) == :stateinput₊stateinput
        # Test rates
        @test isnothing(rates(stateinput))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(stateinput))
        @test isempty(exogenous_input_names(stateinput))
        @test_throws BoundsError exogenous_inputs(stateinput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(stateinput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(stateinput)) == 2
        @test issetequal(exogenous_output_names(stateinput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(stateinput, 1)) == :stateinput₊S_S
        @test nameof(exogenous_outputs(stateinput, :X_S)) == :stateinput₊X_S
        # Test equations
        @test length(equations(stateinput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateinput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :S_S))) .== [0])
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :X_S))) .== [0])
        # Test with symbolic input
        @named stateinput = StateInputToRealOutput([X_S, S_S])
        # Test if it has the right connectors
        @test hasstates(stateinput)
        @test num_states(stateinput) == 1
        @test !hasrates(stateinput)
        @test num_rates(stateinput) == 0
        @test !hasinflows(stateinput)
        @test num_inflows(stateinput) == 0
        @test !hasoutflows(stateinput)
        @test num_outflows(stateinput) == 0
        @test !hasexogenous_inputs(stateinput)
        @test num_exogenous_inputs(stateinput) == 0
        @test hasexogenous_outputs(stateinput)
        @test num_exogenous_outputs(stateinput) == 2
        # Test for name
        @test nameof(stateinput) == :stateinput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateinput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateinput)[1]
        @test_throws BoundsError inflows(stateinput, 1)
        @test_throws ArgumentError inflows(stateinput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateinput)[1]
        @test_throws BoundsError outflows(stateinput, 1)
        @test_throws ArgumentError outflows(stateinput, :outflow)
        # Test states
        @test nameof(states(stateinput)) == :stateinput₊stateinput
        # Test rates
        @test isnothing(rates(stateinput))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(stateinput))
        @test isempty(exogenous_input_names(stateinput))
        @test_throws BoundsError exogenous_inputs(stateinput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(stateinput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(stateinput)) == 2
        @test issetequal(exogenous_output_names(stateinput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(stateinput, 1)) == :stateinput₊S_S
        @test nameof(exogenous_outputs(stateinput, :X_S)) == :stateinput₊X_S
        # Test equations
        @test length(equations(stateinput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateinput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :S_S))) .== [12.5])
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :X_S))) .== [2])
        # Test initial concentrations with array
        @named stateinput = StateInputToRealOutput([X_S, S_S]; initial_states = [5, 3])
        # Test if it has the right connectors
        @test hasstates(stateinput)
        @test num_states(stateinput) == 1
        @test !hasrates(stateinput)
        @test num_rates(stateinput) == 0
        @test !hasinflows(stateinput)
        @test num_inflows(stateinput) == 0
        @test !hasoutflows(stateinput)
        @test num_outflows(stateinput) == 0
        @test !hasexogenous_inputs(stateinput)
        @test num_exogenous_inputs(stateinput) == 0
        @test hasexogenous_outputs(stateinput)
        @test num_exogenous_outputs(stateinput) == 2
        # Test for name
        @test nameof(stateinput) == :stateinput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateinput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateinput)[1]
        @test_throws BoundsError inflows(stateinput, 1)
        @test_throws ArgumentError inflows(stateinput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateinput)[1]
        @test_throws BoundsError outflows(stateinput, 1)
        @test_throws ArgumentError outflows(stateinput, :outflow)
        # Test states
        @test nameof(states(stateinput)) == :stateinput₊stateinput
        # Test rates
        @test isnothing(rates(stateinput))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(stateinput))
        @test isempty(exogenous_input_names(stateinput))
        @test_throws BoundsError exogenous_inputs(stateinput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(stateinput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(stateinput)) == 2
        @test issetequal(exogenous_output_names(stateinput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(stateinput, 1)) == :stateinput₊S_S
        @test nameof(exogenous_outputs(stateinput, :X_S)) == :stateinput₊X_S
        # Test equations
        @test length(equations(stateinput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateinput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :S_S))) .== [3])
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :X_S))) .== [5])
        # Test initial concentrations with map
        @named stateinput = StateInputToRealOutput([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
        # Test if it has the right connectors
        @test hasstates(stateinput)
        @test num_states(stateinput) == 1
        @test !hasrates(stateinput)
        @test num_rates(stateinput) == 0
        @test !hasinflows(stateinput)
        @test num_inflows(stateinput) == 0
        @test !hasoutflows(stateinput)
        @test num_outflows(stateinput) == 0
        @test !hasexogenous_inputs(stateinput)
        @test num_exogenous_inputs(stateinput) == 0
        @test hasexogenous_outputs(stateinput)
        @test num_exogenous_outputs(stateinput) == 2
        # Test for name
        @test nameof(stateinput) == :stateinput
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(stateinput)) == :t
        # Test inflows
        @test_throws BoundsError inflows(stateinput)[1]
        @test_throws BoundsError inflows(stateinput, 1)
        @test_throws ArgumentError inflows(stateinput, :inflow)
        # Test outflows 
        @test_throws BoundsError outflows(stateinput)[1]
        @test_throws BoundsError outflows(stateinput, 1)
        @test_throws ArgumentError outflows(stateinput, :outflow)
        # Test states
        @test nameof(states(stateinput)) == :stateinput₊stateinput
        # Test rates
        @test isnothing(rates(stateinput))
        # Test exogenous inputs
        @test isempty(exogenous_inputs(stateinput))
        @test isempty(exogenous_input_names(stateinput))
        @test_throws BoundsError exogenous_inputs(stateinput, [1, 3])
        @test_throws ArgumentError exogenous_inputs(stateinput, [:q, :X_S])
        # Test exogenous outputs
        @test length(exogenous_outputs(stateinput)) == 2
        @test issetequal(exogenous_output_names(stateinput), [:X_S, :S_S])
        @test nameof(exogenous_outputs(stateinput, 1)) == :stateinput₊S_S
        @test nameof(exogenous_outputs(stateinput, :X_S)) == :stateinput₊X_S
        # Test equations
        @test length(equations(stateinput)) == 2
        # Test that none has defaults
        for unk in unknowns(stateinput)
            @test_throws ErrorException Symbolics.getdefaultval.(unk)
        end
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :S_S))) .== [5.25])
#        @test all(getguess.(unknowns(exogenous_outputs(stateinput, :X_S))) .== [123.5])
    end
end

