@variables q(t) S_S(t) = 12.5 [particulate = false, colloidal = false, soluble = true] X_S(t) = 2 [particulate = true, colloidal = false, soluble = false]

@testset "Init metadata test" begin
    # Test with port
    @named inflow = ProcessSimulator.InflowPort([X_S, S_S])
    @test isnothing(ModelingToolkit.get_metadata(inflow))
    inflow = ProcessSimulator.init_port_metadata(inflow)
    @test ismissing(ModelingToolkit.get_metadata(inflow).name)
    @test ismissing(ModelingToolkit.get_metadata(inflow).type)
    @test ismissing(ModelingToolkit.get_metadata(inflow).isinput)
    @test ismissing(ModelingToolkit.get_metadata(inflow).index)
    @test ismissing(ModelingToolkit.get_metadata(inflow).parent)
    @test isnothing(ModelingToolkit.get_metadata(inflow).other)
    # Test with normal ODESystem
    @named sys = ODESystem(Equation[], t)
    @test isnothing(ModelingToolkit.get_metadata(sys))
    sys = ProcessSimulator.init_port_metadata(sys)
    @test ismissing(ModelingToolkit.get_metadata(sys).name)
    @test ismissing(ModelingToolkit.get_metadata(sys).type)
    @test ismissing(ModelingToolkit.get_metadata(sys).isinput)
    @test ismissing(ModelingToolkit.get_metadata(sys).index)
    @test ismissing(ModelingToolkit.get_metadata(sys).parent)
    @test isnothing(ModelingToolkit.get_metadata(sys).other)
    # Test with normal ODESystem which already has metadata
    @named sys = ODESystem(Equation[], t; metadata="Test System")
    @test ModelingToolkit.get_metadata(sys) == "Test System"
    sys = ProcessSimulator.init_port_metadata(sys)
    @test ismissing(ModelingToolkit.get_metadata(sys).name)
    @test ismissing(ModelingToolkit.get_metadata(sys).type)
    @test ismissing(ModelingToolkit.get_metadata(sys).isinput)
    @test ismissing(ModelingToolkit.get_metadata(sys).index)
    @test ismissing(ModelingToolkit.get_metadata(sys).parent)
    @test ModelingToolkit.get_metadata(sys).other == "Test System"
end

# Test if all process elements set it appropriately (one per type)
function test_metadata(ports, port_names, type, isinput, parent)
    for (i, (port, port_name)) in enumerate(zip(ports, port_names))
        @test ModelingToolkit.get_metadata(port) isa ProcessSimulator.PortMetadata
        @test ModelingToolkit.get_metadata(port).name == port_name
        @test ModelingToolkit.get_metadata(port).type == type
        @test ModelingToolkit.get_metadata(port).isinput == isinput
        @test ModelingToolkit.get_metadata(port).index == i
        @test ModelingToolkit.get_metadata(port).parent == parent
    end
end
function test_pe_metadata(pe)
    hasrates(pe) && test_metadata([rates(pe)], [Symbol(ProcessSimulator.lstrip_namespace(nameof(rates(pe)), nameof(pe)))], :rate, all(isinput.(unknowns(rates(pe)))), pe)
    hasstates(pe) && test_metadata([states(pe)], [Symbol(ProcessSimulator.lstrip_namespace(nameof(states(pe)), nameof(pe)))], :state, all(isinput.(unknowns(states(pe)))), pe)
    hasinflows(pe) && test_metadata(inflows(pe), inflow_names(pe), :flow, true, pe)
    hasoutflows(pe) && test_metadata(outflows(pe), outflow_names(pe), :flow, false, pe)
    hasexogenous_inputs(pe) && test_metadata(exogenous_inputs(pe), exogenous_input_names(pe), :exogenous, true, pe)
    hasexogenous_outputs(pe) && test_metadata(exogenous_outputs(pe), exogenous_output_names(pe), :exogenous, false, pe)
end
@testset "ProcessElements Port Metadata" begin
    @testset "Clarifiers" begin
        @named clarifier = IdealClarifier([S_S, X_S]; f_ns = 0)
        test_pe_metadata(clarifier)
    end

    @testset "FlowElements" begin
        @named influent = Influent(
            [1, ModelingToolkitStandardLibrary.Blocks.Constant(;k = 2, name=:X_S_val), sin],
            [q, X_S, S_S]
            )
        test_pe_metadata(influent)
        @named connector = FlowConnector([S_S, X_S]; n_in = 2, n_out = 2)
        test_pe_metadata(connector)
        @named sensor = Sensor([q, X_S], [X_S, S_S])
        test_pe_metadata(sensor)
        @named unifier = FlowUnifier([S_S, X_S]; n_in = 2)
        test_pe_metadata(unifier)
        @named separator = FlowSeparator([S_S, X_S]; n_out = 2)
        test_pe_metadata(separator)
        @named pump = FlowPump([S_S, X_S])
        test_pe_metadata(pump)
    end

    @testset "modelingtoolkitstandardlibrarycompat" begin
        @named inflow = RealInputToOutflowPort([X_S, S_S])
        test_pe_metadata(inflow)
        @named outflow = InflowPortToRealOutput([X_S, S_S])
        test_pe_metadata(outflow)
        @named reactionoutput = RealInputToReactionOutput(["X_S", "S_S"])
        test_pe_metadata(reactionoutput)
        @named reactioninput = ReactionInputToRealOutput(["X_S", "S_S"])
        test_pe_metadata(reactioninput)
        @named stateoutput = RealInputToStateOutput([X_S, S_S])
        test_pe_metadata(stateoutput)
        @named stateinput = StateInputToRealOutput([X_S, S_S])
        test_pe_metadata(stateinput)
    end

    @testset "Processes" begin
        @named asm1 = ASM1()
		test_pe_metadata(asm1)
        @named aeration = Aeration()
		test_pe_metadata(aeration)
        @named ozonation = OzoneDisinfection()
		test_pe_metadata(ozonation)
        @named asm1 = Process("ASM1")
		test_pe_metadata(asm1)
        @named asm3 = Process("ASM3")
		test_pe_metadata(asm3)
    end

    @testset "Reactors" begin
        @named cstr = CSTR(1500, [S_S, X_S], initial_states = (S_S => 1, X_S => 2))
        test_pe_metadata(cstr)
        @named batch = BatchReactor([S_S, X_S], initial_states = (S_S => 1, X_S => 2))
        test_pe_metadata(batch)
    end
end

@testset "Test PortMetadata for non-BioChemicalTreatment systems" begin
    @named c = ModelingToolkitStandardLibrary.Blocks.Constant(k = 1)
    name, porttype, isinput, index, parent = BioChemicalTreatment.ProcessSimulator.assemble_port_metadata_fields(c.output, c; port_type=:exogenous)
    @test name == :output
    @test porttype == :exogenous
    @test !isinput
    @test index == 1
    @test parent == c

    @named add = ModelingToolkitStandardLibrary.Blocks.Add()
    name, porttype, isinput, index, parent = BioChemicalTreatment.ProcessSimulator.assemble_port_metadata_fields(add.output, add)
    @test name == :output
    @test porttype == :unknown
    @test !isinput
    @test index == 1
    @test parent == add
    name, porttype, isinput, index, parent = BioChemicalTreatment.ProcessSimulator.assemble_port_metadata_fields(add.input1, add)
    @test name == :input1
    @test porttype == :unknown
    @test isinput
    @test index == 1
    @test parent == add
    name, porttype, isinput, index, parent = BioChemicalTreatment.ProcessSimulator.assemble_port_metadata_fields(add.input2, add)
    @test name == :input2
    @test porttype == :unknown
    @test isinput
    @test index == 2
    @test parent == add

    @named pid = ModelingToolkitStandardLibrary.Blocks.PID()
    name, porttype, isinput, index, parent = BioChemicalTreatment.ProcessSimulator.assemble_port_metadata_fields(pid.ctr_output, pid)
    @test name == :ctr_output
    @test porttype == :unknown
    @test !isinput
    @test index == 1
    @test parent == pid
    name, porttype, isinput, index, parent = BioChemicalTreatment.ProcessSimulator.assemble_port_metadata_fields(pid.err_input, pid)
    @test name == :err_input
    @test porttype == :unknown
    @test isinput
    @test index == 1
    @test parent == pid
end
