@variables S_S(t) [particulate = false, colloidal = false, soluble = true] X_S(t) [particulate = true, colloidal = true, soluble = false]

@testset "isparticulate tests" begin
    @test Symbolics.option_to_metadata_type(Val(:particulate)) == ProcessSimulator.VariableParticulate
    @test !isparticulate(S_S)
    @test isparticulate(X_S)
    @test isnothing(isparticulate(t))
    @test isparticulate(t, default = true)
    @test !isparticulate(t, default = false)
    @test all(isparticulate.([t, S_S, X_S]) .== [nothing, false, true])
end

@testset "iscolloidal tests" begin
    @test Symbolics.option_to_metadata_type(Val(:colloidal)) == ProcessSimulator.VariableColloidal
    @test !iscolloidal(S_S)
    @test iscolloidal(X_S)
    @test isnothing(iscolloidal(t))
    @test iscolloidal(t, default = true)
    @test !iscolloidal(t, default = false)
    @test all(iscolloidal.([t, S_S, X_S]) .== [nothing, false, true])
end

@testset "issoluble tests" begin
    @test Symbolics.option_to_metadata_type(Val(:soluble)) == ProcessSimulator.VariableSoluble
    @test issoluble(S_S)
    @test !issoluble(X_S)
    @test isnothing(issoluble(t))
    @test issoluble(t, default = true)
    @test !issoluble(t, default = false)
    @test all(issoluble.([t, S_S, X_S]) .== [nothing, true, false])
end

