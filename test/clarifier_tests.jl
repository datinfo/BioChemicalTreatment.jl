@variables S_S(t) [particulate = false] X_S(t) [particulate = true]

@testset "IdealClarifier tests" begin
    @named clarifier = IdealClarifier([S_S, X_S]; f_ns = 0)
    @test !hasstates(clarifier)
    @test num_states(clarifier) == 0
    @test !hasrates(clarifier)
    @test num_rates(clarifier) == 0
    @test hasinflows(clarifier)
    @test num_inflows(clarifier) == 1
    @test hasoutflows(clarifier)
    @test num_outflows(clarifier) == 2
    @test !hasexogenous_inputs(clarifier)
    @test num_exogenous_inputs(clarifier) == 0
    @test !hasexogenous_outputs(clarifier)
    @test num_exogenous_outputs(clarifier) == 0
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(clarifier)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(clarifier, :sludge))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(clarifier, :effluent))), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(unknowns(clarifier)), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "sludge₊q(t)", "sludge₊S_S(t)", "sludge₊X_S(t)",
            "effluent₊q(t)", "effluent₊S_S(t)", "effluent₊X_S(t)",
        ]
    )
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(clarifier)), ["f_ns"])
    @test length(equations(clarifier)) == (1 + 2 + 2) # MassConservation Flow + 2*MassConservation Components + 2*Components in sludge
end
