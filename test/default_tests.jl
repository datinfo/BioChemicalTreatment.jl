@testset "Default ASM1" begin
    set_default_process(ASM1; name=:asm1)
    clear_default_state_mapping()

    should_states = ProcessSimulator.symbolic_to_namestring.(unknowns(states(ASM1(name=:asm1))))
    state_diff(x) = setdiff(ProcessSimulator.symbolic_to_namestring.(x), should_states)

    # Test defaults for Flow Elemens
    @test state_diff(unknowns(only(outflows(Influent([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]; name=:inf, flowrate=1))))) == ["q"]
    @test state_diff(unknowns(only(outflows(Sensor(:q; name=:sens))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowConnector(n_in=4, n_out=1; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowUnifier(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(inflows(FlowSeparator(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowPump(name=:pump))))) == ["q"]
    @test state_diff(unknowns(only(inflows(IdealClarifier(name=:clar))))) == ["q"]

    # Test defaults for modelingtoolkitstandardlibrarycompat
    @test state_diff(unknowns(only(outflows(RealInputToOutflowPort(name=:compat))))) == ["q"]
    @test state_diff(unknowns(only(inflows(InflowPortToRealOutput(name=:compat))))) == ["q"]
    @test isempty(state_diff(unknowns(rates(RealInputToReactionOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(rates(ReactionInputToRealOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(RealInputToStateOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(StateInputToRealOutput(name=:compat)))))

    # Test defaults for reactors
    @test isempty(state_diff(unknowns(rates(CSTR(1000; name=:react)))))
    @test isempty(state_diff(unknowns(rates(BatchReactor(name=:react)))))
end

@testset "Default ASM1 using macros" begin
    @set_process asm1 = ASM1()
    clear_default_state_mapping()

    should_states = ProcessSimulator.symbolic_to_namestring.(unknowns(states(ASM1(name=:asm1))))
    state_diff(x) = setdiff(ProcessSimulator.symbolic_to_namestring.(x), should_states)

    # Test defaults for Flow Elemens
    @test state_diff(unknowns(only(outflows(Influent([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]; name=:inf, flowrate=1))))) == ["q"]
    @test state_diff(unknowns(only(outflows(Sensor(:q; name=:sens))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowConnector(n_in=4, n_out=1; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowUnifier(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(inflows(FlowSeparator(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowPump(name=:pump))))) == ["q"]
    @test state_diff(unknowns(only(inflows(IdealClarifier(name=:clar))))) == ["q"]

    # Test defaults for modelingtoolkitstandardlibrarycompat
    @test state_diff(unknowns(only(outflows(RealInputToOutflowPort(name=:compat))))) == ["q"]
    @test state_diff(unknowns(only(inflows(InflowPortToRealOutput(name=:compat))))) == ["q"]
    @test isempty(state_diff(unknowns(rates(RealInputToReactionOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(rates(ReactionInputToRealOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(RealInputToStateOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(StateInputToRealOutput(name=:compat)))))

    # Test defaults for reactors
    @test isempty(state_diff(unknowns(rates(CSTR(1000; name=:react)))))
    @test isempty(state_diff(unknowns(rates(BatchReactor(name=:react)))))
end

@testset "Default ASM1 and aeration, mapping by states" begin
    set_default_process("ASM1"; name=:asm1)
    add_default_process(Aeration; name=:aer)
    set_default_state_mapping(S_O2 = [states(Aeration(;name=:aer)).S_O, states(Process("ASM1"; name=:asm1)).S_O2])

    should_states = ProcessSimulator.symbolic_to_namestring.(unknowns(states(Process("ASM1"; name=:asm1))))
    state_diff(x) = setdiff(ProcessSimulator.symbolic_to_namestring.(x), should_states)

    # Test defaults for Flow Elements
    @test state_diff(unknowns(only(outflows(Influent([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]; name=:inf, flowrate=1))))) == ["q"]
    @test state_diff(unknowns(only(outflows(Sensor(:q; name=:sens))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowConnector(n_in=4, n_out=1; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowUnifier(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(inflows(FlowSeparator(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowPump(name=:pump))))) == ["q"]
    @test state_diff(unknowns(only(inflows(IdealClarifier(name=:clar))))) == ["q"]

    # Test defaults for modelingtoolkitstandardlibrarycompat
    @test state_diff(unknowns(only(outflows(RealInputToOutflowPort(name=:compat))))) == ["q"]
    @test state_diff(unknowns(only(inflows(InflowPortToRealOutput(name=:compat))))) == ["q"]
    @test isempty(state_diff(unknowns(rates(RealInputToReactionOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(rates(ReactionInputToRealOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(RealInputToStateOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(StateInputToRealOutput(name=:compat)))))

    # Test defaults for reactors
    @test isempty(state_diff(unknowns(rates(CSTR(1000; name=:react)))))
    @test isempty(state_diff(unknowns(rates(BatchReactor(name=:react)))))
end

@testset "Default ASM1 and aeration, mapping by states using macros" begin
    @set_process asm = Process("ASM1")
    @add_process aer = Aeration()

    @set_state_mapping S_O2 = states(aer).S_O, states(asm).S_O2

    should_states = ProcessSimulator.symbolic_to_namestring.(unknowns(states(Process("ASM1"; name=:asm1))))
    state_diff(x) = setdiff(ProcessSimulator.symbolic_to_namestring.(x), should_states)

    # Test defaults for Flow Elements
    @test state_diff(unknowns(only(outflows(Influent([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]; name=:inf, flowrate=1))))) == ["q"]
    @test state_diff(unknowns(only(outflows(Sensor(:q; name=:sens))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowConnector(n_in=4, n_out=1; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowUnifier(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(inflows(FlowSeparator(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowPump(name=:pump))))) == ["q"]
    @test state_diff(unknowns(only(inflows(IdealClarifier(name=:clar))))) == ["q"]

    # Test defaults for modelingtoolkitstandardlibrarycompat
    @test state_diff(unknowns(only(outflows(RealInputToOutflowPort(name=:compat))))) == ["q"]
    @test state_diff(unknowns(only(inflows(InflowPortToRealOutput(name=:compat))))) == ["q"]
    @test isempty(state_diff(unknowns(rates(RealInputToReactionOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(rates(ReactionInputToRealOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(RealInputToStateOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(StateInputToRealOutput(name=:compat)))))

    # Test defaults for reactors
    @test isempty(state_diff(unknowns(rates(CSTR(1000; name=:react)))))
    @test isempty(state_diff(unknowns(rates(BatchReactor(name=:react)))))
end

@testset "Default ASM1 and aeration, mapping by states using single macro with state renaming" begin
    @set_process begin 
        asm = Process("ASM1") 
        aer = Aeration()
    end

    @set_state_mapping begin
        S_O2 = states(aer).S_O, states(asm).S_O2
        {X_H = states(asm).X_OHO :particulate}
    end

    should_states = map(x-> x == "X_OHO" ? "X_H" : x,ProcessSimulator.symbolic_to_namestring.(unknowns(states(Process("ASM1"; name=:asm1)))))
    state_diff(x) = setdiff(ProcessSimulator.symbolic_to_namestring.(x), should_states)

    # Test defaults for Flow Elements
    @test state_diff(unknowns(only(outflows(Influent([1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]; name=:inf, flowrate=1))))) == ["q"]
    @test state_diff(unknowns(only(outflows(Sensor(:q; name=:sens))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowConnector(n_in=4, n_out=1; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowUnifier(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(inflows(FlowSeparator(; name=:conn))))) == ["q"]
    @test state_diff(unknowns(only(outflows(FlowPump(name=:pump))))) == ["q"]
    @test state_diff(unknowns(only(inflows(IdealClarifier(name=:clar))))) == ["q"]

    # Test defaults for modelingtoolkitstandardlibrarycompat
    @test state_diff(unknowns(only(outflows(RealInputToOutflowPort(name=:compat))))) == ["q"]
    @test state_diff(unknowns(only(inflows(InflowPortToRealOutput(name=:compat))))) == ["q"]
    @test isempty(state_diff(unknowns(rates(RealInputToReactionOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(rates(ReactionInputToRealOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(RealInputToStateOutput(name=:compat)))))
    @test isempty(state_diff(unknowns(states(StateInputToRealOutput(name=:compat)))))

    # Test defaults for reactors
    @test isempty(state_diff(unknowns(rates(CSTR(1000; name=:react)))))
    @test isempty(state_diff(unknowns(rates(BatchReactor(name=:react)))))
end

@testset "Test failures" begin
    @set_process begin 
        asm = Process("ASM1") 
        aer = Aeration()
    end

    @test_logs (:info, r"Adding Set\(\[:DO, :S_O2\]\) to the already existing equal states of \(names = Set\(\[:S_O, :S_O2\]\), particulate = false, colloidal = false, soluble = true\).*") @set_state_mapping begin
        S_O2 = states(aer).S_O, states(asm).S_O2, :DO
        {X_H = states(asm).X_OHO :colloidal}
    end


    @test default_state_mapping() == Dict([
        :X_H  => (names = Set([:X_OHO, :X_H]), particulate = 0, colloidal = 1, soluble = 0)
        :S_O2 => (names = Set([:DO, :S_O, :S_O2]), particulate = 0, colloidal = 0, soluble = 1)
        ])

    @test_throws ArgumentError @add_state_mapping X_H = states(asm).X_OHO
    @test_throws ArgumentError add_default_state_mapping(:S_O, (:S_A, :A_B))    
    @test_throws ArgumentError add_default_state_mapping(:S_O, states(asm).X_OHO)
    @test_throws ArgumentError @add_state_mapping S_O = states(asm).S_O2, :DO
    @test_throws ArgumentError @add_state_mapping S_O = states(asm).S_O2, states(asm).XC_B
end
