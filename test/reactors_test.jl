@variables S_S(t) [particulate = false] X_S(t) [particulate = true]

@testset "CSTR tests" begin    
    @test_throws ArgumentError @named cstr = CSTR(1) # Reactor without states, processes or defaults
    @named cstr = CSTR(1500, [S_S, X_S], initial_states = (S_S => 1, X_S => 2))
    # Test if it has the right connectors
    @test hasstates(cstr)
    @test num_states(cstr) == 1
    @test hasrates(cstr)
    @test num_rates(cstr) == 1
    @test hasinflows(cstr)
    @test num_inflows(cstr) == 1
    @test hasoutflows(cstr)
    @test num_outflows(cstr) == 1
    @test !hasexogenous_inputs(cstr)
    @test num_exogenous_inputs(cstr) == 0
    @test !hasexogenous_outputs(cstr)
    @test num_exogenous_outputs(cstr) == 0
    @test !hassubsystems(cstr)
    @test num_subsystems(cstr) == 0
    # Test for name
    @test nameof(cstr) == :cstr
    # Test for getting iv
    @test nameof.(independent_variables(cstr)) == [:t]
    @test nameof.(ModelingToolkit.independent_variable_symbols(cstr)) == [:t]
    @test nameof(ModelingToolkit.get_iv(cstr)) == :t
    # Test inflow
    @test length(inflows(cstr)) == 1
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(cstr)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(cstr, 1))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(cstr, :inflow))), ["q", "S_S", "X_S"])
    @test_throws ArgumentError inflows(cstr, :not_existing)
    # Test outflow
    @test length(outflows(cstr)) == 1
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(cstr)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(cstr, 1))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(cstr, :outflow))), ["q", "S_S", "X_S"])
    @test_throws ArgumentError outflows(cstr, :not_existing)
    # Test rates
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(cstr))), ["S_S", "X_S"])
    # Test states
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(cstr))), ["S_S", "X_S"])
    # Test for undefined exogenous inputs
    @test isempty(exogenous_inputs(cstr))
    @test_throws BoundsError exogenous_inputs(cstr, 1)
    @test_throws ArgumentError exogenous_inputs(cstr, :not_existing)
    # Test for undefined exogenous outputs
    @test_throws ErrorException exogenous_outputs(cstr)
    @test_throws ErrorException exogenous_outputs(cstr, 1)
    @test_throws ErrorException exogenous_outputs(cstr, :not_existing)
    # Test for non-existing subsystems
    @test isempty(subsystems(cstr))
    @test_throws BoundsError subsystems(cstr, 1)
    @test_throws ArgumentError subsystems(cstr, :not_existing)
    # Test for all unknowns
    @test issetequal(
        string.(unknowns(cstr)), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)",
            "states₊S_S(t)", "states₊X_S(t)",
            "rates₊S_S(t)", "rates₊X_S(t)",
        ]
    )
    # Test particulate
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(cstr))) .=> isparticulate.(unknowns(states(cstr)))) == 
            Dict(["S_S", "X_S"] .=> [false, true])
    # Test default values
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(cstr))) .=> Symbolics.getdefaultval.(unknowns(states(cstr)))) == 
            Dict(["S_S", "X_S"] .=> [1, 2])
    @test issetequal(string.(keys(defaults(cstr))), ["states₊X_S(t)", "states₊S_S(t)", "V"])
    @test issetequal(values(defaults(cstr)), [2, 1, 1500])
    # Test number of equations
    @test length(equations(cstr)) == 5 # 1 for flow, 2 ODE for components, 2 output equal to internal state
    # Test parameters
    @test ProcessSimulator.symbolic_to_namestring.(parameters(cstr)) == ["V"]
    # Test MTK overloads
    @test ModelingToolkit.has_eqs(cstr)
    @test ModelingToolkit.get_eqs(cstr) == equations(cstr)
    @test ModelingToolkit.has_unknowns(cstr)
    @test isempty(ModelingToolkit.get_unknowns(cstr))
    @test ModelingToolkit.has_ps(cstr)
    @test string.(ModelingToolkit.get_ps(cstr)) == ["V"]
    @test ModelingToolkit.has_systems(cstr)
    @test issetequal(string.(nameof.(ModelingToolkit.get_systems(cstr))), ["states", "rates", "inflow", "outflow"])
    @test !ModelingToolkit.is_dde(cstr)
    @test ModelingToolkit.has_parameter_dependencies(cstr)
    @test isempty(ModelingToolkit.get_parameter_dependencies(cstr))
    @test !ModelingToolkit.has_constraints(cstr)
    @test_throws ErrorException ModelingToolkit.get_constraints(cstr)
    @test !ModelingToolkit.has_op(cstr)
    @test_throws ErrorException ModelingToolkit.get_op(cstr)
    @test isempty(ModelingToolkit.observed(cstr))
    @test ModelingToolkit.has_observed(cstr)
    @test isempty(ModelingToolkit.get_observed(cstr))
    @test isempty(ModelingToolkit.get_continuous_events(cstr))
    @test ModelingToolkit.has_defaults(cstr)
    @test ModelingToolkit.get_defaults(cstr) == Dict(parameters(cstr) .=> 1500)
    @test !ModelingToolkit.has_noiseeqs(cstr)
    @test_throws ErrorException ModelingToolkit.get_noiseeqs(cstr)
    @test ModelingToolkit.has_metadata(cstr)
    @test isnothing(ModelingToolkit.get_metadata(cstr))
    @test ModelingToolkit.has_jac(cstr)
    @test isempty(ModelingToolkit.get_jac(cstr).x)
    @test ModelingToolkit.has_tgrad(cstr)
    @test isempty(ModelingToolkit.get_tgrad(cstr).x)

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), cstr)
    @test String(take!(buf)) == "CSTR 'cstr':\nStates (2): see states(cstr)\n  S_S(t) [defaults to 1.0]: S_S\n  X_S(t) [defaults to 2.0]: X_S\nInflows (1): see inflows(cstr)\n  :inflow\nOutflows (1): see outflows(cstr)\n  :outflow\nParameters (1): see parameters(cstr)\n  V [defaults to 1500]"
    show(buf, MIME("text/plain"), cstr; extended=true)
    @test String(take!(buf)) == "CSTR 'cstr':\nStates (2): see states(cstr)\n  S_S(t) [defaults to 1.0]: S_S\n  X_S(t) [defaults to 2.0]: X_S\nRates (2): see rates(cstr)\n  S_S(t) [guess is 0.0]: S_S reaction rate\n  X_S(t) [guess is 0.0]: X_S reaction rate\nInflows (1): see inflows(cstr)\n  :inflow\nOutflows (1): see outflows(cstr)\n  :outflow\nEquations (5):\n  5 standard: see equations(cstr)\nParameters (1): see parameters(cstr)\n  V [defaults to 1500]"
end

@testset "Batch Reactor tests" begin
    @test_throws ArgumentError @named batch = BatchReactor() # Reactor without states or default states
    @named batch = BatchReactor([S_S, X_S], initial_states = (S_S => 1, X_S => 2))
    # Test if it has the right connectors
    @test hasstates(batch)
    @test num_states(batch) == 1
    @test hasrates(batch)
    @test num_rates(batch) == 1
    @test !hasinflows(batch)
    @test num_inflows(batch) == 0
    @test !hasoutflows(batch)
    @test num_outflows(batch) == 0
    @test !hasexogenous_inputs(batch)
    @test num_exogenous_inputs(batch) == 0
    @test !hasexogenous_outputs(batch)
    @test num_exogenous_outputs(batch) == 0
    @test !hassubsystems(batch)
    @test num_subsystems(batch) == 0
    # Test for name
    @test nameof(batch) == :batch
    # Test for getting iv
    @test nameof(ModelingToolkit.get_iv(batch)) == :t
    # Test inflow
    @test isempty(inflows(batch))
    @test_throws BoundsError inflows(batch)[1]
    @test_throws BoundsError inflows(batch, 1)
    @test_throws ArgumentError inflows(batch, :inflow)
    # Test outflow
    @test isempty(outflows(batch))
    @test_throws BoundsError outflows(batch)[1]
    @test_throws BoundsError outflows(batch, 1)
    @test_throws ArgumentError outflows(batch, :outflow)
    # Test rates
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(batch))), ["S_S", "X_S"])
    # Test states
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(batch))), ["S_S", "X_S"])
    # Test for undefined exogenous inputs
    @test isempty(exogenous_inputs(batch))
    @test_throws BoundsError exogenous_inputs(batch, 1)
    @test_throws ArgumentError exogenous_inputs(batch, :not_existing)
    # Test for undefined exogenous outputs
    @test_throws ErrorException exogenous_outputs(batch)
    @test_throws ErrorException exogenous_outputs(batch, 1)
    @test_throws ErrorException exogenous_outputs(batch, :not_existing)
    # Test for non-existing subsystems
    @test isempty(subsystems(batch))
    @test_throws BoundsError subsystems(batch, 1)
    @test_throws ArgumentError subsystems(batch, :not_existing)
    # Test for all unknowns
    @test issetequal(
        string.(unknowns(batch)), 
        [
            "states₊S_S(t)", "states₊X_S(t)",
            "rates₊S_S(t)", "rates₊X_S(t)",
        ]
    )
    # Test particulate
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(batch))) .=> isparticulate.(unknowns(states(batch)))) == 
            Dict(["S_S", "X_S"] .=> [false, true])
    # Test default values
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(batch))) .=> Symbolics.getdefaultval.(unknowns(states(batch)))) == 
            Dict(["S_S", "X_S"] .=> [1, 2])
    # Test number of equations
    @test length(equations(batch)) == 2 # 2 ODE for components


    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), batch)
    @test String(take!(buf)) == "Batch Reactor 'batch':\nStates (2): see states(batch)\n  S_S(t) [defaults to 1.0]: S_S\n  X_S(t) [defaults to 2.0]: X_S"
    show(buf, MIME("text/plain"), batch; extended=true)
    @test String(take!(buf)) == "Batch Reactor 'batch':\nStates (2): see states(batch)\n  S_S(t) [defaults to 1.0]: S_S\n  X_S(t) [defaults to 2.0]: X_S\nRates (2): see rates(batch)\n  S_S(t) [guess is 0.0]: S_S reaction rate\n  X_S(t) [guess is 0.0]: X_S reaction rate\nEquations (2):\n  2 standard: see equations(batch)"
end

