@variables q(t) = 31.23 q2(t) [guess = 1] S_S(t) = 12.5 [particulate = false, colloidal = false, soluble = true] X_S(t) = 2 [particulate = true, colloidal = false, soluble = false]

# Test inflow
@testset "ProcessSimulator.InflowPort tests" begin
    # Test with dict input
    @named inflow = ProcessSimulator.InflowPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true),]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(inflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
            Dict(["q" => nothing, "S_S" => false, "X_S" => true])
    # Test with dict input and single state
    @named inflow = ProcessSimulator.InflowPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false),]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "X_S"])
    for unk in unknowns(inflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "X_S"] .=> [nothing, true])
    # Test with symbolic input
    @named inflow = ProcessSimulator.InflowPort([X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(inflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test with q input
    @named inflow = ProcessSimulator.InflowPort([q, X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(inflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test with q input and nothing as initial_concentrations and initial_flow
    @named inflow = ProcessSimulator.InflowPort([q, X_S, S_S], initial_flow = nothing, initial_concentrations=nothing, set_guess=false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> Symbolics.getdefaultval.(unknowns(inflow))) == 
			Dict(["q" => 31.23, "S_S" => 12.5, "X_S" => 2])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test with q input at different location
    @named inflow = ProcessSimulator.InflowPort([X_S, q, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(inflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test initial flow gues
    @named inflow = ProcessSimulator.InflowPort([X_S, S_S]; initial_flow = 35.6)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> Symbolics.getdefaultval.(unknowns(inflow), nothing)) == 
			Dict(["q" => nothing, "S_S" => nothing, "X_S" => nothing])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test initial concentrations with array
    @named inflow = ProcessSimulator.InflowPort([X_S, S_S]; initial_concentrations = [5, 3])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> Symbolics.getdefaultval.(unknowns(inflow), nothing)) == 
			Dict(["q" => nothing, "S_S" => nothing, "X_S" => nothing])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test initial concentrations with map
    @named inflow = ProcessSimulator.InflowPort([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]), set_guess=false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> Symbolics.getdefaultval.(unknowns(inflow), nothing)) == 
			Dict(["q" => 0, "S_S" => 5.25, "X_S" => 123.5])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(inflow)) .=> isparticulate.(unknowns(inflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test if ErrorException is thrown if multiple non-components are provided
    @test_throws ErrorException ProcessSimulator.InflowPort([q, q2, X_S, S_S]; initial_concentrations = [5, 3], name = :inflow)
end

# Test outflow
@testset "ProcessSimulator.OutflowPort tests" begin
    # Test with dict input
    @named outflow = ProcessSimulator.OutflowPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true),]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(outflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test with dict input and single state
    @named outflow = ProcessSimulator.OutflowPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false),]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "X_S"])
    for unk in unknowns(outflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "X_S"] .=> [nothing, true])
    # Test with symbolic input
    @named outflow = ProcessSimulator.OutflowPort([X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(outflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test with q input
    @named outflow = ProcessSimulator.OutflowPort([q, X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(outflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test with q input and nothing for initial_flow and initial_concentrations
    @named outflow = ProcessSimulator.OutflowPort([q, X_S, S_S], initial_flow = nothing, initial_concentrations = nothing, set_guess = false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> Symbolics.getdefaultval.(unknowns(outflow))) == 
			Dict(["q" => 31.23, "S_S" => 12.5, "X_S" => 2])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test with q input at different location
    @named outflow = ProcessSimulator.OutflowPort([X_S, q, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    for unk in unknowns(outflow)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test initial flow
    @named outflow = ProcessSimulator.OutflowPort([X_S, S_S]; initial_flow = 35.6)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> Symbolics.getdefaultval.(unknowns(outflow), nothing)) == 
			Dict(["q" => nothing, "S_S" => nothing, "X_S" => nothing])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test initial concentrations with array
    @named outflow = ProcessSimulator.OutflowPort([X_S, S_S]; initial_concentrations = [5, 3], set_guess = false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> Symbolics.getdefaultval.(unknowns(outflow), nothing)) == 
			Dict(["q" => 0, "S_S" => 3, "X_S" => 5])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test initial concentrations with map
    @named outflow = ProcessSimulator.OutflowPort([X_S, S_S]; initial_concentrations = Dict(["S_S" => 5.25, "X_S" => 123.5]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)), ["q", "S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> Symbolics.getdefaultval.(unknowns(outflow), nothing)) == 
			Dict(["q" => nothing, "S_S" => nothing, "X_S" => nothing])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(outflow)) .=> isparticulate.(unknowns(outflow))) == 
			Dict(["q", "S_S", "X_S"] .=> [nothing, false, true])
    # Test if ErrorException is thrown if multiple non-components are provided
    @test_throws ErrorException ProcessSimulator.OutflowPort([q, q2, X_S, S_S]; initial_concentrations = [5, 3], name = :outflow)
end

# Test ProcessSimulator.ReactionInputPort
@testset "ProcessSimulator.ReactionInputPort tests" begin
    # Test with array input
    @named reactionInput = ProcessSimulator.ReactionInputPort(["X_S", "S_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionInput)), ["S_S", "X_S"])
    for unk in unknowns(reactionInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    # Test with array input and single state
    @named reactionInput = ProcessSimulator.ReactionInputPort(["X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionInput)), ["X_S"])
    for unk in unknowns(reactionInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    # Test with symbolic input
    @named reactionInput = ProcessSimulator.ReactionInputPort([X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionInput)), ["S_S", "X_S"])
    for unk in unknowns(reactionInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    # Test initial concentrations with array
    @named reactionInput = ProcessSimulator.ReactionInputPort([X_S, S_S]; initial_rates = [5, 3], set_guess = false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionInput)), ["S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionInput)) .=> Symbolics.getdefaultval.(unknowns(reactionInput))) == 
			Dict(["S_S" => 3, "X_S" => 5])
    # Test initial concentrations with map
    @named reactionInput = ProcessSimulator.ReactionInputPort([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionInput)), ["S_S", "X_S"])
    for unk in unknowns(reactionInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
end

# Test ProcessSimulator.ReactionOutputPort
@testset "ProcessSimulator.ReactionOutputPort tests" begin
    # Test with array input
    @named reactionOutput = ProcessSimulator.ReactionOutputPort(["X_S", "S_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionOutput)), ["S_S", "X_S"])
    for unk in unknowns(reactionOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    # Test with array input and single state
    @named reactionOutput = ProcessSimulator.ReactionOutputPort(["X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionOutput)), ["X_S"])
    for unk in unknowns(reactionOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    # Test with symbolic input
    @named reactionOutput = ProcessSimulator.ReactionOutputPort([X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionOutput)), ["S_S", "X_S"])
    for unk in unknowns(reactionOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    # Test initial concentrations with array
    @named reactionOutput = ProcessSimulator.ReactionOutputPort([X_S, S_S]; initial_rates = [5, 3], set_guess = false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionOutput)), ["S_S", "X_S"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionOutput)) .=> Symbolics.getdefaultval.(unknowns(reactionOutput))) == 
			Dict(["S_S" => 3, "X_S" => 5])
    # Test initial concentrations with map
    @named reactionOutput = ProcessSimulator.ReactionOutputPort([X_S, S_S]; initial_rates = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(reactionOutput)), ["S_S", "X_S"])
    for unk in unknowns(reactionOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
end

# Test ProcessSimulator.StateInputPort
@testset "ProcessSimulator.StateInputPort tests" begin
    # Test with dict input
    @named stateInput = ProcessSimulator.StateInputPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true)]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)), ["S_S", "X_S"])
    for unk in unknowns(stateInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)) .=> isparticulate.(unknowns(stateInput))) == 
			Dict(["S_S", "X_S"] .=> [false, true])
    # Test with dict input and non-particulate state
    @named stateInput = ProcessSimulator.StateInputPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "non_part_state" => nothing, "S_S" => (particulate = false, colloidal = false, soluble = true)]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)), ["S_S", "non_part_state", "X_S"])
    for unk in unknowns(stateInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)) .=> isparticulate.(unknowns(stateInput))) == 
			Dict(["S_S", "non_part_state", "X_S"] .=> [false, nothing, true])
    # Test with dict input and single state
    @named stateInput = ProcessSimulator.StateInputPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false)]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)), ["X_S"])
    for unk in unknowns(stateInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test all(isparticulate.(unknowns(stateInput)) .== [true])
    # Test with symbolic input
    @named stateInput = ProcessSimulator.StateInputPort([X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)), ["S_S", "X_S"])
    for unk in unknowns(stateInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)) .=> isparticulate.(unknowns(stateInput))) == 
			Dict(["S_S", "X_S"] .=> [false, true])
    # Test initial concentrations with array
    @named stateInput = ProcessSimulator.StateInputPort([X_S, S_S, q]; initial_states = [5, 3, 23], set_guess = false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)), ["S_S", "X_S", "q"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)) .=> Symbolics.getdefaultval.(unknowns(stateInput))) == 
			Dict(["S_S" => 3, "X_S" => 5, "q" => 23])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)) .=> isparticulate.(unknowns(stateInput))) == 
			Dict(["S_S", "X_S", "q"] .=> [false, true, nothing])
    # Test initial concentrations with map
    @named stateInput = ProcessSimulator.StateInputPort([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)), ["S_S", "X_S"])
    for unk in unknowns(stateInput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateInput)) .=> isparticulate.(unknowns(stateInput))) == 
			Dict(["S_S", "X_S"] .=> [false, true])
end

# Test ProcessSimulator.StateOutputPort
@testset "ProcessSimulator.StateOutputPort tests" begin
    # Test with dict input
    @named stateOutput = ProcessSimulator.StateOutputPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true)]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)), ["S_S", "X_S"])
    for unk in unknowns(stateOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)) .=> isparticulate.(unknowns(stateOutput))) == 
			Dict(["S_S", "X_S"] .=> [false, true])
    # Test with dict input and non-particulate state
    @named stateOutput = ProcessSimulator.StateOutputPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false), "S_S" => (particulate = false, colloidal = false, soluble = true), "q" => nothing]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)), ["S_S", "X_S", "q"])
    for unk in unknowns(stateOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)) .=> isparticulate.(unknowns(stateOutput))) == 
			Dict(["S_S", "X_S", "q"] .=> [false, true, nothing])
    # Test with dict input and single state
    @named stateOutput = ProcessSimulator.StateOutputPort(Dict(["X_S" => (particulate = true, colloidal = false, soluble = false)]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)), ["X_S"])
    for unk in unknowns(stateOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test all(isparticulate.(unknowns(stateOutput)) .== [true])
    # Test with symbolic input
    @named stateOutput = ProcessSimulator.StateOutputPort([X_S, S_S])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)), ["S_S", "X_S"])
    for unk in unknowns(stateOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)) .=> isparticulate.(unknowns(stateOutput))) == 
			Dict(["S_S", "X_S"] .=> [false, true])
    # Test initial concentrations with array
    @named stateOutput = ProcessSimulator.StateOutputPort([X_S, S_S, q]; initial_states = [5, 3, 23], set_guess = false)
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)), ["S_S", "X_S", "q"])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)) .=> Symbolics.getdefaultval.(unknowns(stateOutput))) == 
			Dict(["S_S" => 3, "X_S" => 5, "q" => 23])
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)) .=> isparticulate.(unknowns(stateOutput))) == 
			Dict(["S_S", "X_S", "q"] .=> [false, true, nothing])
    # Test initial concentrations with map
    @named stateOutput = ProcessSimulator.StateOutputPort([X_S, S_S]; initial_states = Dict(["S_S" => 5.25, X_S => 123.5]))
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)), ["S_S", "X_S"])
    for unk in unknowns(stateOutput)
        @test_throws ErrorException Symbolics.getdefaultval.(unk)
    end
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(stateOutput)) .=> isparticulate.(unknowns(stateOutput))) == 
			Dict(["S_S", "X_S"] .=> [false, true])
end


