using BioChemicalTreatment
using Test, Documenter, Suppressor

@testset "BioChemicalTreatment.jl" begin

	@test (@capture_out BioChemicalTreatment.welcome()) == "Welcome to BioChemicalTreatment\n"

	@info "Testing ProcessSimulator Submodule:"
	@testset "ProcessSimulator" begin
		using BioChemicalTreatment
		using ModelingToolkit
		using ModelingToolkitStandardLibrary
		
		@info "	Variables"
		@testset "Variable tests" begin
			include("variables_tests.jl")
		end
		
		@info "	Port Metadata"
		@testset "Port Metadata tests" begin
			include("portmetadata_tests.jl")
		end
		
		@info "	Utils"
		@testset "Utils tests" begin
			include("utils_tests.jl")
		end
		
		@info "	Connectors"
		@testset "Connector tests" begin
			include("connectors_tests.jl")
		end

		@info "	FlowElements"
		@testset "Flowelements tests" begin
			include("flowelements_tests.jl")
		end
		
		@info "	Reactors"
		@testset "Reactor tests" begin
			include("reactors_test.jl")
		end
		
		@info "	Clarifier"
		@testset "Clarifier tests" begin
			include("clarifier_tests.jl")
		end
		
		@info "	Processes"
		@testset "Processes tests" begin
			include("processes_test.jl")
		end
		
		@info "	MTKStdLib compatibility"
		@testset "ModelingToolkitStandardLibrary compatibility tests" begin
			include("modelingtoolkitstandardlibrarycompat_tests.jl")
		end

		@info "	Defaults"
		@testset "Defaults tests" begin
			include("default_tests.jl")
		end

		@info "	ProcessDiagram"
		@testset "ProcessDiagram tests" begin
			include("processdiagram_tests.jl")
		end
	end

	@info "Testing Reactions Submodule"
	@testset "Reactions" begin
		using BioChemicalTreatment.Reactions
		using ModelingToolkit

		@info "	BioChemicalReactionSystem"
		@testset "BioChemicalReactionSystem Tests" begin
			include("biochemicalreactionsystem_test.jl")
		end
	end

	@info "Testing Integration"
	@testset "Integration Tests" begin
		using ModelingToolkit
		using DifferentialEquations

		include("integration_test.jl")
	end

	@info "Testing Docs"
	# Doctests
	DocMeta.setdocmeta!(BioChemicalTreatment, :DocTestSetup, :(using BioChemicalTreatment; using ModelingToolkit); recursive=true)
	doctestfilters = [
		r"(?<=[\.0-9]{6})[\.0-9]+", # Compare numbers only until the fifth significant digit
		r"(?<=\d)\.\d+(?=e-[5-9])", # Compare only leading digits for numbers smaller than 1e-4
		r"(?<=\d)\.\d+(?=e-\d+\d)", # Compare only leading digits for numbers smaller than 1e-10 (2+ digit exponents)
		r"(?<=\d)(\.0)?(?=[^\d])", # Compare integer independent if there is a .0 or not i.e. 1 = 1.0
		r"(?<=ODESystem\(0x)[\da-f]+(?=,)", # Ignore ODESystem tags
		r"(?<=Plot{)[^}]*n=\d*(?=})", # Ignore Plot number
		r"(?<=connect\()[^\)]*(?=\))", # Do not check connection contents (sometimes they are printed, sometimes not)
	]
	doctest(BioChemicalTreatment; manual = true, doctestfilters)

end
