@variables q(t) S_S(t) [particulate = false] X_S(t) [particulate = true]

@testset "Influent tests" begin
    @testset "Standard" begin
        @named influent = Influent(
            [1, ModelingToolkitStandardLibrary.Blocks.Constant(;k = 2, name=:X_S_val), sin],
            [q, X_S, S_S]
            )
        # Test if it has the right connectors
        @test !hasstates(influent)
        @test num_states(influent) == 0
        @test !hasrates(influent)
        @test num_rates(influent) == 0
        @test !hasinflows(influent)
        @test num_inflows(influent) == 0
        @test hasoutflows(influent)
        @test num_outflows(influent) == 1
        @test !hasexogenous_inputs(influent)
        @test num_exogenous_inputs(influent) == 0
        @test !hasexogenous_outputs(influent)
        @test num_exogenous_outputs(influent) == 0
        @test hassubsystems(influent)
        @test num_subsystems(influent) == 4
        # Test for name
        @test nameof(influent) == :influent
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(influent)) == :t
        # Test inflow
        @test isempty(inflows(influent))
        # Test outflow
        @test length(outflows(influent)) == 1
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent)[1])), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, 1))), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, :influent_to_outflowport₊outflow))), ["q", "S_S", "X_S"])
        @test_throws ArgumentError outflows(influent, :not_existing)
        # Test rates
        @test_throws ErrorException rates(influent)
        # Test states
        @test_throws ErrorException states(influent)
        # Test for undefined exogenous inputs
        @test isempty(exogenous_inputs(influent))
        # Test for undefined exogenous outputs
        @test isempty(exogenous_outputs(influent))
        # Test for subsystems
        @test issetequal(subsystem_names(influent), [:influent_to_outflowport, :q_const, :X_S_val, :S_S_fun])
        @test nameof(subsystems(influent, :q_const)) == Symbol("influent", ModelingToolkit.NAMESPACE_SEPARATOR, "q_const")
        @test nameof(subsystems(influent, 1)) in Symbol.("influent", ModelingToolkit.NAMESPACE_SEPARATOR, ["influent_to_outflowport", "q_const", "X_S_val", "S_S_fun"])
        # Test for all unknowns
        @test issetequal(
            string.(unknowns(influent)),
            [
                "influent_to_outflowport₊outflow₊q(t)", "influent_to_outflowport₊outflow₊S_S(t)", 
                "influent_to_outflowport₊outflow₊X_S(t)", "influent_to_outflowport₊q₊u(t)", 
                "influent_to_outflowport₊S_S₊u(t)", "influent_to_outflowport₊X_S₊u(t)",
                "q_const₊output₊u(t)", "S_S_fun₊output₊u(t)", "X_S_val₊output₊u(t)",
            ]
        )
        @test length(equations(influent)) == 3 # 3 (flow + 2 components) connecting Constants to RealInputs 

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), influent)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
        show(buf, MIME("text/plain"), influent; extended=true)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nSubsystems (4): see hierarchy(influent)\n  :influent_to_outflowport\n  :q_const\n  :S_S_fun\n  :X_S_val\nEquations (3):\n  3 connecting: see equations(expand_connections(influent))\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
    end

    # Test with numeric default
    @testset "With numeric default" begin
        @named influent = Influent(
            [ModelingToolkitStandardLibrary.Blocks.Constant(;k = 2, name=:X_S_val), sin],
            [X_S, S_S],
            default = 1
            )
        # Test if it has the right connectors
        @test !hasstates(influent)
        @test num_states(influent) == 0
        @test !hasrates(influent)
        @test num_rates(influent) == 0
        @test !hasinflows(influent)
        @test num_inflows(influent) == 0
        @test hasoutflows(influent)
        @test num_outflows(influent) == 1
        @test !hasexogenous_inputs(influent)
        @test num_exogenous_inputs(influent) == 0
        @test !hasexogenous_outputs(influent)
        @test num_exogenous_outputs(influent) == 0
        @test hassubsystems(influent)
        @test num_subsystems(influent) == 4
        # Test for name
        @test nameof(influent) == :influent
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(influent)) == :t
        # Test inflow
        @test isempty(inflows(influent))
        # Test outflow
        @test length(outflows(influent)) == 1
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent)[1])), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, 1))), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, :influent_to_outflowport₊outflow))), ["q", "S_S", "X_S"])
        @test_throws ArgumentError outflows(influent, :not_existing)
        # Test rates
        @test_throws ErrorException rates(influent)
        # Test states
        @test_throws ErrorException states(influent)
        # Test for undefined exogenous inputs
        @test isempty(exogenous_inputs(influent))
        # Test for undefined exogenous outputs
        @test isempty(exogenous_outputs(influent))
        # Test for subsystems
        @test issetequal(subsystem_names(influent), [:influent_to_outflowport, :q_const, :X_S_val, :S_S_fun])
        @test nameof(subsystems(influent, :q_const)) == Symbol("influent", ModelingToolkit.NAMESPACE_SEPARATOR, "q_const")
        @test nameof(subsystems(influent, 1)) in Symbol.("influent", ModelingToolkit.NAMESPACE_SEPARATOR, ["influent_to_outflowport", "q_const", "X_S_val", "S_S_fun"])
        # Test for all unknowns
        @test issetequal(
            string.(unknowns(influent)),
            [
                "influent_to_outflowport₊outflow₊q(t)", "influent_to_outflowport₊outflow₊S_S(t)", 
                "influent_to_outflowport₊outflow₊X_S(t)", "influent_to_outflowport₊q₊u(t)", 
                "influent_to_outflowport₊S_S₊u(t)", "influent_to_outflowport₊X_S₊u(t)",
                "q_const₊output₊u(t)", "S_S_fun₊output₊u(t)", "X_S_val₊output₊u(t)",
            ]
        )
        @test length(equations(influent)) == 3 # 3 (flow + 2 components) connecting Constants to RealInputs 

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), influent)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
        show(buf, MIME("text/plain"), influent; extended=true)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nSubsystems (4): see hierarchy(influent)\n  :influent_to_outflowport\n  :q_const\n  :S_S_fun\n  :X_S_val\nEquations (3):\n  3 connecting: see equations(expand_connections(influent))\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
    end

    @testset "With stdlib default" begin
        @named influent = Influent(
            ["q" => 1, "S_S" => sin],
            [q, X_S, S_S],
            default = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 2, name=:X_S_val)
            )
        # Test if it has the right connectors
        @test !hasstates(influent)
        @test num_states(influent) == 0
        @test !hasrates(influent)
        @test num_rates(influent) == 0
        @test !hasinflows(influent)
        @test num_inflows(influent) == 0
        @test hasoutflows(influent)
        @test num_outflows(influent) == 1
        @test !hasexogenous_inputs(influent)
        @test num_exogenous_inputs(influent) == 0
        @test !hasexogenous_outputs(influent)
        @test num_exogenous_outputs(influent) == 0
        @test hassubsystems(influent)
        @test num_subsystems(influent) == 4
        # Test for name
        @test nameof(influent) == :influent
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(influent)) == :t
        # Test inflow
        @test isempty(inflows(influent))
        # Test outflow
        @test length(outflows(influent)) == 1
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent)[1])), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, 1))), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, :influent_to_outflowport₊outflow))), ["q", "S_S", "X_S"])
        @test_throws ArgumentError outflows(influent, :not_existing)
        # Test rates
        @test_throws ErrorException rates(influent)
        # Test states
        @test_throws ErrorException states(influent)
        # Test for undefined exogenous inputs
        @test isempty(exogenous_inputs(influent))
        # Test for undefined exogenous outputs
        @test isempty(exogenous_outputs(influent))
        # Test for subsystems
        @test issetequal(subsystem_names(influent), [:influent_to_outflowport, :q_const, :X_S_val, :S_S_fun])
        @test nameof(subsystems(influent, :q_const)) == Symbol("influent", ModelingToolkit.NAMESPACE_SEPARATOR, "q_const")
        @test nameof(subsystems(influent, 1)) in Symbol.("influent", ModelingToolkit.NAMESPACE_SEPARATOR, ["influent_to_outflowport", "q_const", "X_S_val", "S_S_fun"])
        # Test for all unknowns
        @test issetequal(
            string.(unknowns(influent)),
            [
                "influent_to_outflowport₊outflow₊q(t)", "influent_to_outflowport₊outflow₊S_S(t)", 
                "influent_to_outflowport₊outflow₊X_S(t)", "influent_to_outflowport₊q₊u(t)", 
                "influent_to_outflowport₊S_S₊u(t)", "influent_to_outflowport₊X_S₊u(t)",
                "q_const₊output₊u(t)", "S_S_fun₊output₊u(t)", "X_S_val₊output₊u(t)",
            ]
        )
        @test length(equations(influent)) == 3 # 3 (flow + 2 components) connecting Constants to RealInputs 

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), influent)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
        show(buf, MIME("text/plain"), influent; extended=true)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nSubsystems (4): see hierarchy(influent)\n  :influent_to_outflowport\n  :q_const\n  :S_S_fun\n  :X_S_val\nEquations (3):\n  3 connecting: see equations(expand_connections(influent))\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
    end

    @testset "With function default" begin
        @named influent = Influent(
            ["q" => 1, "X_S" => ModelingToolkitStandardLibrary.Blocks.Constant(;k = 2, name=:X_S_val)],
            [q, X_S, S_S],
            default = sin
            )
        # Test if it has the right connectors
        @test !hasstates(influent)
        @test num_states(influent) == 0
        @test !hasrates(influent)
        @test num_rates(influent) == 0
        @test !hasinflows(influent)
        @test num_inflows(influent) == 0
        @test hasoutflows(influent)
        @test num_outflows(influent) == 1
        @test !hasexogenous_inputs(influent)
        @test num_exogenous_inputs(influent) == 0
        @test !hasexogenous_outputs(influent)
        @test num_exogenous_outputs(influent) == 0
        @test hassubsystems(influent)
        @test num_subsystems(influent) == 4
        # Test for name
        @test nameof(influent) == :influent
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(influent)) == :t
        # Test inflow
        @test isempty(inflows(influent))
        # Test outflow
        @test length(outflows(influent)) == 1
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent)[1])), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, 1))), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, :influent_to_outflowport₊outflow))), ["q", "S_S", "X_S"])
        @test_throws ArgumentError outflows(influent, :not_existing)
        # Test rates
        @test_throws ErrorException rates(influent)
        # Test states
        @test_throws ErrorException states(influent)
        # Test for undefined exogenous inputs
        @test isempty(exogenous_inputs(influent))
        # Test for undefined exogenous outputs
        @test isempty(exogenous_outputs(influent))
        # Test for subsystems
        @test issetequal(subsystem_names(influent), [:influent_to_outflowport, :q_const, :X_S_val, :S_S_fun])
        @test nameof(subsystems(influent, :q_const)) == Symbol("influent", ModelingToolkit.NAMESPACE_SEPARATOR, "q_const")
        @test nameof(subsystems(influent, 1)) in Symbol.("influent", ModelingToolkit.NAMESPACE_SEPARATOR, ["influent_to_outflowport", "q_const", "X_S_val", "S_S_fun"])
        # Test for all unknowns
        @test issetequal(
            string.(unknowns(influent)),
            [
                "influent_to_outflowport₊outflow₊q(t)", "influent_to_outflowport₊outflow₊S_S(t)", 
                "influent_to_outflowport₊outflow₊X_S(t)", "influent_to_outflowport₊q₊u(t)", 
                "influent_to_outflowport₊S_S₊u(t)", "influent_to_outflowport₊X_S₊u(t)",
                "q_const₊output₊u(t)", "S_S_fun₊output₊u(t)", "X_S_val₊output₊u(t)",
            ]
        )
        @test length(equations(influent)) == 3 # 3 (flow + 2 components) connecting Constants to RealInputs 

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), influent)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
        show(buf, MIME("text/plain"), influent; extended=true)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nSubsystems (4): see hierarchy(influent)\n  :influent_to_outflowport\n  :q_const\n  :S_S_fun\n  :X_S_val\nEquations (3):\n  3 connecting: see equations(expand_connections(influent))\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
    end

    @testset "With kwarg flowrate" begin
        @named influent = Influent(
            ["X_S" => ModelingToolkitStandardLibrary.Blocks.Constant(;k = 2, name=:X_S_val)],
            [q, X_S, S_S],
            flowrate = 1,
            default = sin
            )
        # Test if it has the right connectors
        @test !hasstates(influent)
        @test num_states(influent) == 0
        @test !hasrates(influent)
        @test num_rates(influent) == 0
        @test !hasinflows(influent)
        @test num_inflows(influent) == 0
        @test hasoutflows(influent)
        @test num_outflows(influent) == 1
        @test !hasexogenous_inputs(influent)
        @test num_exogenous_inputs(influent) == 0
        @test !hasexogenous_outputs(influent)
        @test num_exogenous_outputs(influent) == 0
        @test hassubsystems(influent)
        @test num_subsystems(influent) == 4
        # Test for name
        @test nameof(influent) == :influent
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(influent)) == :t
        # Test inflow
        @test isempty(inflows(influent))
        # Test outflow
        @test length(outflows(influent)) == 1
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent)[1])), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, 1))), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, :influent_to_outflowport₊outflow))), ["q", "S_S", "X_S"])
        @test_throws ArgumentError outflows(influent, :not_existing)
        # Test rates
        @test_throws ErrorException rates(influent)
        # Test states
        @test_throws ErrorException states(influent)
        # Test for undefined exogenous inputs
        @test isempty(exogenous_inputs(influent))
        # Test for undefined exogenous outputs
        @test isempty(exogenous_outputs(influent))
        # Test for subsystems
        @test issetequal(subsystem_names(influent), [:influent_to_outflowport, :q_const, :X_S_val, :S_S_fun])
        @test nameof(subsystems(influent, :q_const)) == Symbol("influent", ModelingToolkit.NAMESPACE_SEPARATOR, "q_const")
        @test nameof(subsystems(influent, 1)) in Symbol.("influent", ModelingToolkit.NAMESPACE_SEPARATOR, ["influent_to_outflowport", "q_const", "X_S_val", "S_S_fun"])
        # Test for all unknowns
        @test issetequal(
            string.(unknowns(influent)),
            [
                "influent_to_outflowport₊outflow₊q(t)", "influent_to_outflowport₊outflow₊S_S(t)", 
                "influent_to_outflowport₊outflow₊X_S(t)", "influent_to_outflowport₊q₊u(t)", 
                "influent_to_outflowport₊S_S₊u(t)", "influent_to_outflowport₊X_S₊u(t)",
                "q_const₊output₊u(t)", "S_S_fun₊output₊u(t)", "X_S_val₊output₊u(t)",
            ]
        )
        @test length(equations(influent)) == 3 # 3 (flow + 2 components) connecting Constants to RealInputs 

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), influent)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
        show(buf, MIME("text/plain"), influent; extended=true)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nSubsystems (4): see hierarchy(influent)\n  :influent_to_outflowport\n  :q_const\n  :S_S_fun\n  :X_S_val\nEquations (3):\n  3 connecting: see equations(expand_connections(influent))\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
    end

    @testset "kwarg flowrate precedence" begin
        @named influent = Influent(
            ["X_S" => ModelingToolkitStandardLibrary.Blocks.Constant(;k = 2, name=:X_S_val), q => 421],
            [q, X_S, S_S],
            flowrate = 1,
            default = sin
            )
        # Test if it has the right connectors
        @test !hasstates(influent)
        @test num_states(influent) == 0
        @test !hasrates(influent)
        @test num_rates(influent) == 0
        @test !hasinflows(influent)
        @test num_inflows(influent) == 0
        @test hasoutflows(influent)
        @test num_outflows(influent) == 1
        @test !hasexogenous_inputs(influent)
        @test num_exogenous_inputs(influent) == 0
        @test !hasexogenous_outputs(influent)
        @test num_exogenous_outputs(influent) == 0
        @test hassubsystems(influent)
        @test num_subsystems(influent) == 4
        # Test for name
        @test nameof(influent) == :influent
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(influent)) == :t
        # Test inflow
        @test isempty(inflows(influent))
        # Test outflow
        @test length(outflows(influent)) == 1
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent)[1])), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, 1))), ["q", "S_S", "X_S"])
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(influent, :influent_to_outflowport₊outflow))), ["q", "S_S", "X_S"])
        @test_throws ArgumentError outflows(influent, :not_existing)
        # Test rates
        @test_throws ErrorException rates(influent)
        # Test states
        @test_throws ErrorException states(influent)
        # Test for undefined exogenous inputs
        @test isempty(exogenous_inputs(influent))
        # Test for undefined exogenous outputs
        @test isempty(exogenous_outputs(influent))
        # Test for subsystems
        @test issetequal(subsystem_names(influent), [:influent_to_outflowport, :q_const, :X_S_val, :S_S_fun])
        @test nameof(subsystems(influent, :q_const)) == Symbol("influent", ModelingToolkit.NAMESPACE_SEPARATOR, "q_const")
        @test nameof(subsystems(influent, 1)) in Symbol.("influent", ModelingToolkit.NAMESPACE_SEPARATOR, ["influent_to_outflowport", "q_const", "X_S_val", "S_S_fun"])
        # Test for all unknowns
        @test issetequal(
            string.(unknowns(influent)),
            [
                "influent_to_outflowport₊outflow₊q(t)", "influent_to_outflowport₊outflow₊S_S(t)", 
                "influent_to_outflowport₊outflow₊X_S(t)", "influent_to_outflowport₊q₊u(t)", 
                "influent_to_outflowport₊S_S₊u(t)", "influent_to_outflowport₊X_S₊u(t)",
                "q_const₊output₊u(t)", "S_S_fun₊output₊u(t)", "X_S_val₊output₊u(t)",
            ]
        )
        @test length(equations(influent)) == 3 # 3 (flow + 2 components) connecting Constants to RealInputs
        # Test if kwarg superseeds state for flowrate
        @test Symbolics.getdefaultval(convert(ODESystem, influent).q_const.k) == 1

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), influent)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
        show(buf, MIME("text/plain"), influent; extended=true)
        @test String(take!(buf)) == "Influent 'influent':\nOutflows (1): see outflows(influent)\n  :influent_to_outflowport₊outflow\nSubsystems (4): see hierarchy(influent)\n  :influent_to_outflowport\n  :q_const\n  :S_S_fun\n  :X_S_val\nEquations (3):\n  3 connecting: see equations(expand_connections(influent))\nParameters (2): see parameters(influent)\n  q_const₊k [defaults to 1.0]: Constant output value of block\n  X_S_val₊k [defaults to 2]: Constant output value of block"
    end

    # Test non matching lengths of states and values
    @test_throws ArgumentError Influent([1, 3.36, sin], [X_S, S_S]; name=:test)
    @test_throws ArgumentError Influent([3.36, sin], [q, X_S, S_S]; name=:test)
    # Test forgetting q
    @test_throws ArgumentError Influent([3.36, sin], [X_S, S_S]; name=:test)
    # Test invalid function
    @test_throws ArgumentError Influent([(a, b) -> a*b, 3.36, sin], [X_S, q, S_S]; name=:test)
    # Test invalid block
    @test_throws ArgumentError Influent([ModelingToolkitStandardLibrary.Blocks.RealInput(name=:hi), 3.36, sin], [X_S, q, S_S]; name=:test)
end

@testset "Sensor tests" begin
    @named sensor = Sensor([q, X_S], [X_S, S_S])
    # Test if it has the right connectors
    @test !hasstates(sensor)
    @test num_states(sensor) == 0
    @test !hasrates(sensor)
    @test num_rates(sensor) == 0
    @test hasinflows(sensor)
    @test num_inflows(sensor) == 1
    @test hasoutflows(sensor)
    @test num_outflows(sensor) == 1
    @test !hasexogenous_inputs(sensor)
    @test num_exogenous_inputs(sensor) == 0
    @test hasexogenous_outputs(sensor)
    @test num_exogenous_outputs(sensor) == 2
    # Test for name
    @test nameof(sensor) == :sensor
    # Test for getting iv
    @test nameof(ModelingToolkit.get_iv(sensor)) == :t
    # Test inflow
    @test length(inflows(sensor)) == 1
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(sensor)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(sensor, 1))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(sensor, :inflow))), ["q", "S_S", "X_S"])
    @test_throws ArgumentError inflows(sensor, :not_existing)
    # Test outflow
    @test length(outflows(sensor)) == 1
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(sensor)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(sensor, 1))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(sensor, :outflow))), ["q", "S_S", "X_S"])
    @test_throws ArgumentError outflows(sensor, :not_existing)
    # Test rates
    @test_throws ErrorException rates(sensor)
    # Test states
    @test_throws ErrorException states(sensor)
    # Test for undefined exogenous inputs
    @test isempty(exogenous_inputs(sensor))
    # Test for undefined exogenous outputs
    @test length(exogenous_outputs(sensor)) == 2
    @test issetequal(exogenous_output_names(sensor), [:q, :X_S])
    @test nameof(exogenous_outputs(sensor, :q)) == :sensor₊q
    @test_throws ArgumentError exogenous_outputs(sensor, :S_S)
    # Test for all unknowns
    @test issetequal(
        string.(unknowns(sensor)),
        [
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)",
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "q₊u(t)", "X_S₊u(t)",
        ]
    )
    @test length(equations(sensor)) == 3 # 3 (flow + 2 sensors)

    # Test sensing invalid states
    @test_throws ArgumentError Sensor(:S_S, [X_S]; name=:test)
    @test_throws ArgumentError Sensor([:q, :S_S], [q, X_S]; name=:test)

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), sensor)
    @test String(take!(buf)) == "Sensor for q and X_S 'sensor':\nInflows (1): see inflows(sensor)\n  :inflow\nOutflows (1): see outflows(sensor)\n  :outflow\nExogenous Outputs (2): see exogenous_outputs(sensor)\n  :q\n  :X_S"
    show(buf, MIME("text/plain"), sensor; extended=true)
    @test String(take!(buf)) == "Sensor for q and X_S 'sensor':\nInflows (1): see inflows(sensor)\n  :inflow\nOutflows (1): see outflows(sensor)\n  :outflow\nExogenous Outputs (2): see exogenous_outputs(sensor)\n  :q\n  :X_S\nEquations (5):\n  2 standard: see equations(sensor)\n  3 connecting: see equations(expand_connections(sensor))"
end

@testset "FlowConnector tests" begin
    @named connector = FlowConnector([S_S, X_S]; n_in = 2, n_out = 2)
    # Test if it has the right connectors
    @test !hasstates(connector)
    @test num_states(connector) == 0
    @test !hasrates(connector)
    @test num_rates(connector) == 0
    @test hasinflows(connector)
    @test num_inflows(connector) == 2
    @test hasoutflows(connector)
    @test num_outflows(connector) == 2
    @test !hasexogenous_inputs(connector)
    @test num_exogenous_inputs(connector) == 0
    @test !hasexogenous_outputs(connector)
    @test num_exogenous_outputs(connector) == 0
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(connector)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(connector)[2])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(connector)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(connector)[2])), ["q", "S_S", "X_S"])
    @test issetequal(
        string.(unknowns(convert(ODESystem, connector))),
        [
            "inflow_1₊q(t)", "inflow_1₊S_S(t)", "inflow_1₊X_S(t)",
            "inflow_2₊q(t)", "inflow_2₊S_S(t)", "inflow_2₊X_S(t)",
            "outflow_1₊q(t)", "outflow_1₊S_S(t)", "outflow_1₊X_S(t)",
            "outflow_2₊q(t)", "outflow_2₊S_S(t)", "outflow_2₊X_S(t)",
        ]
    )
    @test length(equations(connector)) == (1 + 2 + 2) # MassConservation Flow + 2*MassConservation Components + 2*Outflow Components equal

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), connector)
    @test String(take!(buf)) == "FlowConnector 'connector':\nInflows (2): see inflows(connector)\n  :inflow_1\n  :inflow_2\nOutflows (2): see outflows(connector)\n  :outflow_1\n  :outflow_2"
    show(buf, MIME("text/plain"), connector; extended=true)
    @test String(take!(buf)) == "FlowConnector 'connector':\nInflows (2): see inflows(connector)\n  :inflow_1\n  :inflow_2\nOutflows (2): see outflows(connector)\n  :outflow_1\n  :outflow_2\nEquations (5):\n  5 standard: see equations(connector)"
end

@testset "FlowUnifier tests" begin
    @named unifier = FlowUnifier([S_S, X_S]; n_in = 2)
    # Test if it has the right connectors
    @test !hasstates(unifier)
    @test num_states(unifier) == 0
    @test !hasrates(unifier)
    @test num_rates(unifier) == 0
    @test hasinflows(unifier)
    @test num_inflows(unifier) == 2
    @test hasoutflows(unifier)
    @test num_outflows(unifier) == 1
    @test !hasexogenous_inputs(unifier)
    @test num_exogenous_inputs(unifier) == 0
    @test !hasexogenous_outputs(unifier)
    @test num_exogenous_outputs(unifier) == 0
    @test length(inflows(unifier)) == 2
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(unifier, 1))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(unifier, :inflow_1))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(unifier)[2])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(unifier, :inflow_2))), ["q", "S_S", "X_S"])
    @test length(outflows(unifier)) == 1
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(unifier)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(unifier, :outflow))), ["q", "S_S", "X_S"])
    @test isempty(exogenous_inputs(unifier))
    @test isempty(exogenous_outputs(unifier))
    @test issetequal(
        string.(unknowns(convert(ODESystem, unifier))), 
        [
            "inflow_1₊q(t)", "inflow_1₊S_S(t)", "inflow_1₊X_S(t)",
            "inflow_2₊q(t)", "inflow_2₊S_S(t)", "inflow_2₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)"
        ]
    )
    @test length(equations(unifier)) == (1 + 2) # MassConservation Flow + 2*MassConservation Components

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), unifier)
    @test String(take!(buf)) == "FlowUnifier 'unifier':\nInflows (2): see inflows(unifier)\n  :inflow_1\n  :inflow_2\nOutflows (1): see outflows(unifier)\n  :outflow"
    show(buf, MIME("text/plain"), unifier; extended=true)
    @test String(take!(buf)) == "FlowUnifier 'unifier':\nInflows (2): see inflows(unifier)\n  :inflow_1\n  :inflow_2\nOutflows (1): see outflows(unifier)\n  :outflow\nEquations (3):\n  3 standard: see equations(unifier)"
end

@testset "FlowSeparator tests" begin
    @named separator = FlowSeparator([S_S, X_S]; n_out = 2)
    # Test if it has the right connectors
    @test !hasstates(separator)
    @test num_states(separator) == 0
    @test !hasrates(separator)
    @test num_rates(separator) == 0
    @test hasinflows(separator)
    @test num_inflows(separator) == 1
    @test hasoutflows(separator)
    @test num_outflows(separator) == 2
    @test !hasexogenous_inputs(separator)
    @test num_exogenous_inputs(separator) == 0
    @test !hasexogenous_outputs(separator)
    @test num_exogenous_outputs(separator) == 0
    @test length(inflows(separator)) == 1
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(separator)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(separator, :inflow))), ["q", "S_S", "X_S"])
    @test length(outflows(separator)) == 2
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(separator)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(separator, :outflow_1))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(separator)[2])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(separator, :outflow_2))), ["q", "S_S", "X_S"])
    @test isempty(exogenous_inputs(separator))
    @test isempty(exogenous_outputs(separator))
    @test issetequal(
        string.(unknowns(convert(ODESystem, separator))), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow_1₊q(t)", "outflow_1₊S_S(t)", "outflow_1₊X_S(t)",
            "outflow_2₊q(t)", "outflow_2₊S_S(t)", "outflow_2₊X_S(t)",
        ]
    )
    @test length(equations(separator)) == (1 + 2 + 2) # MassConservation Flow + 2*MassConservation Components + 2*Outflow Components equal

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), separator)
    @test String(take!(buf)) == "FlowSeparator 'separator':\nInflows (1): see inflows(separator)\n  :inflow\nOutflows (2): see outflows(separator)\n  :outflow_1\n  :outflow_2"
    show(buf, MIME("text/plain"), separator; extended=true)
    @test String(take!(buf)) == "FlowSeparator 'separator':\nInflows (1): see inflows(separator)\n  :inflow\nOutflows (2): see outflows(separator)\n  :outflow_1\n  :outflow_2\nEquations (5):\n  5 standard: see equations(separator)"
end

@testset "FlowPump tests" begin
    # Test with given flowrate
    @named pump = FlowPump([S_S, X_S]; flowrate=1)
    # Test if it has the right connectors
    @test !hasstates(pump)
    @test num_states(pump) == 0
    @test !hasrates(pump)
    @test num_rates(pump) == 0
    @test hasinflows(pump)
    @test num_inflows(pump) == 1
    @test hasoutflows(pump)
    @test num_outflows(pump) == 1
    @test !hasexogenous_inputs(pump)
    @test num_exogenous_inputs(pump) == 0
    @test !hasexogenous_outputs(pump)
    @test num_exogenous_outputs(pump) == 0
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(pump)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(pump, :inflow))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(pump)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(pump, :outflow))), ["q", "S_S", "X_S"])
    @test isempty(exogenous_inputs(pump))
    @test isempty(exogenous_outputs(pump))
    @test issetequal(
        string.(unknowns(convert(ODESystem, pump))), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)",
        ]
    )
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(pump)), ["q_pumped"])
    @test length(equations(pump)) == (1 + 1) # Connect inflow and outflow + Pumprate
    # Test with flowrate port
    @named pump = FlowPump([S_S, X_S])
    # Test if it has the right connectors
    @test !hasstates(pump)
    @test num_states(pump) == 0
    @test !hasrates(pump)
    @test num_rates(pump) == 0
    @test hasinflows(pump)
    @test num_inflows(pump) == 1
    @test hasoutflows(pump)
    @test num_outflows(pump) == 1
    @test hasexogenous_inputs(pump)
    @test num_exogenous_inputs(pump) == 1
    @test !hasexogenous_outputs(pump)
    @test num_exogenous_outputs(pump) == 0
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(pump)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(inflows(pump, :inflow))), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(pump)[1])), ["q", "S_S", "X_S"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(outflows(pump, :outflow))), ["q", "S_S", "X_S"])
    @test length(exogenous_inputs(pump)) == 1
    @test isempty(exogenous_outputs(pump))
    @test issetequal(
        string.(unknowns(convert(ODESystem, pump))), 
        [
            "inflow₊q(t)", "inflow₊S_S(t)", "inflow₊X_S(t)",
            "outflow₊q(t)", "outflow₊S_S(t)", "outflow₊X_S(t)",
            "q_pumped₊u(t)"
        ]
    )
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(pump)), [])
    @test length(equations(pump)) == (1 + 1) # Connect inflow and outflow + Pumprate

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), pump)
    @test String(take!(buf)) == "Flow Pump 'pump':\nInflows (1): see inflows(pump)\n  :inflow\nOutflows (1): see outflows(pump)\n  :outflow\nExogenous Inputs (1): see exogenous_inputs(pump)\n  :q_pumped"
    show(buf, MIME("text/plain"), pump; extended=true)
    @test String(take!(buf)) == "Flow Pump 'pump':\nInflows (1): see inflows(pump)\n  :inflow\nOutflows (1): see outflows(pump)\n  :outflow\nExogenous Inputs (1): see exogenous_inputs(pump)\n  :q_pumped\nEquations (4):\n  1 standard: see equations(pump)\n  3 connecting: see equations(expand_connections(pump))"
end
