@testset "Ozonation CSTRs" begin
    function build_ozonation_cascade(n_react, t_end=50, q=10000/24, v=1000/n_react, k_o3=10/24, k_d=1500/24)
        p = Process[OzoneDisinfection(; kO3=0, kd=k_d, name=Symbol("Disin1"))]
        r = Reactor[CSTR(v, unknowns(states(p[end])), initial_states=[0.5, 0], name=Symbol("React1"))]
        r_eq = Equation[
            connect(states(p[end]), states(r[end])),
            connect(rates(p[end]), rates(r[end]))
            ]
        for i in 2:n_react
            push!(p, OzoneDisinfection(; kO3=k_o3, kd=k_d, name=Symbol("Disin", i)))
            push!(r, CSTR(v, unknowns(states(p[end])), initial_states=[0.5, 0], name=Symbol("React", i)))
            append!(r_eq, [
                connect(states(p[end]), states(r[end])),
                connect(rates(p[end]), rates(r[end]))
            ])
        end
        @named inf = Influent([0.5, 1.0], unknowns(states(p[1])), flowrate=q)

        # Connect
        connect_eqs = [
            connect(outflows(inf, 1), inflows(r[1], 1)),
            connect.(outflows.(r[1:end-1], Ref(1)), inflows.(r[2:end], Ref(1)))...
        ]

        # Build System
        @named sys = ODESystem([connect_eqs..., r_eq...], t, systems=[inf, r..., p...])
        sys_simp = structural_simplify(sys)
        prob = ODEProblem(sys_simp, [], (0, t_end))
        solve(prob, save_on=false, save_start=false, abstol=1e-6, reltol=1e-3).u[1]
    end
    
    tol = 1e-2
    @test isapprox(build_ozonation_cascade(2)[end-1:end], [0.333, 9.99e-4], rtol=tol)
    @test isapprox(build_ozonation_cascade(4)[end-1:end], [0.256, 2.30e-5], rtol=tol)
    @test isapprox(build_ozonation_cascade(10)[end-1:end], [0.212, 1.91e-8], rtol=tol)
    @test isapprox(build_ozonation_cascade(20)[end-1:end], [0.198, 2.54e-11], rtol=tol)
    @test isapprox(build_ozonation_cascade(30)[end-1:end], [0.193, 4.56e-13], rtol=tol)
end

@testset "Ozonation CSTRs with defaults" begin
    function build_ozonation_cascade(n_react, t_end=50, q=10000/24, v=1000/n_react, k_o3=10/24, k_d=1500/24)
        set_default_process(OzoneDisinfection; kO3=k_o3, kd=k_d, name=:disin)
        clear_default_state_mapping()

        r = Reactor[CSTR(v, OzoneDisinfection(; kO3=0, kd=k_d, name=:react1_disin), initial_states=[0.5, 0], name=Symbol("React1"), use_default_processes=false)]
        append!(r, [CSTR(v, initial_states=[0.5, 0], name=Symbol("React", i)) for i in 2:n_react])
        @named inf = Influent([0.5, 1.0], flowrate=q)

        # Connect
        connect_eqs = [
            connect(outflows(inf, 1), inflows(r[1], 1)),
            connect.(outflows.(r[1:end-1], Ref(1)), inflows.(r[2:end], Ref(1)))...
        ]

        # Build System
        @named sys = ODESystem(connect_eqs, t, systems=[inf, r...])
        sys_simp = structural_simplify(sys)
        prob = ODEProblem(sys_simp, [], (0, t_end))
        solve(prob, save_on=false, save_start=false, abstol=1e-6, reltol=1e-3).u[1]
    end
    
    tol = 1e-2
    @test isapprox(build_ozonation_cascade(2)[end-1:end], [0.333, 9.99e-4], rtol=tol)
    @test isapprox(build_ozonation_cascade(4)[end-1:end], [0.256, 2.30e-5], rtol=tol) 
    @test isapprox(build_ozonation_cascade(10)[end-1:end], [0.212, 1.91e-8], rtol=tol) 
    @test isapprox(build_ozonation_cascade(20)[end-1:end], [0.198, 2.54e-11], rtol=tol) 
    @test isapprox(build_ozonation_cascade(30)[end-1:end], [0.193, 4.56e-13], rtol=tol)
end

@testset "Ozonation CSTRs with defaults and renaming var" begin
    function build_ozonation_cascade(n_react, t_end=50, q=10000/24, v=1000/n_react, k_o3=10/24, k_d=1500/24)
        set_default_process(OzoneDisinfection; kO3=k_o3, kd=k_d, name=:disin)
        set_default_state_mapping(;D = (:S_O3, false, false, true))

        r = Reactor[CSTR(v, OzoneDisinfection(; kO3=0, kd=k_d, name=:react1_disin), initial_states=[0.5, 0], name=Symbol("React1"), use_default_processes=false)]
        append!(r, [CSTR(v, initial_states=[0.5, 0], name=Symbol("React", i)) for i in 2:n_react])
        @named inf = Influent([0.5, 1.0], flowrate=q)

        # Connect
        connect_eqs = [
            connect(outflows(inf, 1), inflows(r[1], 1)),
            connect.(outflows.(r[1:end-1], Ref(1)), inflows.(r[2:end], Ref(1)))...
        ]

        # Build System
        @named sys = ODESystem(connect_eqs, t, systems=[inf, r...])
        sys_simp = structural_simplify(sys)
        prob = ODEProblem(sys_simp, [], (0, t_end))
        solve(prob, save_on=false, save_start=false, abstol=1e-6, reltol=1e-3).u[1]
    end
    
    tol = 1e-2
    @test isapprox(build_ozonation_cascade(2)[end-1:end], [0.333, 9.99e-4], rtol=tol)
    @test isapprox(build_ozonation_cascade(4)[end-1:end], [0.256, 2.30e-5], rtol=tol) 
    @test isapprox(build_ozonation_cascade(10)[end-1:end], [0.212, 1.91e-8], rtol=tol) 
    @test isapprox(build_ozonation_cascade(20)[end-1:end], [0.198, 2.54e-11], rtol=tol) 
    @test isapprox(build_ozonation_cascade(30)[end-1:end], [0.193, 4.56e-13], rtol=tol)
end

@testset "Reactor with multiple processes" begin
    clear_default_processes()
    clear_default_state_mapping()
    @named asm_simp = Process("ASM1") # Use the matrix-defined version with default parameters
    @named aer_simp = Aeration() # The aeration process
    @named react_simp = CSTR(1000, [asm_simp, aer_simp]; 
        initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1], 
        state_mapping=Dict([:S_O2 => (names=Set([:S_O, :S_O2]), particulate=false, colloidal=false, soluble=true)])
        )
    @named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)
    @named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], unknowns(states(asm_simp)); flowrate = 18446)
    eqs_simp = [
        connect(outflows(influent, 1), inflows(react_simp, 1)), 
        connect(exogenous_inputs(react_simp, :aer_simp₊k_La), aeration_ctrl.output)
    ]

    @named model_simp = ODESystem(eqs_simp, t, systems=[influent, aeration_ctrl, react_simp])
    model_simp_simplified = structural_simplify(model_simp)
    prob_simp = SteadyStateProblem(model_simp_simplified, [])
    @test all(isapprox.(solve(prob_simp).u, [7.016847510878976, 62.41728227193586, 6.277661959224699, 1.504632769052528e-36, 31.79586515230564, 0.0, 7.130770716150561, 30.000000000000004, 197.83648822189184, 10.39362706372623, 0.0, 35.46214548201206, 0.09535522150644032, 51.2], atol=1e-10))
end

@testset "Reactor with multiple processes using defaults" begin
    set_default_process([Process, Aeration], [("ASM1",), ()]; name=[:asm, :aer])
    set_default_state_mapping(S_O2 = (Set([:S_O, :S_O2]), false, false, true))
    @named react_simp = CSTR(1000; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    @named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)
    @named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], flowrate = 18446)

    eqs_simp = [
        connect(outflows(influent, 1), inflows(react_simp, 1)), 
        connect(exogenous_inputs(react_simp, :react_simp_aer₊k_La), aeration_ctrl.output)
    ]

    @named model_simp = ODESystem(eqs_simp, t, systems=[influent, aeration_ctrl, react_simp])
    model_simp_simplified = structural_simplify(model_simp)

    prob_simp = SteadyStateProblem(model_simp_simplified, [])
    @test all(isapprox.(solve(prob_simp).u, [7.016847510878976, 62.41728227193586, 6.277661959224699, 1.504632769052528e-36, 31.79586515230564, 0.0, 7.130770716150561, 30.000000000000004, 197.83648822189184, 10.39362706372623, 0.0, 35.46214548201206, 0.09535522150644032, 51.2], atol=1e-10))
end

@testset "Reactor with multiple processes using defaults and renaming Processes" begin
    set_default_process([Process, Aeration], [("ASM1",), ()]; name=[:asm, :aer])
    set_default_state_mapping(S_P_Oxy = (Set([:S_O, :S_O2]), false, false, true))
    @named react_simp = CSTR(1000; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    @named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)
    @named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], flowrate = 18446)

    eqs_simp = [
        connect(outflows(influent, 1), inflows(react_simp, 1)), 
        connect(exogenous_inputs(react_simp, :react_simp_aer₊k_La), aeration_ctrl.output)
    ]

    @named model_simp = ODESystem(eqs_simp, t, systems=[influent, aeration_ctrl, react_simp])
    model_simp_simplified = structural_simplify(model_simp)

    prob_simp = SteadyStateProblem(model_simp_simplified, [])
    @test all(isapprox.(solve(prob_simp).u, [7.016847510878976, 62.41728227193586, 6.277661959224699, 1.504632769052528e-36, 31.79586515230564, 0.0, 7.130770716150561, 30.000000000000004, 197.83648822189184, 10.39362706372623, 0.0, 35.46214548201206, 0.09535522150644032, 51.2], atol=1e-10))
end
