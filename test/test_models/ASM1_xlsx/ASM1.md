# Activated Sludge Model (ASM1)
The original source of the activated sludge model is Henze et al. (1987). This model has been tested and extended since, so that we decided to further include the information from Hauduc et al. (2010) and Corominas et al. (2010). The goal was to provide these models in a machine readable form with a mass balance that sums up to zero.

## References
```
@article{Hauduc:2010,
  author = {Hauduc, H. and Rieger, L. and Takács, I. and Héduit, A. and Vanrolleghem, P. A. and Gillot, S.},
  title = "{A systematic approach for model verification: application on seven published activated sludge models}",
  journal = {Water Science and Technology},
  volume = {61},
  number = {4},
  pages = {825-839},
  year = {2010},
  month = {02},
  issn = {0273-1223},
  doi = {10.2166/wst.2010.898},
  url = {https://doi.org/10.2166/wst.2010.898},
}
@article{Henze:1987,
  title={A general model for single-sludge wastewater treatment systems.},
  author={Henze, Mogens and Grady, CP Leslie and Gujer, W and Marais, GVR and Matsuo, T},
  volume = {21},
  pages = {505-515},
  year={1987},
  journaltitle= {Water Research},
  doi={https://doi.org/10.1016/0043-1354(87)90058-3}
}
@article{Corominas:2010,
	title = {New framework for standardized notation in wastewater treatment modelling},
	volume = {61},
	issn = {0273-1223},
	doi = {10.2166/wst.2010.912},
	pages = {841-857},
	number = {4},
	journaltitle = {Water Science and Technology},
	author = {Corominas, Ll. and Rieger, L. and Takács, I. and Ekama, G. and Hauduc, H. and Vanrolleghem, P. A. and Oehmen, A. and Gernaey, K. V. and van Loosdrecht, M. C. M. and Comeau, Y.},
	date = {2010-02-01}
}
```

## Explanation of the files
### Composition matrix
The composition matrix is included to be able to make the mass balance for COD, N, and the charge. Therefore, for every state in the model, the composition matrix contains how much of the respective component (COD, N, charge) is contained per unit of the respective state variable.
### Matrix
We chose the corrected version by Hauduc et al. (2010). The matrix contains all states that are involved in a reaction.
### Parameters
- `name` The name of the parameter that the model will use. We took it from Hauduc et al. (2010) and changed it into a machine readable form.
- `description` A string that describes the parameter.
- `latex` A latex form in which the parameter can be displayed. Usually shorter and with a greek symbol. The mapping from Hauduc et al. (2010) is:
  * decay rates: b -> $\beta$,
  * rate constants: q -> $\lambda$,
  * correction factor: n -> $\eta$,
  * maximum growth rate: m -> $\mu$,
  * saturation coefficient: K -> $\kappa$,
  * Yield: Y -> $\gamma$,
  * conversion factor: i -> $\iota$,
  * fraction: f -> $\zeta$

- `unit` the short form of the units that are used.
- `unit_description` A description of the unit containing for example if only grams of Nitrogen or COD are included, i.e. what is actually measured.
- `value` A standard value for the parameter. This might need to be calibrated.
- `expression` Some parameters are calculated from other parameter. They have an expression. A parameter should either have an expression or a value, not both.
- `temperature` The temperature for which the typical parameter value is chosen.
- `type` The type of parameter that is used. It can be a kinectic parameter, a stoichiometric, a factor or a mass for ASM1.

### States
- `name` is the name of the state that the model will use. We took it from Hauduc et al. (2010) and changed it into a machine readable form.
- `description` is a string that describes the state.
- `latex` is a latex form in which the state can be displayed. The meaning for those who do not correspond fully with Hauduc et al. (2010) is:
  * autotrophic -> a,
  * heterotrophic -> h,
  * anoxic -> an,
  * biomass -> b,
  * lysis -> l,
  * non biodegradable -> nb,
  * colloidal -> c,
  * alkalinity -> alk

- `unit` is the short form of the units that are used.
- `unit_description` is description of the unit containing for example if only grams of Nitrogen or COD are included, i.e. what is actually measured.
- `particle_size` describes the size of the particles described by the state. `particulate`, `colloidal`or `soluble` are possible. If multiple apply they are separated by comma (`,`)

### Process rates
- `name` is the name of the process that the model will use. It is a very short form from Hauduc et al. (2010) the explanation is the following:
  * growth -> g,
  * heterotrophic -> h,
  * autotrophic -> a,
  * anoxic -> An,
  * aerobic -> O2,
  * decay -> d,
  * ammonification -> am,
  * non biodegradable -> nb,
  * colloidal -> c,
  * alkalinity -> alk,
  * hydrolysis of organics -> ho

- `description` is a string that describes the process rates.
- `latex` is a latex form in which the rate can be displayed. We use the short form from the name here.
- `equation` is the equation that describes the process rate taken from Hauduc at al. (2010), which originates from Henze et al. (1987)
