
@variables S_S(t)=3 X_S(t) [guess=23] S_O(t)
@named sys1 = ODESystem(Equation[], t, [S_S, S_O], [])
@named sys2 = ODESystem(Equation[], t, [X_S], [])
@named composed_sys = compose(sys2, sys1)

# Test symbolic_to_namestring
@testset "symbolic_to_namestring tests" begin
    @test issetequal(ProcessSimulator.symbolic_to_namestring.([t, X_S, S_S, S_O]), ["t", "X_S", "S_S", "S_O"])
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(composed_sys)), ["X_S", "S_S", "S_O"])
end

# Test split_namespace
@testset "split_namespace tests" begin
    @test issetequal(ProcessSimulator.split_namespace.([t, X_S, S_S, S_O]), [["t"], ["X_S(t)"], ["S_S(t)"], ["S_O(t)"]])
    @test issetequal(ProcessSimulator.split_namespace.(unknowns(composed_sys)), [["X_S(t)"], ["sys1", "S_S(t)"], ["sys1", "S_O(t)"]])
end

# Test strip_namespace
@testset "strip_namespace tests" begin
    @test issetequal(ProcessSimulator.lstrip_namespace.(unknowns(composed_sys), Ref(Symbol("sys1₊S_S(t)₊H"))), ["X_S(t)", "sys1₊S_S(t)", "sys1₊S_O(t)"])
    @test issetequal(ProcessSimulator.rstrip_namespace.(unknowns(composed_sys), Ref(Symbol("sys1₊X_S(t)"))), ["X_S(t)", "sys1₊S_S(t)", "sys1₊S_O(t)"])
end

# Test to_ordered_array
@testset "to_ordered_array tests" begin
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], [1, 2, 3.8]) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict([X_S => 2, S_S => 1, S_O => 3.8])) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict([:X_S => 2, :S_S => 1, :S_O => 3.8])) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict(["X_S" => 2, "S_S" => 1, "S_O" => 3.8])) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict([Symbolics.unwrap(X_S) => 2, Symbolics.unwrap(S_S) => 1, Symbolics.unwrap(S_O) => 3.8])) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict(["X_S" => 2, S_S => 1, :S_O => 3.8])) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict([X_S => 2, S_S => 1])) .== [1, 2, 0])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict([S_O => 3.8, S_S => 1])) .== [1, 0, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], Dict([X_S => 2, S_O => 3.8])) .== [0, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], [(X_S => 2), (S_S => 1), (S_O => 3.8)]) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], (X_S => 2, S_S => 1, S_O => 3.8)) .== [1, 2, 3.8])
    @test all(ProcessSimulator.to_ordered_array(["S_S", "X_S", "S_O"], []) .== [0, 0, 0])
end

@testset "get_default_or_guess tests" begin
    @test ProcessSimulator.get_default_or_guess(S_S, 0) == 3
    @test ProcessSimulator.get_default_or_guess(X_S, 0) == 23
    @test ProcessSimulator.get_default_or_guess(S_O, 0) == 0  
end
