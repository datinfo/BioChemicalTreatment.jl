
@testset "ASM1 Tests" begin
    @named asm1 = ASM1()
    # Test if it has the right connectors
    @test hasstates(asm1)
    @test num_states(asm1) == 1
    @test hasrates(asm1)
    @test num_rates(asm1) == 1
    @test !hasinflows(asm1)
    @test num_inflows(asm1) == 0
    @test !hasoutflows(asm1)
    @test num_outflows(asm1) == 0
    @test !hasexogenous_inputs(asm1)
    @test num_exogenous_inputs(asm1) == 0
    @test !hasexogenous_outputs(asm1)
    @test num_exogenous_outputs(asm1) == 0
    # Test for name
    @test nameof(asm1) == :asm1
    # Test for getting iv
    @test nameof(ModelingToolkit.get_iv(asm1)) == :t
    # Test states
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))), 
        ["S_I", "S_S", "S_O", "S_NO", "S_NH", "S_ND", "S_ALK", "X_I", "X_S", "X_BH", "X_BA", "X_P", "X_ND"])
    # Test rates
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(asm1))), 
        ["S_I", "S_S", "S_O", "S_NO", "S_NH", "S_ND", "S_ALK", "X_I", "X_S", "X_BH", "X_BA", "X_P", "X_ND"])
    # Test parameters
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(asm1)), 
        ["Y_H", "Y_A", "i_XB", "i_XP", "f_P", "mu_H", "K_S", "K_OH", "K_NO", "b_H", "eta_g", "mu_A", 
         "K_NH", "K_OA", "b_A", "kappa_h", "eta_h", "K_X", "kappa_a"])
    # Test for not having flows
    @test_throws ErrorException inflows(asm1)
    @test_throws ErrorException inflows(asm1, :is_there_one)
    @test_throws ErrorException outflows(asm1)
    @test_throws ErrorException outflows(asm1, :is_there_one)
    # Test for exogenous inputs (none here)
    @test isempty(exogenous_inputs(asm1))
    @test_throws ArgumentError exogenous_inputs(asm1, :is_there_one)
    # Test for undefined exogenous outputs
    @test_throws ErrorException exogenous_outputs(asm1)
    # Test all unknowns
    @test issetequal(string.(unknowns(asm1)), 
        ["states₊S_I(t)", "states₊S_S(t)", "states₊S_O(t)", "states₊S_NO(t)", "states₊S_NH(t)", "states₊S_ND(t)", "states₊S_ALK(t)", 
         "states₊X_I(t)", "states₊X_S(t)", "states₊X_BH(t)", "states₊X_BA(t)", "states₊X_P(t)", "states₊X_ND(t)",
         "rates₊S_I(t)", "rates₊S_S(t)", "rates₊S_O(t)", "rates₊S_NO(t)", "rates₊S_NH(t)", "rates₊S_ND(t)", "rates₊S_ALK(t)", 
         "rates₊X_I(t)", "rates₊X_S(t)", "rates₊X_BH(t)", "rates₊X_BA(t)", "rates₊X_P(t)", "rates₊X_ND(t)"])
    # Test if particulate metadata is correct
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> isparticulate.(unknowns(states(asm1)))) == 
        Dict("S_I" => false, "S_S" => false, "S_O" => false, "S_NO" => false, "S_NH" => false, "S_ND" => false, "S_ALK" => false, 
             "X_I" => true, "X_S" => true, "X_BH" => true, "X_BA" => true, "X_P" => true, "X_ND" => true)
    # Test correct number of equations
    @test length(equations(asm1)) == 13

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), asm1)
    @test String(take!(buf)) == "Process ASM1 'asm1':\nStates (13): see states(asm1)\n  S_ALK(t) [guess is 0.0]: S_ALK\n  S_I(t) [guess is 0.0]: S_I\n  S_ND(t) [guess is 0.0]: S_ND\n  S_NH(t) [guess is 0.0]: S_NH\n  S_NO(t) [guess is 0.0]: S_NO\n  S_O(t) [guess is 0.0]: S_O\n  S_S(t) [guess is 0.0]: S_S\n  X_BA(t) [guess is 0.0]: X_BA\n  X_BH(t) [guess is 0.0]: X_BH\n  X_I(t) [guess is 0.0]: X_I\n  X_ND(t) [guess is 0.0]: X_ND\n  X_P(t) [guess is 0.0]: X_P\n  X_S(t) [guess is 0.0]: X_S\nParameters (19): see parameters(asm1)\n  Y_H [defaults to 0.67]\n  Y_A [defaults to 0.24]\n  i_XB [defaults to 0.086]\n  i_XP [defaults to 0.01]\n  f_P [defaults to 0.08]\n  mu_H [defaults to 6.0]\n  K_S [defaults to 20.0]\n  K_OH [defaults to 0.2]\n  K_NO [defaults to 0.5]\n  b_H [defaults to 0.62]\n  eta_g [defaults to 0.8]\n  mu_A [defaults to 0.8]\n  K_NH [defaults to 1.0]\n  K_OA [defaults to 0.4]\n  b_A [defaults to 0.1]\n  kappa_h [defaults to 3.0]\n  eta_h [defaults to 0.4]\n  K_X [defaults to 0.03]\n  kappa_a [defaults to 0.08]"
    show(buf, MIME("text/plain"), asm1; extended=true)
    @test String(take!(buf)) == "Process ASM1 'asm1':\nStates (13): see states(asm1)\n  S_ALK(t) [guess is 0.0]: S_ALK\n  S_I(t) [guess is 0.0]: S_I\n  S_ND(t) [guess is 0.0]: S_ND\n  S_NH(t) [guess is 0.0]: S_NH\n  S_NO(t) [guess is 0.0]: S_NO\n  S_O(t) [guess is 0.0]: S_O\n  S_S(t) [guess is 0.0]: S_S\n  X_BA(t) [guess is 0.0]: X_BA\n  X_BH(t) [guess is 0.0]: X_BH\n  X_I(t) [guess is 0.0]: X_I\n  X_ND(t) [guess is 0.0]: X_ND\n  X_P(t) [guess is 0.0]: X_P\n  X_S(t) [guess is 0.0]: X_S\nRates (13): see rates(asm1)\n  S_ALK(t) [guess is 0.0]: S_ALK reaction rate\n  S_I(t) [guess is 0.0]: S_I reaction rate\n  S_ND(t) [guess is 0.0]: S_ND reaction rate\n  S_NH(t) [guess is 0.0]: S_NH reaction rate\n  S_NO(t) [guess is 0.0]: S_NO reaction rate\n  S_O(t) [guess is 0.0]: S_O reaction rate\n  S_S(t) [guess is 0.0]: S_S reaction rate\n  X_BA(t) [guess is 0.0]: X_BA reaction rate\n  X_BH(t) [guess is 0.0]: X_BH reaction rate\n  X_I(t) [guess is 0.0]: X_I reaction rate\n  X_ND(t) [guess is 0.0]: X_ND reaction rate\n  X_P(t) [guess is 0.0]: X_P reaction rate\n  X_S(t) [guess is 0.0]: X_S reaction rate\nEquations (13):\n  13 standard: see equations(asm1)\nParameters (19): see parameters(asm1)\n  Y_H [defaults to 0.67]\n  Y_A [defaults to 0.24]\n  i_XB [defaults to 0.086]\n  i_XP [defaults to 0.01]\n  f_P [defaults to 0.08]\n  mu_H [defaults to 6.0]\n  K_S [defaults to 20.0]\n  K_OH [defaults to 0.2]\n  K_NO [defaults to 0.5]\n  b_H [defaults to 0.62]\n  eta_g [defaults to 0.8]\n  mu_A [defaults to 0.8]\n  K_NH [defaults to 1.0]\n  K_OA [defaults to 0.4]\n  b_A [defaults to 0.1]\n  kappa_h [defaults to 3.0]\n  eta_h [defaults to 0.4]\n  K_X [defaults to 0.03]\n  kappa_a [defaults to 0.08]"
end

@testset "Aeration tests" begin
    @named aeration = Aeration()
    # Test if it has the right connectors
    @test hasstates(aeration)
    @test num_states(aeration) == 1
    @test hasrates(aeration)
    @test num_rates(aeration) == 1
    @test !hasinflows(aeration)
    @test num_inflows(aeration) == 0
    @test !hasoutflows(aeration)
    @test num_outflows(aeration) == 0
    @test hasexogenous_inputs(aeration)
    @test num_exogenous_inputs(aeration) == 1
    @test !hasexogenous_outputs(aeration)
    @test num_exogenous_outputs(aeration) == 0
    # Test for name
    @test nameof(aeration) == :aeration
    # Test for getting iv
    @test nameof(ModelingToolkit.get_iv(aeration)) == :t
    # Test states
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(aeration))), ["S_O"])
    # Test rates
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(aeration))), ["S_O"])
    # Test for not having flows
    @test_throws ErrorException inflows(aeration)
    @test_throws ErrorException outflows(aeration)
    # Test exogenous_inputs
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(exogenous_inputs(aeration)[1])), ["u"])
    # Test exogenous_inputs with name
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(exogenous_inputs(aeration, :k_La))), ["u"])
    # Test exogenous_inputs with invalid name
    @test_throws ArgumentError exogenous_inputs(aeration, :k_L)
    @test_throws ArgumentError exogenous_inputs(aeration, [:k_La, :S_O_max])
    # Test for undefined exogenous outputs
    @test_throws ErrorException exogenous_outputs(aeration)
    # Test parameters
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(aeration)), ["S_O_max"])
    # Test all unknowns
    @test issetequal(string.(unknowns(aeration)), ["states₊S_O(t)", "rates₊S_O(t)", "k_La₊u(t)"])
    # Test if particulate metadata is correct
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(aeration))) .=> isparticulate.(unknowns(states(aeration)))) == 
        Dict("S_O" => false)
    # Test correct number of equations
    @test length(equations(aeration)) == 1

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), aeration)
    @test String(take!(buf)) == "Process Aeration 'aeration':\nStates (1): see states(aeration)\n  S_O(t) [guess is 0.0]: S_O\nExogenous Inputs (1): see exogenous_inputs(aeration)\n  :k_La\nParameters (1): see parameters(aeration)\n  S_O_max [defaults to 8]"
    show(buf, MIME("text/plain"), aeration; extended=true)
    @test String(take!(buf)) == "Process Aeration 'aeration':\nStates (1): see states(aeration)\n  S_O(t) [guess is 0.0]: S_O\nRates (1): see rates(aeration)\n  S_O(t) [guess is 0.0]: S_O reaction rate\nExogenous Inputs (1): see exogenous_inputs(aeration)\n  :k_La\nEquations (1):\n  1 standard: see equations(aeration)\nParameters (1): see parameters(aeration)\n  S_O_max [defaults to 8]"
end

@testset "OzoneDisinfection tests" begin
    @named ozonation = OzoneDisinfection()
    # Test if it has the right connectors
    @test hasstates(ozonation)
    @test num_states(ozonation) == 1
    @test hasrates(ozonation)
    @test num_rates(ozonation) == 1
    @test !hasinflows(ozonation)
    @test num_inflows(ozonation) == 0
    @test !hasoutflows(ozonation)
    @test num_outflows(ozonation) == 0
    @test !hasexogenous_inputs(ozonation)
    @test num_exogenous_inputs(ozonation) == 0
    @test !hasexogenous_outputs(ozonation)
    @test num_exogenous_outputs(ozonation) == 0
    # Test for name
    @test nameof(ozonation) == :ozonation
    # Test for getting iv
    @test nameof(ModelingToolkit.get_iv(ozonation)) == :t
    # Test states
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(ozonation))), ["S_O3", "X_B"])
    # Test rates
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(ozonation))), ["S_O3", "X_B"])
    # Test for not having flows
    @test_throws ErrorException inflows(ozonation)
    @test_throws ErrorException inflows(ozonation, :is_there_one)
    @test_throws ErrorException outflows(ozonation)
    @test_throws ErrorException outflows(ozonation, :is_there_one)
    # Test for exogenous inputs (none here)
    @test isempty(exogenous_inputs(ozonation))
    @test_throws ArgumentError exogenous_inputs(ozonation, :is_there_one)
    # Test for undefined exogenous outputs
    @test_throws ErrorException exogenous_outputs(ozonation)
    # Test parameters
    @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(ozonation)), ["kO3", "kd"])
    # Test all unknowns
    @test issetequal(string.(unknowns(ozonation)), ["states₊S_O3(t)", "states₊X_B(t)", "rates₊S_O3(t)", "rates₊X_B(t)"])
    # Test if particulate metadata is correct
    @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(ozonation))) .=> isparticulate.(unknowns(states(ozonation)))) == 
        Dict("S_O3" => false, "X_B" => true)
    # Test correct number of equations
    @test length(equations(ozonation)) == 2

    # Test displaying
    buf = IOBuffer()
    show(buf, MIME("text/plain"), ozonation)
    @test String(take!(buf)) == "Process OzoneDisinfection 'ozonation':\nStates (2): see states(ozonation)\n  S_O3(t) [guess is 0.0]: S_O3\n  X_B(t) [guess is 0.0]: X_B\nParameters (2): see parameters(ozonation)\n  kO3 [defaults to 10]\n  kd [defaults to 1500]"
    show(buf, MIME("text/plain"), ozonation; extended=true)
    @test String(take!(buf)) == "Process OzoneDisinfection 'ozonation':\nStates (2): see states(ozonation)\n  S_O3(t) [guess is 0.0]: S_O3\n  X_B(t) [guess is 0.0]: X_B\nRates (2): see rates(ozonation)\n  S_O3(t) [guess is 0.0]: S_O3 reaction rate\n  X_B(t) [guess is 0.0]: X_B reaction rate\nEquations (2):\n  2 standard: see equations(ozonation)\nParameters (2): see parameters(ozonation)\n  kO3 [defaults to 10]\n  kd [defaults to 1500]"
end


@testset "Process Tests" begin
    @testset "Model from folder and name" begin
        @named asm1 = Process(joinpath(@__DIR__, "test_models", "ASM1"), "ASM1")
        # Test if it has the right connectors
        @test hasstates(asm1)
        @test num_states(asm1) == 1
        @test hasrates(asm1)
        @test num_rates(asm1) == 1
        @test !hasinflows(asm1)
        @test num_inflows(asm1) == 0
        @test !hasoutflows(asm1)
        @test num_outflows(asm1) == 0
        @test !hasexogenous_inputs(asm1)
        @test num_exogenous_inputs(asm1) == 0
        @test !hasexogenous_outputs(asm1)
        @test num_exogenous_outputs(asm1) == 0
        # Test for name
        @test nameof(asm1) == :asm1
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(asm1)) == :t
        # Test states
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test rates
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test parameters
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(asm1)), 
            ["Y_OHO", "f_XUBiolys", "Y_ANO", "i_NXBio", "i_NXUE", "i_NO3N2", "i_CODNO3", "i_CODN2", "i_ChargeSNHx", "i_ChargeSNOx", 
            "q_XCBSBhyd", "K_XCBhyd", "n_qhydAx", "m_OHOMax", "n_mOHOAx", "K_SBOHO", "b_OHO", "K_O2OHO", "K_NOxOHO", "K_NHxOHO", 
            "m_ANOMax", "b_ANO", "q_am", "K_O2ANO", "K_NHxANO", "COD_neg", "COD_pos", "COD_C", "COD_N", "COD_H", "COD_O", "COD_S", 
            "COD_P", "COD_Fe", "M_N"])
        # Test for not having flows
        @test_throws ErrorException inflows(asm1)
        @test_throws ErrorException inflows(asm1, :is_there_one)
        @test_throws ErrorException outflows(asm1)
        @test_throws ErrorException outflows(asm1, :is_there_one)
        # Test for exogenous inputs (none here)
        @test isempty(exogenous_inputs(asm1))
        @test_throws ArgumentError exogenous_inputs(asm1, :is_there_one)
        # Test for undefined exogenous outputs
        @test_throws ErrorException exogenous_outputs(asm1)
        # Test all unknowns
        @test issetequal(string.(unknowns(asm1)), 
            ["states₊S_Alk(t)", "states₊S_B(t)", "states₊S_BN(t)", "states₊S_N2(t)", "states₊S_NHx(t)", "states₊S_NOx(t)", "states₊S_O2(t)", 
            "states₊S_U(t)", "states₊XC_B(t)", "states₊XC_BN(t)", "states₊X_ANO(t)", "states₊X_OHO(t)", "states₊X_UE(t)", "states₊X_UInf(t)",
            "rates₊S_Alk(t)", "rates₊S_B(t)", "rates₊S_BN(t)", "rates₊S_N2(t)", "rates₊S_NHx(t)", "rates₊S_NOx(t)", "rates₊S_O2(t)", 
            "rates₊S_U(t)", "rates₊XC_B(t)", "rates₊XC_BN(t)", "rates₊X_ANO(t)", "rates₊X_OHO(t)", "rates₊X_UE(t)", "rates₊X_UInf(t)",
            "g_hO2(t)", "g_hAn(t)", "g_aO2(t)", "d_h(t)", "d_a(t)", "am_N(t)", "ho(t)", "ho_N(t)"])
        # Test if particulate metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> isparticulate.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => true, "X_OHO" => true, "X_UE" => true, "X_UInf" => true)
        # Test if colloidal metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> iscolloidal.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test if soluble metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> issoluble.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => true, "S_B" => true, "S_BN" => true, "S_N2" => true, "S_NHx" => true, "S_NOx" => true, "S_O2" => true, "S_U" => true, 
            "XC_B" => false, "XC_BN" => false, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test correct number of equations
        @test length(equations(asm1)) == 22

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), asm1)
        @test String(take!(buf)) == "Process ASM1 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
        show(buf, MIME("text/plain"), asm1; extended=true)
        @test String(take!(buf)) == "Process ASM1 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nRates (14): see rates(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk reaction rate\n  S_B(t) [guess is 0.0]: S_B reaction rate\n  S_BN(t) [guess is 0.0]: S_BN reaction rate\n  S_N2(t) [guess is 0.0]: S_N2 reaction rate\n  S_NHx(t) [guess is 0.0]: S_NHx reaction rate\n  S_NOx(t) [guess is 0.0]: S_NOx reaction rate\n  S_O2(t) [guess is 0.0]: S_O2 reaction rate\n  S_U(t) [guess is 0.0]: S_U reaction rate\n  XC_B(t) [guess is 0.0]: XC_B reaction rate\n  XC_BN(t) [guess is 0.0]: XC_BN reaction rate\n  X_ANO(t) [guess is 0.0]: X_ANO reaction rate\n  X_OHO(t) [guess is 0.0]: X_OHO reaction rate\n  X_UE(t) [guess is 0.0]: X_UE reaction rate\n  X_UInf(t) [guess is 0.0]: X_UInf reaction rate\nEquations (22):\n  22 standard: see equations(asm1)\nGet_Unknowns (8): see get_unknowns(asm1)\n  g_hO2(t): Aerobic growth of heterotrophs\n  g_hAn(t): Anoxic growth of heterotrophs\n  g_aO2(t): Aerobic growth of autotrophs\n  d_h(t): Decay of heterotrophs\n  d_a(t): Decay of autotrophs\n  am_N(t): Ammonification of soluble organic Nitrogen\n  ho(t): Hydrolysis of entrapped organics\n  ho_N(t): Hydrolysis of entrapped organic nitrogen\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
    end
    @testset "Model from files" begin
        folder = joinpath(@__DIR__, "test_models", "ASM1")
        @named asm1 = Process(joinpath(folder, "ASM1_states.csv"), joinpath(folder, "ASM1_parameters.csv"),
            joinpath(folder, "ASM1_processrates.csv"), joinpath(folder, "ASM1_matrix.csv"), joinpath(folder, "ASM1_compositionmatrix.csv"))
        # Test if it has the right connectors
        @test hasstates(asm1)
        @test num_states(asm1) == 1
        @test hasrates(asm1)
        @test num_rates(asm1) == 1
        @test !hasinflows(asm1)
        @test num_inflows(asm1) == 0
        @test !hasoutflows(asm1)
        @test num_outflows(asm1) == 0
        @test !hasexogenous_inputs(asm1)
        @test num_exogenous_inputs(asm1) == 0
        @test !hasexogenous_outputs(asm1)
        @test num_exogenous_outputs(asm1) == 0
        # Test for name
        @test nameof(asm1) == :asm1
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(asm1)) == :t
        # Test states
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test rates
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test parameters
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(asm1)), 
            ["Y_OHO", "f_XUBiolys", "Y_ANO", "i_NXBio", "i_NXUE", "i_NO3N2", "i_CODNO3", "i_CODN2", "i_ChargeSNHx", "i_ChargeSNOx", 
            "q_XCBSBhyd", "K_XCBhyd", "n_qhydAx", "m_OHOMax", "n_mOHOAx", "K_SBOHO", "b_OHO", "K_O2OHO", "K_NOxOHO", "K_NHxOHO", 
            "m_ANOMax", "b_ANO", "q_am", "K_O2ANO", "K_NHxANO", "COD_neg", "COD_pos", "COD_C", "COD_N", "COD_H", "COD_O", "COD_S", 
            "COD_P", "COD_Fe", "M_N"])
        # Test for not having flows
        @test_throws ErrorException inflows(asm1)
        @test_throws ErrorException inflows(asm1, :is_there_one)
        @test_throws ErrorException outflows(asm1)
        @test_throws ErrorException outflows(asm1, :is_there_one)
        # Test for exogenous inputs (none here)
        @test isempty(exogenous_inputs(asm1))
        @test_throws ArgumentError exogenous_inputs(asm1, :is_there_one)
        # Test for undefined exogenous outputs
        @test_throws ErrorException exogenous_outputs(asm1)
        # Test all unknowns
        @test issetequal(string.(unknowns(asm1)), 
            ["states₊S_Alk(t)", "states₊S_B(t)", "states₊S_BN(t)", "states₊S_N2(t)", "states₊S_NHx(t)", "states₊S_NOx(t)", "states₊S_O2(t)", 
            "states₊S_U(t)", "states₊XC_B(t)", "states₊XC_BN(t)", "states₊X_ANO(t)", "states₊X_OHO(t)", "states₊X_UE(t)", "states₊X_UInf(t)",
            "rates₊S_Alk(t)", "rates₊S_B(t)", "rates₊S_BN(t)", "rates₊S_N2(t)", "rates₊S_NHx(t)", "rates₊S_NOx(t)", "rates₊S_O2(t)", 
            "rates₊S_U(t)", "rates₊XC_B(t)", "rates₊XC_BN(t)", "rates₊X_ANO(t)", "rates₊X_OHO(t)", "rates₊X_UE(t)", "rates₊X_UInf(t)",
            "g_hO2(t)", "g_hAn(t)", "g_aO2(t)", "d_h(t)", "d_a(t)", "am_N(t)", "ho(t)", "ho_N(t)"])
        # Test if particulate metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> isparticulate.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => true, "X_OHO" => true, "X_UE" => true, "X_UInf" => true)
        # Test if colloidal metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> iscolloidal.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test if soluble metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> issoluble.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => true, "S_B" => true, "S_BN" => true, "S_N2" => true, "S_NHx" => true, "S_NOx" => true, "S_O2" => true, "S_U" => true, 
            "XC_B" => false, "XC_BN" => false, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test correct number of equations
        @test length(equations(asm1)) == 22

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), asm1)
        @test String(take!(buf)) == "Process MatrixDefinedReaction 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
        show(buf, MIME("text/plain"), asm1; extended=true)
        @test String(take!(buf)) == "Process MatrixDefinedReaction 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nRates (14): see rates(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk reaction rate\n  S_B(t) [guess is 0.0]: S_B reaction rate\n  S_BN(t) [guess is 0.0]: S_BN reaction rate\n  S_N2(t) [guess is 0.0]: S_N2 reaction rate\n  S_NHx(t) [guess is 0.0]: S_NHx reaction rate\n  S_NOx(t) [guess is 0.0]: S_NOx reaction rate\n  S_O2(t) [guess is 0.0]: S_O2 reaction rate\n  S_U(t) [guess is 0.0]: S_U reaction rate\n  XC_B(t) [guess is 0.0]: XC_B reaction rate\n  XC_BN(t) [guess is 0.0]: XC_BN reaction rate\n  X_ANO(t) [guess is 0.0]: X_ANO reaction rate\n  X_OHO(t) [guess is 0.0]: X_OHO reaction rate\n  X_UE(t) [guess is 0.0]: X_UE reaction rate\n  X_UInf(t) [guess is 0.0]: X_UInf reaction rate\nEquations (22):\n  22 standard: see equations(asm1)\nGet_Unknowns (8): see get_unknowns(asm1)\n  g_hO2(t): Aerobic growth of heterotrophs\n  g_hAn(t): Anoxic growth of heterotrophs\n  g_aO2(t): Aerobic growth of autotrophs\n  d_h(t): Decay of heterotrophs\n  d_a(t): Decay of autotrophs\n  am_N(t): Ammonification of soluble organic Nitrogen\n  ho(t): Hydrolysis of entrapped organics\n  ho_N(t): Hydrolysis of entrapped organic nitrogen\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
    end
    @testset "Model from xlsx" begin
        folder = joinpath(@__DIR__, "test_models", "ASM1_xlsx")
        @test_throws ErrorException asm1 = Process(joinpath(folder, "ASM1.xlsx"); name=:asm1)
        using XLSX # Load XLSX for the extension
        @named asm1 = Process(joinpath(folder, "ASM1.xlsx"))
        # Test if it has the right connectors
        @test hasstates(asm1)
        @test num_states(asm1) == 1
        @test hasrates(asm1)
        @test num_rates(asm1) == 1
        @test !hasinflows(asm1)
        @test num_inflows(asm1) == 0
        @test !hasoutflows(asm1)
        @test num_outflows(asm1) == 0
        @test !hasexogenous_inputs(asm1)
        @test num_exogenous_inputs(asm1) == 0
        @test !hasexogenous_outputs(asm1)
        @test num_exogenous_outputs(asm1) == 0
        # Test for name
        @test nameof(asm1) == :asm1
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(asm1)) == :t
        # Test states
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test rates
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test parameters
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(asm1)), 
            ["Y_OHO", "f_XUBiolys", "Y_ANO", "i_NXBio", "i_NXUE", "i_NO3N2", "i_CODNO3", "i_CODN2", "i_ChargeSNHx", "i_ChargeSNOx", 
            "q_XCBSBhyd", "K_XCBhyd", "n_qhydAx", "m_OHOMax", "n_mOHOAx", "K_SBOHO", "b_OHO", "K_O2OHO", "K_NOxOHO", "K_NHxOHO", 
            "m_ANOMax", "b_ANO", "q_am", "K_O2ANO", "K_NHxANO", "COD_neg", "COD_pos", "COD_C", "COD_N", "COD_H", "COD_O", "COD_S", 
            "COD_P", "COD_Fe", "M_N"])
        # Test for not having flows
        @test_throws ErrorException inflows(asm1)
        @test_throws ErrorException inflows(asm1, :is_there_one)
        @test_throws ErrorException outflows(asm1)
        @test_throws ErrorException outflows(asm1, :is_there_one)
        # Test for exogenous inputs (none here)
        @test isempty(exogenous_inputs(asm1))
        @test_throws ArgumentError exogenous_inputs(asm1, :is_there_one)
        # Test for undefined exogenous outputs
        @test_throws ErrorException exogenous_outputs(asm1)
        # Test all unknowns
        @test issetequal(string.(unknowns(asm1)), 
            ["states₊S_Alk(t)", "states₊S_B(t)", "states₊S_BN(t)", "states₊S_N2(t)", "states₊S_NHx(t)", "states₊S_NOx(t)", "states₊S_O2(t)", 
            "states₊S_U(t)", "states₊XC_B(t)", "states₊XC_BN(t)", "states₊X_ANO(t)", "states₊X_OHO(t)", "states₊X_UE(t)", "states₊X_UInf(t)",
            "rates₊S_Alk(t)", "rates₊S_B(t)", "rates₊S_BN(t)", "rates₊S_N2(t)", "rates₊S_NHx(t)", "rates₊S_NOx(t)", "rates₊S_O2(t)", 
            "rates₊S_U(t)", "rates₊XC_B(t)", "rates₊XC_BN(t)", "rates₊X_ANO(t)", "rates₊X_OHO(t)", "rates₊X_UE(t)", "rates₊X_UInf(t)",
            "g_hO2(t)", "g_hAn(t)", "g_aO2(t)", "d_h(t)", "d_a(t)", "am_N(t)", "ho(t)", "ho_N(t)"])
        # Test if particulate metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> isparticulate.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => true, "X_OHO" => true, "X_UE" => true, "X_UInf" => true)
        # Test if colloidal metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> iscolloidal.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test if soluble metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> issoluble.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => true, "S_B" => true, "S_BN" => true, "S_N2" => true, "S_NHx" => true, "S_NOx" => true, "S_O2" => true, "S_U" => true, 
            "XC_B" => false, "XC_BN" => false, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test correct number of equations
        @test length(equations(asm1)) == 22

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), asm1)
        @test String(take!(buf)) == "Process MatrixDefinedReaction 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
        show(buf, MIME("text/plain"), asm1; extended=true)
        @test String(take!(buf)) == "Process MatrixDefinedReaction 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nRates (14): see rates(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk reaction rate\n  S_B(t) [guess is 0.0]: S_B reaction rate\n  S_BN(t) [guess is 0.0]: S_BN reaction rate\n  S_N2(t) [guess is 0.0]: S_N2 reaction rate\n  S_NHx(t) [guess is 0.0]: S_NHx reaction rate\n  S_NOx(t) [guess is 0.0]: S_NOx reaction rate\n  S_O2(t) [guess is 0.0]: S_O2 reaction rate\n  S_U(t) [guess is 0.0]: S_U reaction rate\n  XC_B(t) [guess is 0.0]: XC_B reaction rate\n  XC_BN(t) [guess is 0.0]: XC_BN reaction rate\n  X_ANO(t) [guess is 0.0]: X_ANO reaction rate\n  X_OHO(t) [guess is 0.0]: X_OHO reaction rate\n  X_UE(t) [guess is 0.0]: X_UE reaction rate\n  X_UInf(t) [guess is 0.0]: X_UInf reaction rate\nEquations (22):\n  22 standard: see equations(asm1)\nGet_Unknowns (8): see get_unknowns(asm1)\n  g_hO2(t): Aerobic growth of heterotrophs\n  g_hAn(t): Anoxic growth of heterotrophs\n  g_aO2(t): Aerobic growth of autotrophs\n  d_h(t): Decay of heterotrophs\n  d_a(t): Decay of autotrophs\n  am_N(t): Ammonification of soluble organic Nitrogen\n  ho(t): Hydrolysis of entrapped organics\n  ho_N(t): Hydrolysis of entrapped organic nitrogen\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
    end
    @testset "Failing Composition" begin
        @test_throws ErrorException (@named asm1 = Process(joinpath(@__DIR__, "test_models", "ASM1_wrongsymboliccomp"), "ASM1"))
        @test_logs (:warn, r"The composition of the read in Reaction System does not fulfill requirement neither symbolically nor with values plugged in.*") (@named asm1 = Process(joinpath(@__DIR__, "test_models", "ASM1_wrongsymboliccomp"), "ASM1"; error_invalid_composition = false))
        @test_nowarn (@named asm1 = Process(joinpath(@__DIR__, "test_models", "ASM1_wrongsymboliccomp"), "ASM1"; i_NO3N2 = "(COD_N-(COD_N+3*COD_O+COD_neg))/(M_N)"))
        @test_logs (:warn, r"The composition of the read in Reaction Sytem does fulfill requirement only with values plugged in but not symbolically. Please check your model and be careful when adapting parameters at runtime.*") (@named asm1 = Process(joinpath(@__DIR__, "test_models", "ASM1_wrongsymboliccomp"), "ASM1"; i_NO3N2 = 2.857142857142857))
    end
    @testset "ASM1" begin
        @named asm1 = Process("ASM1")
        # Test if it has the right connectors
        @test hasstates(asm1)
        @test num_states(asm1) == 1
        @test hasrates(asm1)
        @test num_rates(asm1) == 1
        @test !hasinflows(asm1)
        @test num_inflows(asm1) == 0
        @test !hasoutflows(asm1)
        @test num_outflows(asm1) == 0
        @test !hasexogenous_inputs(asm1)
        @test num_exogenous_inputs(asm1) == 0
        @test !hasexogenous_outputs(asm1)
        @test num_exogenous_outputs(asm1) == 0
        # Test for name
        @test nameof(asm1) == :asm1
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(asm1)) == :t
        # Test states
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test rates
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(asm1))), 
            ["S_Alk", "S_B", "S_BN", "S_N2", "S_NHx", "S_NOx", "S_O2", "S_U", "XC_B", "XC_BN", "X_ANO", "X_OHO", "X_UE", "X_UInf"])
        # Test parameters
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(asm1)), 
            ["Y_OHO", "f_XUBiolys", "Y_ANO", "i_NXBio", "i_NXUE", "i_NO3N2", "i_CODNO3", "i_CODN2", "i_ChargeSNHx", "i_ChargeSNOx", 
            "q_XCBSBhyd", "K_XCBhyd", "n_qhydAx", "m_OHOMax", "n_mOHOAx", "K_SBOHO", "b_OHO", "K_O2OHO", "K_NOxOHO", "K_NHxOHO", 
            "m_ANOMax", "b_ANO", "q_am", "K_O2ANO", "K_NHxANO", "COD_neg", "COD_pos", "COD_C", "COD_N", "COD_H", "COD_O", "COD_S", 
            "COD_P", "COD_Fe", "M_N"])
        # Test for not having flows
        @test_throws ErrorException inflows(asm1)
        @test_throws ErrorException inflows(asm1, :is_there_one)
        @test_throws ErrorException outflows(asm1)
        @test_throws ErrorException outflows(asm1, :is_there_one)
        # Test for exogenous inputs (none here)
        @test isempty(exogenous_inputs(asm1))
        @test_throws ArgumentError exogenous_inputs(asm1, :is_there_one)
        # Test for undefined exogenous outputs
        @test_throws ErrorException exogenous_outputs(asm1)
        # Test all unknowns
        @test issetequal(string.(unknowns(asm1)), 
            ["states₊S_Alk(t)", "states₊S_B(t)", "states₊S_BN(t)", "states₊S_N2(t)", "states₊S_NHx(t)", "states₊S_NOx(t)", "states₊S_O2(t)", 
            "states₊S_U(t)", "states₊XC_B(t)", "states₊XC_BN(t)", "states₊X_ANO(t)", "states₊X_OHO(t)", "states₊X_UE(t)", "states₊X_UInf(t)",
            "rates₊S_Alk(t)", "rates₊S_B(t)", "rates₊S_BN(t)", "rates₊S_N2(t)", "rates₊S_NHx(t)", "rates₊S_NOx(t)", "rates₊S_O2(t)", 
            "rates₊S_U(t)", "rates₊XC_B(t)", "rates₊XC_BN(t)", "rates₊X_ANO(t)", "rates₊X_OHO(t)", "rates₊X_UE(t)", "rates₊X_UInf(t)",
            "g_hO2(t)", "g_hAn(t)", "g_aO2(t)", "d_h(t)", "d_a(t)", "am_N(t)", "ho(t)", "ho_N(t)"])
        # Test if particulate metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> isparticulate.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => true, "X_OHO" => true, "X_UE" => true, "X_UInf" => true)
        # Test if colloidal metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> iscolloidal.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => false, "S_B" => false, "S_BN" => false, "S_N2" => false, "S_NHx" => false, "S_NOx" => false, "S_O2" => false, "S_U" => false, 
            "XC_B" => true, "XC_BN" => true, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test if soluble metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm1))) .=> issoluble.(unknowns(states(asm1)))) == 
            Dict("S_Alk" => true, "S_B" => true, "S_BN" => true, "S_N2" => true, "S_NHx" => true, "S_NOx" => true, "S_O2" => true, "S_U" => true, 
            "XC_B" => false, "XC_BN" => false, "X_ANO" => false, "X_OHO" => false, "X_UE" => false, "X_UInf" => false)
        # Test correct number of equations
        @test length(equations(asm1)) == 22

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), asm1)
        @test String(take!(buf)) == "Process ASM1 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
        show(buf, MIME("text/plain"), asm1; extended=true)
        @test String(take!(buf)) == "Process ASM1 'asm1':\nStates (14): see states(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_BN(t) [guess is 0.0]: S_BN\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  XC_BN(t) [guess is 0.0]: XC_BN\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_UE(t) [guess is 0.0]: X_UE\n  X_UInf(t) [guess is 0.0]: X_UInf\nRates (14): see rates(asm1)\n  S_Alk(t) [guess is 0.0]: S_Alk reaction rate\n  S_B(t) [guess is 0.0]: S_B reaction rate\n  S_BN(t) [guess is 0.0]: S_BN reaction rate\n  S_N2(t) [guess is 0.0]: S_N2 reaction rate\n  S_NHx(t) [guess is 0.0]: S_NHx reaction rate\n  S_NOx(t) [guess is 0.0]: S_NOx reaction rate\n  S_O2(t) [guess is 0.0]: S_O2 reaction rate\n  S_U(t) [guess is 0.0]: S_U reaction rate\n  XC_B(t) [guess is 0.0]: XC_B reaction rate\n  XC_BN(t) [guess is 0.0]: XC_BN reaction rate\n  X_ANO(t) [guess is 0.0]: X_ANO reaction rate\n  X_OHO(t) [guess is 0.0]: X_OHO reaction rate\n  X_UE(t) [guess is 0.0]: X_UE reaction rate\n  X_UInf(t) [guess is 0.0]: X_UInf reaction rate\nEquations (22):\n  22 standard: see equations(asm1)\nGet_Unknowns (8): see get_unknowns(asm1)\n  g_hO2(t): Aerobic growth of heterotrophs\n  g_hAn(t): Anoxic growth of heterotrophs\n  g_aO2(t): Aerobic growth of autotrophs\n  d_h(t): Decay of heterotrophs\n  d_a(t): Decay of autotrophs\n  am_N(t): Ammonification of soluble organic Nitrogen\n  ho(t): Hydrolysis of entrapped organics\n  ho_N(t): Hydrolysis of entrapped organic nitrogen\nParameters (35): see parameters(asm1)\n  K_NHxOHO [defaults to 0.05]\n  m_ANOMax [defaults to 0.8]\n  m_OHOMax [defaults to 6]\n  K_O2ANO [defaults to 0.4]\n  K_XCBhyd [defaults to 0.03]\n  COD_P [defaults to 40]\n  COD_C [defaults to 32]\n  Y_ANO [defaults to 0.24]\n  COD_O [defaults to -16]\n  M_N [defaults to 14]\n  i_ChargeSNHx [defaults to 0.0714286]\n  COD_N [defaults to -24]\n  i_NXBio [defaults to 0.086]\n  Y_OHO [defaults to 0.67]\n  b_ANO [defaults to 0.15]\n  K_NOxOHO [defaults to 0.5]\n  i_CODNO3 [defaults to -4.57143]\n  n_qhydAx [defaults to 0.4]\n  K_O2OHO [defaults to 0.2]\n  i_NXUE [defaults to 0.06]\n  q_am [defaults to 0.08]\n  COD_H [defaults to 8]\n  K_SBOHO [defaults to 20]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  f_XUBiolys [defaults to 0.08]\n  b_OHO [defaults to 0.62]\n  i_CODN2 [defaults to -1.71429]\n  COD_pos [defaults to -8]\n  n_mOHOAx [defaults to 0.8]\n  K_NHxANO [defaults to 1]\n  COD_S [defaults to 48]\n  COD_Fe [defaults to 24]\n  COD_neg [defaults to 8]\n  i_ChargeSNOx [defaults to -0.0714286]"
    end
    @testset "ASM3" begin
        @named asm3 = Process("ASM3")
        # Test if it has the right connectors
        @test hasstates(asm3)
        @test num_states(asm3) == 1
        @test hasrates(asm3)
        @test num_rates(asm3) == 1
        @test !hasinflows(asm3)
        @test num_inflows(asm3) == 0
        @test !hasoutflows(asm3)
        @test num_outflows(asm3) == 0
        @test !hasexogenous_inputs(asm3)
        @test num_exogenous_inputs(asm3) == 0
        @test !hasexogenous_outputs(asm3)
        @test num_exogenous_outputs(asm3) == 0
        # Test for name
        @test nameof(asm3) == :asm3
        # Test for getting iv
        @test nameof(ModelingToolkit.get_iv(asm3)) == :t
        # Test states
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm3))), 
            ["S_B", "S_U", "S_O2", "XC_B", "X_U", "S_NHx", "S_NOx", "S_N2", "X_OHO", "X_ANO", "X_OHOStor", "S_Alk", "X_TSS"])
        # Test rates
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(unknowns(rates(asm3))), 
        ["S_B", "S_U", "S_O2", "XC_B", "X_U", "S_NHx", "S_NOx", "S_N2", "X_OHO", "X_ANO", "X_OHOStor", "S_Alk", "X_TSS"])
        # Test parameters
        @test issetequal(ProcessSimulator.symbolic_to_namestring.(parameters(asm3)), 
            ["f_SUXCBhyd", "Y_StorOHOOx", "Y_StorOHOAx", "Y_SBStorOx", "Y_SBStorAx", "f_XUBiolys", "Y_ANO", "i_NSB", "i_NSU", 
             "i_NXU", "i_NXCB", "i_NXBio", "i_TSSXU", "i_TSSXCB", "i_TSSXBio", "i_TSSXOHOStor", "i_NO3N2", "i_CODNO3", "i_CODN2", 
             "i_ChargeSNHx", "i_ChargeSNOx", "q_XCBSBhyd", "K_XCBhyd", "q_SBStor", "m_OHOMax", "n_mOHOAx", "K_SBOHO", "K_StorOHO", 
             "m_OHOOx", "m_OHOAx", "m_StorOx", "m_StorAx", "K_O2OHO", "K_NOxOHO", "K_NHxOHO", "K_AlkOHO", "m_ANOMax", "m_ANOOx", 
             "m_ANOAx", "K_O2ANO", "K_NHxANO", "K_AlkANO", "COD_neg", "COD_pos", "COD_C", "COD_N", "COD_H", "COD_O", "COD_S", "COD_P", 
             "COD_Fe", "M_N", "v1_SNHx", "v2_SNHx", "v3_SNHx", "v3_SNOx", "v4_SNHx", "v5_SNHx", "v5_SNOx", "v6_SNHx", "v7_SNHx", "v7_SNOx", 
             "v9_SNOx", "v10_SNHx", "v10_SNOx", "v11_SNHx", "v12_SNHx", "v12_SNOx"])
        # Test for not having flows
        @test_throws ErrorException inflows(asm3)
        @test_throws ErrorException inflows(asm3, :is_there_one)
        @test_throws ErrorException outflows(asm3)
        @test_throws ErrorException outflows(asm3, :is_there_one)
        # Test for exogenous inputs (none here)
        @test isempty(exogenous_inputs(asm3))
        @test_throws ArgumentError exogenous_inputs(asm3, :is_there_one)
        # Test for undefined exogenous outputs
        @test_throws ErrorException exogenous_outputs(asm3)
        # Test all unknowns
        @test issetequal(string.(unknowns(asm3)), 
            ["states₊S_B(t)", "states₊S_U(t)", "states₊S_O2(t)", "states₊XC_B(t)", "states₊X_U(t)", "states₊S_NHx(t)", "states₊S_NOx(t)", 
             "states₊S_N2(t)", "states₊X_OHO(t)", "states₊X_ANO(t)", "states₊X_OHOStor(t)", "states₊S_Alk(t)", "states₊X_TSS(t)",
             "rates₊S_B(t)", "rates₊S_U(t)", "rates₊S_O2(t)", "rates₊XC_B(t)", "rates₊X_U(t)", "rates₊S_NHx(t)", "rates₊S_NOx(t)", 
             "rates₊S_N2(t)", "rates₊X_OHO(t)", "rates₊X_ANO(t)", "rates₊X_OHOStor(t)", "rates₊S_Alk(t)", "rates₊X_TSS(t)",
             "hy(t)", "s_hO2(t)", "s_hAn(t)", "g_hO2(t)", "g_hAn(t)", "er_hO2(t)", "er_hAn(t)", "r_hsO2(t)", "r_hsAn(t)", "g_a(t)", 
             "er_aO2(t)", "er_aAn(t)"])
        # Test if particulate metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm3))) .=> isparticulate.(unknowns(states(asm3)))) == 
            Dict("S_B" => false, "S_U" => false, "S_O2" => false, "XC_B" => true, "X_U" => true, "S_NHx" => false, "S_NOx" => false, "S_N2" => false, 
                 "X_OHO" => true, "X_ANO" => true, "X_OHOStor" => true, "S_Alk" => false, "X_TSS" => true)
        # Test if colloidal metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm3))) .=> iscolloidal.(unknowns(states(asm3)))) == 
            Dict("S_B" => false, "S_U" => false, "S_O2" => false, "XC_B" => true, "X_U" => false, "S_NHx" => false, "S_NOx" => false, "S_N2" => false, 
                "X_OHO" => false, "X_ANO" => false, "X_OHOStor" => false, "S_Alk" => false, "X_TSS" => false)
        # Test if soluble metadata is correct
        @test Dict(ProcessSimulator.symbolic_to_namestring.(unknowns(states(asm3))) .=> issoluble.(unknowns(states(asm3)))) == 
            Dict("S_B" => true, "S_U" => true, "S_O2" => true, "XC_B" => false, "X_U" => false, "S_NHx" => true, "S_NOx" => true, "S_N2" => true, 
                "X_OHO" => false, "X_ANO" => false, "X_OHOStor" => false, "S_Alk" => true, "X_TSS" => false)
        # Test correct number of equations
        @test length(equations(asm3)) == 25

        # Test displaying
        buf = IOBuffer()
        show(buf, MIME("text/plain"), asm3)
        @test String(take!(buf)) == "Process ASM3 'asm3':\nStates (13): see states(asm3)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_OHOStor(t) [guess is 0.0]: X_OHOStor\n  X_TSS(t) [guess is 0.0]: X_TSS\n  X_U(t) [guess is 0.0]: X_U\nParameters (68): see parameters(asm3)\n  m_OHOMax [defaults to 2]\n  K_O2ANO [defaults to 0.5]\n  K_XCBhyd [defaults to 1]\n  v10_SNHx [defaults to -4.23667]\n  Y_ANO [defaults to 0.24]\n  i_TSSXOHOStor [defaults to 0.6]\n  i_ChargeSNHx [defaults to 0.0714286]\n  v3_SNHx [defaults to 0.03]\n  i_NXBio [defaults to 0.07]\n  m_OHOOx [defaults to 0.2]\n  q_SBStor [defaults to 5]\n  K_O2OHO [defaults to 0.2]\n  K_AlkANO [defaults to 0.5]\n  v10_SNOx [defaults to 4.16667]\n  m_OHOAx [defaults to 0.1]\n  f_SUXCBhyd [defaults to 0]\n  COD_S [defaults to 48]\n  K_NHxOHO [defaults to 0.01]\n  m_ANOMax [defaults to 1]\n  v1_SNHx [defaults to 0.01]\n  COD_P [defaults to 40]\n  K_StorOHO [defaults to 1]\n  COD_O [defaults to -16]\n  v4_SNHx [defaults to -0.07]\n  v2_SNHx [defaults to 0.03]\n  i_TSSXU [defaults to 0.75]\n  Y_StorOHOAx [defaults to 0.54]\n  K_SBOHO [defaults to 2]\n  v11_SNHx [defaults to 0.066]\n  COD_pos [defaults to -8]\n  v6_SNHx [defaults to 0.066]\n  i_TSSXBio [defaults to 0.9]\n  COD_neg [defaults to 8]\n  m_ANOOx [defaults to 0.15]\n  Y_SBStorOx [defaults to 0.85]\n  v12_SNOx [defaults to -0.28]\n  COD_C [defaults to 32]\n  Y_SBStorAx [defaults to 0.8]\n  M_N [defaults to 14]\n  K_NOxOHO [defaults to 0.5]\n  v9_SNOx [defaults to -0.35]\n  K_AlkOHO [defaults to 0.1]\n  f_XUBiolys [defaults to 0.2]\n  m_StorOx [defaults to 0.2]\n  Y_StorOHOOx [defaults to 0.63]\n  i_CODN2 [defaults to -1.71429]\n  K_NHxANO [defaults to 1]\n  n_mOHOAx [defaults to 0.6]\n  v5_SNOx [defaults to -0.298148]\n  COD_Fe [defaults to 24]\n  v5_SNHx [defaults to -0.07]\n  i_NSU [defaults to 0.01]\n  m_ANOAx [defaults to 0.05]\n  i_NXU [defaults to 0.02]\n  COD_N [defaults to -24]\n  i_NSB [defaults to 0.03]\n  i_CODNO3 [defaults to -4.57143]\n  v3_SNOx [defaults to -0.07]\n  v12_SNHx [defaults to 0.066]\n  COD_H [defaults to 8]\n  m_StorAx [defaults to 0.1]\n  v7_SNOx [defaults to -0.28]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  i_TSSXCB [defaults to 0.75]\n  v7_SNHx [defaults to 0.066]\n  i_NXCB [defaults to 0.04]\n  i_ChargeSNOx [defaults to -0.0714286]"
        show(buf, MIME("text/plain"), asm3; extended=true)
        @test String(take!(buf)) == "Process ASM3 'asm3':\nStates (13): see states(asm3)\n  S_Alk(t) [guess is 0.0]: S_Alk\n  S_B(t) [guess is 0.0]: S_B\n  S_N2(t) [guess is 0.0]: S_N2\n  S_NHx(t) [guess is 0.0]: S_NHx\n  S_NOx(t) [guess is 0.0]: S_NOx\n  S_O2(t) [guess is 0.0]: S_O2\n  S_U(t) [guess is 0.0]: S_U\n  XC_B(t) [guess is 0.0]: XC_B\n  X_ANO(t) [guess is 0.0]: X_ANO\n  X_OHO(t) [guess is 0.0]: X_OHO\n  X_OHOStor(t) [guess is 0.0]: X_OHOStor\n  X_TSS(t) [guess is 0.0]: X_TSS\n  X_U(t) [guess is 0.0]: X_U\nRates (13): see rates(asm3)\n  S_Alk(t) [guess is 0.0]: S_Alk reaction rate\n  S_B(t) [guess is 0.0]: S_B reaction rate\n  S_N2(t) [guess is 0.0]: S_N2 reaction rate\n  S_NHx(t) [guess is 0.0]: S_NHx reaction rate\n  S_NOx(t) [guess is 0.0]: S_NOx reaction rate\n  S_O2(t) [guess is 0.0]: S_O2 reaction rate\n  S_U(t) [guess is 0.0]: S_U reaction rate\n  XC_B(t) [guess is 0.0]: XC_B reaction rate\n  X_ANO(t) [guess is 0.0]: X_ANO reaction rate\n  X_OHO(t) [guess is 0.0]: X_OHO reaction rate\n  X_OHOStor(t) [guess is 0.0]: X_OHOStor reaction rate\n  X_TSS(t) [guess is 0.0]: X_TSS reaction rate\n  X_U(t) [guess is 0.0]: X_U reaction rate\nEquations (25):\n  25 standard: see equations(asm3)\nGet_Unknowns (12): see get_unknowns(asm3)\n  hy(t): Hydrolysis\n  s_hO2(t): Aerobic storage of XOHO,Stor\n  s_hAn(t): Anoxic storage of XOHO,Stor\n  g_hO2(t): Aerobic growth of XOHO\n  g_hAn(t): Anoxic growth of XOHO (denitrification)\n  er_hO2(t): Aerobic endogenous respiration of XOHO\n  er_hAn(t): Anoxic endogenous respiration of XOHO\n  r_hsO2(t): Aerobic respiration of XOHO,Stor\n  r_hsAn(t): Anoxic respiration of XOHO,Stor\n  g_a(t): Growth of XANO (Nitrification)\n  er_aO2(t): Aerobic endogenous respiration of XANO\n  er_aAn(t): Anoxic endogenous respiration of XANO\nParameters (68): see parameters(asm3)\n  m_OHOMax [defaults to 2]\n  K_O2ANO [defaults to 0.5]\n  K_XCBhyd [defaults to 1]\n  v10_SNHx [defaults to -4.23667]\n  Y_ANO [defaults to 0.24]\n  i_TSSXOHOStor [defaults to 0.6]\n  i_ChargeSNHx [defaults to 0.0714286]\n  v3_SNHx [defaults to 0.03]\n  i_NXBio [defaults to 0.07]\n  m_OHOOx [defaults to 0.2]\n  q_SBStor [defaults to 5]\n  K_O2OHO [defaults to 0.2]\n  K_AlkANO [defaults to 0.5]\n  v10_SNOx [defaults to 4.16667]\n  m_OHOAx [defaults to 0.1]\n  f_SUXCBhyd [defaults to 0]\n  COD_S [defaults to 48]\n  K_NHxOHO [defaults to 0.01]\n  m_ANOMax [defaults to 1]\n  v1_SNHx [defaults to 0.01]\n  COD_P [defaults to 40]\n  K_StorOHO [defaults to 1]\n  COD_O [defaults to -16]\n  v4_SNHx [defaults to -0.07]\n  v2_SNHx [defaults to 0.03]\n  i_TSSXU [defaults to 0.75]\n  Y_StorOHOAx [defaults to 0.54]\n  K_SBOHO [defaults to 2]\n  v11_SNHx [defaults to 0.066]\n  COD_pos [defaults to -8]\n  v6_SNHx [defaults to 0.066]\n  i_TSSXBio [defaults to 0.9]\n  COD_neg [defaults to 8]\n  m_ANOOx [defaults to 0.15]\n  Y_SBStorOx [defaults to 0.85]\n  v12_SNOx [defaults to -0.28]\n  COD_C [defaults to 32]\n  Y_SBStorAx [defaults to 0.8]\n  M_N [defaults to 14]\n  K_NOxOHO [defaults to 0.5]\n  v9_SNOx [defaults to -0.35]\n  K_AlkOHO [defaults to 0.1]\n  f_XUBiolys [defaults to 0.2]\n  m_StorOx [defaults to 0.2]\n  Y_StorOHOOx [defaults to 0.63]\n  i_CODN2 [defaults to -1.71429]\n  K_NHxANO [defaults to 1]\n  n_mOHOAx [defaults to 0.6]\n  v5_SNOx [defaults to -0.298148]\n  COD_Fe [defaults to 24]\n  v5_SNHx [defaults to -0.07]\n  i_NSU [defaults to 0.01]\n  m_ANOAx [defaults to 0.05]\n  i_NXU [defaults to 0.02]\n  COD_N [defaults to -24]\n  i_NSB [defaults to 0.03]\n  i_CODNO3 [defaults to -4.57143]\n  v3_SNOx [defaults to -0.07]\n  v12_SNHx [defaults to 0.066]\n  COD_H [defaults to 8]\n  m_StorAx [defaults to 0.1]\n  v7_SNOx [defaults to -0.28]\n  i_NO3N2 [defaults to 2.85714]\n  q_XCBSBhyd [defaults to 3]\n  i_TSSXCB [defaults to 0.75]\n  v7_SNHx [defaults to 0.066]\n  i_NXCB [defaults to 0.04]\n  i_ChargeSNOx [defaults to -0.0714286]"
    end
end
