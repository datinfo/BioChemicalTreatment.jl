"""
    ProcessDiagram(systems, equations; only_flow = true, printing_options...)

Draw a the process diagram resulting of the `systems` connected by `equations`.

!!! warning
    This feature is considered experimental and a nice looking diagram is not guaranteed!
    However, manual ordering of the elements can help with this.

!!! note
    Currently, only printing using TikZ/LaTeX is supported.

The systems are automatically positioned in a grid s.t. it should look nice.
The rows in the grid are named `chains`, the columns `columns`.

However, it can be adapted manually using the appropriate functions.

!!! note "Indexing"
    For adapting the diagram, generally indices are required.
    When using this, consider that it the indices are 1-based and start on the top left.
    I.e. the top chain (row) is at index 1, as is the most left column.

# Parameters

- `systems`: The vector containing all systems. Each one must be [`AbstractProcessElement`](@ref) or [`ModelingToolkit.ODESystem`](@extref :jl:type:`ModelingToolkit.ODESystem`)
- `equations`: The connection equations between the systems.
- `only_flow`: If only the flow connections are to be drawn. Defaults to `true`
- `printing_options`: Options for the displaying. See below for details.

# Printing Options

- `linewidth = "0.4pt"`*: The linewidth for the graph
- `nodedistance = ".5cm"`*: The spacing between the nodes/elements in the graph
- `blockwidth = "4.5cm"`*: The text width for the elements in the graph (wider text is scaled)
- `blockheight = "2em+10mm"`*: The text height for the elements in the graph (higher text is scaled)
- `innersep = "5mm"`*: The separation between the text and the shape in a node.
- `background = nothing`: The background color for the whole graph. `nothing` for transparent or valid color for LaTex.
- `print_connector_names = false`: If each name of the connector is supposed to be printed at the edge of the element where it connects.
    This is intended only for debugging, the names might be printed in very different sizes and also very small.

* Argument for tikz, provide as string with the appropriate unit. If you do arithmetics, wrap the whole in parenthesis.
"""
struct ProcessDiagram
    systems::AbstractVector{Union{AbstractProcessElement, ODESystem}}
    equations::AbstractVector{Equation}
    connection_properties
    chains
    shifts

    printing_options::Dict

    function ProcessDiagram(
        systems::AbstractVector{Union{AbstractProcessElement, ODESystem}}, 
        equations::AbstractVector{Equation}; 
        only_flow = true, 
        linewidth="0.4pt", nodedistance="1cm", blockwidth="4.5cm", blockheight="15mm", 
        innersep="2.5mm", background=nothing, print_connector_names=false
        )

        #### collect the printing options to a dict
        printing_options = Dict(pairs((; linewidth, nodedistance, blockwidth, blockheight,
            innersep, background, print_connector_names)))

        # Parse the systems and connections and assemble the ordering of the diagram

        # Extract metadata for all connection equations
        conn_props = metadata_connections(equations, systems)

        # Check if all systems are used in the connections and reciprocal
        conn_sys = unique([elem.parent for conn in conn_props for elem in conn])
        if !isempty(setdiff(conn_sys, systems))
            @warn "Not all systems used in the connections are in the systems vector. Missing systems are:\n\t$(join(nameof.(setdiff(conn_sys, systems)), "\n\t"))"
        end
        if !isempty(setdiff(systems, conn_sys))
            @warn "Not all systems in the systems vector are used in the connections. Missing systems are:\n\t$(join(nameof.(setdiff(systems, conn_sys)), "\n\t"))"
        end
        
        # filter out all non-flows if supposed to
        if only_flow 
            conn_props = [conn for conn in conn_props if conn[1].type == :flow]
        end

        # Check if there are any connections remaining, otherwise throw ArgumentError
        if isempty(conn_props)
            throw(ArgumentError("Invalid ProcessDiagram: There are no$(only_flow ? " flow" : "") connections given"))
        end

        # Assemble the chains of systems (always following first ports)
        chains = assemble_chains(conn_props)
        
        # Order the chains horizontally (compute the shifts)
        chains, horizontal_shifts = order_chains_horizontally(chains)

        # Make chains to contain AbstractProcessElements instead of the metadata
        chains = metadatachain_to_elementchain(chains)

        # Generate the Process-diagram
        pd = new(systems, equations, conn_props, chains, horizontal_shifts, printing_options)

        # Postprocess the element

        # Make verticals straight (as possible)
        straightify_verticals!(pd)

        # Squeeze the diagram as possible
        squeeze_vertically!(pd)

        # Final cleanup
        cleanup!(pd)

        # Return the element
        return pd
    end

    function ProcessDiagram(systems::AbstractVector, equations::AbstractVector; kwargs...)
        # Check if all elements are valid systems  ()
        !all(isa.(systems, Union{AbstractProcessElement, ODESystem})) && error("All systems must be of type AbstractProcessElement or ODESystem")
        systems = Vector{Union{AbstractProcessElement, ODESystem}}(systems)
        # Check if all given equations are valid equations 
        !all(isa.(equations, Equation)) && error("All given equations must be of type Equation")
        equations = Vector{Equation}(equations)
        ProcessDiagram(systems, equations; kwargs...)
    end
end

##### Functions for printing the diagram #####
"""
    set_linewidth!(pd, linewidth)

Set the linewidth for all lines when printing `pd`.

This is used as argument for tikz, provide as string with 
the appropriate units. If you do arithmetics, wrap the whole in parenthesis.
"""
function set_linewidth!(pd::ProcessDiagram, linewidth)
    pd.printing_options[:linewidth] = linewidth
    return pd
end
"""
    set_nodedistance!(pd, nodedistance)

Set the distance inbetween the blocks when printing `pd`.

This is used as argument for tikz, provide as string with 
the appropriate units. If you do arithmetics, wrap the whole in parenthesis.
"""
function set_nodedistance!(pd::ProcessDiagram, nodedistance)
    pd.printing_options[:nodedistance] = nodedistance
    return pd
end
"""
    set_blockwidth!(pd, blockwidth)

Set the width of the blocks when printing `pd`.

This is used as argument for tikz, provide as string with 
the appropriate units. If you do arithmetics, wrap the whole in parenthesis.
"""
function set_blockwidth!(pd::ProcessDiagram, blockwidth)
    pd.printing_options[:blockwidth] = blockwidth
    return pd
end
"""
    set_blockheight!(pd, blockheight)

Set the height of the blocks when printing `pd`.

This is used as argument for tikz, provide as string with 
the appropriate units. If you do arithmetics, wrap the whole in parenthesis.
"""
function set_blockheight!(pd::ProcessDiagram, blockheight)
    pd.printing_options[:blockheight] = blockheight
    return pd
end
"""
    set_innersep!(pd, innersep)

Set the inner separation of the nodes (i.e. the distance between the drawn box 
and the text) when printing `pd`.

This is used as argument for tikz, provide as string with 
the appropriate units. If you do arithmetics, wrap the whole in parenthesis.
"""
function set_innersep!(pd::ProcessDiagram, innersep)
    pd.printing_options[:innersep] = innersep
    return pd
end
"""
    set_background!(pd, background)

Set the background color when printing `pd`. Set to `nothing` for transparent.

This is used as argument for tikz, provide as string with a valid latex color definition or `nothing`.
"""
function set_background!(pd::ProcessDiagram, background)
    pd.printing_options[:background] = background
    return pd
end
"""
    print_connector_names!(pd, print_names=true)

Set if the name of each connector is supposed to be printed at the edge of the element where it connects.

This is helpful for debugging, but the names might be printed in very different sizes and also very small.
"""
function print_connector_names!(pd::ProcessDiagram, print_names=true)
    pd.printing_options[:print_connector_names] = print_names
    return pd
end

##### Functions for manipulating the positioning in an existing ProcessDiagram #####
### Chains ###
"""
    switch_chains!(pd::ProcessDiagram, idx1::Integer, idx2::Integer)

Switch two chains (rows) in the [`ProcessDiagram`](@ref).
The indices are 1-based and from the top.
"""
function switch_chains!(pd::ProcessDiagram, idx1::Integer, idx2::Integer)
    chain1 = pd.chains[idx1]
    shift1 = pd.shifts[idx1]

    pd.chains[idx1] = pd.chains[idx2]
    pd.shifts[idx1] = pd.shifts[idx2]
    pd.chains[idx2] = chain1
    pd.shifts[idx2] = shift1
    return pd
end
"""
    move_chain_vertically!(pd::ProcessDiagram, old_idx::Integer, new_idx::Integer)

Move a chain (row) vertically from `old_idx` to `new_idx` in the [`ProcessDiagram`](@ref).
The indices are 1-based and from the top.
"""
function move_chain_vertically!(pd::ProcessDiagram, old_idx::Integer, new_idx::Integer)
    chain = pd.chains[old_idx]
    shift = pd.shifts[old_idx]

    popat!(pd.chains, old_idx)
    popat!(pd.shifts, old_idx)
    insert!(pd.chains, new_idx, chain)
    insert!(pd.shifts, new_idx, shift)
    return pd
end
"""
    move_chain_horizontally!(pd::ProcessDiagram, chain_idx::Integer, shift::Integer)

Shift the chain (row) at `chain_idx` horizontally by `shift` in the [`ProcessDiagram`](@ref).
The indices are 1-based and from the top.
"""
function move_chain_horizontally!(pd::ProcessDiagram, chain_idx::Integer, shift::Integer)
    pd.shifts[chain_idx] += shift
    return pd
end
"""
    merge_chains!(pd::ProcessDiagram, idx1::Integer, idx2::Integer)

Merge the two chains (rows) at `idx1` and `idx2` in the [`ProcessDiagram`](@ref).
The indices are 1-based and from the top.
"""
function merge_chains!(pd::ProcessDiagram, idx1::Integer, idx2::Integer)
    lengths = pd.shifts[[idx1, idx2]] .+ length.(pd.chains[[idx1, idx2]])
    max_length = maximum(lengths)
    chain1 = [repeat([missing], pd.shifts[idx1]); pd.chains[idx1]; repeat([missing], max_length - lengths[1])]
    chain2 = [repeat([missing], pd.shifts[idx2]); pd.chains[idx2]; repeat([missing], max_length - lengths[2])]
    all(.!ismissing.(chain1) .|| .!ismissing.(chain2)) && throw(ArgumentError("Cannot merge chains $idx1 and $idx2. They share elements in the same column."))
    while !isempty(pd.chains[idx2]) # While there are elements in the second chain, move them to the first one
        move_elem!(pd, (idx2, pd.shifts[idx2]+1), (idx1, pd.shifts[idx2]+1))
    end
    # Remove the now empty second chain
    deleteat!(pd.chains, idx2)
    deleteat!(pd.shifts, idx2)
    return pd
end

### Columns ###
"""
    remove_column!(pd::ProcessDiagram, column_idx::Integer)

Remove column `column_idx` from the [`ProcessDiagram`](@ref) and return it.
The indices are 1-based and from the left.
Indices of columns with indices `>= column_idx` are decreased by `1`.

!!! warning
    This can mess up the [`ProcessDiagram`](@ref) and make it incomplete if non-empty columns are removed.
"""
function remove_column!(pd::ProcessDiagram, column_idx::Integer)
    column = []
    for i in eachindex(pd.chains)
        push!(column, get_elem(pd, i, column_idx))
        if column_idx <= pd.shifts[i]
            # On the left -> decrease shift
            pd.shifts[i] -= 1
        elseif pd.shifts[i] < column_idx && column_idx <= pd.shifts[i] + length(pd.chains[i])
            # Column is removed from within this chain delete the element
            deleteat!(pd.chains[i], column_idx - pd.shifts[i])
        else
            # On the right, do nothing
        end
    end
    # Return the removed column
    return column
end
"""
    insert_column!(pd::ProcessDiagram, column_idx::Integer, column::AbstractArray)

Insert `column` at `column_idx` into the [`ProcessDiagram`](@ref).
The indices are 1-based and from the left.
Indices of columns with indices `>= column_idx` are increased by `1`.

!!! warning
    This can mess up the [`ProcessDiagram`](@ref) and make it incomplete if new systems are inserted.
"""
function insert_column!(pd::ProcessDiagram, column_idx::Integer, column::AbstractArray)
    length(column) == length(pd.chains) || throw(ArgumentError("The given column is incompatible. It has length $(length(column)), but the process diagram has length $(length(pd.chains))"))
    for i in eachindex(pd.chains)
        insert_elem!(pd, i, column_idx, column[i])
    end
    return pd
end
"""
    insert_empty_columns!(chains, shifts, idx, n_cols = 1)

Insert empty columns (all `missing`) into the [`ProcessDiagram`](@ref) defined by `chains`, `shifts`.

!!! warning
    If you do some indexing into the `shifts`, do it using `view(shifts, idxs)` and not 
    directly (`shifts[idxs]`), as otherwise they are not updated.

# Parameters
- `chains`: The chains of the [`ProcessDiagram`](@ref)
- `shifts`: The shifts of the [`ProcessDiagram`](@ref)
- `idx`: The index to insert the empty column to
- `n_cols`: The number of empty columns to insert
"""
function insert_empty_columns!(chains, shifts, idx, n_cols = 1)
    for i in eachindex(chains)
        if idx <= shifts[i]
            shifts[i] += n_cols
        elseif idx <= shifts[i] + length(chains[i]) - 1
            insert!.(Ref(chains[i]), idx-shifts[i]+1:idx-shifts[i]+n_cols, missing)
        end
    end
end
"""
    insert_empty_columns!(pd, idx, n_cols = 1)

Insert empty columns into the [`ProcessDiagram`](@ref) `pd`.

# Parameters
- `pd`: The [`ProcessDiagram`](@ref) to insert
- `idx`: The index to insert the empty column to
- `n_cols`: The number of empty columns to insert
"""
function insert_empty_columns!(pd::ProcessDiagram, idx, n_cols = 1)
    insert_empty_columns!(pd.chains, pd.shifts, idx, n_cols)
    return pd
end
"""
    move_column!(pd::ProcessDiagram, old_idx::Integer, new_idx::Integer)

Move column at `old_idx` to `new_idx`.
The indices are 1-based and from the left.

Columns right of `old_idx`, are shifted to the left and the ones right of `new_idx` to the right.
Columns right of both are left where they are.
"""
function move_column!(pd::ProcessDiagram, old_idx::Integer, new_idx::Integer)
    column = remove_column!(pd, old_idx)
    insert_column!(pd, new_idx, column)
end
"""
    switch_columns!(pd::ProcessDiagram, idx1::Integer, idx2::Integer)

Switch columns at `idx1` and `idx2`.
The indices are 1-based and from the left.
"""
function switch_columns!(pd::ProcessDiagram, idx1::Integer, idx2::Integer)
    if idx1 < idx2
        move_column!(pd, idx2, idx1)
        move_column!(pd, idx1+1, idx2)
    else
        move_column!(pd, idx1, idx2)
        move_column!(pd, idx2+1, idx1)
    end
    return pd
end
"""
    isempty_column(pd::ProcessDiagram, column_idx::Integer)

Get if the column at `column_idx` is empty (does not contain any systems.)
The indices are 1-based and from the left.
"""
function isempty_column(pd::ProcessDiagram, column_idx::Integer)
    column = []
    for i in eachindex(pd.chains)
        push!(column, get_elem(pd, i, column_idx))
    end
    # Return if all are missing
    return all(ismissing.(column))
end

### Elements ###
"""
    get_elem(pd, chain_idx, column_idx)

Get the element at (`chain_idx`, `column_idx`). If there is none, return `missing`

`chain_idx` is the row, `column_idx` the column index.
The indices are relative to a grid and start from the top left (treated including empty positions).
"""
function get_elem(pd, chain_idx, column_idx)
    if column_idx <= pd.shifts[chain_idx]
        # On the left -> no elem there -> just return missing
        return missing
    elseif pd.shifts[chain_idx] < column_idx && column_idx <= pd.shifts[chain_idx] + length(pd.chains[chain_idx])
        # Column is got from within this chain
        return pd.chains[chain_idx][column_idx - pd.shifts[chain_idx]]
    else
        # On the right, just return missing
        return missing
    end
end
"""
    remove_elem!(pd, chain_idx, column_idx)

Remove the element at (`chain_idx`, `column_idx`) and return it. If there is none, return `missing`.
If an element is removed, it is replaced by `missing` and no elements are shifted.

`chain_idx` is the row, `column_idx` the column index.
The indices are relative to a grid and start from the top left (treated including empty positions).

!!! warning
    This can mess up the [`ProcessDiagram`](@ref) and make it incomplete if a non-missing element is removed.
"""
function remove_elem!(pd, chain_idx, column_idx)
    if column_idx <= pd.shifts[chain_idx]
        # On the left -> no elem there -> just return missing
        elem = missing
    elseif pd.shifts[chain_idx] < column_idx && column_idx <= pd.shifts[chain_idx] + length(pd.chains[chain_idx])
        # Column is removed from within this chain -> set to missing
        elem = pd.chains[chain_idx][column_idx - pd.shifts[chain_idx]]
        pd.chains[chain_idx][column_idx - pd.shifts[chain_idx]] = missing
        # Remove leading and trailing "missing"
        strip_chain!(pd, chain_idx)
    else
        # On the right, just return missing
        elem = missing
    end
    return elem
end
"""
    insert_elem!(pd, chain_idx, column_idx, elem)

Insert the element `elem` at (`chain_idx`, `column_idx`).
All elements right of this one, until the next missing one, are shifted to the right.

`chain_idx` is the row, `column_idx` the column index.
The indices are relative to a grid and start from the top left (treated including empty positions).

!!! warning
    This can mess up the [`ProcessDiagram`](@ref) and make it incomplete if a non-existant element is inserted.
"""
function insert_elem!(pd, chain_idx, column_idx, elem)
    if isempty(pd.chains[chain_idx])
        # If the chain is empty, simply add the element and set the shift
        push!(pd.chains[chain_idx], elem)
        pd.shifts[chain_idx] = column_idx-1
        return
    end
    if column_idx <= pd.shifts[chain_idx]
        # Column is inserted on the left of this chain
        # If it is missing, do not do anything (there is already a missing value inplace)
        ismissing(elem) && return
        # Prepend missing values and the column as first
        prepend!(pd.chains[chain_idx], repeat([missing], pd.shifts[chain_idx] - column_idx))
        pushfirst!(pd.chains[chain_idx], elem)
        # Set the shift
        pd.shifts[chain_idx] = column_idx - 1
    elseif column_idx <= pd.shifts[chain_idx] + length(pd.chains[chain_idx])
        # Column is inserted within this chain
        insert!(pd.chains[chain_idx], column_idx - pd.shifts[chain_idx], elem)
        # Delete next missing value to only shift until it
        next_miss = findfirst(ismissing, pd.chains[chain_idx][column_idx - pd.shifts[chain_idx] + 1:end])
        isnothing(next_miss) || deleteat!(pd.chains[chain_idx], column_idx - pd.shifts[chain_idx] + next_miss)
    else
        # Column is inserted on the right of this chain
        # Missing Value -> do nothing
        ismissing(elem) && return
        # Append missing values and the column as last
        append!(pd.chains[chain_idx], repeat([missing], column_idx - (pd.shifts[chain_idx] + length(pd.chains[chain_idx])) - 1))
        push!(pd.chains[chain_idx], elem)
    end
    return pd
end
"""
    move_elem!(pd, from, to)

Move the element currently at position `from` to position `to`. The positions are to given as tuples of `(chain_idx, column_idx)`.

`chain_idx` is the row, `column_idx` the column index.
The indices are relative to a grid and start from the top left (treated including empty positions).
"""
function move_elem!(pd::ProcessDiagram, from::Tuple, to::Tuple)
    insert_elem!(pd, to..., remove_elem!(pd, from...))
end
"""
    switch_elem!(pd, first, second)

Switch the elements at positions `first` and `second`. The positions are to given as tuples of `(chain_idx, column_idx)`.

`chain_idx` is the row, `column_idx` the column index.
The indices are relative to a grid and start from the top left (treated including empty positions).
"""
function switch_elem!(pd::ProcessDiagram, first::Tuple, second::Tuple)
    elem1 = remove_elem!(pd, first...)
    elem2 = remove_elem!(pd, second...)
    insert_elem!(pd, second..., elem1)
    insert_elem!(pd, first..., elem2)
end

##### Functions for cleaning up an existing ProcessDiagram #####
"""
    strip_chain!(pd, chain_idx)

Strip the chain (row) at `chain_idx` by removing all leading and trailing `missing` and adapting the shift to retain the same positioning.

The indices are 1-based and start from the top.
"""
function strip_chain!(pd::ProcessDiagram, chain_idx::Integer)
    # Get index vector for missing elements
    miss = ismissing.(pd.chains[chain_idx])

    # Get the number of missing in the beginning/ending
    n_miss_begin = length([1 for idx in eachindex(miss) if all(miss[1:idx])])
    n_miss_end = length([1 for idx in eachindex(miss) if all(miss[idx:end])])
    # Remove the missing values and shift appropriately
    pd.chains[chain_idx] = pd.chains[chain_idx][n_miss_begin+1:end - n_miss_end]
    pd.shifts[chain_idx] += n_miss_begin
    return pd
end
"""
    strip_chains!(pd)

Strip all chains in the [`ProcessDiagram`](@ref) `pd`. See [`strip_chain!`](@ref) for details on stripping.
"""
function strip_chains!(pd::ProcessDiagram)
    for idx in eachindex(pd.chains)
        strip_chain!(pd, idx)
    end
    return pd
end
"""
    remove_empty_chains!(pd)

Remove all empty chains (rows) in the [`ProcessDiagram`](@ref) `pd`.
"""
function remove_empty_chains!(pd::ProcessDiagram)
    empty_chains = findall(isempty, pd.chains)
    # Sort and reverse to not have indexing errors (e.g. if index `1` is removed, the old `2` is now `1`...)
    for idx in reverse(sort(empty_chains))
        deleteat!(pd.chains, idx)
        deleteat!(pd.shifts, idx)
    end
    return pd
end
"""
    remove_empty_columns!(pd)

Remove all empty columns in the [`ProcessDiagram`](@ref) `pd`.
"""
function remove_empty_columns!(pd::ProcessDiagram)
    # Go through all columns
    for col in 1:maximum(pd.shifts .+ length.(pd.chains)) 
        if isempty_column(pd, col)
            remove_column!(pd, col)
        end
    end
    pd
end
"""
    cleanup!(pd)

Clean up the [`ProcessDiagram`](@ref) `pd` by removeing all empty chains and columns.

Meaning:
1. Strip all chains ([`strip_chains!`](@ref))
2. Remove all empty chains ([`remove_empty_chains!`](@ref))
3. Remove all empty columns ([`remove_empty_columns!`](@ref))
"""
function cleanup!(pd::ProcessDiagram)
    strip_chains!(pd)
    remove_empty_chains!(pd)
    remove_empty_columns!(pd)
end

##### Helper Functions #####
"""
    metadatachain_to_elementchain(chains::AbstractVector{<:AbstractVector})

Transform each [`PortMetadata`](@ref) in the chains to its parent.
If the element is not a [`PortMetadata`](@ref), leave as it is.
"""
function metadatachain_to_elementchain(chains::AbstractVector{<:AbstractVector})
    eltype(typeof(chains))[eltype(typeof(chain))[elem isa PortMetadata ? elem.parent : elem for elem in chain] for chain in chains]
end

"""
    inchain(elem, chains)

Get if `elem` is contained in the `chains`.
"""
function inchain(elem, chains)
    elem in skipmissing([(metadatachain_to_elementchain(chains)...)...])
end
"""
    inchain(elem::PortMetadata, chains)

Get if the parent of `elem` is contained in the `chains`.
"""
function inchain(elem::PortMetadata, chains)
    inchain(elem.parent, chains)
end

"""
    get_element_chains(elem, chains::AbstractVector)

Get all indices of the chains (row indices) which contain `elem`.
"""
function get_element_chains(elem, chains::AbstractVector) # Get the row index of the element in the chains
    findall([(!ismissing(elem) && elem in skipmissing(chain)) for chain in metadatachain_to_elementchain(chains)])
end
"""
    get_element_chains(elem::PortMetadata, chains::AbstractVector)

Get all indices of the chains (row indices) which contain the parent of `elem`.
"""
function get_element_chains(elem::PortMetadata, chains::AbstractVector) # Get the row index of the element in the chains
    get_element_chains(elem.parent, chains)
end

"""
    get_element_positions(chains, shifts, elem)

Get all positions where `elem` is found in `chains` considering the chain shifts in `shifts`.

Returns a vector with tuples of the form (`chain_idx`, `column_idxs`) for each chain containing `elem`. 
`chain_idx` is the index of the chain, `column_idxs` is an **array** with all columns in this chain which contain this elem. 
"""
function get_element_positions(chains, shifts, elem) # Get the index (x, y) of the element, x is the horizontal position (in "nodes"), y the chain number (vertical position in nodes)
    chain_idx = get_element_chains(elem, chains)
    x = [shifts[idx] .+ findall(isequal(elem), chains[idx]) for idx in chain_idx]
    collect(zip(chain_idx, x))
end
function get_element_positions(chains, shifts, elem::PortMetadata) # Get the index (x, y) of the element, x is the horizontal position (in "nodes"), y the chain number (vertical position in nodes)
    get_element_positions(chains, shifts, elem.parent)
end
"""
    get_element_positions(pd::ProcessDiagram, elem)

Get all positions where `elem` is found in the chains of `pd`. See [`get_element_positions`](@ref) for details.
"""
function get_element_positions(pd::ProcessDiagram, elem) # Get the index (x, y) of the element, x is the horizontal position (in "nodes"), y the chain number (vertical position in nodes)
    get_element_positions(pd.chains, pd.shifts, elem)
end

"""
    get_connected(conn_props, elem)

Get all elements connected by the `conn_props` to the given element `elem`.
"""
function get_connected(conn_props, elem)
    connected1 = [conn[2].parent for conn in conn_props if conn[1].parent == elem]
    connected2 = [conn[1].parent for conn in conn_props if conn[2].parent == elem]
    unique([connected1; connected2])
end
"""
    get_connected(conn_props, elem::PortMetadata)

Get all elements connected by the `conn_props` to the parent of `elem`.
"""
function get_connected(conn_props, elem::PortMetadata)
    get_connected(conn_props, elem.parent)
end

##### Functions to construct the diagram and for ordering the elements #####

"""
    metadata_connections(eqns, systems)

Transform the connection equations (made using `connect()`) to tuples of [`PortMetadata`](@ref) of the connected ports.

!!! note
    Considers only "connection" equations (the ones made using `connect()`) and all others are filtered out
"""
function metadata_connections(eqns, systems)
    # TODO How to handle non-`connect()` equations?
    # Filter out non-connection equations
    conn_eqns = [eqn for eqn in eqns if eqn.lhs isa Connection && eqn.rhs isa Connection]

    # Transform connections to tuple of connected systems (only right hand side of the connection equations contain systems)
    connection_sys = getproperty.(getproperty.(conn_eqns, :rhs), :systems)
    
    # Get metadata for each system (contains info about the connector for BioChemicalTreatment connectors)
    [(get_portmetadata(conn[1], systems), get_portmetadata(conn[2], systems)) for conn in connection_sys]
end

"""
    get_portmetadata(sys, systems)

Get the port metadata of the system. 

If it is already in the metadata of `sys`, simply return it.

If not, find the parent system in `systems` and use [`assemble_port_metadata_fields`](@ref) to get the associated metadata.
A system in `systems` is considered a potential parent of `sys` if:
- The name of `sys` (including namespacing) starts with the name of the parent
- If there are multiple, take the one with the shortest name (the most upper parent)
"""
function get_portmetadata(sys, systems)
    # If has proper metadata -> return it
    ModelingToolkit.get_metadata(sys) isa PortMetadata && return ModelingToolkit.get_metadata(sys)
    # Otherwise construct new metadata
    # Find parent
    parent = missing # Default in missing
    pot_parents = systems[lstrip_namespace.(nameof(sys), nameof.(systems)) .!= string(nameof(sys))] # Potential parents names can be stripped of the left of the name (are contained in the namespace)
    !isempty(pot_parents) && (parent = pot_parents[last(findmin(length, string.(nameof.(pot_parents))))]) # Get the potential parent with the shortest name
    # Assemble the metadata
    PortMetadata(assemble_port_metadata_fields(sys, parent)..., ModelingToolkit.get_metadata(sys))
end

"""
    assemble_chains(metadata_connections; conn_type_precedence = [[:flow], [:state, :rate, :exogenous, :unknown]])

Assemble chains (considered straight rows) of systems from the given metadata connections and directly order them vertically.

# Parameters
- `metadata_connections`: The connections that define the `ProcessDiagram`.
- `conn_type_precedence`: The precedences between the different connectors. First only connections of the first precedence type are considered, then of the second etc.
    Is a vector of vectors the inner of which contain the types of same precedence.

# Assembling a chain
To assemble a chain it is proceeded as follows (details below):
1. Find a first element for the chain
2. Find next elements in the chain
3. Order the chain vertically into the already assembled chains

## Finding the first element
Determined as (First rule that matches a system):
1. Systems that have only outputs and no inputs (in this precedence type)
2. Any that has an output (in this precedence type)
3. Any that has a connector (in- or output) in this precedence type
If none is found, go to the next precedence types or stop

For all of these rules, they is always first checked for systems already in the chain before going to new ones.

## Finding the next elements in the chain
Add new systems to the chain, until no matching is found anymore.

A new system is considered matching if and only if:
- The system is connected at output port 1 with a connection of the given precedence type.
- The system is not yet in the current chain
- The system is not yet in any already assembled chain

For the last element in the chain, an exception is made and it can be already in an assembled chain *or* the first one of the current chain.

Like this, the first and last elements in each chain can occur in an other chain and then indicate that there is a connection between them.
This is later used to horizontally order the chains.

## Ordering the chain vertically into the already ordered chains
This is done using the following heuristic:

Default to the bottom (last index), then move up one by one as long as any of the following holds (checked in this order):
- No connection to the next upper one and none to any of the lower ones
- No connection to any lower one but a connection between any lower one and an upper one
- No connection to any lower one but the next upper one is connected to a lower one
"""
function assemble_chains(metadata_connections; conn_type_precedence = [[:flow], [:state, :rate, :exogenous, :unknown]])
    # Assemble chains with single straights, always first outputs are assumed straight
    conn_remaining = copy(metadata_connections) # Get the remaining connections (first all, some to be removed)
    chains = Vector{Any}[] # Initialize empty vector for the chains
    conn_type = popfirst!(conn_type_precedence) # Get the highest precedence types
    higher_precedence_types = []
    while !isempty(conn_remaining)

        ##### Get the first system for the new chain #####
        # Determined as (take first that maches that matches a system, first checked for systems already in the chain):
        # 1. Systems that have only outputs and no inputs (in this precedence type)
        # 2. Any that has an output (in this precedence type)
        # 3. Any that has a connector (in- or output) in this precedence type
        # If none is found, go to the next precedence types or stop

        # Systems that do not have inputs of this type (precedence to that are already in a chain): Get all output connections of systems which occur only once as outputs
        first_sys = [sys for sys in [(conn_remaining...)...] if inchain(sys, chains) && sys.type in conn_type && !sys.isinput && sum([sys.parent == sys_cmp.parent && sys_cmp.type in conn_type for sys_cmp in [(conn_remaining...)...]]) == 1]
        isempty(first_sys) && (first_sys = [sys for sys in [(conn_remaining...)...] if sys.type in conn_type && !sys.isinput && sum([sys.parent == sys_cmp.parent && sys_cmp.type in conn_type for sys_cmp in [(conn_remaining...)...]]) == 1])
        # If there is none, take all that are outputs (precedence to that are already in a chain)
        isempty(first_sys) && (first_sys = [sys for sys in [(conn_remaining...)...] if inchain(sys, chains) && sys.type in conn_type && !sys.isinput])
        isempty(first_sys) && (first_sys = [sys for sys in [(conn_remaining...)...] if sys.type in conn_type && !sys.isinput])
        # As a last resort, if there is none, just take any (precedence to that are already in a chain)
        isempty(first_sys) && (first_sys = [sys for sys in [(conn_remaining...)...] if inchain(sys, chains) && sys.type in conn_type])
        isempty(first_sys) && (first_sys = [sys for sys in [(conn_remaining...)...] if sys.type in conn_type])
        # If there is none, go for next precedence or stop
        if isempty(first_sys)
            isempty(conn_type_precedence) && break # No precedence left -> stop
            # Go to next lower precedence
            append!(higher_precedence_types, conn_type)
            conn_type = popfirst!(conn_type_precedence)
            continue
        end

        ##### Assemble the new chain #####
        # Add a new chain, starting with the first element
        # Then, add always system at output port 1 (of the given precedence) to the chain.
        # If another output port is connected, this will be started in a new chain
        # Systems are only added if they are not yet in the current chain or in any of the chains with higher precedence

        new_chain = Any[first(first_sys)] # Start with the first system (the first of the ones matching)
        last_elem = nothing
        updated = true
        while updated # While the last system in the chain connects with output port 1 to another, add the system to the chain
            updated = false
            last_elem = nothing
            for (i, conn) in enumerate(conn_remaining)
                # If the connection does not have the correct type, ignore it
                conn[1].type in conn_type || conn[2].type in conn_type || continue
                # If to check for both directions of the connection
                if conn[1].parent == last(new_chain).parent && conn[2].isinput && !inchain(conn[2], [new_chain[2:end]])
                    # Check if the connection is straight or the first one in the new chain
                    #TODO: Currently assuming first output is straight.  Add to metadata which one can be straight?
                    if conn[1].index == 1 || length(new_chain) == 1
                        # If the element is already the first in this chain, it is a potential last element (but not allowed in the middle)
                        if conn[2].parent == first(new_chain).parent
                            last_elem = (i, conn[2])
                            continue
                        end
                        # If the element is already in the middle of another chain, mark as potential last element (not allowed in the middle of a chain)
                        if inchain(conn[2], chains)
                            contain_chains = metadatachain_to_elementchain(chains[get_element_chains(conn[2], chains)])
                            if conn[2].parent in [(getindex.(contain_chains, UnitRange.(2, lastindex.(contain_chains).-1))...)...]
                                last_elem = (i, conn[2])
                                continue
                            end
                        end
                        # Add the new element, remove from the remaining ones and continue looking for the next one
                        push!(new_chain, conn[2])
                        deleteat!(conn_remaining, i)
                        updated = true
                        break
                    end
                elseif conn[2].parent == last(new_chain).parent && conn[1].isinput && !inchain(conn[1], [new_chain[2:end]])
                    # Check if the connection is straight or the first one in the new chain
                    #TODO: Currently assuming first output is straight. Add to metadata which one can be straight?
                    if conn[2].index == 1 || length(new_chain) == 1
                        # If the element is already the first in this chain, it is a potential last element (but not allowed in the middle)
                        if conn[1].parent == first(new_chain).parent
                            last_elem = (i, conn[1])
                            continue
                        end
                        # If the element is already in the middle of another chain, mark as potential last element (not allowed in the middle of a chain)
                        if inchain(conn[1], chains)
                            contain_chains = metadatachain_to_elementchain(chains[get_element_chains(conn[1], chains)])
                            if conn[1].parent in [(getindex.(contain_chains, UnitRange.(2, lastindex.(contain_chains).-1))...)...]
                                last_elem = (i, conn[1])
                                continue
                            end
                        end
                        # Add the new element, remove from the remaining ones and continue looking for the next one
                        push!(new_chain, conn[1])
                        deleteat!(conn_remaining, i)
                        updated = true
                        break
                    end
                end
            end
        end
        # Add the last element if not nothing
        if !isnothing(last_elem)
            push!(new_chain, last_elem[2])
            deleteat!(conn_remaining, last_elem[1])
        end

        # If the new chain has length 2 and both elements are already in a chain, Skip adding the new chain, as only an internal connection
        if length(new_chain) == 2 && inchain(first(new_chain), chains) && inchain(last(new_chain), chains)
            continue
        end

        ##### Order the chain vertically into the already existing ones #####
        # Heuristic:
        # Default on the bottom, then move up one by one as long as any of the following holds (in this order):
        # - No connection to the next upper one and none to any of the lower ones 
        # - No connection to any lower one but a connection between any lower one and an upper one
        # - No connection to any lower one but the next upper one is connected to a lower one

        # Find the index to insert the chain at (vertical ordering)
        chainidx = lastindex(chains)+1 # Default at last position
        connects_to = unique([(get_element_chains.(new_chain[[1, end]], Ref(chains))...)...]) # Find the chains this chain connects to
        if !isempty(connects_to) # If not connected to any of the existing chains, just add at the last position
            # Shift upwards when certain conditions are met and not yet at top
            while chainidx > firstindex(chains)
                # If not connected to the directy upper one nor any of the lower ones
                if !any(connects_to .== chainidx - 1) && !any(connects_to .>= chainidx)
                    # -> Shift up
                    chainidx -= 1
                    continue
                end
                # If not connected to any lower ones (but to the directly upper one as already checked)
                if !any(connects_to .>= chainidx)
                    # Get the chains the upper and lower ones connect to
                    connects_to_up = [unique([(get_element_chains.(row, Ref(chains))...)...]) for row in eachrow([first.(chains[1:chainidx-1]) last.(chains[1:chainidx-1])])] # Find the chains the upper chains connects to
                    connects_to_low = [unique([(get_element_chains.(row, Ref(chains))...)...]) for row in eachrow([first.(chains[chainidx:end]) last.(chains[chainidx:end])])] # Find the chains the lower chains connects to
                    # If any of the lower ones is connected to an upper one
                    if any(unique([(connects_to_low...)...]) .< chainidx)
                        # -> Shift up
                        chainidx -= 1
                        continue
                    end
                    # If the next upper one is connected to a lower one
                    if any(connects_to_up[end] .>= chainidx)
                        # -> Shift up
                        chainidx -= 1
                        continue
                    end
                end
                break
            end
        end
        # Insert the new chain
        insert!(chains, chainidx, new_chain)
    end
    return chains
end

"""
    find_reference_chains(chains)

Find the indices of the reference chains to start horizontally ordering the other chains.

Chosen as (first that matches):
- First **and** last elements in the chain do not occur in other chains (multiple can match).
- First **or"" last element in the chain does not match another chain. If multiple, pick the longest one.
- Pick the longest chain
"""
function find_reference_chains(chains)
    # Check for all if they connect to another one at first/last of the chain
    match_first = [any([first(chain).parent == comp_elem.parent for comp_chain in chains for comp_elem in comp_chain if comp_chain != chain]) for chain in chains]
    match_last = [any([last(chain).parent == comp_elem.parent for comp_chain in chains for comp_elem in comp_chain if comp_chain != chain]) for chain in chains]
    # Pick those with neither first nor last one matching as reference
    references = findall(.!match_first .&& .!match_last)
    isempty(references) || return references # If there is any, return it

    # If none exists take the longest one which does not match on one side
    matching_one_end = findall(.!match_first .|| .!match_last)
    if !isempty(matching_one_end)
        return [matching_one_end[findmax(length, chains[matching_one_end])[2]]]
    else
        # Otherwise just pick the longest one
        return [findmax(length, chains)[2]]
    end
end
"""
    order_chains_horizontally(chains)

Get the horizontal shifts to move the chains on the grid.

Logic: 
1. Find a reference chain (see [`find_reference_chains`](@ref))
2. While not all chains are ordered, pick a random (not yet ordered) chain to order from a pot (initially containing all unordered chains)
    - If the first or last element of this chain occurs in the middle of any unordered chain
        - Remove this one from the pot 
        - Put the ones this connects to the middle of back into the pot
        - Pick a new one
    - If there is no match of the first or last element in this chain with an already ordered one
        - Remove this one from the pot
        - If there are no new ones to pick, pick new reference chains from all not yet ordered ones and put all not yet ordered ones back into the pot
        - Pick a new one 
    - Find the horizontal index of the first and last elements in the already ordered chains
    - If the chain has only one or two elements and all of them are contained in others
        - Remove it (as it is an internal connection of another chain) 
        - Pick a new one
    - If only one end of the chain matches
        - Make it the first one (reverse if it is the last one)
        - Insert missing columns of the length of this chain (no vertical overlap with any except the first element)
        - Remove the first element from the chain (the same as contained in the already ordered chain)
        - Set the shift to have the first element overlapping with the matching one
    - If both ends of the chain match
        - If the first is more to the right as the last
            - Reverse the chain
        - If neccessary, insert as much columns s.t. the first and last element can match
        - Remove the first and last elements from the chain (the same as contained in the already ordered chain)
        - Set the shift to have the first element overlapping with the matching one

The random picking of chains is done intelligently: If a chain is not yet to be ordered, it is removed from the pot. 
As soon as a new chain has been ordered, all non-ordered chains are put back into the pot.

Returns the new chains (redundant removed and reversed where neccessary) and the shifts to shift the chains horizontally.
"""
function order_chains_horizontally(chains)
    # Horizontal ordering of chains
    chain_shifts = Vector{Union{Missing, Int}}(missing, size(chains)) # Initialize shifts as missing
    # Set shift of reference chains to 0
    chain_shifts[find_reference_chains(chains)] .= 0

    chain_shifts_tmp = copy(chain_shifts) # Temporary chain_shifts to avoid picking non-connected ones mutliple times
    while any(ismissing.(chain_shifts)) # While there is any non-associated chain
        # Pick a random chain where the shift is missing
        i = findall(ismissing, chain_shifts_tmp)[round(Int, rand()*count(ismissing, chain_shifts_tmp) + 0.5)]

        # If first or last element occurs in the middle of a not yet ordered chain -> pick new one
        first_matching_middle = [any([chains[i][1].parent == comp_elem.parent for comp_elem in chain[2:end-1]]) for chain in chains[ismissing.(chain_shifts)]]
        last_matching_middle = [any([chains[i][end].parent == comp_elem.parent for comp_elem in chain[2:end-1]]) for chain in chains[ismissing.(chain_shifts)]]
        if any(first_matching_middle) || any(last_matching_middle)
            # If these ones are already discarded, put them one back into the pot
            chain_shifts_tmp[ismissing.(chain_shifts)][first_matching_middle .|| last_matching_middle] .= missing
            # Remove the current one from the pot
            chain_shifts_tmp[i] = -1
            # Continue the horizontal ordering with the new status
            continue
        end

        # Find the connection of the first, resp. last element in the chain to order (i) with the already ordered chains
        first_match = [findfirst([!ismissing(comp_elem) && chains[i][1].parent == comp_elem.parent for comp_elem in chain]) for chain in chains[.!ismissing.(chain_shifts)]]
        last_match = [findfirst([!ismissing(comp_elem) && chains[i][end].parent == comp_elem.parent for comp_elem in chain]) for chain in chains[.!ismissing.(chain_shifts)]]
        # If there is none with a first or last match -> pick a new one
        if all(isnothing.(first_match)) && all(isnothing.(last_match))
            # Set a dummy (non-missing) value to the temporary picking array to avoid re-picking this element
            chain_shifts_tmp[i] = -1
            if !any(ismissing.(chain_shifts_tmp))
                # If there is none remaining, find a new reference chain for the non-yet ordered ones
                new_ref = find_reference_chains(chains[ismissing.(chain_shifts)])
                # Set the corresponding ones to 0 (need to take the subset of the missing ones)
                chain_shifts[ismissing.(chain_shifts)][new_ref] .= 0
                # Reset the temporary chain shifts
                chain_shifts_tmp = copy(chain_shifts)
            end
            # Continue the horizontal ordering with the new status
            continue
        end

        # If there are chains matching both ends, restrict to them
        match_both = .!isnothing.(first_match) .&& .!isnothing.(last_match)
        if any(match_both)
            first_match = first_match[match_both]
            last_match = last_match[match_both]
        end
        # Pick any to order with (first one which has a match)
        first_match, match_first = findmax(replace(first_match, nothing => typemin(Int)))
        last_match, match_last = findmin(replace(last_match, nothing => typemax(Int)))

        # If nothing found (typemax/typemin), set to nothing
        first_match == typemin(Int) && (first_match = nothing)
        last_match == typemax(Int) && (last_match = nothing)
        
        # Get the index to compare: then indexing into match_both and comp_idx
        comp_idx = findall(.!ismissing.(chain_shifts)) # Of the ones not missing in the temporary shift
        any(match_both) && (comp_idx = comp_idx[match_both]) # If any have both matching, restrict to those

        # Add the chain shifts to the match indexes        
        !isnothing(first_match) && (first_match += chain_shifts[comp_idx[match_first]])
        !isnothing(last_match) && (last_match += chain_shifts[comp_idx[match_last]])

        # If the chain length is less or equal to the first/last matches (one or two elems which both match), delete the chain (both elements are already contained)
        if length(chains[i]) <= !isnothing(first_match) + !isnothing(last_match)
            deleteat!(chains, i)
            deleteat!(chain_shifts, i)
            # We picked a new chain -> Reset the temporary chain shifts for the next ones
            chain_shifts_tmp = copy(chain_shifts);
            continue
        end

        # Both nothing cannot occur here: already handled above
        if isnothing(first_match) && !isnothing(last_match)
            # The most left does not match, but the most right one does
            # -> Reverse the chain and shift it
            reverse!(chains[i])
            insert_empty_columns!(
                view(chains, .!ismissing.(chain_shifts)),
                view(chain_shifts, .!ismissing.(chain_shifts)),
                last_match - 1,
                length(chains[i]) - 2
                )
            chain_shifts[i] = last_match- 1
            popfirst!(chains[i]) # Remove the first part of the chain (which is the connection to the chain already there)

            # We updated the new chain -> Reset the temporary chain shifts for the next ones
            chain_shifts_tmp = copy(chain_shifts);
            continue
        elseif !isnothing(first_match) && isnothing(last_match) 
            # The most left one matches, but the most right one not
            # -> only shift the chain
            insert_empty_columns!(
                view(chains, .!ismissing.(chain_shifts)),
                view(chain_shifts, .!ismissing.(chain_shifts)),
                first_match - 1,
                length(chains[i]) - 2
                )
            chain_shifts[i] = first_match - 1
            popfirst!(chains[i]) # Remove the first part of the chain (which is the connection to the chain already there)

            # We updated the new chain -> Reset the temporary chain shifts for the next ones
            chain_shifts_tmp = copy(chain_shifts);
            continue
        end
        # Both match
        if last_match < first_match 
            # The chain to add is reversed -> reverse it and treat as non-reversed
            reverse!(chains[i])
            tmp = first_match
            first_match = last_match
            last_match = tmp
        end

        # If the chain is longer than the in-between parts, insert empty columns
        if last_match - first_match < length(chains[i]) - 2
            insert_empty_columns!(
                view(chains, .!ismissing.(chain_shifts)),
                view(chain_shifts, .!ismissing.(chain_shifts)),
                first_match - 1,
                length(chains[i]) - 3 - (last_match - first_match)
                )
        end

        # Shift appropriately and remove first and last chain elements (the connections to the already existing chain)
        chain_shifts[i] = first_match - 1
        popfirst!(chains[i])
        pop!(chains[i])

        # We updated the new chain -> Reset the temporary chain shifts for the next ones
        chain_shifts_tmp = copy(chain_shifts);
    end

    # Return the chains and the corresponding horizontal shifts
    return chains, chain_shifts
end

"""
    straightify_verticals!(pd)

Shift full chains to make the vertical connections in the `ProcessDiagram` as straight as possible.

# Heuristic
Go over all chains and do (in this sequence):
1. If one of the endings is not connecting (starting/ending node) -> do not shift
2. If both ends are connected in one and the same direction only -> shift by the minimal shift in this direction
3. If no end has a non-straight connection to a middle element -> do not shift
4. If any end has a non-straight connection to a middle element and all of those are in the same direction -> shift by the most often occuring shift in this direction
"""
function straightify_verticals!(pd::ProcessDiagram)
    straightified = true
    while straightified
        straightified = false
        for (idx, chain) in enumerate(pd.chains)
            # Get the positions of start/end
            pos_first = only(get_element_positions(pd, first(chain)))
            pos_last = only(get_element_positions(pd, last(chain)))
            # Get the connected elements which are not in the same chain
            connected_elems_first = [elem for elem in get_connected(pd.connection_properties, first(chain)) if !inchain(elem, [chain])]
            connected_elems_last = [elem for elem in get_connected(pd.connection_properties, last(chain)) if !inchain(elem, [chain])]
            # If it has a starting/ending node (any that has no connection to outside the chain)
            if isempty(connected_elems_first) || isempty(connected_elems_last)
                # Skip this chain
                continue
            end
            # Get the positions of the elements connected to the first/last 
            conn_pos_first = only.(get_element_positions.(Ref(pd), connected_elems_first))
            conn_pos_last = only.(get_element_positions.(Ref(pd), connected_elems_last))
            # Get the chain indices of the connected
            chains_first = getindex.(conn_pos_first, 1)
            chains_last = getindex.(conn_pos_last, 1)
            # Get the columns indices of the connected
            cols_first = only.(getindex.(conn_pos_first, 2))
            cols_last = only.(getindex.(conn_pos_last, 2))
            # Get the relative shifts of the connected to the current
            shift_first = cols_first .- only(pos_first[2])
            shift_last = cols_last .- only(pos_last[2])

            # If all shifts are positive/negative -> shift the chain in the direction
            if all(shift_first .> 0) && all(shift_last .> 0)
                pd.shifts[idx] += minimum([shift_first; shift_last])
                straightified = true
                continue
            elseif all(shift_first .< 0) && all(shift_last .< 0)
                pd.shifts[idx] += maximum([shift_first; shift_last])
                straightified = true
                continue
            end
            # Get if the connections are to a middle element
            to_middle_first = [all(Ref(elem) .!= pd.chains[chain_idx][[1, end]]) for (elem, chain_idx) in zip(connected_elems_first, chains_first)]
            to_middle_last = [all(Ref(elem) .!= pd.chains[chain_idx][[1, end]]) for (elem, chain_idx) in zip(connected_elems_last, chains_last)]
            # Get straight to a middle element
            s_to_middle_first = to_middle_first .&& shift_first .== 0
            s_to_middle_last = to_middle_last .&& shift_last .== 0
            # Get non-straight to a middle element
            ns_to_middle_first = to_middle_first .&& shift_first .!= 0
            ns_to_middle_last = to_middle_last .&& shift_last .!= 0
            # If it has no non-straight connection to a middle element
            if !any(ns_to_middle_first) && !any(ns_to_middle_last)
                # Skip this chain
                continue
            end
            # If all shifts have the same sign, shift it by the most occurring value of it in the right direction
            if all(shift_first[ns_to_middle_first] .> 0) && all(shift_last[ns_to_middle_last] .> 0) || all(shift_first[ns_to_middle_first] .< 0) && all(shift_last[ns_to_middle_last] .< 0)
                # Count number of occurrencies
                d = Dict{Int, Int}()
                for val in [shift_first[to_middle_first]; shift_last[to_middle_last]]
                    d[val] = get(d, val, 0) + 1
                end
                # Sort to array of pairs according to the absolute shifts (1st column)
                # Sorting such that the first index of maximum occurrence has lowest absolute shift later
                d = sort(collect(d); lt=(x, y) -> isless(abs(x[1]), abs(y[1])))
                # Find first index of the maximum occurrence (is minimum absolute shift, as sorted w.r.t. them)
                shift = last(findmax(x -> x[2], d))
                # Get the shift for the index
                shift = d[shift][1]
                # Do the shift if not 0
                if shift != 0
                    pd.shifts[idx] += shift
                    straightified = true
                    continue
                end
            end
        end
    end
end

"""
    get_blocking(pd::ProcessDiagram, chain_idx::Integer, ref_chain::Integer)

Get a boolean array which is true where the chain at index `idx` blocks the space for the elements in `ref_chain`.
A chain is blocking each index which is:
- Occupied by an element
- A connection going to the left/right, but not to the reference chain
"""
function get_blocking(pd::ProcessDiagram, chain_idx::Integer, ref_chain::Integer)
    # Get placeholder for all blocking elements
    blocking = repeat([false], maximum(pd.shifts .+ length.(pd.chains)))
    for i in eachindex(blocking)
        # Get the element at this position
        elem = get_elem(pd, chain_idx, i)
        # If there is any, it is blocking and continue
        (blocking[i] = !ismissing(elem)) && continue
        # If there is none, check next elements to the left/right
        # Start with left
        left_elem_idx = i <= pd.shifts[chain_idx] ? nothing : findlast(!ismissing, pd.chains[chain_idx][1:min(i - pd.shifts[chain_idx], end)])
        if !isnothing(left_elem_idx)
            # Get the left elem
            left_elem = pd.chains[chain_idx][left_elem_idx]
            # Get the positions of the connected elements
            conn_pos = [only(get_element_positions(pd, elem)) for elem in get_connected(pd.connection_properties, left_elem) if !inchain(elem, [pd.chains[ref_chain]])]
            # Get the columns
            conn_cols = [(last.(conn_pos)...)...]
            # If any of the columns is larger than this one, consider as blocking and continue
            (blocking[i] = any(conn_cols .>= i)) && continue
        end
        # Same for right
        right_elem_idx = pd.shifts[chain_idx] + length(pd.chains[chain_idx]) < i ? nothing : findfirst(!ismissing, pd.chains[chain_idx][max(1, i - pd.shifts[chain_idx]):end])
        if !isnothing(right_elem_idx)
            # Get the right elem
            right_elem = pd.chains[chain_idx][max(1, i - pd.shifts[chain_idx]) + right_elem_idx - 1]
            # Get the positions of the connected elements
            conn_pos = [only(get_element_positions(pd, elem)) for elem in get_connected(pd.connection_properties, right_elem) if !inchain(elem, [pd.chains[ref_chain]])]
            # Get the columns
            conn_cols = [(last.(conn_pos)...)...]
            # If any of the columns is larger than this one, consider as blocking and continue
            (blocking[i] = any(conn_cols .<= i)) && continue
        end
    end
    return blocking
end

"""
    squeeze_vertically!(pd::ProcessDiagram)

Squeeze the process diagram in the vertical direction by merging neighbouring chains as long as possible.

Merging is considered possible as long as:
- There are no overlaps between systems
- There is no connection:
    - From the first to the left to an element further than (or at same distance) the last of the other chain
    - From the last to the right to an element further than (or at same distance) the first of the other chain

Repeated iteratively, as long as chains can be merged.
"""
function squeeze_vertically!(pd::ProcessDiagram)
    merged = true
    while merged
        merged = false
        for i = reverse(eachindex(pd.chains))
            j = i
            while j < lastindex(pd.chains) && !any(get_blocking(pd, i, j+1) .&& get_blocking(pd, j+1, i))
                j += 1
            end
            if j > i
                merge_chains!(pd, j, i)
                merged = true
                break
            end
        end
    end
end

"""
    order_connections(pd, elem)

Order the connections of the node `elem` in direction of connection (left, right, up, down) and edge direction.

Is used for nice printing of the connections, that they exit the node at a good position.

Returns an Array of Arrays (similar to 2d-matrix), each element of which is associated with a direction for the connection and a direction of the first edge.
The ordering is as follows:

| Node edge \\ first bend direction    | left or up | straight        | down or right |
|:------------------------------------:|:----------:|:---------------:|:-------------:|
| **left**                             |left-up     | left-straight   | left-down     |
| **right**                            |right-up    | right-straight  | right-down    |
| **top**                              |top-left    | top-straight    | top-right     |
| **bottom**                           |bottom-left | bottom-straight | bottom-right  |

Each element in this table is a vector of connections (tuples of the connected port metadata, always the one being part of `elem.parent` first) which have this property.
"""
function order_connections(pd, elem)
    # Count the connections from/to this one
    # Count also if they make a corner and if yes in which direction
    conn_order = [[[], [], []], # left-up, left-straight, left-down
              [[], [], []],     # right-up, right-straight, right-down
              [[], [], []],     # top-left, top-straight, top-right
              [[], [], []]]     # bottom-left, bottom-straight, bottom-right
    for conn in pd.connection_properties
        # Filter out connections not related to this element
        (elem.parent in getproperty.(conn, :parent)) || continue

        # Check if the connection is reversed, i.e. the connected element is the second one.
        conn_reversed = conn[2].parent == elem.parent

        # Get both elems to connect, first is always the one to check
        elem1 = !conn_reversed ? conn[1] : conn[2]
        elem2 = !conn_reversed ? conn[2] : conn[1]

        # Get the positions of both nodes, two times only to have only once
        node_pos1 = only.(only(get_element_positions(pd, elem1)))
        node_pos2 = only.(only(get_element_positions(pd, elem2)))
        shift = node_pos2 .- node_pos1

        if any(shift .== 0)
            # Get indexes of all inbetween (including the two to be connected)
            idx_inbetween = StepRange.(node_pos1, (shift.>0) .* 2 .- 1, node_pos2)
            # If all elements on straight inbetween are "missing"
            if all(ismissing.(get_elem.(Ref(pd), idx_inbetween...)[2:end-1])) # Exclude start and end, as these are the elements to be connected
                # Go straight
                push!(conn_order[
                    1*(shift[2] < 0) +    # 1 right of 2
                    2*(shift[2] > 0) +    # 2 right of 1
                    3*(shift[1] < 0) +    # 2 above 1
                    4*(shift[1] > 0)      # 1 above 2
                    ][2], (elem1, elem2))
                continue
            else
                if shift[1] == 0 # Horizontal connection
                    # Go up(+) or down(-) by half a block into the separator between the nodes (sides are always free)
                    check_node_pos = !conn_reversed ? node_pos1 : node_pos2 # Switch the checking if reversed to have the same precedence
                    if (check_node_pos[1] != 1 && !ismissing(get_elem(pd, check_node_pos[1]-1, check_node_pos[2]))) && 
                        (check_node_pos[1] == length(pd.chains) || ismissing(get_elem(pd, check_node_pos[1]+1, check_node_pos[2])))
                        # If the above block is there, but the below not, go downwards
                        push!(conn_order[4][
                            1 +                 # Going left
                            (shift[2] > 0)*2    # If going right 
                            ], (elem1, elem2))
                    else # Default upwards
                        push!(conn_order[3][
                            1 +                 # Going left
                            (shift[2] > 0)*2    # If going right
                            ], (elem1, elem2))
                    end
                    continue
                else # Vertical connection
                    # Go right(+) or left(-) by half a block into the separator between the nodes (sides are always free)
                    check_node_pos = !conn_reversed ? node_pos1 : node_pos2 # Switch the checking if reversed to have the same precedence
                    if (check_node_pos[2] != maximum(pd.shifts .+ length.(pd.chains)) && !ismissing(get_elem(pd, check_node_pos[1], check_node_pos[2]+1))) && 
                        (check_node_pos[2] == 1 || ismissing(get_elem(pd, check_node_pos[1], check_node_pos[2]-1)))
                        # If the right block is there, but the left not, go left
                        push!(conn_order[1][
                            1 +                 # Going up
                            (shift[1] > 0)*2    # If going down
                            ], (elem1, elem2))
                    else # Default right
                        push!(conn_order[2][
                            1 +                 # Going up
                            (shift[1] > 0)*2    # If going down
                            ], (elem1, elem2))
                    end
                    continue
                end
            end
        else
            # Get indexes of all inbetween (including the two to be connected)
            idx_inbetween = StepRange.(node_pos1, (sign.(shift).>0) .* 2 .- 1, node_pos2)

            # Check if vertical-horizontal or horizontal-vertical connection (straight with one corner) is free
            vertical_horizontal_free = 
                all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1], first(idx_inbetween[2]))[2:end])) && # Check vertical (excluding 1st which is to be connected)
                all(ismissing.(get_elem.(Ref(pd), last(idx_inbetween[1]), idx_inbetween[2])[1:end-1])) # Check horizontal  (excluding last which is to be connected)
            horizontal_vertical_free = 
                all(ismissing.(get_elem.(Ref(pd), first(idx_inbetween[1]), idx_inbetween[2])[2:end])) && # Check horizontal (excluding 1st which is to be connected)
                all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1], last(idx_inbetween[2]))[1:end-1]))   # Check vertical (excluding last which is to be connected)
            
            # Functions to set the connection order
            function set_vertical_horizontal() 
                push!(conn_order[
                    3 +               # Going up
                    (shift[1] > 0)    # If going down
                    ][
                    1 +                 # Going left
                    (shift[2] > 0)*2    # If going right
                    ], (elem1, elem2))
            end
            function set_horizontal_vertical() 
                push!(conn_order[
                    1 +               # This is left
                    (shift[2] > 0)    # If going right
                    ][
                    1 +                 # Going up
                    (shift[1] > 0)*2    # If going down
                    ], (elem1, elem2))
            end

            # Check if all elements on connection with one corner are missing
            if (!conn_reversed && vertical_horizontal_free) || (conn_reversed && horizontal_vertical_free)
                !conn_reversed && set_vertical_horizontal()
                conn_reversed && set_horizontal_vertical()
                continue
            elseif (!conn_reversed && horizontal_vertical_free) || (conn_reversed && vertical_horizontal_free)
                !conn_reversed && set_horizontal_vertical()
                conn_reversed && set_vertical_horizontal()
                continue
            else
                # Double edged line
                # If first vertical is free, simply up/down
                if all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1][1:Int(end/2+iseven(shift[1])*0.5)], first(idx_inbetween[2]))[2:end]))
                    push!(conn_order[
                        3 +               # This is top
                        (shift[1] > 0)    # If going down
                        ][
                        1 +                 # Going left
                        (shift[2] > 0)*2    # If going right
                        ], (elem1, elem2))
                else
                    # Otherwise left/right, then up/down
                    push!(conn_order[
                        1 +               # This is left
                        (shift[2] > 0)    # If going right
                        ][
                        1 +                 # Going up
                        (shift[1] > 0)*2    # If going down
                        ], (elem1, elem2))
                end
                continue
            end
        end
    end
    return conn_order
end


##### Printing functions #####
"""
    get_node_style(elem; opts=missing, get_def=false)

Get the node style for printing the element using Latex/Tikz. 
Determines the styling of the resulting node. Parameters for it are given in `opts`. Check [`ProcessDiagram`](@ref) for the defined options.

If `get_def=true`, return the style definition (i.e. what should be inserted as STYLE_OPTIONS into `\\tikzstyle{name} = [STYLE_OPTIONS]`), otherwise only the name.

!!! note
    If `get_def = true`, opts must be given as well.

Defaults to a simple rectangular block.

!!! note
    If you add new styles, make sure that the resulting block has:
    - width: `blockwidth`
    - height: `blockheight`
    as otherwise the plotting will not match.
    If you need the text width, 
"""
function get_node_style(elem; blockwidth=missing, blockheight=missing, textwidth=missing, 
    textheight=missing, linewidth=missing, nodedistance=missing, innersep=missing, 
    background=nothing, print_connector_names=false, get_def=false) # Get the printing type for the element
    # Default(independent of elem): A simple rectangular block.
    # The name 
    name = "block"
    # If the definition is not requested, simply return the name
    get_def || return name
    
    # Define and return the style
    style = "draw, rectangle, line width=$linewidth, 
            text width=$textwidth, minimum width=$blockwidth,
            minimum height=$blockheight, 
            align=center"
    return style
end
function get_node_style(elem::PortMetadata; kwargs...)
    get_node_style(elem.parent; kwargs...)
end
"""
    get_name(elem)

Get the node name for printing using Latex/Tikz.
Needs to be a unique key to refer to in the diagram.
"""
function get_name(elem) # Get the node name for the element
    string(nameof(elem))
end
function get_name(elem::PortMetadata)
    get_name(elem.parent)
end
"""
    get_label(elem)

Get the node label for printing using Latex/Tikz.
Is the text printed within the node.
"""
function get_label(elem) # Get the node label for the element
    latexify(nameof(elem))
end
function get_label(elem::PortMetadata)
    get_label(elem.parent)
end
"""
    get_port_label(elem)

Get the node label for printing of the port using Latex/Tikz.
Is the text printed within the port node.
"""
function get_port_label(elem::PortMetadata)
    latexify(elem.name)
end

"""
    get_latex_preamble(pd::ProcessDiagram; include_tikz=true)

Get the preamble for a latex document with a tikzpicture showing `pd`.

If `include_tikz`, then also import `tikz` itself.
"""
function get_latex_preamble(pd::ProcessDiagram; include_tikz=true)
    # Define needed latex packages
    latexpackages = ["adjustbox"]
    # Define needed tikz libraries
    tikzlibraries = ["positioning", "calc", "math"]

    # Add the required tikz dependency if argument set
    include_tikz && push!(latexpackages, "tikz")

    # Add the tikz backgrounds library if required
    isnothing(pd.printing_options[:background]) || push!(tikzlibraries, "backgrounds")

    # Assemble The Commands
    preamble_str = """
    % Packages
    """
    for pkg in latexpackages
        preamble_str *= """
        \\usepackage{$pkg}
        """
    end
    preamble_str *= """

    % Tikz libraries
    """
    for lib in tikzlibraries
        preamble_str *= """
        \\usetikzlibrary{$lib}
        """
    end
    return preamble_str
end

"""
    get_tikzpicture_options(pd::ProcessDiagram)

Get the options for the tikzpicture showing `pd`.
"""
function get_tikzpicture_options(pd::ProcessDiagram)
    options = """
    node distance=$(pd.printing_options[:nodedistance]), 
    line width=$(pd.printing_options[:linewidth])
    """
    if !isnothing(pd.printing_options[:background])
        options *= """,
        background rectangle/.style={fill=$(pd.printing_options[:background])}, show background rectangle
        """
    end
    return options
end

"""
    get_tikzpicture_body(pd::ProcessDiagram)

Get the body for the tikzpicture showing `pd`.

Does the heavylifting for making a tikzpicture from the [`ProcessDiagram`](@ref).

Contains placing all nodes and the connections inbetween.

Places the nodes according to the ordering in `pd` and considers it for drawing the connections inbetween.
"""
function get_tikzpicture_body(pd::ProcessDiagram)
    # Get computed layouting properties
    textwidth = "($(pd.printing_options[:blockwidth]) - 2*$(pd.printing_options[:innersep]))"
    textheight = "($(pd.printing_options[:blockheight]) - 2*$(pd.printing_options[:innersep]))"
    total_blockwidth = "($(pd.printing_options[:blockwidth]) + $(pd.printing_options[:linewidth]))" # Including outer sep, which defaults to linewidth/2. Needed for positioning

    # Evaluate text width and height in latex
    latex_str = """\
    \\newdimen\\maxtextheight
    \\newdimen\\maxtextwidth
    \\tikzmath{
        \\maxtextheight=$textheight);
        \\maxtextwidth=$textwidth);
    }
    """

    # Get all style definitions
    latex_str *= "\n% Style Definitions\n"
    processed_names = []
    for elem in [(pd.chains...)...]
        name = get_node_style(elem)
        if !(name in processed_names)
            style_def = get_node_style(elem; pd.printing_options..., textwidth, textheight, get_def=true)
            latex_str *= """
            \\tikzstyle{$name} = [$style_def]
            """
            push!(processed_names, name)
        end
    end

    # Newline into latex style definition for nicer reading
    latex_str *= "\n% Placing all blocks"

    # Print the nodes and position them appropriately
    for i in eachindex(pd.chains) # Go through each chain
        latex_str *= "\n% Row $i\n"
        n_missing = 0
        for j in eachindex(pd.chains[i]) # And each element within
            # Compute the positioning of the element
            if i == firstindex(pd.chains) && j == firstindex(pd.chains[i])
                # First element -> just at default position
                position = ""
            elseif j == firstindex(pd.chains[i])
                # First element of new chain
                if pd.shifts[i] - pd.shifts[i-1] < 0
                    # First element is left of the first element of the last chain
                    n_nodes_between = 1 - (pd.shifts[i] - pd.shifts[i-1] + 1) - 1 # 1-(...) is the index, -1 as the last one is the destination
                    n_dist_between = n_nodes_between + 1 # One more distance than nodes inbetween (one distance at each end)
                    position = "below left= and (" * string(n_nodes_between) * "*" * total_blockwidth * "+" * string(n_dist_between) * "*" * pd.printing_options[:nodedistance] * ") of " * get_name(pd.chains[i-1][1])
                elseif pd.shifts[i] - pd.shifts[i-1] >= length(pd.chains[i-1])
                    # First element is right of the last element the last chain
                    n_nodes_between = pd.shifts[i] - pd.shifts[i-1] + 1 - length(pd.chains[i-1]) - 1 # -1 as this is including the destination
                    n_dist_between = n_nodes_between + 1 # One more distance than nodes inbetween (one distance at each end)
                    position = "below right= and (" * string(n_nodes_between) * "*" * total_blockwidth * "+" * string(n_dist_between) * "*" * pd.printing_options[:nodedistance] * ") of " * get_name(pd.chains[i-1][end])
                else
                    # First element is directly below an element of the last chain
                    idx_above = pd.shifts[i] - pd.shifts[i-1] + 1 # Index below which in the upper chain
                    if ismissing(pd.chains[i-1][idx_above])
                        # If the one directly above is missing, find the earlier one which is not missing
                        last_nonmiss = findlast(.!ismissing.(pd.chains[i-1][1:idx_above]))
                        n_nodes_between = idx_above - last_nonmiss - 1 # -1 as this is including the destination
                        n_dist_between = n_nodes_between + 1 # One more distance than nodes inbetween (one distance at each end)
                        position = "below right= and (" * string(n_nodes_between) * "*" * total_blockwidth * "+" * string(n_dist_between) * "*" * pd.printing_options[:nodedistance] * ") of " * get_name(pd.chains[i-1][last_nonmiss])
                    else
                        position = "below= of " * get_name(pd.chains[i-1][idx_above])
                    end
                end
            else
                # If missing, increase the counter and do not add a node
                if ismissing(pd.chains[i][j])
                    n_missing += 1
                    continue
                end
                # Next in the chain -> right of previous one
                position = "right=(" * string(n_missing) * "*" * total_blockwidth * "+" * string(n_missing + 1) * "*" * pd.printing_options[:nodedistance] * ") of " * get_name(pd.chains[i][j - n_missing - 1])
                n_missing = 0
            end
            style = get_node_style(pd.chains[i][j])
            name = get_name(pd.chains[i][j])
            label = get_label(pd.chains[i][j])
            latex_str *= """
                \\node [$(join([style, position], ", "))] ($name) {\\adjustbox{max width=\\the\\maxtextwidth, max totalheight=\\the\\maxtextheight}{$label}};
                """
        end
    end

    # Newline into latex style definition for nicer reading
    latex_str *= "\n% Making all connections\n"

    # Connections
    for conn in pd.connection_properties
        # Get both elems to connect
        elem1 = conn[1]
        elem2 = conn[2]
        # Get the direction of the arrow (always head at input)
        arrow = elem1.isinput ? "<-" : "-"
        elem2.isinput && (arrow *= ">")

        # Get the positions of both nodes, two times only to have only once
        node_pos1 = only.(only(get_element_positions(pd, elem1)))
        node_pos2 = only.(only(get_element_positions(pd, elem2)))
        shift = node_pos2 .- node_pos1

        conn_type_precedence = [:flow, :state, :rate, :exogenous, :unknown]
        # Function to sort by type, input and index
        sort_by_type = ComposedFunction(ComposedFunction(ComposedFunction(
            Base.Fix2(findfirst, conn_type_precedence), 
            isequal), 
            Base.Fix2(getproperty, :type)), 
            first)
        sort_by_input = ComposedFunction(Base.Fix2(getproperty, :isinput), first)
        sort_by_index = ComposedFunction(Base.Fix2(getproperty, :index), first)
        # Function to get the index of the port in the given elem_vec
        function find_pos_index(elem_vec, elem; conn_type_precedence = conn_type_precedence)
            sorted = sort(sort(sort(elem_vec; by=sort_by_index), by=sort_by_input), by=sort_by_type)
            findfirst(isequal(elem), sorted)
        end
        
        # Find the positions of the connections
        pos_left = []
        pos_right = []
        rel_pos = []
        portname_maxheight = []
        portname_maxwidth = []
        for (elem, elem_other) in zip(conn, reverse(conn))
            # Get the connection counts for this element
            conn_order = order_connections(pd, elem)
            conn_idx = [in.(Ref((elem, elem_other)), order_dir) for order_dir in conn_order]
            conn_dir = findfirst(any, conn_idx)
            bend_dir = findfirst(conn_idx[conn_dir])
            n_spaces = sum(length.(conn_order[conn_dir])) + 1
            # Get the relative position
            push!(rel_pos, (find_pos_index(conn_order[conn_dir][bend_dir], (elem, elem_other)) + sum(length.(conn_order[conn_dir][1:bend_dir-1]))) ./ n_spaces)
            if conn_dir == 1 # Left Side
                push!(pos_left, "north west")
                push!(pos_right, "south west")
                # Get the maximum size for the node with the port name
                push!(portname_maxheight, "{($(pd.printing_options[:blockheight]) / $n_spaces)}")
                push!(portname_maxwidth, "{$(pd.printing_options[:innersep])}")
            elseif conn_dir == 2 # Right side
                push!(pos_left, "north east")
                push!(pos_right, "south east")
                # Get the maximum size for the node with the port name
                push!(portname_maxheight, "{($(pd.printing_options[:blockheight]) / $n_spaces)}")
                push!(portname_maxwidth, "{$(pd.printing_options[:innersep])}")
            elseif conn_dir == 3 # Top side
                push!(pos_left, "north west")
                push!(pos_right, "north east")
                # Get the maximum size for the node with the port name
                push!(portname_maxheight, "{$(pd.printing_options[:innersep])}")
                push!(portname_maxwidth, "{($(pd.printing_options[:blockwidth]) / $n_spaces)}")
            elseif conn_dir == 4 # Bottom side
                push!(pos_left, "south west")
                push!(pos_right, "south east")
                # Get the maximum size for the node with the port name
                push!(portname_maxheight, "{$(pd.printing_options[:innersep])}")
                push!(portname_maxwidth, "{($(pd.printing_options[:blockwidth]) / $n_spaces)}")
            end
        end
        line_pos = "\$(" .* get_name.(conn) .* "." .* pos_left .* ")!" .* string.(rel_pos) .* "!(" .* get_name.(conn) .* "." .* pos_right .* ")\$"
        
        if any(shift .== 0)
            # Get indexes of all inbetween (including the two to be connected)
            idx_inbetween = StepRange.(node_pos1, (sign.(shift).>0) .* 2 .- 1, node_pos2)
            # If all elements on straight inbetween are "missing"
            if all(ismissing.(get_elem.(Ref(pd), idx_inbetween...)[2:end-1])) # Exclude start and end, as these are the elements to be connected
                if shift[1] < 0 # 2 above 1
                    line = "|-"
                elseif shift[1] > 0 # 1 above 2
                    line = "|-"
                elseif shift[2] < 0 # 1 right of 2
                    line = "-|"
                else # 2 right of 1
                    line = "-|"
                end
                # If all elements inbetween are "missing", Connect by straight line
                line_type = "$line (\$($(line_pos[1]))!0.5!($(line_pos[2]))\$) $(reverse(line))" 
            else
                if shift[1] == 0 # Horizontal connection
                    # Go up(+) or down(-) by half a block into the separator between the nodes
                    dir = "+" # Default up
                    # If the above block is there, but the below not, go downwards (sides are always free)
                    (node_pos1[1] != 1 && !ismissing(get_elem(pd, node_pos1[1]-1, node_pos1[2]))) && 
                        (node_pos1[1] == length(pd.chains) || ismissing(get_elem(pd, node_pos1[1]+1, node_pos1[2]))) && 
                        (dir = "-")
                    line_type = "-- (\$($(line_pos[1])) $dir (0, {$(pd.printing_options[:nodedistance]) / 2})\$) -|"
                else # Vertical connection
                    # Go right(+) or left(-) by half a block into the separator between the nodes 
                    dir = "+" # Default right
                    # If the right block is there, but the left not, go left (sides are always free)
                    (node_pos1[2] != maximum(pd.shifts .+ length.(pd.chains)) && !ismissing(get_elem(pd, node_pos1[1], node_pos1[2]+1))) && 
                        (node_pos1[2] == 1 || ismissing(get_elem(pd, node_pos1[1], node_pos1[2]-1))) && 
                        (dir = "-")
                    line_type = "-- (\$($(line_pos[1])) $dir ({$(pd.printing_options[:nodedistance]) / 2}, 0)\$) |-"
                end
            end
        else
            # Get indexes of all inbetween (including the two to be connected)
            idx_inbetween = StepRange.(node_pos1, (sign.(shift).>0) .* 2 .- 1, node_pos2)
            # Check if all elements on connection with one corner are missing
            if all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1], first(idx_inbetween[2]))[2:end])) && # Check vertical (excluding 1st which is to be connected)
                all(ismissing.(get_elem.(Ref(pd), last(idx_inbetween[1]), idx_inbetween[2])[1:end-1])) # Check horizontal  (excluding last which is to be connected)
                # If all elements inbetween are "missing", Connect by vertical-horizontal line
                line_type = "|-"
            elseif all(ismissing.(get_elem.(Ref(pd), first(idx_inbetween[1]), idx_inbetween[2])[2:end])) && # Check horizontal (excluding 1st which is to be connected)
                all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1], last(idx_inbetween[2]))[1:end-1])) # Check vertical (excluding last which is to be connected)
                # If all elements inbetween are "missing", Connect by horizontal-vertical line
                line_type = "-|"
            else
                # If all elements inbetween are "missing", Connect by vertical-horizontal-vertical line
                if isodd(shift[1]) || # If there are an even number of systems inbetween (the shift is odd), make the connection in the middle (is inbetween blocks)
                    all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1][Int(end/2+0.5)], idx_inbetween[2]))) # Or all middle ones are missing
                    # If first half vertical is free, go |-, else -|-
                    if all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1][1:Int(end/2+iseven(shift[1])*0.5)], first(idx_inbetween[2]))[2:end]))
                        line_type = "|- "
                    else
                        line_type = "-- (\$($(line_pos[1])) + ({$(sign(shift[2])) * ($(pd.printing_options[:nodedistance])) / 2}, 0)\$) |- "
                    end
                    line_type *= "(\$($(line_pos[1]))!0.5!($(line_pos[2]))\$) "
                    # If second half vertical is free, go -|, else -|-
                    if all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1][Int(end/2+iseven(shift[1])*0.5):end], last(idx_inbetween[2]))[1:end-1]))
                        line_type *= "-|"
                    else
                        line_type *= "-| (\$($(line_pos[2])) + ({$(-sign(shift[2])) * ($(pd.printing_options[:nodedistance])) / 2}, 0)\$) -- "
                    end
                else # Otherwise shift down by half a block height and nodedistance 
                    # If first half vertical is free, go |-, else -|-
                    if all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1][1:Int(end/2+iseven(shift[1])*0.5)], first(idx_inbetween[2]))[2:end]))
                        line_type = "|- "
                    else
                        line_type = "-- (\$($(line_pos[1])) + ({$(sign(shift[2])) * ($(pd.printing_options[:nodedistance])) / 2}, 0)\$) |- "
                    end
                    line_type *= "(\$($(line_pos[1]))!0.5!($(line_pos[2])) - (0, {$(pd.printing_options[:blockheight]) / 2 + $(pd.printing_options[:nodedistance]) / 2})\$) "
                    # If second half vertical is free, go -|, else -|-
                    if all(ismissing.(get_elem.(Ref(pd), idx_inbetween[1][Int(end/2+iseven(shift[1])*0.5):end], last(idx_inbetween[2]))[1:end-1]))
                        line_type *= "-|"
                    else
                        line_type *= "-| (\$($(line_pos[2])) + ({$(-sign(shift[2])) * ($(pd.printing_options[:nodedistance])) / 2}, 0)\$) -- "
                    end
                end
            end
        end

        if pd.printing_options[:print_connector_names]
            startnode_name = "$(nameof(elem1.parent))_$(elem1.type)_$(Int(elem1.isinput))_$(elem1.index)"
            endnode_name = "$(nameof(elem2.parent))_$(elem2.type)_$(Int(elem2.isinput))_$(elem2.index)"
            latex_str *= """
                \\node[inner sep=0,fill=white] at ($(line_pos[1])) ($startnode_name) {\\adjustbox{max width=$(portname_maxwidth[1]), max totalheight=$(portname_maxheight[1])}{$(get_port_label(elem1))}};
                \\node[inner sep=0,fill=white] at ($(line_pos[2])) ($endnode_name) {\\adjustbox{max width=$(portname_maxwidth[2]), max totalheight=$(portname_maxheight[2])}{$(get_port_label(elem2))}};
                \\draw[$arrow] ($startnode_name) $line_type ($endnode_name);
                """
        else
            latex_str *= """
                \\draw[$arrow] ($(line_pos[1])) $line_type ($(line_pos[2]));
                """
        end
    end
    return latex_str
end

"""
    get_tikzpicture(pd::ProcessDiagram)

Get the TikZ/LaTeX code for the tikzpicture representing `pd`.

Includes the full tikzpicture environment, but no preamble or similar.
"""
function get_tikzpicture(pd::ProcessDiagram)
    """\
    \\begin{tikzpicture}[
    $(get_tikzpicture_options(pd))]

    $(get_tikzpicture_body(pd))
    \\end{tikzpicture}\
    """
end

"""
    get_latex(pd::ProcessDiagram)

Get the LaTeX code for a complete, renderable, LaTeX document containing the tikzpicture representing `pd`.

Includes every thing needed, from preamble, to the document itself.

Uses the standalone documentclass.
"""
function get_latex(pd::ProcessDiagram)
    """\
    \\documentclass[tikz]{standalone}

    $(get_latex_preamble(pd))

    \\begin{document}

    $(get_tikzpicture(pd))

    \\end{document}\
    """
end

# Overload the show function for latex output
Base.show(io::IO, ::MIME"application/x-latex", pd::ProcessDiagram) = print(io, get_latex(pd))
Base.show(io::IO, ::MIME"text/latex", pd::ProcessDiagram) = print(io, get_latex(pd))
