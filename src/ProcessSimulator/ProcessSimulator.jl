module ProcessSimulator
    using Reexport
    @reexport using ModelingToolkit
    @reexport using ModelingToolkitStandardLibrary
    using Latexify
    using Setfield

    using ModelingToolkit: t_nounits as t, D_nounits as D 
    using ..Reactions

    # Export t for time
    export t
    @doc raw"""
        t
    
    Symbol for time.
    """ t

    # Utils
    include("utils.jl") 

    export isparticulate, iscolloidal, issoluble
    include("variables.jl")

    # The abstract flow element type
    export states, rates, inflows, outflows, exogenous_inputs, exogenous_outputs, subsystems
    export num_states, num_rates, num_inflows, num_outflows, num_exogenous_inputs, num_exogenous_outputs, num_subsystems
    export hasstates, hasrates, hasinflows, hasoutflows, hasexogenous_inputs, hasexogenous_outputs, hassubsystems
    export inflow_names, outflow_names, exogenous_input_names, exogenous_output_names, subsystem_names
    include("abstractprocesselement.jl")

    include("portmetadata.jl")

    # Connector systems
    include("connectors.jl")

    # Systems to connect to ModelingToolkitStandardLibrary Blocks
    export MTKStandardLibraryInterface
    export InflowPortToRealOutput, RealInputToOutflowPort
    export ReactionInputToRealOutput, RealInputToReactionOutput
    export StateInputToRealOutput, RealInputToStateOutput
    include("modelingtoolkitstandardlibrarycompat.jl")

    # FlowElements
    export FlowElement
    export Influent, Sensor
    export FlowConnector, FlowUnifier, FlowSeparator
    export FlowPump
    export IdealClarifier
    include("flowelements.jl")

    # Processes
    export Process
    export OzoneDisinfection, ASM1, Aeration
    include("processes.jl")

    # Defaults
    export default_processes
    export set_default_process, add_default_process, clear_default_processes
    export @set_process, @add_process
    export default_state_mapping
    export set_default_state_mapping, add_default_state_mapping, clear_default_state_mapping
    export @set_state_mapping, @add_state_mapping
    export get_default_state, default_states
    include("defaults.jl")

    # Reactors
    export Reactor
    export CSTR, BatchReactor
    include("reactors.jl")

    # Process printing: ProcessDiagram
    export ProcessDiagram
    export set_linewidth!, set_nodedistance!, set_blockwidth!, set_blockheight!
    export set_innersep!, set_background!, print_connector_names!
    export switch_chains!, move_chain_vertically!, move_chain_horizontally!, merge_chains!
    export insert_empty_columns!, switch_columns!, move_column!
    export get_elem, move_elem!, switch_elem!
    export cleanup!
    export get_latex_preamble, get_tikzpicture, get_latex
    include("processdiagram.jl")
end