"""
    MTKStandardLibraryInterface

An implementation of [`AbstractProcessElement`](@ref) for compatibility interface elements for the ModelingToolkitStandardLibrary.

It has fields:
- `eqs`: The equations
- `t`: Time symbol
- `ode_system`: A `ODESystem` from `ModelingToolkit` for simulating the reactor
- `inflows`: Vector of the inflow connectors of the reactor
- `outflows`: Vector of the outflow connectors of the reactor
- `rates`: The rates connector of the reactor
- `states`: The states connector of the reactor
- `exogenous_inputs`: The exogenous input connectors to the reactor or the subsystems.
- `exogenous_outputs`: The exogenous output connectors to the reactor or the subsystems.
- `name`: The name of the reactors

It supports the following interfaces from the [`AbstractProcessElement`](@ref):
- [`states`](@ref)
- [`rates`](@ref)
- [`inflows`](@ref)
- [`outflows`](@ref)
- [`exogenous_inputs`](@ref)
- [`exogenous_outputs`](@ref)
"""
struct MTKStandardLibraryInterface <: AbstractProcessElement
    eqs::Vector{Equation}
    t::Symbolics.BasicSymbolic{Real}

    ode_system::ODESystem

    inflows::Vector{ODESystem}
    outflows::Vector{ODESystem}
    rates::Union{ODESystem, Nothing}
    states::Union{ODESystem, Nothing}
    exogenous_inputs::Vector{ODESystem}
    exogenous_outputs::Vector{ODESystem}

    name::Symbol

    function MTKStandardLibraryInterface(eqs, t; inflows=ODESystem[], outflows=ODESystem[], rates=ODESystem[], states=ODESystem[], systems=ODESystem[], exogenous_inputs=ODESystem[], exogenous_outputs=ODESystem[], name)
        # Add port metadata to all ports
        inflows = init_port_metadata.(inflows)
        outflows = init_port_metadata.(outflows)
        rates = init_port_metadata.(rates)
        states = init_port_metadata.(states)
        exogenous_inputs = init_port_metadata.(exogenous_inputs)
        exogenous_outputs = init_port_metadata.(exogenous_outputs)
        ode_system = compose(ODESystem(eqs, t, [], []; systems=systems, name=name), [inflows..., outflows..., rates..., states..., exogenous_inputs..., exogenous_outputs...])
        element = new(eqs, ModelingToolkit.get_iv(ode_system), ode_system, 
            get_namespaced_property.(Ref(ode_system), nameof.(inflows)),
            get_namespaced_property.(Ref(ode_system), nameof.(outflows)),
            isempty(rates) ? nothing : get_namespaced_property(ode_system, nameof(rates[1])),
            isempty(states) ? nothing : get_namespaced_property(ode_system, nameof(states[1])),
            get_namespaced_property.(Ref(ode_system), nameof.(exogenous_inputs)),
            get_namespaced_property.(Ref(ode_system), nameof.(exogenous_outputs)),
            name)
        # Automatically set the port metadata of all ports
        set_port_metadata!(element)
        return element
    end
end

function Base.convert(::Type{<:ODESystem}, x::MTKStandardLibraryInterface)
    x.ode_system
end

function ModelingToolkit.equations(sys::MTKStandardLibraryInterface)
    sys.eqs
end

function inflows(sys::MTKStandardLibraryInterface)
    sys.inflows
end

function outflows(sys::MTKStandardLibraryInterface)
    sys.outflows
end

function states(sys::MTKStandardLibraryInterface)
    sys.states
end

function rates(sys::MTKStandardLibraryInterface)
    sys.rates
end

function exogenous_inputs(sys::MTKStandardLibraryInterface)
    sys.exogenous_inputs
end

function exogenous_outputs(sys::MTKStandardLibraryInterface)
    sys.exogenous_outputs
end

"""
    realports_from_combinedport(port::ODESystem; name, output_port = false)

Helper function for StandardLibrary Ports compatibility: Generates a RealInput/RealOutput for each state in the port.
Returns a ODESystem with the equations for setting the new ports and the corresponding state equal and the newly generated In-/Outputs
"""
function realports_from_combinedport(port::ODESystem; output_port = false)
    # Get Port Type for the Real ports (RealInput or RealOutput)
    if output_port
        PortType = ModelingToolkitStandardLibrary.Blocks.RealOutput
    else
        PortType = ModelingToolkitStandardLibrary.Blocks.RealInput
    end
    # Vectors for the ModelingToolkitStandardLibrary Ports and the equations
    ports = ODESystem[]
    eqns = Equation[]

    # Generate the ModelingToolkitStandardLibrary Ports and the equations (all states from the combined port are equal to the new Single port)
    for state in unknowns(port)
        push!(ports, PortType(;name = Symbol(symbolic_to_namestring(state)), guess=get_default_or_guess(state, 0)))
        push!(eqns, ports[end].u ~ getproperty(port, Symbol(symbolic_to_namestring(state))))
    end

    # Return a ODESystem for the combination and the vector with the ports
    eqns, ports
end

"""
    RealInputToOutflowPort(components=default_states(); initial_flow=nothing, initial_concentrations=nothing, name)

A system which connects to a [`OutflowPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealInput`s](@extref ModelingToolkitStandardLibrary.Blocks.RealInput) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`OutflowPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@extref ModelingToolkitStandardLibrary :doc:`index`) to be loaded, as it is only useful with this library.

# Connectors
- `outflow`: The [`OutflowPort`](@ref) to connect
- `q`: A [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@extref) for the flow rate
- One [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@extref) for each component in the flow
"""
@component function RealInputToOutflowPort(components = default_states(); initial_flow = nothing, initial_concentrations = nothing, name)
    # Start with the OutflowPort and construct the RealInputs based on this -> components can be provided in any format supported by OutflowPort
    # Create the OutflowPort
    @named outflow = OutflowPort(components; initial_flow, initial_concentrations)

    # Get the system with the equality equations and the individual ports
    eqns, ports = realports_from_combinedport(outflow; output_port = false)

    # Compose and return the system
    MTKStandardLibraryInterface(eqns, t; outflows=[outflow], exogenous_inputs=ports, name)
end

"""
    InflowPortToRealOutput(components=default_states(); initial_flow=nothing, initial_concentrations=nothing, name)

A system which connects to a [`InflowPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealOutput`s](@extref ModelingToolkitStandardLibrary.Blocks.RealOutput) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
Internally the equations ensure equality inbetween the corresponding symbols. Takes the same inputs as [`InflowPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@extref ModelingToolkitStandardLibrary :doc:`index`) to be loaded, as it is only useful with this library.

# Connectors
- `inflow`: The [`InflowPort`](@ref) to connect
- `q`: A [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@extref) for the flow rate
- One [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@extref) for each component in the flow
"""
@component function InflowPortToRealOutput(components=default_states(); initial_flow = nothing, initial_concentrations = nothing, name)
    # Start with the InflowPort and construct the RealOutputs based on this -> components can be provided in any format supported by InflowPort
    # Create the InflowPort
    @named inflow = InflowPort(components; initial_flow, initial_concentrations)

    # Get the system with the equality equations and the individual ports
    eqns, ports = realports_from_combinedport(inflow; output_port = true)

    # Compose and return the system
    MTKStandardLibraryInterface(eqns, t; inflows=[inflow], exogenous_outputs=ports, name)
end


"""
    RealInputToReactionOutput(components=default_states(); initial_rates=nothing, name)

A system which connects to a [`ReactionOutputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealInput`s](@extref ModelingToolkitStandardLibrary.Blocks.RealInput) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`ReactionOutputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@extref ModelingToolkitStandardLibrary :doc:`index`) to be loaded, as it is only useful with this library.

# Connectors
- `reactionoutput`: The [`ReactionOutputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@extref) for each component
"""
@component function RealInputToReactionOutput(components=default_states(); initial_rates = nothing, name)
    # Start with the ReactionOutputPort and construct the RealInputs based on this -> components can be provided in any format supported by ReactionInputPort
    # Create the ReactionInputPort
    @named reactionoutput = ReactionOutputPort(components; initial_rates)

    # Get the equality equations and the individual ports
    eqns, ports = realports_from_combinedport(reactionoutput; output_port = false)

    # Compose and return the system
    MTKStandardLibraryInterface(eqns, t; rates=[reactionoutput], exogenous_inputs=ports, name)
end

"""
    ReactionInputToRealOutput(components=default_states(); initial_rates=nothing, name)

A system which connects to a [`ReactionInputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealOutput`s](@extref ModelingToolkitStandardLibrary.Blocks.RealOutput) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`ReactionInputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@extref ModelingToolkitStandardLibrary :doc:`index`) to be loaded, as it is only useful with this library.

# Connectors
- `reactioninput`: The [`ReactionInputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@extref) for each component
"""
@component function ReactionInputToRealOutput(components=default_states(); initial_rates = nothing, name)
    # Start with the ReactionInputPort and construct the RealOutputs based on this -> components can be provided in any format supported by ReactionInputPort
    # Create the ReactionInputPort
    @named reactioninput = ReactionInputPort(components; initial_rates)

    # Get the equality equations and the individual ports
    eqns, ports = realports_from_combinedport(reactioninput; output_port = true)

    # Compose and return the system
    MTKStandardLibraryInterface(eqns, t; rates=[reactioninput], exogenous_outputs=ports, name)
end


"""
    RealInputToStateOutput(states=default_states(); initial_states=nothing, name)

A system which connects to a [`StateOutputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealInput`s](@extref ModelingToolkitStandardLibrary.Blocks.RealInput) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`StateOutputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@extref ModelingToolkitStandardLibrary :doc:`index`) to be loaded, as it is only useful with this library.

# Connectors
- `stateoutput`: The [`StateOutputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@extref) for each state
"""
@component function RealInputToStateOutput(states=default_states(); initial_states = nothing, name)
    # Start with the StateOutputPort and construct the RealInputs based on this -> components can be provided in any format supported by StateOutputPort
    # Create the StateOutputPort
    @named stateoutput = StateOutputPort(states; initial_states)

    # Get the equality equations and the individual ports
    eqns, ports = realports_from_combinedport(stateoutput; output_port = false)

    # Compose and return the system
    MTKStandardLibraryInterface(eqns, t; states=[stateoutput], exogenous_inputs=ports, name)
end


"""
    StateInputToRealOutput(components; initial_states=nothing, name)

A system which connects to a [`StateInputPort`](@ref) and to a series of [`ModelingToolkitStandardLibrary.Blocks.RealOutput`s](@extref ModelingToolkitStandardLibrary.Blocks.RealOutput) on the other sides for compatibility with the ModelingToolkitStandardLibrary.
The system ensures equality between the corresponding symbols. 
Takes the same inputs as [`StateInputPort`](@ref).

!!! info
    This function needs the [`ModelingToolkitStandardLibrary`](@extref ModelingToolkitStandardLibrary :doc:`index`) to be loaded, as it is only useful with this library.

# Connectors
- `stateinput`: The [`StateInputPort`](@ref) to connect
- One [`ModelingToolkitStandardLibrary.Blocks.RealOutput`](@extref) for each component
"""
@component function StateInputToRealOutput(states=default_states(); initial_states = nothing, name)
    # Start with the StateInputPort and construct the RealOutputs based on this -> components can be provided in any format supported by StateInputPort
    # Create the StateInputPort
    @named stateinput = StateInputPort(states; initial_states)

    # Get the equality equations and the individual ports
    eqns, ports = realports_from_combinedport(stateinput; output_port = true)

    # Compose and return the system
    MTKStandardLibraryInterface(eqns, t; states=[stateinput], exogenous_outputs=ports, name)
end
