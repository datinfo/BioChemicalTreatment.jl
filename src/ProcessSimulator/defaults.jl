#####
# DEFAULT PROCESSES interface
#####

"""
    DEFAULT_PROCESSES

Collection of functions to be called for generating the default processes for new reactors.
"""
DEFAULT_PROCESSES::Vector{<:Union{Function, Type{Process}}} = Vector{Union{Function, Type{Process}}}()
"""
    DEFAULT_PROCESSES_ARGS

Collection of function arguments to generate the default processes for new reactors.
"""
DEFAULT_PROCESSES_ARGS::Vector{Tuple} = Tuple[]
"""
    DEFAULT_PROCESSES_KWARGS

Collection of function keyword arguments to generate the default processes for new reactors.
"""
DEFAULT_PROCESSES_KWARGS::Vector{<:AbstractDict} = Dict[]
"""
    DEFAULT_PROCESSES_NAMES

Collection of names for the default processes for new reactors.
"""
DEFAULT_PROCESSES_NAMES::Vector{Symbol} = Dict[]

"""
    clear_default_processes()::Bool

Remove all default processes.
Already created reactors are not modified, this holds only for new ones.
"""
function clear_default_processes()::Bool
    global DEFAULT_PROCESSES = Vector{Union{Function, Type{Process}}}()
    global DEFAULT_PROCESSES_ARGS = Tuple[]
    global DEFAULT_PROCESSES_KWARGS = Dict[]
    global DEFAULT_PROCESSES_NAMES = Symbol[]
    return true
end

"""
    add_default_process(process::Function, args...; name, kwargs...)::Bool

Add a default process for newly created reactors.

## Example: Adding `ASM1` as default process.

Let's say normally `ASM1` is directly created using:

```jldoctest
julia> asm = ASM1(; name=:asm1, Y_H = 1); #with setting a parameter

julia> nameof(asm)
:asm1
```

Adding it as default then works as:

```jldoctest; setup = :(clear_default_processes())
julia> add_default_process(ASM1; name=:asm1, Y_H = 1) #with setting a parameter
true
julia> nameof(only(default_processes(name_prefix="")))
:asm1
```

And from then on this process will be added for every new process, until it gets removed from here.

## Arguments:
- `process`: Function to call to build the process
- `args...`: The arguments to call the functions with
- `name`: The name of the process. Will be extended for each system to avoid name clashes.
- `kwargs...`: Keyword arguments to generate the process
"""
function add_default_process(process::Union{Function, Type{Process}}, args...; name, kwargs...)::Bool
    push!(DEFAULT_PROCESSES, process)
    push!(DEFAULT_PROCESSES_ARGS, args)
    push!(DEFAULT_PROCESSES_KWARGS, kwargs)
    push!(DEFAULT_PROCESSES_NAMES, name)
    return true
end


"""
    add_default_process(process::AbstractString, args...; name, kwargs...)::Bool

Add a default matrix-defined process for newly created reactors. (Simplified syntax)

## Example: Adding the matrix-defined `ASM1` as default process.

Let's say normally `ASM1` is directly created using:

```jldoctest
julia> asm = Process("ASM1"; name=:asm1, Y_OHO = 1); #with setting a parameter

julia> nameof(asm)
:asm1
```

Adding it as default then works as:

```jldoctest; setup = :(clear_default_processes())
julia> add_default_process("ASM1"; name=:asm1, Y_OHO = 1) #with setting a parameter
true
julia> nameof(only(default_processes(name_prefix="")))
:asm1
```

This is equivalent to the more general

```jldoctest; setup = :(clear_default_processes())
julia> add_default_process(Process, "ASM1"; name=:asm1, Y_OHO = 1) #with setting a parameter
true
julia> nameof(only(default_processes(name_prefix="")))
:asm1
```

which is as well the form to be used when e.g. loading from files.

## Arguments:
- `process`: Name of the matrix-defined process (only for predefined ones)
- `args...`: The arguments to call the functions with
- `name`: The name of the process. Will be extended for each system to avoid name clashes.
- `kwargs...`: Keyword arguments to generate the process
"""
add_default_process(process::AbstractString, args...; name, kwargs...)::Bool = add_default_process(Process, process, args...; name, kwargs...)

"""
    add_default_process(process::Vector, args::Vector{<:Tuple} = Tuple[]; name::Vector{Symbol}, kwargs::Vector{<:AbstractDict} = [Dict()])::Bool

Add a series of processes to the default processes at once. Equivalent to adding all individually.

## Example

```jldoctest; setup = :(clear_default_processes())
julia> add_default_process([Process, ASM1], [("ASM1",), ()]; name=[:asm1, :asm2], kwargs=[Dict([:Y_OHO => 1]), Dict([:Y_H => 2])]) #with setting a parameter
true
julia> nameof.(default_processes(name_prefix=""))
2-element Vector{Symbol}:
 :asm1
 :asm2
```
"""
function add_default_process(process::AbstractVector, args::AbstractVector{<:Tuple} = Tuple[]; name::Vector{Symbol}, kwargs::Vector{<:AbstractDict} = [Dict()])::Bool
    all(broadcast((p, a, n, kw) -> add_default_process(p, a...; name=n, kw...), process, args, name, kwargs))
end

"""
    add_process

Add a process to the default processes. 
Syntax is specified as usually generating a process, the `name` argument is taken from the variable name (similar to `@named`)
Further the process is assigned to a variable with the same name.

It is possible to add multiple processes at once.

## Example

```jldoctest; setup = :(clear_default_processes())
julia> @add_process aer = Aeration()
Process Aeration 'aer':
States (1): see states(aer)
  S_O(t) [guess is 0.0]: S_O
Exogenous Inputs (1): see exogenous_inputs(aer)
  :k_La
Parameters (1): see parameters(aer)
  S_O_max [defaults to 8]
julia> nameof.(default_processes(name_prefix=""))
1-element Vector{Symbol}:
 :aer
julia> nameof(aer) # the variables also exist
:aer
```

The macro also allows adding multiple processes at once

```jldoctest; setup = :(clear_default_processes())
@add_process begin
    asm1 = Process("ASM1"; Y_OHO=1)
    asm2 = ASM1(Y_H=2)
end # Multiple processes can be added at once
nameof.(default_processes(name_prefix=""))

# output

2-element Vector{Symbol}:
 :asm1
 :asm2
```
"""
macro add_process(expr::Expr)
    # If provided as block, treat all lines individually
    if expr.head == :block
        return esc(:(@add_process($(expr.args[isa.(expr.args, Expr)]...))))
    end

    @assert (expr.head == :(=) && expr.args[1] isa Symbol &&
    expr.args[2] isa Expr && expr.args[2].head == :call) "Must provide a valid process call"

    name = expr.args[1]
    func = expr.args[2].args[1]
    args = expr.args[2].args[2:end]

    # Split args into kwargs and other args. Enables ; syntax for kwargs definition
    kwargs = args[[typeof(arg) == Expr && arg.head == :parameters for arg in args]]
    !isempty(kwargs) && (kwargs = only(kwargs).args)
    args = args[.![typeof(arg) == Expr && arg.head == :parameters for arg in args]]

    quote
        $name = $(func)($(args...); $(kwargs...), name=Symbol($(string(name))));
        $(:add_default_process)($func, $(args...); $(kwargs...), name=Symbol($(string(name))));
        $name
    end |> esc
end
macro add_process(exprs...)
    Expr(:block, [esc(:(@add_process($expr))) for expr in exprs]...)
end

"""
    set_default_process(process, args...; name, kwargs...)::Bool

Set the default process. Clears all current ones and adds the supplied ones.

## Example

```jldoctest; setup = :(clear_default_processes())
julia> add_default_process(Process, "ASM1"; name=:asm1, Y_OHO = 1) #with setting a parameter
true
julia> nameof(only(default_processes(name_prefix="")))
:asm1
julia> set_default_process(Process, "ASM1"; name=:asm_replaced, Y_OHO = 1) #note the set
true
julia> nameof(only(default_processes(name_prefix="")))
:asm_replaced
```
"""
set_default_process(process, args...; name, kwargs...)::Bool = clear_default_processes() && add_default_process(process, args...; name, kwargs...)

"""
    @set_process

Set the default process. Clears all current ones and adds the supplied ones.
Syntax is equal to the one of [`@add_process`](@ref).

## Example

```jldoctest; setup = :(clear_default_processes())
julia> @set_process asm = Process("ASM1", Y_OHO = 1) #with setting a parameter
Process ASM1 'asm':
States (14): see states(asm)
  S_Alk(t) [guess is 0.0]: S_Alk
  S_B(t) [guess is 0.0]: S_B
  S_BN(t) [guess is 0.0]: S_BN
  S_N2(t) [guess is 0.0]: S_N2
  S_NHx(t) [guess is 0.0]: S_NHx
  S_NOx(t) [guess is 0.0]: S_NOx
  S_O2(t) [guess is 0.0]: S_O2
  S_U(t) [guess is 0.0]: S_U
  ⋮
Parameters (35): see parameters(asm)
  K_NHxOHO [defaults to 0.05]
  m_ANOMax [defaults to 0.8]
  m_OHOMax [defaults to 6]
  K_O2ANO [defaults to 0.4]
  K_XCBhyd [defaults to 0.03]
  COD_P [defaults to 40]
  COD_C [defaults to 32]
  Y_ANO [defaults to 0.24]
  ⋮
julia> nameof(only(default_processes(name_prefix="")))
:asm
julia> @set_process asm_replaced = Process("ASM1"; Y_OHO = 1) #note the set
Process ASM1 'asm_replaced':
States (14): see states(asm_replaced)
  S_Alk(t) [guess is 0.0]: S_Alk
  S_B(t) [guess is 0.0]: S_B
  S_BN(t) [guess is 0.0]: S_BN
  S_N2(t) [guess is 0.0]: S_N2
  S_NHx(t) [guess is 0.0]: S_NHx
  S_NOx(t) [guess is 0.0]: S_NOx
  S_O2(t) [guess is 0.0]: S_O2
  S_U(t) [guess is 0.0]: S_U
  ⋮
Parameters (35): see parameters(asm_replaced)
  K_NHxOHO [defaults to 0.05]
  m_ANOMax [defaults to 0.8]
  m_OHOMax [defaults to 6]
  K_O2ANO [defaults to 0.4]
  K_XCBhyd [defaults to 0.03]
  COD_P [defaults to 40]
  COD_C [defaults to 32]
  Y_ANO [defaults to 0.24]
  ⋮
julia> nameof(only(default_processes(name_prefix="")))
:asm_replaced
```
"""
macro set_process(exprs...)
    esc(:(clear_default_processes() && @add_process($(exprs...))))
end

"""
    default_processes(;name_prefix = rand(UInt), name_postfix = "")::Vector{Process}

Get the default processes.

Returns a vector of the generated default processes. For the names, a prefix and postfix can be added to avoid name clashes.

## Keyword arguments:
- `name_prefix`: Prefix for the name. Defaults to a random number.
- `name_postfix`: Postfix for the name. Defaults to nothing.
"""
function default_processes(;name_prefix = rand(UInt), name_postfix = "")::Vector{Process}
    [proc(arg...; name=Symbol(name_prefix, isempty(string(name_prefix)) ? "" : "_", name, isempty(string(name_postfix)) ? "" : "_", name_postfix), kwarg...)
        for (proc, arg, kwarg, name) in 
            zip(DEFAULT_PROCESSES, DEFAULT_PROCESSES_ARGS, DEFAULT_PROCESSES_KWARGS, DEFAULT_PROCESSES_NAMES)
    ]
end

#####
# DEFAULT EQUAL STATES interface
#####

"""
    DEFAULT_STATE_MAPPING::AbstractDict{Symbol, <:AbstractSet}

The matting from STATE_NAME => Set of states which are mapped to it.
"""
DEFAULT_STATE_MAPPING::Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} = Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}}()

"""
    clear_default_state_mapping()::Bool

Clear the current default state mapping. Only affects future generated systems.
"""
function clear_default_state_mapping()::Bool
    global DEFAULT_STATE_MAPPING = Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}}()
    return true
end

"""
    add_default_state_mapping(name::Symbol, equal_states::AbstractSet{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool)::Bool

Add a new state mapping.

If any of the newly added equal states is already in an other group of equal states, 
the newly added states get set equal to all in this group.

If the newly added equal states combine multiple other groups (intersection with multiples),
all of these sets get combined to a new one.

## Example

```jldoctest
julia> set_default_state_mapping(;S_O = ([:S_O, :S_O2], false, false, true), X = ([:X_H, :X_OHO], true, false, false))
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2]), particulate = 0, colloidal = 0, soluble …
julia> add_default_state_mapping(:S_O, [:S_O, :S_OHO], false, false, true)
┌ Info: Adding Set([:S_O, :S_OHO]) to the already existing equal states of (names = Set([:S_O, :S_O2]), particulate = false, colloidal = false, soluble = true)
│ 
│  Now the following states are all considered equal:
│ 
└  Set([:S_O, :S_O2, :S_OHO])
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2, :S_OHO]), particulate = 0, colloidal = 0, …
julia> add_default_state_mapping(:X, [:X_H, :S_OHO], true, false, false)
ERROR: ArgumentError: Cannot add Set([:X_H, :X, :S_OHO]) to the state mapping with the name X.
This overlapps with the existing mappings Set{Symbol}[Set([:X_OHO, :X_H, :X]), Set([:S_O, :S_O2, :S_OHO])]
which are associated with the different names [:X, :S_O].
[...]
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2, :S_OHO]), particulate = 0, colloidal = 0, …
julia> add_default_state_mapping(:X, [:X_H, :X_B], false, true, false) # If added, need to have same solubility properties
ERROR: ArgumentError: Cannot add Set([:X_H, :X_B, :X]) to the state mapping with the name X.
X is already mapped to Set([:X_OHO, :X_H, :X]) which have solubility properties (particulate,)
while the given states have the following solubility properties: (colloidal,).
[...]
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2, :S_OHO]), particulate = 0, colloidal = 0, …
```
"""
function add_default_state_mapping(name::Symbol, equal_states::AbstractSet{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool)::Bool
    equal_states = Set([equal_states..., name])
    mappings = collect(DEFAULT_STATE_MAPPING)
    inset = .!isdisjoint.(Ref(equal_states), getproperty.(last.(mappings), :names))
    if !any(inset)
        DEFAULT_STATE_MAPPING[name] = (;names=equal_states, particulate, colloidal, soluble)
    elseif sum(inset) == 1
        if first(only(mappings[inset])) != name
            throw(ArgumentError("Cannot add $equal_states to the state mapping with the name $name.\n\
            This overlapps with the existing mapping $(last(only(mappings[inset])))\n\
            which has the name $(first(only(mappings[inset])))."))
        end
        if last(only(mappings[inset])).particulate != particulate || last(only(mappings[inset])).colloidal != colloidal || last(only(mappings[inset])).soluble != soluble
            throw(ArgumentError("Cannot add $equal_states to the state mapping with the name $name.\n$name is already mapped to \
            $(last(only(mappings[inset])).names) which have solubility properties ($(last(only(mappings[inset])).particulate ? "particulate," : "")\
            $(last(only(mappings[inset])).colloidal ? "colloidal," : "")$(last(only(mappings[inset])).soluble ? "soluble" : ""))\nwhile the given \
            states have the following solubility properties: ($(particulate ? "particulate," : "")\
            $(colloidal ? "colloidal," : "")$(soluble ? "soluble" : ""))."))
        end
        @info "Adding $equal_states to the already existing equal states of $(last(only(mappings[inset])))\n\n \
        Now the following states are all considered equal:\n\n $(union(last(only(mappings[inset])).names, equal_states))"
        union!(DEFAULT_STATE_MAPPING[name].names, equal_states)
    else
        throw(ArgumentError("Cannot add $equal_states to the state mapping with the name $name.\n\
        This overlapps with the existing mappings $(getproperty.(last.(mappings[inset]), :names))\n\
        which are associated with the different names $(first.(mappings[inset]))."))
    end
    return true
end

"""
    add_default_state_mapping(name::Symbol, equal_states::AbstractSet{<:Union{<:Num, <:SymbolicUtils.BasicSymbolic}})::Bool

Add a new state mapping.

Provide the equal states as symbols, allows to extract particulate info directly from there.

## Example

```jldoctest
julia> @variables S_O [particulate=false, colloidal=false, soluble=true] S_O2 [particulate=false, colloidal=false, soluble=true]
2-element Vector{Num}:
  S_O
 S_O2
julia> @variables X_H [particulate=true, colloidal=false, soluble=false] X_OHO [particulate=true, colloidal=false, soluble=false]
2-element Vector{Num}:
   X_H
 X_OHO
julia> set_default_state_mapping(;S_O = [S_O, S_O2], X = [X_H, X_OHO])
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2]), particulate = 0, colloidal = 0, soluble …
julia> add_default_state_mapping(:S_O, [:S_O, :S_OHO], false, false, true)
┌ Info: Adding Set([:S_O, :S_OHO]) to the already existing equal states of (names = Set([:S_O, :S_O2]), particulate = false, colloidal = false, soluble = true)
│ 
│  Now the following states are all considered equal:
│ 
└  Set([:S_O, :S_O2, :S_OHO])
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2, :S_OHO]), particulate = 0, colloidal = 0, …
julia> add_default_state_mapping(:X, [:X_H, :S_OHO], true, false, false)
ERROR: ArgumentError: Cannot add Set([:X_H, :X, :S_OHO]) to the state mapping with the name X.
This overlapps with the existing mappings Set{Symbol}[Set([:X_OHO, :X_H, :X]), Set([:S_O, :S_O2, :S_OHO])]
which are associated with the different names [:X, :S_O].
[...]
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2, :S_OHO]), particulate = 0, colloidal = 0, …
```
"""
function add_default_state_mapping(name, equal_states::AbstractSet{<:Union{<:Num, <:SymbolicUtils.BasicSymbolic}})
    if !(allequal(isparticulate.(equal_states)) && allequal(iscolloidal.(equal_states)) && allequal(issoluble.(equal_states)))
        throw(ArgumentError("Cannot set the given states ($equal_states) equal. They have different solubility properties:\n\
        $(["$n: $(p ? "particulate, " : "")$(c ? "colloidal, " : "")$(s ? "soluble" : "")" for (n, p, c, s) in zip(equal_states, isparticulate.(equal_states), iscolloidal.(equal_states), issoluble.(equal_states))])"))
    else
        particulate = isparticulate(first(equal_states))
        colloidal = iscolloidal(first(equal_states))
        soluble = issoluble(first(equal_states))
    end
    add_default_state_mapping(Symbol(symbolic_to_namestring(name)), Symbol.(symbolic_to_namestring.(equal_states)), particulate, colloidal, soluble)
end
function add_default_state_mapping(name, equal_states::Union{<:AbstractArray, <:AbstractSet, <:Tuple})
    # Split into symbolic and other states
    sym_states = filter(s -> s isa Num || s isa SymbolicUtils.BasicSymbolic, equal_states)
    nonsym_states = filter(s -> !(s isa Num || s isa SymbolicUtils.BasicSymbolic), equal_states)

    if isempty(sym_states)
        throw(ArgumentError("Cannot add non-symbolic equal states without solubility properties. Please provide them or at least one symbolic carrying it."))
    end

    # Add them individually, first symbolic ones and then others, with solubility properties of symbolic ones
    add_default_state_mapping(name, Set{Union{Num, SymbolicUtils.BasicSymbolic}}(sym_states))
    add_default_state_mapping(name, Set(nonsym_states), isparticulate(first(sym_states)), iscolloidal(first(sym_states)), issoluble(first(sym_states)))
end
add_default_state_mapping(name, equal_states::AbstractVector{<:Union{<:Num, <:SymbolicUtils.BasicSymbolic}}) = add_default_state_mapping(name, Set(equal_states))
add_default_state_mapping(name, equal_states::Union{<:Num, SymbolicUtils.BasicSymbolic}) = add_default_state_mapping(name, Set([equal_states]))
add_default_state_mapping(name, equal_states::Union{Symbol, String}, particulate::Bool, colloidal::Bool, soluble::Bool) = add_default_state_mapping(Symbol(symbolic_to_namestring(name)), Set([Symbol(symbolic_to_namestring(equal_states))]), particulate, colloidal, soluble)
add_default_state_mapping(name, equal_states, particulate::Bool, colloidal::Bool, soluble::Bool) = add_default_state_mapping(Symbol(symbolic_to_namestring(name)), Set(Symbol.(symbolic_to_namestring.(equal_states))), particulate, colloidal, soluble)
add_default_state_mapping(name, (equal_states, particulate, colloidal, soluble)::Tuple{Any, Bool, Bool, Bool}) = add_default_state_mapping(name, equal_states, particulate, colloidal, soluble)

"""
    add_default_state_mapping(;kwargs...)::Bool

Add a new set of state mappings. Takes kwargs as inputs for nice syntax.

## Example

```jldoctest; setup = :(clear_default_state_mapping())
julia> add_default_state_mapping(; a = ([:a, :b], true, false, false)) # Note the semicolon in the beginning
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 1 entry:
  :a => (names = Set([:a, :b]), particulate = 1, colloidal = 0, soluble = 0)
julia> d = Dict([:d => ([:e, :f], true, false, false)])
Dict{Symbol, Tuple{Vector{Symbol}, Bool, Bool, Bool}} with 1 entry:
  :d => ([:e, :f], 1, 0, 0)
julia> add_default_state_mapping(;d...) # Allows dicts to be splatted
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :a => (names = Set([:a, :b]), particulate = 1, colloidal = 0, soluble = 0)
  :d => (names = Set([:f, :d, :e]), particulate = 1, colloidal = 0, soluble = 0)
```
"""
add_default_state_mapping(;kwargs...)::Bool = all([add_default_state_mapping(k, v) for (k, v) in kwargs])

"""
    @add_state_mapping

Add a new set of state mappings using a macro.

Macro syntax is:
- New symbol (the states are mapped to) (e.g. `S_O2`)
- `=`
- Tuple of mapped states (e.g. `(S_O2, S_O)`)
- Vector of solubility properties (optional if at least one of the mapped states is a symbol) (e.g. `[:soluble]`)

E.g. the syntax for a example would be

```jldoctest; setup = :(clear_default_state_mapping())
julia> @add_state_mapping S_O2 = (:S_O, :S_O2) [:soluble]
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 1 entry:
  :S_O2 => (names = Set([:S_O, :S_O2]), particulate = 0, colloidal = 0, soluble…
```

## Example

```jldoctest; setup = :(clear_default_state_mapping())
julia> @add_state_mapping a = (:a, :b) [:colloidal]
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 1 entry:
  :a => (names = Set([:a, :b]), particulate = 0, colloidal = 1, soluble = 0)
julia> @add_state_mapping X_H = :X_OHO [:particulate] # For single mappings, only the symbols can be provided
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :a   => (names = Set([:a, :b]), particulate = 0, colloidal = 1, soluble = 0)
  :X_H => (names = Set([:X_OHO, :X_H]), particulate = 1, colloidal = 0, soluble…
```

Multiple mappings can also provided, but they need to be wrapped in {...} if using a `begin ... end` block.


```jldoctest; setup = :(clear_default_state_mapping())
@add_state_mapping begin
    {X_A = :X_ANO [:particulate]}
    {S_ALK = :S_alk [:soluble]}
end
default_state_mapping()

# output

Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X_A   => (names = Set([:X_A, :X_ANO]), particulate = 1, colloidal = 0, solub…
  :S_ALK => (names = Set([:S_ALK, :S_alk]), particulate = 0, colloidal = 0, sol…
```
"""
macro add_state_mapping(args...)
    # Parse Blocks
    if length(args) == 1 && only(args) isa Expr && only(args).head == :block
        args = filter(x -> !(x isa LineNumberNode), only(args).args)
        args = map(x -> x.head == :bracescat ? only(x.args) : :($x,), args)
        return Expr(:block, [esc(:(@add_state_mapping($(arg.args...)))) for arg in args]...)
    end

    # If multiple given, split into multiple macro calls
    if length(args) > 1 && !all(isa.(args[2:end], QuoteNode) .|| (isa.(args[2:end], Expr) .&& getproperty.(args[2:end], :head) .== :vect))
        indiv = [Union{Expr, QuoteNode}[args[1]]]
        for arg in args[2:end]
            if arg isa QuoteNode || (arg isa Expr && arg.head == :vect)
                push!(last(indiv), arg)
            else
                push!(indiv, Union{Expr, QuoteNode}[arg])
            end
        end
        return Expr(:block, [esc(:(@add_state_mapping($(arg...)))) for arg in indiv]...)
    end

    # Split into expression and arguments
    expr = args[1]
    args = args[2:end]
    isempty(args) && (args = :([]))
    !(args isa Expr) && length(args) == 1 && (args = only(args))

    @assert (expr.head == :(=) && expr.args[1] isa Symbol) "Must provide a valid state map. State maps are defined using '=' e.g. 'S_O = :S_O2'"
    @assert (args isa QuoteNode || (args isa Expr && args.head == :vect)) "Arguments must be provided as single symbol or contained in a vector."
    if args isa QuoteNode
        args = [args]
    else
        args = args.args
    end
    name = Symbol("$(expr.args[1])")
    equal_states = expr.args[2]
    if equal_states isa Expr && equal_states.head == :tuple
        equal_states = equal_states.args
    else
        equal_states = [equal_states]
    end

    particulate = false
    colloidal = false
    soluble = false
    any(args .== Ref(:(:particulate))) && (particulate = true)
    any(args .== Ref(:(:colloidal))) && (colloidal = true)
    any(args .== Ref(:(:soluble))) && (soluble = true)

    if !any([particulate, colloidal, soluble])
        esc(:(add_default_state_mapping(Symbol($(string(name))), Set([$(equal_states...)]))))
    else
        esc(:(add_default_state_mapping(Symbol($(string(name))), Set([$(equal_states...)]), $particulate, $colloidal, $soluble)))
    end
end

"""
    set_default_state_mapping(...)::Bool

Clear the current state mappings and set the new ones.
Accepts all syntax available for `add_default_state_mapping()`

## Example

```jldoctest
julia> set_default_state_mapping(;S_O = ([:S_O, :S_O2], false, false, true), X = ([:X_H, :X_OHO], true, false, false))
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2]), particulate = 0, colloidal = 0, soluble …
julia> set_default_state_mapping(:S_O, [:S_O, :S_OHO], false, false, true)
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 1 entry:
  :S_O => (names = Set([:S_O, :S_OHO]), particulate = 0, colloidal = 0, soluble…
```
"""
set_default_state_mapping(args...; kwargs...)::Bool = clear_default_state_mapping() && add_default_state_mapping(args...; kwargs...)

"""
    @set_state_mapping

Clear the current state mappings and set new ones.
Uses the same syntax as [`@add_state_mapping`](@ref)

## Example

```jldoctest
julia> @set_state_mapping S_O = (:S_O, :S_O2) [:soluble] X = (:X_H, :X_OHO) [:particulate]
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 2 entries:
  :X   => (names = Set([:X_OHO, :X_H, :X]), particulate = 1, colloidal = 0, sol…
  :S_O => (names = Set([:S_O, :S_O2]), particulate = 0, colloidal = 0, soluble …
julia> @set_state_mapping S_O = (:S_O, :S_OHO) [:soluble]
true
julia> default_state_mapping()
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} with 1 entry:
  :S_O => (names = Set([:S_O, :S_OHO]), particulate = 0, colloidal = 0, soluble…
```
"""
macro set_state_mapping(expr, args...)
    esc(:(clear_default_state_mapping() && @add_state_mapping($expr, $(args...))))
end

"""
    default_state_mapping()::Dict{Symbol, Set}

Get the current state mapping.
"""
function default_state_mapping()::Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}}
    return DEFAULT_STATE_MAPPING
end


"""
    get_default_state(sys::Symbol; state_mapping::AbstractDict{Symbol, <:AbstractSet} = default_state_mapping())::Symbol

Get the default state to which the given symbol is mapped to.
I.e. a reverse lookup in the state mapping.

If the given symbol is not in the state mapping, it is directly returned.

Throws an error if the mapping of this symbol is invalid (i.e. different defaults for this same symbol).
The default mapping should not allow this. But it cannot be excluded in case of a bug or
a provided mapping.
No error is thrown if the mapping is invalid, but everything is fine for the symbol asked for.

## Example
```jldoctest
julia> set_default_state_mapping(S_O2=(Set([:S_O, :S_O2]), false, false, true))
true
julia> get_default_state(:S_O)
:S_O2
julia> get_default_state(:S_O2)
:S_O2
julia> get_default_state(:X_H)
:X_H
julia> get_default_state(:S_O, state_mapping=Dict([:S_O2 => (names=Set([:S_O, :S_O2]), particulate=false, colloidal=false, soluble=true), :DO => (names=Set([:S_O]), particulate=false, colloidal=false, soluble=true)]))
ERROR: State Mapping error: S_O is mapped to multiple default states ([:DO, :S_O2]).

This means there is an error in the state mapping
Dict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}}(:DO => (names = Set([:S_O]), particulate = 0, colloidal = 0, soluble = 1), :S_O2 => (names = Set([:S_O, :S_O2]), particulate = 0, colloidal = 0, soluble = 1)).
If you used the default one: Did you modify any internals? If not, please file an issue.
[...]
julia> get_default_state(:S_O2, state_mapping=Dict([:S_O2 => (names=Set([:S_O, :S_O2]), particulate=false, colloidal=false, soluble=true), :DO => (names=Set([:S_O]), particulate=false, colloidal=false, soluble=true)]))
:S_O2
```
"""
function get_default_state(sys::Symbol; state_mapping::AbstractDict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} = default_state_mapping())::Symbol
    lookup = [k for (k, v) in state_mapping if sys in v.names]
    if isempty(lookup)
        return sys
    elseif length(lookup) == 1 
        return only(lookup)
    else
        throw(ErrorException("State Mapping error: $sys is mapped to multiple default states ($lookup).\n\n\
                This means there is an error in the state mapping\n$state_mapping.\n\
                If you used the default one: Did you modify any internals? If not, please file an issue."))
    end
end
"""
    get_default_state(sys; state_mapping = default_state_mapping())::Symbol

Allow for non-symbol state queries (e.g. Symbolics variables or strings.)

## Examples
```jldoctest
julia> set_default_state_mapping(S_O2=(Set([:S_O, :S_O2]), false, false, true))
true
julia> get_default_state("S_O")
:S_O2
julia> get_default_state(only(@variables S_O(t)))
:S_O2
```
"""
function get_default_state(sys; state_mapping = default_state_mapping())::Symbol
    get_default_state(Symbol(symbolic_to_namestring(sys)); state_mapping)
end

"""
    default_states(;processes = default_processes(), state_mapping = default_state_mapping())

Get the default states using the given processes and mapping.

Get the combined states of all given processes with all equally named ones only once.
Applies the state_mapping to rename given states and combine the ones mapped to the same name.
"""
function default_states(;processes = default_processes(), state_mapping = default_state_mapping())::Vector{Num}
    processes_states = isempty(processes) ? [] : Num.(union(unknowns.(states.(processes))...))
    isempty(state_mapping) && return sort(unique(processes_states); by=string)

    all_states = vcat(processes_states, [only(@variables $key(t) [particulate=val.particulate, colloidal=val.colloidal, soluble=val.soluble]) for (key, val) in state_mapping if !(key in Symbol.(symbolic_to_namestring.(processes_states)))]) 
    mapped_states = map(x -> begin
        mapped = [only(all_states[key .== Symbol.(symbolic_to_namestring.(all_states))]) for (key, val) in collect(state_mapping) if Symbol(symbolic_to_namestring(x)) in val.names]
        isempty(mapped) && return x
        return only(mapped)
    end, processes_states)
    return sort(unique(mapped_states); by=string)
end
