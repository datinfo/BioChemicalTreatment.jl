"""
    PortMetadata

Struct to be associated with a Port-type [`ODESystem`](@ref). 
Contains:

- `name`: Port Name
- `type`: Port type
- `isinput`: If the port is an input
- `index`: The number of the port
- `parent`: The parent system
- `other`: Any other metadata
"""
mutable struct PortMetadata
    name::Union{Missing, Symbol}
    type::Union{Missing, Symbol}
    isinput::Union{Missing, Bool}
    index::Union{Missing, Integer}
    parent::Union{Missing, AbstractProcessElement, ModelingToolkit.AbstractSystem}
    other

    function PortMetadata(name, type, isinput, index, parent, other = nothing)
        new(name, type, isinput, index, parent, other)
    end

    function PortMetadata(other)
        PortMetadata(missing, missing, missing, missing, missing, other)
    end
end

"""
    init_port_metadata(port)

Initialize the metadata of an ODESystem with a PortMetadata struct to save port metadata.

!!! warning
    Does **not** mutate the object inplace and needs to be reassigned afterwards.
"""
function init_port_metadata(port::ModelingToolkit.AbstractODESystem)
    @set port.metadata = PortMetadata(ModelingToolkit.get_metadata(port))
end

"""
    set_port_metadata!(port, pe::AbstractProcessElement)

Set the metadata of `port` which is an connector of the processelement `pe` according to its position in the process element. 
"""
function set_port_metadata!(port::ModelingToolkit.AbstractODESystem, pe::AbstractProcessElement)
    name, type, isinput, index, parent = assemble_port_metadata_fields(port, pe)
    # Set the name of the port. Strip the parent name
    ModelingToolkit.get_metadata(port).name = name
    ModelingToolkit.get_metadata(port).type = type
    ModelingToolkit.get_metadata(port).isinput = isinput
    ModelingToolkit.get_metadata(port).index = index
    ModelingToolkit.get_metadata(port).parent = parent
end
"""
    set_port_metadata!(pe::AbstractProcessElement, getter_function)

Set the metadata of all ports of `pe` which are output by applying the `getter_function` on it. The metadata is set according to its position in the process element.

Usually, the `getter_function` is one of 
- `states`: Set the metadata of all states in the system
- `rates`: Set the metadata of all rates in the system
- `inflows`: Set the metadata of all inflows in the system
- `outflows`: Set the metadata of all outflows in the system
- `exogenous_inputs`: Set the metadata of all exogenous_inputs in the system
- `exogenous_outputs`: Set the metadata of all exogenous_outputs in the system
"""
function set_port_metadata!(pe::AbstractProcessElement, getter_function::Function)
    ports = getter_function(pe)
    if ports isa ModelingToolkit.AbstractODESystem
        set_port_metadata!(ports, pe)
        return
    elseif isnothing(ports)
        return
    end
    for port in ports
        set_port_metadata!(port, pe)
    end
end
"""
    set_port_metadata!(pe::AbstractProcessElement)

Set the metadata all connectors in the processelement `pe` according to the type and it position in the process element. 
"""
function set_port_metadata!(pe::AbstractProcessElement)
    hasstates(pe) && set_port_metadata!(pe, states)
    hasrates(pe) && set_port_metadata!(pe, rates)
    hasinflows(pe) && set_port_metadata!(pe, inflows)
    hasoutflows(pe) && set_port_metadata!(pe, outflows)
    hasexogenous_inputs(pe) && set_port_metadata!(pe, exogenous_inputs)
    hasexogenous_outputs(pe) && set_port_metadata!(pe, exogenous_outputs)
    nothing
end

"""
    assemble_port_metadata_fields(port, pe::AbstractProcessElement)

Get the metadata fields of `port` which is an connector of the processelement `pe` according to its position in the process element.
Throws an error if the port is not found in any of the connectors of the process element `pe`. 
"""
function assemble_port_metadata_fields(port::ModelingToolkit.AbstractSystem, parent::AbstractProcessElement)
    # Set the name of the port. Strip the parent name
    name = Symbol(lstrip_namespace(nameof(port), nameof(parent))) # Strip parent name
    # Set the port metadata depending on the type of the port
    if hasstates(parent) && port == states(parent)
        type = :state
        isinput = all(ModelingToolkit.isinput.(unknowns(port)))
        index = 1
    elseif hasrates(parent) && port == rates(parent)
        type = :rate
        isinput = all(ModelingToolkit.isinput.(unknowns(port)))
        index = 1
    elseif hasinflows(parent) && port in inflows(parent) 
        type = :flow
        isinput = true
        index = findfirst(Ref(port) .== inflows(parent))   
    elseif hasoutflows(parent) && port in outflows(parent)
        type = :flow
        isinput = false
        index = findfirst(Ref(port) .== outflows(parent))   
    elseif hasexogenous_inputs(parent) && port in exogenous_inputs(parent) 
        type = :exogenous
        isinput = true
        index = findfirst(Ref(port) .== exogenous_inputs(parent))   
    elseif hasexogenous_outputs(parent) && port in exogenous_outputs(parent) 
        type = :exogenous
        isinput = false
        index = findfirst(Ref(port) .== exogenous_outputs(parent))
    else
        error("The system $name is not in any of the connectors of the processelement $(nameof(parent))")
    end
    return name, type, isinput, index, parent
end

"""
    assemble_port_metadata_fields(port, parent::ModelingToolkit.AbstractSystem; port_type = :unknown)

Get the metadata fields of `port` which is an connector of the system `pe`.
The fields are assigned as followed
- `name`: The name of the `port` with the name of the parent stripped on the left.
- `type`: The given `port_type` argument. Defaults to :unknown.
- `isinput`: `false` if the any part of the namespace in the name (without the parent) starts with the regex `r"[Oo]ut(put)?"`. Otherwise `true` -> Default treated as 
- `index`: If any part of the namespace in the name (without the parent) ends with a number, the last of these numbers is the index. If none, default to 1.
- `parent`: The given parent system

Throws a warning if the name of the port does not start with the name of the parent.
"""
function assemble_port_metadata_fields(port::ModelingToolkit.AbstractSystem, parent::ModelingToolkit.AbstractSystem; port_type = :unknown)
    # Set the name of the port. Strip the parent name
    name = lstrip_namespace(nameof(port), nameof(parent)) # Strip parent name
    name == string(nameof(port)) && (@warn "The name of the port ($name) does not start with the name of the parent ($(nameof(parent))). Are you sure the port is part of the parent?")

    # Check if isinput 
    # if any name part matches with r"[Oo]ut(put)?", treat as output, otherwise as input
    isinput = !any(occursin.(r"[Oo]ut(put)?", split_namespace(name)))

    # Get index, number if at the end of the name. Defaults to 1 (may result in multiple index ones for the same system...)
    index = match.(r"[0-9]+$", split_namespace(name))
    !all(isnothing.(index)) && (index = tryparse.(Int, getproperty.(index, :match)))
    all(isnothing.(index)) && (index = 1) # Default is 1 if no match
    index isa AbstractArray && (index = last(index))

    return Symbol(name), port_type, isinput, index, parent
end
