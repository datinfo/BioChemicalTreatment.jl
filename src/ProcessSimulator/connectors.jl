"""
    InflowPort(components; initial_flow, initial_concentrations, set_guess, name)

A connector for connecting inflows carrying components.

# Parameters:
- `components`: The components in the flow. Can be provided as:
    - A `Dict` with keys being Strings representing the names of the components in the flow and values being Boolean meaning if the component is particulate or not. Does not include the flow rate.
    - An array of symbols for the components. Name and if particulate or not are taken from these symbols. May or may not include the the flow rate `q`.
- `initial_flow`: The initial value for the flow rate `q`. Defaults to 0.
- `initial_concentrations`: Initial concentrations of the components in the flow, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each component or the default values of the symbols if given as symbols.
- `set_guess`: Initial values set only guesses. This is to prevent contradicting initial conditions. Defaults to true.

# States:
- `q`: The flow rate
- A state for each component specified by the `components` parameter.
"""
@connector function InflowPort(components::Dict{<:AbstractString, @NamedTuple{particulate::Bool, colloidal::Bool, soluble::Bool}}; initial_flow = 0.0, initial_concentrations = zeros(length(components)), set_guess = true, name)
    # Support initial concentrations in all formats that ModelingToolkit supports
    initial_concentrations = to_ordered_array(collect(keys(components)), initial_concentrations)
    # Set the initial flow to 0 if not given
    if isnothing(initial_flow)
        initial_flow = 0.0
    end

    # Flow state
    if set_guess
        states = @variables q(t) [
            input = true,
            guess = initial_flow,
            description = "Flow Rate"
        ]
    else
        states = @variables q(t) = initial_flow [
            input = true,
            description = "Flow Rate"
        ]
    end

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add particulate and description metadata for each
    for (component, c_init) = zip(collect(components)[p], initial_concentrations[p])
        symb = Symbol(component[1])
        if set_guess
            state = @variables $symb(t) [
                input = true,                    
                particulate = component[2].particulate,
                colloidal = component[2].colloidal,
                soluble = component[2].soluble,
                guess = c_init,
                description = component[1]*" concentration"
            ]
        else
            state = @variables $symb(t) = c_init [
                input = true,
                particulate = component[2].particulate,
                colloidal = component[2].colloidal,
                soluble = component[2].soluble,
                description = component[1]*" concentration"
            ]
        end
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function InflowPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_flow = nothing, initial_concentrations = nothing, set_guess = true, name)
    # Get if any component is the flow rate (Distinguished by not having an isparticulate attribute)
    flow_connect = isnothing.(isparticulate.(components; default = nothing))
    if sum(flow_connect) > 1
        error("All components in the flow must have the particulate attribute (One without is accepted and considered as the flow rate)")
    end
    # Get the initial flow if not given (from the symbolic variable default or 0)
    if isnothing(initial_flow)
        initial_flow = any(flow_connect) ? get_default_or_guess(components[flow_connect][1], 0.0) : 0.0
    end
    # Sort the flow rate out from components
    components = components[.!flow_connect]
    # Get the initial concentrations if not given (from symbolic variable default or 0)
    if isnothing(initial_concentrations)
        initial_concentrations = get_default_or_guess.(components, 0)
    end
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_concentrations) <: Pair)
        initial_concentrations = Dict(zip(components, initial_concentrations))
    end

    # Get the names of the components and if they are particulate, then combine them to a dict
    names = symbolic_to_namestring.(components)
    particulate = isparticulate.(components; default = nothing)
    colloidal = iscolloidal.(components; default = nothing)
    soluble = issoluble.(components; default = nothing)
    particle_size = broadcast(
        (p, c, s) -> all(isnothing.([p, c, s])) ? 
        nothing : 
        (particulate = isnothing(p) ? false : p, colloidal = isnothing(c) ? false : c, soluble = isnothing(s) ? false : s), 
        particulate, colloidal, soluble)
    comp_dict = Dict(zip(names, particle_size))

    InflowPort(comp_dict; initial_flow, initial_concentrations, set_guess, name)
end
@connector function InflowPort(components::AbstractArray{<:Symbolics.Num}; initial_flow = nothing, initial_concentrations = nothing, set_guess = true, name)
    # Unwrap the Num to a Symbolic and forward it
    InflowPort(Symbolics.value.(components); initial_flow, initial_concentrations, set_guess, name)
end

"""
    OutflowPort(components; initial_flow, initial_concentrations, set_guess, name)

A connector for connecting outflows carrying components.

# Parameters:
- `components`: The components in the flow. Can be provided as:
    - A `Dict` with keys being Strings representing the names of the components in the flow and values being Boolean meaning if the component is particulate or not. Does not include the flow rate.
    - An array of symbols for the components. Name and if particulate or not are taken from these symbols. May or may not include the the flow rate `q`.
- `initial_flow`: The initial value for the flow rate `q`. Defaults to 0.
- `initial_concentrations`: Initial concentrations of the components in the flow, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each component or the default values of the symbols if given as symbols.
- `set_guess`: Initial values set only guesses. This is to prevent contradicting initial conditions. Defaults to true.

# States:
- `q`: The flow rate
- A state for each component specified by the `components` parameter.
"""
@connector function OutflowPort(components::Dict{<:AbstractString, @NamedTuple{particulate::Bool, colloidal::Bool, soluble::Bool}}; initial_flow = 0.0, initial_concentrations = zeros(length(components)), set_guess = true, name)
    # Support initial concentrations in all formats that ModelingToolkit supports
    initial_concentrations = to_ordered_array(collect(keys(components)), initial_concentrations)
    # Set the initial flow to 0 if not given
    if isnothing(initial_flow)
        initial_flow = 0.0
    end

    # Flow state
    if set_guess
        states = @variables q(t) [
            output = true,
            guess = initial_flow,
            description = "Flow Rate"
        ]
    else
        states = @variables q(t) = initial_flow [
            output = true,
            description = "Flow Rate"
        ]
    end

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add particulate and description metadata for each
    for (component, c_init) = zip(collect(components)[p], initial_concentrations[p])
        symb = Symbol(component[1])
        if set_guess
            state = @variables $symb(t) [
                output = true,
                particulate = component[2].particulate,
                colloidal = component[2].colloidal,
                soluble = component[2].soluble,
                guess = c_init,
                description = component[1]*" concentration"
            ]
        else
            state = @variables $symb(t) = c_init [
                output = true,
                particulate = component[2].particulate,
                colloidal = component[2].colloidal,
                soluble = component[2].soluble,
                description = component[1]*" concentration"
            ]
        end
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function OutflowPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_flow = nothing, initial_concentrations = nothing, set_guess = true, name)
    # Get if any component is the flow rate (Distinguished by not having an isparticulate attribute)
    flow_connect = isnothing.(isparticulate.(components; default = nothing))
    if sum(flow_connect) > 1
        error("All components in the flow must have the particulate attribute (One without is accepted and considered as the flow rate)")
    end
    # Get the initial flow if not given (from the symbolic variable default or 0)
    if isnothing(initial_flow)
        initial_flow = any(flow_connect) ? get_default_or_guess(components[flow_connect][1], 0.0) : 0.0
    end
    # Sort the flow rate out
    components = components[.!flow_connect]
    # Get the initial concentrations if not given (from symbolic variable default or 0)
    if isnothing(initial_concentrations)
        initial_concentrations = get_default_or_guess.(components, 0)
    end
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_concentrations) <: Pair)
        initial_concentrations = Dict(zip(components, initial_concentrations))
    end

    # Get the names of the components and if they are particulate, then combine them to a dict
    names = symbolic_to_namestring.(components)
    particulate = isparticulate.(components; default = nothing)
    colloidal = iscolloidal.(components; default = nothing)
    soluble = issoluble.(components; default = nothing)
    particle_size = broadcast(
        (p, c, s) -> all(isnothing.([p, c, s])) ? 
        nothing : 
        (particulate = isnothing(p) ? false : p, colloidal = isnothing(c) ? false : c, soluble = isnothing(s) ? false : s), 
        particulate, colloidal, soluble)
    comp_dict = Dict(zip(names, particle_size))

    OutflowPort(comp_dict; initial_flow, initial_concentrations, set_guess, name)
end
@connector function OutflowPort(components::AbstractArray{<:Symbolics.Num}; initial_flow = nothing, initial_concentrations = nothing, set_guess = true, name)
    # Unwrap the Num to a Symbolic and forward it
    OutflowPort(Symbolics.value.(components); initial_flow, initial_concentrations, set_guess, name)
end


"""
    ReactionInputPort(components; initial_rates, name)

A connector for connecting reaction rates as input.

# Parameters:
- `components`: The components. Can be provided as:
    - An array of Strings for the component names.
    - An array of symbols for the components, from which the names are taken.
- `initial_rates`: The initial reaction rates, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for all rates or the default value if given as symbolic.
- `set_guess`: Initial values set only guesses. This is to prevent contradicting initial conditions. Defaults to true

# States:
- A state for each reaction rate specified by the `components` parameter.
"""
@connector function ReactionInputPort(components::AbstractArray{<:AbstractString}; initial_rates = zeros(length(components)), set_guess = true, name)
    # Support initial rates in all formats that ModelingToolkit supports
    initial_rates = to_ordered_array(components, initial_rates)

    # Vector for taking up all the states
    states = Symbolics.Num[]

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add description metadata for each
    for (component, init) = zip(collect(components)[p], initial_rates[p])
        symb = Symbol(component)
        if set_guess
            state = @variables $symb(t) [
                input = true,
                guess = init,
                description = component*" reaction rate"
            ]
        else
            state = @variables $symb(t) = init [
                input = true,
                description = component*" reaction rate"
            ]
        end
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function ReactionInputPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_rates = nothing, set_guess = true, name)
    if isnothing(initial_rates)
        initial_rates = get_default_or_guess.(components, 0)
    end
    # Get the component rates and forward it
    names = symbolic_to_namestring.(components)
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_rates) <: Pair)
        initial_rates = Dict(zip(components, initial_rates))
    end

    ReactionInputPort(names; initial_rates, set_guess, name)
end
@connector function ReactionInputPort(components::AbstractArray{<:Symbolics.Num}; initial_rates = nothing, set_guess = true, name)
    # Unwrap the components to a Symbolic and forward it
    ReactionInputPort(Symbolics.value.(components); initial_rates, set_guess, name)
end


"""
    ReactionOutputPort(components; initial_rates, name)

A connector for connecting reaction rates as output.

# Parameters:
- `components`: The components. Can be provided as:
    - An array of Strings for the component names.
    - An array of symbols for the components, from which the names are taken.
- `initial_rates`: The initial reaction rates, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for all rates or the default value if given as symbolic.
- `set_guess`: Initial values set only guesses. This is to prevent contradicting initial conditions. Defaults to true

# States:
- A state for each reaction rate specified by the `components` parameter.
"""
@connector function ReactionOutputPort(components::AbstractArray{<:AbstractString}; initial_rates = zeros(length(components)), set_guess = true, name)
    # Support initial rates in all formats that ModelingToolkit supports
    initial_rates = to_ordered_array(components, initial_rates)

    # Vector to be filled with the states
    states = Symbolics.Num[]

    # Sort the components as otherwise the connect() sometimes connects wrong components
    p = sortperm(collect(components), by = x->x[1])
    # State for each component: Add description metadata for each
    for (component, init) = zip(collect(components)[p], initial_rates[p])
        symb = Symbol(component)
        if set_guess
            state = @variables $symb(t) [
                output = true,
                guess = init,
                description = component*" reaction rate"
            ]
        else
            state = @variables $symb(t) = init [
                output = true,
                description = component*" reaction rate"
            ]
        end
        append!(states, state)
    end

    ODESystem(Equation[], t, states, []; name = name)
end
@connector function ReactionOutputPort(components::AbstractArray{<:SymbolicUtils.Symbolic}; initial_rates = nothing, set_guess = true, name)    # Set initial states to default if given as nothing
    if isnothing(initial_rates)
        initial_rates = get_default_or_guess.(components, 0)
    end
    # Get the component names and forward them
    names = symbolic_to_namestring.(components)
    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_rates) <: Pair)
        initial_rates = Dict(zip(components, initial_rates))
    end

    ReactionOutputPort(names; initial_rates, set_guess, name)
end
@connector function ReactionOutputPort(components::AbstractArray{<:Symbolics.Num}; initial_rates = nothing, set_guess = true, name)
    # Unwrap the nums and forward them
    ReactionOutputPort(Symbolics.value.(components); initial_rates, set_guess, name)
end


"""
    StateInputPort(states; initial_states, name)

A connector for connecting reactor states as input.

# Parameters:
- `states`: The states. Can be provided as:
    - A `Dict` with keys being Strings representing the names of the states and values being a named tuple with fields `par`Boolean or nothing meaning if the component is particulate or not (or not a component).
    - An array of symbols for the components. Name and if particulate or not are taken from these symbols.
- `initial_states`: Initial states, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each state or the default values of the symbols if given as symbols.
- `set_guess`: Initial values set only guesses. This is to prevent contradicting initial conditions. Defaults to true

# States:
- A state for each state specified by the `states` parameter
"""
@connector function StateInputPort(states::Dict{<:AbstractString, <:Union{Nothing, @NamedTuple{particulate::Bool, colloidal::Bool, soluble::Bool}}}; initial_states = zeros(length(states)), set_guess = true, name)
    # Support initial rates in all formats that ModelingToolkit supports
    initial_states = to_ordered_array(collect(keys(states)), initial_states)

    # Vector to be filled with the states
    vars = Symbolics.Num[]

    # Sort the states as otherwise the connect() sometimes connects wrong states
    p = sortperm(collect(states), by = x->x[1])
    # Add the states, with the description metadata and the particulate if given.
    for (state, init) = zip(collect(states)[p], initial_states[p])
        symb = Symbol(state[1])
        if !isnothing(state[2])
            if set_guess            
                s = @variables $symb(t) [
                    input = true,
                    particulate = state[2].particulate,
                    colloidal = state[2].colloidal,
                    soluble = state[2].soluble,
                    guess = init,
                    description = state[1]
                ]
            else
                s = @variables $symb(t) = init [
                    input = true,                    
                    particulate = state[2].particulate,
                    colloidal = state[2].colloidal,
                    soluble = state[2].soluble,
                    description = state[1]
                ]
            end
        else
            if set_guess
                s = @variables $symb(t) [
                    input = true,
                    guess = init,
                    description = state[1]
                ]
            else
                s = @variables $symb(t) = init [
                    input = true,
                    description = state[1]
                ]
            end
        end
        append!(vars, s)
    end

    ODESystem(Equation[], t, vars, []; name = name)
end
@connector function StateInputPort(states::AbstractArray{<:SymbolicUtils.Symbolic}; initial_states = nothing, set_guess = true, name)
    # Set initial states to default if given as nothing
    if isnothing(initial_states)
        initial_states = get_default_or_guess.(states, 0)
    end
    # Get the names and isparticulate of the given states and assemble it to a dict
    names = symbolic_to_namestring.(states)
    particulate = isparticulate.(states; default = nothing)
    colloidal = iscolloidal.(states; default = nothing)
    soluble = issoluble.(states; default = nothing)
    particle_size = broadcast(
        (p, c, s) -> all(isnothing.([p, c, s])) ? 
        nothing : 
        (particulate = isnothing(p) ? false : p, colloidal = isnothing(c) ? false : c, soluble = isnothing(s) ? false : s), 
        particulate, colloidal, soluble)
    state_dict = Dict(zip(names, particle_size))

    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_states) <: Pair)
        initial_states = Dict(zip(states, initial_states))
    end

    StateInputPort(state_dict; initial_states, set_guess, name)
end
@connector function StateInputPort(states::AbstractArray{<:Symbolics.Num}; initial_states = nothing, set_guess = true, name)
    # Unwrap the Nums and forward them
    StateInputPort(Symbolics.value.(states); initial_states, set_guess, name)
end


"""
    StateOutputPort(states; initial_states, name)

A connector for connecting reactor states as output.

# Parameters
- `states`: The states. Can be provided as:
    - A `Dict` with keys being Strings representing the names of the states and values being Boolean or nothing meaning if the component is particulate or not (or not a component).
    - An array of symbols for the components. Name and if particulate or not are taken from these symbols.
- `initial_states`: Initial states, supports array and variable map (keys can be string, symbol or symbolic variables). Defaults to 0 for each state or the default values of the symbols if given as symbols.
- `set_guess`: Initial values set only guesses. This is to prevent contradicting initial conditions. Defaults to true

# States:
- A state for each state specified by the `states` parameter
"""
@connector function StateOutputPort(states::Dict{<:AbstractString, <:Union{Nothing, @NamedTuple{particulate::Bool, colloidal::Bool, soluble::Bool}}}; initial_states = zeros(length(states)), set_guess = true, name)
    # Support initial rates as array or map
    initial_states = to_ordered_array(collect(keys(states)), initial_states)

    # Vector to be filled with the state symbols
    vars = Symbolics.Num[]

    # Sort the states as otherwise the connect() sometimes connects wrong states
    p = sortperm(collect(states), by = x->x[1])
    # Add the states, with the description metadata and the particulate if given.
    for (state, init) = zip(collect(states)[p], initial_states[p])
        symb = Symbol(state[1])
        if !isnothing(state[2])
            if set_guess
                s = @variables $symb(t) [
                    output = true,
                    particulate = state[2].particulate,
                    colloidal = state[2].colloidal,
                    soluble = state[2].soluble,
                    guess = init,
                    description = state[1]
                ]
            else
                s = @variables $symb(t) = init [
                    output = true,
                    particulate = state[2].particulate,
                    colloidal = state[2].colloidal,
                    soluble = state[2].soluble,
                    description = state[1]
                ]
            end
        else
            if set_guess
                s = @variables $symb(t) [
                    output = true,
                    guess = init,
                    description = state[1]
                ]
            else
                s = @variables $symb(t) = init [
                    output = true,
                    description = state[1]
                ]
            end
        end
        append!(vars, s)
    end

    ODESystem(Equation[], t, vars, []; name = name)
end
@connector function StateOutputPort(states::AbstractArray{<:SymbolicUtils.Symbolic}; initial_states = nothing, set_guess = true, name)
    # Set initial states to default if given as nothing
    if isnothing(initial_states)
        initial_states = get_default_or_guess.(states, 0)
    end
    # Get the name and isparticulate of the symbols and assemble to a dict
    names = symbolic_to_namestring.(states)
    particulate = isparticulate.(states; default = nothing)
    colloidal = iscolloidal.(states; default = nothing)
    soluble = issoluble.(states; default = nothing)
    particle_size = broadcast(
        (p, c, s) -> all(isnothing.([p, c, s])) ? 
        nothing : 
        (particulate = isnothing(p) ? false : p, colloidal = isnothing(c) ? false : c, soluble = isnothing(s) ? false : s), 
        particulate, colloidal, soluble)
    state_dict = Dict(zip(names, particle_size))

    # Transform the initial condition to a map to ensure ordering
    if !(eltype(initial_states) <: Pair)
        initial_states = Dict(zip(states, initial_states))
    end

    StateOutputPort(state_dict; initial_states, set_guess, name)
end
@connector function StateOutputPort(states::AbstractArray{<:Symbolics.Num}; initial_states = nothing, set_guess = true, name)
    # Unwrap the Nums and forward it
    StateOutputPort(Symbolics.value.(states); initial_states, set_guess, name)
end

