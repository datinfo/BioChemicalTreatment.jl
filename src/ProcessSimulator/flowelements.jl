"""
    FlowElement

An implementation of [`AbstractProcessElement`](@ref) for flow elements without dynamics, e.g. a flow unifier or splitter.

It has fields:
- `eqs`: The equations
- `t`: Time symbol
- `ps`: Parameters
- `ode_system`: A `ODESystem` from `ModelingToolkit` for simulating the reactor
- `systems`: Vector of subsystems of the reactor (can be empty)
- `inflows`: Vector of the inflow connectors of the reactor
- `outflows`: Vector of the outflow connectors of the reactor
- `exogenous_inputs`: The exogenous input connectors to the reactor or the subsystems.
- `exogenous_outputs`: The exogenous output connectors to the reactor or the subsystems.
- `name`: The name of the reactors

It supports the following interfaces from the [`AbstractProcessElement`](@ref):
- [`inflows`](@ref)
- [`outflows`](@ref)
- [`exogenous_inputs`](@ref)
- [`exogenous_outputs`](@ref)
- [`subsystems`](@ref)
"""
struct FlowElement <: AbstractProcessElement
    typestr::String
    eqs::Vector{Equation}
    t::Symbolics.BasicSymbolic{Real}
    ps::Vector{Symbolics.BasicSymbolic{Real}}

    ode_system::ODESystem
    systems::Vector{ODESystem}

    inflows::Vector{ODESystem}
    outflows::Vector{ODESystem}
    exogenous_inputs::Vector{ODESystem}
    exogenous_outputs::Vector{ODESystem}

    name::Symbol

    function FlowElement(typestr, eqs, t, ps, inflows, outflows; systems=ODESystem[], exogenous_inputs=ODESystem[], exogenous_outputs=ODESystem[], name)
        # Add port metadata to all ports
        inflows = init_port_metadata.(inflows)
        outflows = init_port_metadata.(outflows)
        exogenous_inputs = init_port_metadata.(exogenous_inputs)
        exogenous_outputs = init_port_metadata.(exogenous_outputs)
        ode_system = compose(ODESystem(eqs, t, [], ps; systems=systems, name=name), [inflows..., outflows..., exogenous_inputs..., exogenous_outputs...])
        element = new(typestr,
            eqs, ModelingToolkit.get_iv(ode_system), 
            ModelingToolkit.value.(ps), ode_system, 
            get_namespaced_property.(Ref(ode_system), nameof.(systems)),
            get_namespaced_property.(Ref(ode_system), nameof.(inflows)),
            get_namespaced_property.(Ref(ode_system), nameof.(outflows)),
            get_namespaced_property.(Ref(ode_system), nameof.(exogenous_inputs)),
            get_namespaced_property.(Ref(ode_system), nameof.(exogenous_outputs)),
            name)
        # Automatically set the port metadata of all ports
        set_port_metadata!(element)
        return element
    end
end

function Base.convert(::Type{<:ODESystem}, x::FlowElement)
    x.ode_system
end

function ModelingToolkit.equations(sys::FlowElement)
    sys.eqs
end

function inflows(sys::FlowElement)
    sys.inflows
end

function outflows(sys::FlowElement)
    sys.outflows
end

function exogenous_inputs(sys::FlowElement)
    sys.exogenous_inputs
end

function exogenous_outputs(sys::FlowElement)
    sys.exogenous_outputs
end

function subsystems(sys::FlowElement)
    sys.systems
end

"""
    Influent(sources, states=default_states(); name, flowrate=nothing, default=nothing)

A component to provide an influent for a simulation.

The `sources` argument can be a vector an element for each state in `states` or it can be a dict (vector of pairs...) from state name to the source.
In the latter case, if the argument `default` has a value, all states that are not set are set to the default.

There are three possibilities for each single source:
- Numeric constant
- Function taking a single numeric time argument
- ODESystem with `source.output` evaluating to a valid [`RealOutput`](@extref ModelingToolkitStandardLibrary.Blocks.RealOutput) port.
    All Source blocks from the [`ModelingToolkitStandardLibrary`](https://docs.sciml.ai/ModelingToolkitStandardLibrary/stable/API/blocks/#Source-Blocks)
    fulfill this property, but other systems would be thinkable as well.

# Parameters:
- `sources`: The vector of sources defining the values.
- `states`: The components in the Flows.
- `flowrate`: Named parameter to specify the flowrate. Set to `nothing` if included in the states or to take the default. If provided in the states and as parameter directly, the parameter takes precedence
- `default`: The default for non-specified sources. Only has an effect on the flow rate, or if `sources` is provided as a dictionary.

# Connectors:
- `outflow`: The outflow provided by the `Influent`
"""
@component function Influent(sources, states = default_states(); name, flowrate=nothing, default=nothing)
    # Make input sources a dict if not yet
    if !(eltype(sources) <: Pair)
        length(states) != length(sources) && throw(ArgumentError("Different number of influent sources and states provided!\n\nStates:  $states\nSources: $sources"))
        sources = Dict{Any, Any}(states .=> sources)
    elseif !(sources isa Dict)
        sources = Dict{Any, Any}(sources)
    end

    # Add influent if provided as kwarg
    if !isnothing(flowrate)
        sources["q"] = flowrate
    end

    # Get the outflow of the influent
    influent = RealInputToOutflowPort(states; name=Symbol(name, "_to_outflowport"))

    # Support sources as array or map
    try
        sources = to_ordered_array(unknowns(outflows(influent, 1)), sources; default)
    catch e
        if e isa ModelingToolkit.MissingVariablesError
            throw(ArgumentError("Influent sources underdefined. Please provide a default or a valid source for the following variables:\n\n\t$(e.vars)"))
        else
            rethrow()
        end
    end

    # Now we have sources for all states in the order of the outflow of influent
    states = unknowns(outflows(influent, 1))

    source_blocks = ODESystem[]
    eqs = Equation[]
    for (statename, source) = zip(symbolic_to_namestring.(states), sources)
        source_block = get_influent_source(statename, source)
        push!(source_blocks, source_block)
        push!(eqs, connect(source_block.output, exogenous_inputs(influent, Symbol(statename))))
    end

    FlowElement("Influent", eqs, t, [], [], outflows(influent); systems=[influent, source_blocks...], name = name)
end
"""
    get_influent_source(state, val)

Internal function to get a source block for a influent depending on the type.
"""
function get_influent_source(state, val::ODESystem)
    if hasproperty(val, :output)
        return val
    else
        throw(ArgumentError("Invalid influent provided for state $state. If provided as ODESystem, it must have a property called \"output\"."))
    end
end
function get_influent_source(state, val::T where T <: Number)
    ModelingToolkitStandardLibrary.Blocks.Constant(;k = val, name=Symbol(state, "_const"))
end
function get_influent_source(state, val::Function)
    try
        val(1.23) # Test if the function takes exactly one float argument (used for time)
        ModelingToolkitStandardLibrary.Blocks.TimeVaryingFunction(val; name=Symbol(state, "_fun"))
    catch
        throw(ArgumentError("Invalid influent provided for state $state. If provided as function, it must have a method accepting one numeric argument."))
    end
end

"""
    Sensor(sensed, states = default_states(); name)

A component to provide certain states as real outputs.
Is convenient if certain states are to be extracted for control.

!!! note
    This system does not include any measurement model nor noise. It simply gets the information out.

# Parameters:
- `sensed`: The sources defining the values. Either a vector or a single value
- `states`: The components in the Flows.
- `flowrate`: Named parameter to specify the flowrate. Set to `nothing` if included in the states.

# Connectors:
- `inflow`: The inflow to the sensor
- `outflow`: The outflow of the sensor (equal to the inflow)
- one exogenous_output for each sensed state. Named after the state.
"""
@component function Sensor(sensed::AbstractVector, states = default_states(); name)
    @named inflow = InflowPort(states)
    @named outflow = OutflowPort(states)

    sensed = Symbol.(symbolic_to_namestring.(sensed))

    if !all(hasproperty.(Ref(inflow), sensed))
        throw(ArgumentError("Invalid states to sense!\nThere are no states called $(sensed[.!hasproperty.(Ref(inflow), sensed)]) exist. Available states are $(propertynames(inflow))."))
    end

    sensor_outputs = ODESystem[]
    eqs = Equation[connect(inflow, outflow)]
    for sens in sensed
        push!(sensor_outputs, ModelingToolkitStandardLibrary.Blocks.RealOutput(;guess = get_default_or_guess(getproperty(inflow, sens), 0), name=sens))
        push!(eqs, sensor_outputs[end].u ~ getproperty(inflow, sens))
    end

    FlowElement("Sensor for " * join(string.(sensed), ", ", " and "), eqs, t, [], [inflow], [outflow]; exogenous_outputs=sensor_outputs, name = name)
end
Sensor(sensed, states = default_states(); name) = Sensor([sensed], states; name)

"""
    FlowConnector(state = default_states(); n_in, n_out, name)

A component for connecting multiple flows.

This system takes an arbitrary number of inputs and splits it into a number of outputs. 
Thereby, it is ensuring mass conservation and assumes ideal mixing between the incoming flows
such that the concentrations in all outflows are equal. The flow rates of the outflows are to be 
provided by the following systems, e.g. using a [`FlowPump`](@ref) system. The flow rates have to be
provided for all except one outflow, the last one is determined from a mass balance.

Specifically, it has the following equations (``C`` always stands for an arbitrary component):
- Conservation of mass:
```math
\\begin{aligned}
\\sum_{i = 0}^{n_{in}}q_{in,i} &= \\sum_{i=0}^{n_{out}}q_{out,i}\\\\
\\sum_{i = 0}^{n_{in}}C_{in,i}q_{in,i} &= \\sum_{i=0}^{n_{out}}C_{out,i}q_{out,i}
\\end{aligned}
```
- Ideal mixing:
```math
C_{out,i} = C_{out,j} \\quad \\forall i,j\\in \\{1 ... n_{out}\\}
```

# Parameters:
- `states`: The components in the flows
- `n_in`: The number of inflows
- `n_out`: The number of outflows

# Connectors:
- `inflow_i`: The i-th inflow of the `FlowConnector` (if only one inflow, the `_i` is omitted and the resulting name is `inflow`)
- `outflow_i`: The i-th outflow of the `FlowConnector` (if only one inflow, the `_i` is omitted and the resulting name is `outflow`)
"""
@component function FlowConnector(state = default_states(); n_in, n_out, name, typestr = "FlowConnector")
    # Get all inflow and outflow connectors
    inflows = ODESystem[]
    if n_in == 1
        push!(inflows, InflowPort(state; name = :inflow))
    else
        for i = 1:n_in
            push!(inflows, InflowPort(state; name = Symbol("inflow_"*string(i))))
        end
    end
    outflows = ODESystem[]
    if n_out == 1
        push!(outflows, OutflowPort(state; name = :outflow))
    else
        for i = 1:n_out
            push!(outflows, OutflowPort(state; name = Symbol("outflow_"*string(i))))
        end
    end

    # Assemble the equations
    eqs = Equation[]
    # Mass conservation: Flows (inflow = outflow)
    push!(eqs, sum(getproperty.(inflows, :q)) ~ sum(getproperty.(outflows, :q)))
    # Mass conservation: Component masses sum(q_in*c_in) = sum(q_out*c_out)
    for statename in symbolic_to_namestring.(unknowns(outflows[1]))
        if statename != "q"
            push!(eqs, sum(getproperty.(inflows, Symbol(statename)) .* getproperty.(inflows, :q)) ~ sum(getproperty.(outflows, Symbol(statename)) .* getproperty.(outflows, :q)))
        end
    end
    # All components equal in all outflows
    for outflow = outflows[2:end]
        # Equate all components
        for statename in symbolic_to_namestring.(unknowns(outflows[1]))
            if statename != "q"
                push!(eqs, Equation(getproperty.([outflows[1], outflow], Symbol(statename))...))
            end
        end
    end

    # Compose and return the ODESystem
    FlowElement(typestr, eqs, t, [], inflows, outflows; name=name)
end

"""
    FlowUnifier(state = default_states(); n_in=2, name)

A component for unifying multiple flows into a single one.
It is an alias for the `FlowConnector` having only one outflow.

# Parameters:
- `states`: The components in the flows
- `n_in`: The number of inflows, defaults to 2

# Connectors:
- `inflow_i`: The i-th inflow of the `FlowUnifier`
- `outflow`: The outflow of the `FlowUnifier`
"""
@component FlowUnifier(state=default_states(); n_in=2, name) = FlowConnector(state; n_in=n_in, n_out=1, name, typestr="FlowUnifier")

"""
    FlowSeparator(state = default_states(); n_out=2, name)

A component for separating a single flow into multiple.
It is an alias for the `FlowConnector` having only one inflow.

The flow rates along which the flow is to be split have to be provided by the following systems, e.g. a [`FlowPump`](@ref).
Connect e.g. a [`FlowPump`](@ref) to all except one outflow, the last one is then already fixed by the mass balance.

# Parameters:
- `states`: The components in the flows
- `n_out`: The number of outflows, defaults to 2

# Connectors:
- `inflow`: The inflow of the `FlowSeparator`
- `outflow_i`: The i-th outflow of the `FlowSeparator`
"""
@component FlowSeparator(state=default_states(); n_out=2, name) = FlowConnector(state; n_in=1, n_out=n_out, name, typestr="FlowSeparator")

"""
    FlowPump(state = default_states(); flowrate=nothing, name)

A component modeling a flow pump.
It models an ideal flow pump, where a provided flow is pumped through.

The `FlowPump` has one inflow and one outflow and it merely specifies the flow passing through.

!!! warning 
    If the flow pumped by a `FlowPump` does not match what is incoming, the flow will need to find another way. 
    This can either be that the flow takes another route (through a [`FlowSeparator`](@ref)), or that it is 
    accumulated/depleted in a previous system that can do so (i.e. has a variable volume). If none of this is possible,
    the created system can not be successfully simulated, as the equations will be inconsistent.

!!! note
    The `FlowPump` does not split the flow in any way. If you intend to extract a sidestream from
    a main flow, use a combination of a [`FlowSeparator`](@ref) (to split the flows) and a `FlowPump` 
    at one outlet (to specify the flowrate there).

# Parameters:
- `states`: The components in the flows
- `flowrate`: The flowrate to pump. If `nothing` (default) an input is added for setting it dynamically

# Connectors:
- `inflow`: The inflow of the Pump
- `outflow`: The outflow of the Pump
- `q_pumped`: **Optional**: [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@extref) to provide the pumped flow rate. Added only if the parameter `flowrate` is `nothing`, otherwise the flowrate is considered fixed at the given value.
"""
@component function FlowPump(state=default_states(); flowrate=nothing, name)
    # Get inflow and outflow ports
    @named inflow = InflowPort(state)
    @named outflow = OutflowPort(state)

    # Equations: Connecting inflow and outflow (must be equal)
    eqs = [
        connect(inflow, outflow)
    ]

    # Enforce pumped flow (either from input port or fixed if given) and create system
    if isnothing(flowrate)
        @named q_pumped = ModelingToolkitStandardLibrary.Blocks.RealInput(nin=1)
        push!(eqs, inflow.q ~ q_pumped.u)
        FlowElement("Flow Pump", eqs, t, [], [inflow], [outflow]; exogenous_inputs=[q_pumped], name=name)
    else
        @parameters q_pumped = flowrate
        push!(eqs, inflow.q ~ q_pumped)
        FlowElement("Flow Pump", eqs, t, [q_pumped], [inflow], [outflow]; name=name)
    end
end

"""
    IdealClarifier(states = default_states(); f_ns = 0, name)

An Ideal Clarifier.
This system defines an ideal clarifier. It is a static system that ensures mass conservation and ideally 
settles all particulate compounds: a parameter for the unsettlable fraction of solids can be given.
The flow rates of the outflows (effluent or sludge) are to be provided by the following systems, e.g. using a [`FlowPump`](@ref) system.

# Parameters:
- `states`: The components that the clarifier should have
- `f_ns`: The unsettlable fraction of solids

# Connectors:
- `inflow`: The inflow of the IdealClarifier
- `effluent`: The effluent of the IdealClarifier
- `sludge`: The the sludge outflow of the IdealClarifier
"""
@component function IdealClarifier(states=default_states(); f_ns = 0, name)
    # Get all connectors
    @named inflow = InflowPort(states)
    @named effluent = OutflowPort(states)
    @named sludge = OutflowPort(states)
    
    # The parameter for the fraction of non-settlable solids
    ps = @parameters f_ns = f_ns

    # Mass conservation: flowrates (inflow rate = outflow rate)
    eqs = [inflow.q ~ effluent.q + sludge.q]
    # Mass conservation: Component masses q_in*c_in = q_eff*c_eff + q_s*c_s
    for statename in symbolic_to_namestring.(unknowns(inflow))
        if statename != "q" # Not for q itself
            push!(eqs, getproperty(inflow, Symbol(statename)) * inflow.q ~ getproperty(effluent, Symbol(statename)) * effluent.q + getproperty(sludge, Symbol(statename)) * sludge.q)
        end
    end
    # Solids separation: If solid, only f_ns of inflow in effluent, otherwise equal concentration as in inflow (sludge follows from mass conservation)
    for state in unknowns(inflow)
        statename = symbolic_to_namestring(state)
        if statename != "q" # Not for q itself
            if isparticulate(state) # Particulate component
                push!(eqs, getproperty(effluent, Symbol(statename)) ~ f_ns * getproperty(inflow, Symbol(statename))) 
            else # Soluble component
                push!(eqs, getproperty(effluent, Symbol(statename)) ~ getproperty(inflow, Symbol(statename)))
            end
        end
    end

    # Compose and return the IdealClarifier system
    FlowElement("IdealClarifier", eqs, t, ps, [inflow], [effluent, sludge]; name=name)
end