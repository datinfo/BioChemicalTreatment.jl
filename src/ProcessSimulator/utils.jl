"""
    symbolic_to_namestring(sym; namespace_separator=ModelingToolkit.NAMESPACE_SEPARATOR)

A helper function to get the name of a symbol as string. Removes all namespacing (split by `namespace_separator`) and dependencies.
E.g.
```jldoctest; setup = :(sym = "system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "variable(t)")
julia> sym
"system₊component₊variable(t)"
julia> ProcessSimulator.symbolic_to_namestring(sym)
"variable"
```
"""
function symbolic_to_namestring(sym; namespace_separator::Char=ModelingToolkit.NAMESPACE_SEPARATOR)::String
    # Remove namespacing
    symbol = strip_namespace(sym; namespace_separator)
    # Remove dependencies
    strip_dependencies(symbol)
end

"""
    strip_dependencies(sym)

Strip the dependencies (e.g. t, in brackets `(t)`) of `sym`. If `sym` isa Symbolics variable, it's name is taken.
E.g.
```jldoctest; setup = :(sym = Symbolics.Num("system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "variable(t)"))
julia> sym
"system₊component₊variable(t)"
julia> ProcessSimulator.strip_dependencies(sym)
"system₊component₊variable"
```

Works independent of the number of arguments in the dependencies:
```jldoctest; setup = :(sym = Symbolics.Num("system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "variable(t, x, y)"))
julia> sym
"system₊component₊variable(t, x, y)"
julia> ProcessSimulator.strip_dependencies(sym)
"system₊component₊variable"
```

Or as well for Symbols (or everything convertable to a string) and without any dependency:
```jldoctest; setup = :(sym = Symbol("system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "parameter"))
julia> sym
:system₊component₊parameter
julia> ProcessSimulator.strip_dependencies(sym)
"system₊component₊parameter"
```
"""
function strip_dependencies(sym::AbstractString)::String
    split(sym, '(')[1]
end
function strip_dependencies(sym)::String
    strip_dependencies(string(sym))
end
function strip_dependencies(sym::Symbolics.Num)::String
    strip_dependencies(Symbolics.unwrap(sym))
end

"""
    strip_namespace(sym; namespace_separator=ModelingToolkit.NAMESPACE_SEPARATOR)

Removes the entire namespacing of the symbol name of `sym`. Namespace is separated by `namespace_separator`.
E.g.
```jldoctest; setup = :(sym = "system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "subcomponent")
julia> sym
"system₊component₊subcomponent"
julia> ProcessSimulator.strip_namespace(sym)
"subcomponent"
```
"""
function strip_namespace(sym; namespace_separator::Char=ModelingToolkit.NAMESPACE_SEPARATOR)::String
    split_namespace(sym; namespace_separator)[end]
end

"""
    lstrip_namespace(sym, str; namespace_separator=ModelingToolkit.NAMESPACE_SEPARATOR)

Removes the namespacing matching `str` of the beginning of symbol name of `sym`. Namespacing is separated by `namespace_separator`.
E.g.
```jldoctest lstrip_namespace; setup = :(sym = "system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "subcomponent")
julia> sym
"system₊component₊subcomponent"
julia> ProcessSimulator.lstrip_namespace(sym, "system")
"component₊subcomponent"
julia> ProcessSimulator.lstrip_namespace(sym, "system₊component")
"subcomponent"
```

If `sym` does not contain `str` in the beginning, nothing is stripped (no matter of how much matches) and the input is returned as a whole:
```jldoctest lstrip_namespace
julia> ProcessSimulator.lstrip_namespace(sym, "system₊component1")
"system₊component₊subcomponent"
```
"""
function lstrip_namespace(sym, str; namespace_separator::Char=ModelingToolkit.NAMESPACE_SEPARATOR)::String
    sym_split = split_namespace(sym; namespace_separator)
    str_split = split_namespace(str; namespace_separator)
    while !isempty(sym_split) && !isempty(str_split)
        if sym_split[1] != str_split[1]
            return string(sym)
        end
        popfirst!(sym_split)
        popfirst!(str_split)
    end
    if !isempty(str_split)
        return string(sym)
    end
    return join(sym_split, namespace_separator)
end

"""
    rstrip_namespace(sym, str; namespace_separator=ModelingToolkit.NAMESPACE_SEPARATOR)

Removes the namespacing matching `str` of the end of symbol name of `sym`. Namespace separator is set by `namespace_separator`
E.g.
```jldoctest rstrip_namespace; setup = :(sym = "system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "subcomponent")
julia> sym
"system₊component₊subcomponent"
julia> ProcessSimulator.rstrip_namespace(sym, "subcomponent")
"system₊component"
julia> ProcessSimulator.rstrip_namespace(sym, "component₊subcomponent")
"system"
```

If `sym` does not contain `str` in the beginning, nothing is stripped (no matter of how much matches) and the input is returned as a whole:
```jldoctest rstrip_namespace
julia> ProcessSimulator.rstrip_namespace(sym, "component1₊subcomponent")
"system₊component₊subcomponent"
```
"""
function rstrip_namespace(sym, str; namespace_separator::Char=ModelingToolkit.NAMESPACE_SEPARATOR)::String
    sym_split = split_namespace(sym; namespace_separator)
    str_split = split_namespace(str; namespace_separator)
    while !isempty(sym_split) && !isempty(str_split)
        if sym_split[end] != str_split[end]
            return string(sym)
        end
        pop!(sym_split)
        pop!(str_split)
    end
    if !isempty(str_split)
        return string(sym)
    end
    return join(sym_split, namespace_separator)
end

"""
    split_namespace(sym; namespace_separator=ModelingToolkit.NAMESPACE_SEPARATOR)

Get the namespace parts of a symbol as strings. 
The namespace is assumed to be split by the given `namespace_separator`. Defaults to `ModelingToolkit.NAMESPACE_SEPARATOR`.
E.g.
```jldoctest; setup = :(sym = "system" * ModelingToolkit.NAMESPACE_SEPARATOR * "component" * ModelingToolkit.NAMESPACE_SEPARATOR * "subcomponent")
julia> sym
"system₊component₊subcomponent"
julia> ProcessSimulator.split_namespace(sym)
3-element Vector{SubString{String}}:
 "system"
 "component"
 "subcomponent"
julia> sym = "system.component.subcomponent"
"system.component.subcomponent"
julia> ProcessSimulator.split_namespace(sym, namespace_separator='.')
3-element Vector{SubString{String}}:
 "system"
 "component"
 "subcomponent"
```
"""
function split_namespace(sym::AbstractString; namespace_separator::Char=ModelingToolkit.NAMESPACE_SEPARATOR)::Vector{SubString{String}}
    # split namespacing
    split(string(sym), namespace_separator)
end
function split_namespace(sym; kwargs...)::Vector{SubString{String}}
    split_namespace(string(sym); kwargs...)
end
function split_namespace(sym::Symbolics.Num; kwargs...)::Vector{SubString{String}}
    split_namespace(Symbolics.unwrap(sym); kwargs...)
end

"""
    get_namespaced_property(value, name; namespace_separator=ModelingToolkit.NAMESPACE_SEPARATOR)

Get a propery of `value`, where `name` can contain namespacing separated by `namespace_separator`.
Works on any value.
E.g.
```jldoctest; setup = :(a = (;b=(;c=2)))
julia> a
(b = (c = 2,),)
julia> BioChemicalTreatment.ProcessSimulator.get_namespaced_property(a, "b.c"; namespace_separator='.')
2
julia> BioChemicalTreatment.ProcessSimulator.get_namespaced_property(a, "b/c"; namespace_separator='/')
2
```
"""
function get_namespaced_property(value, name; namespace_separator::Char=ModelingToolkit.NAMESPACE_SEPARATOR)
    names = Symbol.(split_namespace(name; namespace_separator))
    while !isempty(names)
        value = getproperty(value, first(names))
        popfirst!(names)
    end
    value
end

"""
    to_ordered_array(order, vals; default = 0)

A helper function to transform the values to a array with elements ordered as given by `order`.
Supports a array or variable maps (keys as Symbols, Strings or Symbolic variables).
The default gets applied to all elements in `order`, which do not have a corresponding entry in `vals`.

If `default = nothing`, no defaults are applied and an error is thrown if no value is supplied for any element
in `order`.
"""
function to_ordered_array(order::AbstractArray{<:AbstractString}, vals; default = 0)
    # If the initial conditions are given as dict or vector of pairs
    if eltype(vals) <: Pair
        # Make initial conditions a dict if not yet
        vals = vals isa Dict ? vals : Dict(vals)
        # Make keys to be strings (if variable remove namespacing, otherwise simply convert to string)
        vals = Dict(zip([isa(k, SymbolicUtils.Symbolic) || isa(k, Symbolics.Num) ? symbolic_to_namestring(k) : string(k) for k in Symbolics.value.(keys(vals))], values(vals)))
    end
    # Use ModelingToolkit.varmap_to_vars to get correct order and set ic to zero where not given
    ModelingToolkit.varmap_to_vars(vals, order; defaults = (isnothing(default) ? Dict() : Dict(zip(order, fill(default, length(order))))))
end
to_ordered_array(order, vals; default = 0) = to_ordered_array(symbolic_to_namestring.(order), vals; default)

"""
    get_default_or_guess(x, default=nothing)

Get default if assigned or a guess. Uses `get_default_or_guess` from ModelingToolkit 
and extends it with the possibility of specifying a default value.
"""
function get_default_or_guess(x, default=nothing)
    val = ModelingToolkit.get_default_or_guess(x)
    return isnothing(val) ? default : val
end
