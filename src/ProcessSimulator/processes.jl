"""
    Process

An implementation of [`AbstractProcessElement`](@ref) for processes used to 
compute and provide the reaction rates to reactors.

It has fields:
- `eqs`: The equations
- `t`: Time symbol
- `ps`: Parameters
- `ode_system`: A `ODESystem` from `ModelingToolkit` for simulating the reactor
- `rates`: The rates connector of the reactor
- `states`: The states connector of the reactor
- `exogenous_inputs`: The exogenous input connectors to the reactor or the subsystems.
- `internal_states`: States that are internal to the process but neither input nor 
    output from the system. (e.g. for intermediate computations)
- `name`: The name of the reactors

It supports the following interfaces from the [`AbstractProcessElement`](@ref):
- [`states`](@ref)
- [`rates`](@ref)
- [`exogenous_inputs`](@ref)
"""
struct Process <: AbstractProcessElement
    typestr::String
    eqs::Vector{Equation}
    t::Symbolics.BasicSymbolic{Real}
    ps::Vector{Symbolics.BasicSymbolic{Real}}

    ode_system::ODESystem
    rates::ODESystem
    states::ODESystem
    exogenous_inputs::Vector{ODESystem}

    internal_states::Vector{Symbolics.BasicSymbolic{Real}}

    name::Symbol

    function Process(typestr::String, eqs::AbstractVector{<:Equation}, t, ps, rates, states, exogenous_inputs=ODESystem[]; name, internal_states = [])
        # Add port metadata to all ports
        rates = init_port_metadata(rates)
        states = init_port_metadata(states)
        exogenous_inputs = init_port_metadata.(exogenous_inputs)
        ode_system = compose(ODESystem(eqs, t, internal_states, ps; name=name), [rates, states, exogenous_inputs...])
        process = new(
            typestr,
            eqs, ModelingToolkit.get_iv(ode_system), 
            ModelingToolkit.value.(ps), ode_system, 
            get_namespaced_property(ode_system, nameof(rates)), 
            get_namespaced_property(ode_system, nameof(states)), 
            get_namespaced_property.(Ref(ode_system), nameof.(exogenous_inputs)), 
            ModelingToolkit.value.(internal_states), name)
        # Automatically set the port metadata of all ports
        set_port_metadata!(process)
        return process
    end
end

function typestr(sys::Process)
    return "Process " * sys.typestr
end

"""
    Process(args...; name, error_invalid_composition=true, kwargs...)

Loads a process from files over the `BioChemicalReactionSystem`. Generates the process and checks if the composition requirement is fulfilled.

## Arguments
- `args...`: The arguments to pass on to the [`Reactions.BioChemicalReactionSystem`](@ref). See docs there for possiblities.

## Keyword Arguments
- `name`: The name of the created system
- `error_invalid_composition`: If an error should be thrown on invalid compositions. Defaults to true to force the user
    to actively activate that the invalid composition works. A warning is thrown instead of an error if this argument is
    set to `false`
- `kwargs...`: Keyword arguments specifying the parameters of the model. An error is thrown if no matching parameter is found to set. 
    Can be values or expressions of the other parameters (but be careful for circular expressions, they are not supported).
"""
@component function Process(args...; name, error_invalid_composition=true, kwargs...)
    # Get BioChemicalReactionSystem
    reactionsys = BioChemicalReactionSystem(args...)

    # Check composition and throw warnings/errors as appropriate
    if !check_composition(reactionsys; params_substitution = :expressions, kwargs...)
        if !check_composition(reactionsys; params_substitution = :values, kwargs...)
            if error_invalid_composition
                error("The composition of the read in Reaction System does not fulfill requirement neither \
                symbolically nor with values plugged in. Set 'error_invalid_composition' to false if you are sure that your process works properly.\n\
                It was created with arguments '$args'")
            else
                @warn "The composition of the read in Reaction System does not fulfill requirement neither \
                symbolically nor with values plugged in. Only warning due to 'error_invalid_composition' \
                set to false. Please ensure that the model is correct.
                It was created with arguments '$args'"
            end
        else
            @warn "The composition of the read in Reaction Sytem does fulfill requirement only with values plugged in but not symbolically. \
            Please check your model and be careful when adapting parameters at runtime.\n\
            It was created with arguments '$args'."
        end
    end

    # Get states and rates inputs and outputs
    @named states = StateInputPort(reactionsys.states)
    @named rates = ReactionOutputPort(unknowns(states))

    # Get parameters, values and set parameter defaults to the values and convert to modelingtoolkit parameter
    parameter_values = Reactions.parameter_values(reactionsys; kwargs...)
    parameter_values = Dict([Symbol(symbolic_to_namestring(p)) => v for (p, v) in parameter_values])
    parameters = [only(@parameters $p = v) for (p, v) in parameter_values]

    # Get rate equations (qithout process rates plugged in)
    rate_eqs = assemble_equations(reactionsys)
    rate_eqs = [getproperty(rates, Symbol(symbolic_to_namestring(r))) ~ rate_eqs[r] for r in unknowns(rates)]
    # Get process rate equations
    processrates = reactionsys.processrates
    processrate_eqns = [pr ~ reactionsys.processrate_eqns[pr] for pr in processrates]

    # Get full equations
    eqns = [rate_eqs; processrate_eqns]

    # Substitute states from input port into equations
    state_map = Dict([s => getproperty(states, Symbol(symbolic_to_namestring(s))) for s in reactionsys.states])
    eqns = substitute.(eqns, Ref(state_map))

    # Substitute parameters into equations (replace the ones from the reactionsys)
    parameters_map = Dict([p => only(parameters[symbolic_to_namestring.(parameters) .== symbolic_to_namestring(p)]) for p in reactionsys.parameters])
    eqns = substitute.(eqns, Ref(parameters_map))

    Process(string(reactionsys.name), eqns, t, parameters, rates, states; name=name, internal_states=processrates)
end

function Base.convert(::Type{<:ODESystem}, x::Process)
    x.ode_system
end

function ModelingToolkit.equations(sys::Process)
    sys.eqs
end

function states(sys::Process)
    sys.states
end

function rates(sys::Process)
    sys.rates
end

function exogenous_inputs(sys::Process)
    sys.exogenous_inputs
end

"""
    ASM1(; name, Y_H=0.67, Y_A=0.24, i_XB=0.086, i_XP=0.01, f_P=0.08, mu_H=6.0, K_S=20.0, K_OH=0.2, K_NO=0.5, b_H=0.62,
         eta_g=0.8, mu_A=0.8, K_NH=1.0, K_OA=0.4, b_A=0.1, kappa_h=3.0, eta_h=0.4, K_X=0.03, kappa_a=0.08)

A component that defines the process rate in the activated sludge model number 1 (ASM1). Taken from [Henze:2006](@cite)


!!! warning "Initial State"
    When using this Model, one needs to set the initial state/initial condition **in the corresponding reactor**. This is because the default values are all `0`, and the some of the process equations have a division by `0` if all states are `0`!

# Parameters:
-  stoichiometric parameter (all at 20 C, neutral pH):
  - `Y_H` heterotrophic yield (default 0.67 gCOD/gCOD)
  - `Y_A` autotrophic yield (default 0.24 gCOD/gCOD)
  - `i_XB` nitrogen fraction in biomass (default 0.086 gN/gCOD)
  - `i_XP` nitrogen fraction in endogenous mass (default 0.01 gN/gCOD)
  - `f_P` fraction of biomass leading to particulate material (default 0.08 -)
-  kinetic parameter heterotrophs:
  - `mu_H` maximum specific growth rate (default 6.0 1/day)
  - `K_S` substrate saturation constant (default 20.0 gCOD/m3)
  - `K_OH` Oxygen saturation constant (default 0.2 gO2/m3)
  - `K_NO` nitrate saturation constant (default 0.5 gNO3-N/m3)
  - `b_H` specific decay rate (default 0.62 1/day)
  - `eta_g` anoxic growth correction factor (default 0.8 -)
-  kinetic parameter autotrophs:
  - `mu_A` maximum specific growth rate (default 0.8 1/day)
  - `K_NH` ammonium saturation constant (default 1.0 gO2/m3)
  - `K_OA` oxygen saturation constant (default 0.4 gO2/m3)
  - `b_A` specific decay rate (default 0.1 1/day)
-  hydrolysis parameter
  - `kappa_h` maximum specific hydrolysis rate (default 3.0 1/day)
  - `eta_h` anoxic hydrolysis correction factor (default 0.4 -)
  - `K_X` half-saturation coefficient for hydrolysis of XS (default 0.03 -)
-  ammonification
  - `kappa_a` ammonification rate constant (default 0.08 m3/(gCOD*day))

# States:
  - `S_I`   soluble inert organic matter
  - `S_S`   readily biodegradable substrate
  - `S_O`   oxygen
  - `S_NO`  nitrate and nitrite nitrogen
  - `S_NH`  ammonium and ammonia nitrogen
  - `S_ND`  soluble biodegradable organic nitrogen
  - `S_ALK` alkalinity
  - `X_I`   particulate inert organic matter
  - `X_S`   slowly biodegradable substrate
  - `X_BH`  active heterotrophic biomass
  - `X_BA`  active autotrophic biomass
  - `X_P`   particulate products from biomass decay
  - `X_ND`  particulate biodegradable organic
"""
@component function ASM1(; name, Y_H=0.67, Y_A=0.24, i_XB=0.086, i_XP=0.01, f_P=0.08, 
    mu_H=6.0, K_S=20.0, K_OH=0.2, K_NO=0.5, b_H=0.62, eta_g=0.8, mu_A=0.8, K_NH=1.0, 
    K_OA=0.4, b_A=0.1, kappa_h=3.0, eta_h=0.4, K_X=0.03, kappa_a=0.08)

    @named states = StateInputPort(Dict([
           "S_I" => (particulate = false, colloidal = false, soluble = true),  # soluble inert organic matter
           "S_S" => (particulate = false, colloidal = false, soluble = true),  # readily biodegradable substrate
           "S_O" => (particulate = false, colloidal = false, soluble = true),  # oxygen
           "S_NO" => (particulate = false, colloidal = false, soluble = true), # nitrate and nitrite nitrogen
           "S_NH" => (particulate = false, colloidal = false, soluble = true), # ammonium and ammonia nitrogen
           "S_ND" => (particulate = false, colloidal = false, soluble = true), # soluble biodegradable organic nitrogen
           "S_ALK" => (particulate = false, colloidal = false, soluble = true),# alkalinity
           "X_I" => (particulate = true, colloidal = false, soluble = false),   # particulate inert organic matter
           "X_S" => (particulate = true, colloidal = false, soluble = false),   # slowly biodegradable substrate
           "X_BH" => (particulate = true, colloidal = false, soluble = false),  # active heterotrophic biomass
           "X_BA" => (particulate = true, colloidal = false, soluble = false),  # active autotrophic biomass
           "X_P" => (particulate = true, colloidal = false, soluble = false),   # particulate products from biomass decay
           "X_ND" => (particulate = true, colloidal = false, soluble = false)   # particulate biodegradable organic nitrogen
           ]))
    @named rates = ReactionOutputPort(unknowns(states))

    # parameters
    stoich_params = @parameters Y_H=Y_H Y_A=Y_A i_XB=i_XB i_XP=i_XP f_P=f_P
    # kinetic parameter heterotrophs
    heterotrophs_params = @parameters mu_H=mu_H K_S=K_S K_OH=K_OH K_NO=K_NO b_H=b_H eta_g=eta_g
    # kinetic parameter autotrophs
    autotrops_params = @parameters mu_A=mu_A K_NH=K_NH K_OA=K_OA b_A=b_A
    # hydrolysis parameter
    hydrolysis_params = @parameters kappa_h=kappa_h eta_h=eta_h K_X=K_X
    # ammonification
    ammonifiaction_params = @parameters kappa_a=kappa_a

    # Switching function
    switch_func(x, y) = x ./ (x .+ y)

    # Process rate
    aer_gr_het(S_S, S_O, X_BH) = mu_H * switch_func(S_S, K_S) * switch_func(S_O, K_OH) * X_BH
    an_gr_het(S_S, S_O, S_NO, X_BH) = mu_H * switch_func(S_S, K_S) * switch_func(K_OH, S_O) * switch_func(S_NO, K_NO) * eta_g * X_BH
    aer_gr_aut(S_NH, S_O, X_BA) = mu_A * switch_func(S_NH, K_NH) * switch_func(S_O, K_OA) * X_BA
    dec_het(X_BH) = b_H * X_BH
    dec_aut(X_BA) = b_A * X_BA
    ammon_solorg_N(S_ND, X_BH) = kappa_a * S_ND * X_BH
    hydrol_org(X_BH, X_S, S_O, S_NO) = (
                                kappa_h * switch_func(X_S, (X_BH * K_X))
                                * ( switch_func(S_O, K_OH)
                                + eta_h * switch_func(K_OH, S_O) * switch_func(S_NO, K_NO) ) * X_BH
                               )
    hydrol_org_N(X_BH, S_O, S_NO, X_ND, X_S) = hydrol_org(X_BH, X_S, S_O, S_NO) * X_ND / X_S


    eqs = [

        rates.S_I ~ 0
        rates.S_S ~ (hydrol_org(states.X_BH, states.X_S, states.S_O, states.S_NO)
                              - (aer_gr_het(states.S_S, states.S_O, states.X_BH) + an_gr_het(states.S_S, states.S_O, states.S_NO, states.X_BH)) / Y_H
                              )
        rates.X_I ~ 0
        rates.X_S ~ ((1 - f_P) * dec_het(states.X_BH) + (1 - f_P) * dec_aut(states.X_BA)
                      - hydrol_org(states.X_BH, states.X_S, states.S_O, states.S_NO)
                    )
        rates.X_BH ~ aer_gr_het(states.S_S, states.S_O, states.X_BH) + an_gr_het(states.S_S, states.S_O, states.S_NO, states.X_BH) - dec_het(states.X_BH)
        rates.X_BA ~ aer_gr_aut(states.S_NH, states.S_O, states.X_BA) - dec_aut(states.X_BA)
        rates.X_P ~ f_P * (dec_het(states.X_BH) + dec_aut(states.X_BA))
        rates.S_O ~ -(1 - Y_H) / Y_H * aer_gr_het(states.S_S, states.S_O, states.X_BH) - (4.57 - Y_A) / Y_A * aer_gr_aut(states.S_NH, states.S_O, states.X_BA)
        rates.S_NO ~ -(1 - Y_H) / (2.86 * Y_H) * an_gr_het(states.S_S, states.S_O, states.S_NO, states.X_BH) + 1 / Y_A * aer_gr_aut(states.S_NH, states.S_O, states.X_BA)
        rates.S_NH ~ (
                      ammon_solorg_N(states.S_ND, states.X_BH)
                      - i_XB * (aer_gr_het(states.S_S, states.S_O, states.X_BH) + an_gr_het(states.S_S, states.S_O, states.S_NO, states.X_BH))
                      - (i_XB + 1/Y_A) * aer_gr_aut(states.S_NH, states.S_O, states.X_BA)
                      )
        rates.S_ND ~ hydrol_org_N(states.X_BH, states.S_O, states.S_NO, states.X_ND, states.X_S) - ammon_solorg_N(states.S_ND, states.X_BH)
        rates.X_ND ~ (
                      (i_XB - f_P * i_XP) * (dec_het(states.X_BH) + dec_aut(states.X_BA)) - hydrol_org_N(states.X_BH, states.S_O, states.S_NO, states.X_ND, states.X_S)
                      )
        rates.S_ALK ~ (
                       ((1 - Y_H) / (14 * 2.86 * Y_H) - i_XB / 14) * an_gr_het(states.S_S, states.S_O, states.S_NO, states.X_BH)
                       - i_XB/14 * aer_gr_het(states.S_S, states.S_O, states.X_BH)
                       - (i_XB / 14 + 1 / (7 * Y_A)) * aer_gr_aut(states.S_NH, states.S_O, states.X_BA)
                       + 1/14 * ammon_solorg_N(states.S_ND, states.X_BH)
                      )
    ]

    parameters = vcat(stoich_params, heterotrophs_params, autotrops_params, hydrolysis_params, ammonifiaction_params)

    Process("ASM1", eqs, t, parameters, rates, states; name=name) 
end

"""
    OzoneDisinfection(; name, kO3=10, kd=1500)

A component that defines two processes in a ozone disinfection i.e. decay of ozone and disinfection. This can be used as an example for testing the package.

# Parameters:
- `kO3` the ozone decay constant
- `kD` the disinfection constant

# States:
- `S_O3` the dissolved ozone concentration
- `X_B` the bacteria concentration (particulate)
"""
@component function OzoneDisinfection(; name, kO3=10, kd=1500)
    @named states = StateInputPort(Dict([
           "S_O3" => (particulate = false, colloidal = false, soluble = true), # soluble ozone concentration
           "X_B" => (particulate = true, colloidal = false, soluble = false),   # bacteria concentration
           ]))
    @named rates = ReactionOutputPort(unknowns(states))

    # parameters
    parameters = @parameters kO3=kO3 kd=kd

    eqs = [
        rates.S_O3 ~ -kO3 * states.S_O3             # decay of ozone
        rates.X_B ~ -kd * states.X_B * states.S_O3   # disinfection
    ]

    Process("OzoneDisinfection", eqs, t, parameters, rates, states; name=name)
end

"""
    Aeration(; name, k_La=240, S_O_max=8)

A component that defines a simple the aeration process using the oxygen transfer coefficient k_L*a and the oxygen saturation concentration.

# Parameters:
- `k_La` initial oxygen transfer coefficient (default 240 1/d)
- `S_O_max` the saturation concentration for oxygen (default 8 g/m^3) 

# States:
- `S_O` oxygen

# Connectors
- `state` The reactor states to connect (only dissolved oxygen)
- `rate` The calculated rate (dissolved oxygen only)
- `k_La` The oxygen transfer coefficent. Is a [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@extref), for usage with the systems from the `ModelingToolkitStandardLibrary` which provides e.g. controllers.
"""
@component function Aeration(; name, k_La = 240, S_O_max = 8)

    @named states = StateInputPort(Dict([
           "S_O" => (particulate = false, colloidal = false, soluble = true),  # oxygen
           ]))
    @named rates = ReactionOutputPort(unknowns(states))

    # variables
    @named k_La = ModelingToolkitStandardLibrary.Blocks.RealInput(nin=1, guess=k_La)
    # parameters
    parameters = @parameters S_O_max=S_O_max

    eqs = [
        rates.S_O ~ k_La.u * (S_O_max - states.S_O)
    ]

    Process("Aeration", eqs, t, parameters, rates, states, [k_La]; name=name)
end 

