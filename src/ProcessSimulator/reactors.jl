"""
    Reactor(eqs, t, ps, inflows, outflows, rates, states; systems=ODESystem[], exogenous_inputs=ODESystem[], name)

An implementation of [`AbstractProcessElement`](@ref) for reactors. They implement the mass transport,
while the processes happening are provided by a [`Process`](@ref).

It has fields:
- `eqs`: The equations
- `t`: Time symbol
- `ps`: Parameters
- `ode_system`: A `ODESystem` from `ModelingToolkit` for simulating the reactor
- `systems`: Vector of subsystems of the reactor (can be empty)
- `inflows`: Vector of the inflow connectors of the reactor
- `outflows`: Vector of the outflow connectors of the reactor
- `rates`: The rates connector of the reactor
- `states`: The states connector of the reactor
- `exogenous_inputs`: The exogenous input connectors to the reactor or the subsystems.
- `name`: The name of the reactors

It supports the following interfaces from the [`AbstractProcessElement`](@ref):
- [`states`](@ref)
- [`rates`](@ref)
- [`inflows`](@ref)
- [`outflows`](@ref)
- [`exogenous_inputs`](@ref)
- [`subsystems`](@ref)
"""
struct Reactor <: AbstractProcessElement
    typestr::String
    eqs::Vector{Equation}
    t::Symbolics.BasicSymbolic{Real}
    ps::Vector{Symbolics.BasicSymbolic{Real}}

    ode_system::ODESystem
    systems::Vector{ODESystem}

    inflows::Vector{ODESystem}
    outflows::Vector{ODESystem}
    rates::ODESystem
    states::ODESystem

    exogenous_inputs::Vector{ODESystem}

    name::Symbol

    function Reactor(typestr, eqs, t, ps, inflows, outflows, rates, states; systems=ODESystem[], exogenous_inputs=ODESystem[], name)
        # Add port metadata to all ports
        inflows = init_port_metadata.(inflows)
        outflows = init_port_metadata.(outflows)
        rates = init_port_metadata(rates)
        states = init_port_metadata(states)
        ode_system = compose(ODESystem(eqs, t, [], ps; systems, name), [inflows..., rates, states, outflows...])
        reactor = new(
            typestr,
            eqs, ModelingToolkit.get_iv(ode_system), 
            ModelingToolkit.value.(ps), ode_system, 
            get_namespaced_property.(Ref(ode_system), nameof.(systems)), 
            get_namespaced_property.(Ref(ode_system), nameof.(inflows)), 
            get_namespaced_property.(Ref(ode_system), nameof.(outflows)),
            get_namespaced_property(ode_system, nameof(rates)), 
            get_namespaced_property(ode_system, nameof(states)), 
            get_namespaced_property.(Ref(ode_system), nameof.(exogenous_inputs)), 
            name)
        # Automatically set the port metadata of all ports
        set_port_metadata!(reactor)
        return reactor            
    end
end

"""
    Reactor(reactor::Reactor, process::Process; name)

Alternative constructor for a `Reactor`, providing a composite reactor with a process in it.

Takes a `reactor` and a `process` and returns a new `Reactor` wrapping the two and connecting them, 
such that "the process is taking place in the reactor".
"""
function Reactor(reactor::Reactor, process::Process; name)
    connect_eqns = [
        connect(rates(process), rates(reactor))
        connect(states(reactor), states(process))
    ]

    Reactor(typestr(reactor) * " with " * process.typestr, 
        connect_eqns, t, [], inflows(reactor), outflows(reactor), rates(reactor), states(reactor); 
        systems=[process, reactor], 
        exogenous_inputs=union(exogenous_inputs.([reactor, process])...),
        name=name)
end


"""
    Reactor(reactor::Reactor, processes::Vector{Process}; name)

Alternative constructor for a `Reactor`, providing a composite reactor with multiple processes in it.

Takes a `reactor` and a set of `processes` and returns a new `Reactor` wrapping and connecting them, 
such that "the processes are all taking place in the reactor".

For the combination of the `Process`es, it is assumed that equally named states are the same 
(e.g. if two processes have a state called `S_O`, they are associated with one state of the reactor, 
i.e. they get the same input and the output rates are **added** to give the resulting rate of the 
overall state). Equivalently, if two states are called differently, they are treated as different
(e.g. `S_O != S_O2`).
"""
function Reactor(reactor::Reactor, processes::Vector{Process}; name, state_mapping::AbstractDict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}} = default_state_mapping())
    # Vector of all states for the reactor (only one for filtered out ones)
    reactor_states = unknowns(states(reactor))

    # Read in all states in processes and reactor
    all_states = union(unknowns.(states.(processes))..., reactor_states)

    # Dict from all states to mapped to one
    state_dict = Dict(zip(all_states, get_default_state.(all_states; state_mapping)))
    selected_states = Symbol.(symbolic_to_namestring.(default_states(;processes, state_mapping)))
    
    # Dict from reactor states to mapped to
    reactor_state_dict = Dict(zip(getindex.(Ref(state_dict), reactor_states), Symbol.(symbolic_to_namestring.(reactor_states))))
    # Dict from states
    processes_state_dict = [Dict(zip(getindex.(Ref(state_dict), unknowns(states(process))), Symbol.(symbolic_to_namestring.(unknowns(states(process)))))) for process in processes]

    # System to split all states
    state_split = StateInputToRealOutput(reactor_states; name = Symbol(name, "_state_split"))
    
    # Systems to reassemble the states
    state_assembles = map(x -> RealInputToStateOutput(unknowns(states(x)); name =Symbol(name, "_state_assemble_", nameof(x))), processes)

    # Systems to split the rates
    rate_splits = map(x -> ReactionInputToRealOutput(unknowns(states(x)); name =Symbol(name, "_rate_split_", nameof(x))), processes)

    # System to reassemble full rates
    rate_assemble = RealInputToReactionOutput(reactor_states; name = Symbol(name, "_rate_assemble"))

    # Connection equations
    connect_eqns = [
        # Reactor to state split
        connect(states(reactor), states(state_split))
        # Each splitted state to each of the corresponding assemblers
        [[connect(exogenous_outputs(state_split, reactor_state_dict[state]), exogenous_inputs(assemble, assemble_dict[state])) 
            for state in selected_states 
                if haskey(assemble_dict, Symbol(symbolic_to_namestring(state)))] 
                for (assemble, assemble_dict) in zip(state_assembles, processes_state_dict)]...
        # Assembled states to corresponding processes
        connect.(states.(state_assembles), states.(processes))
        # Processes to corresponding rate splitters
        connect.(rates.(processes), rates.(rate_splits))
        # Add splitted rates together and set to the assemled rate
        [exogenous_inputs(rate_assemble, reactor_state_dict[state]).u ~ 
            sum([exogenous_outputs(split, split_dict[state]).u 
                for (split, split_dict) in zip(rate_splits, processes_state_dict) 
                    if haskey(split_dict, state)]) 
            for state in selected_states]...
        # Rate assembler to the reactor
        connect(rates(rate_assemble), rates(reactor))
    ]

    Reactor(typestr(reactor) * " with " * join(getproperty.(processes, :typestr), ", ", " and "), 
        connect_eqns, t, [], inflows(reactor), outflows(reactor), rates(reactor), states(reactor); 
        systems=[reactor, processes..., state_split, state_assembles..., rate_splits..., rate_assemble], 
        exogenous_inputs=union(exogenous_inputs.([reactor, processes...])...),
        name=name)
end

function Base.convert(::Type{<:ODESystem}, x::Reactor)
    x.ode_system
end

function ModelingToolkit.equations(sys::Reactor)
    sys.eqs
end

function states(sys::Reactor)
    sys.states
end

function rates(sys::Reactor)
    sys.rates
end

function inflows(sys::Reactor)
    sys.inflows
end

function outflows(sys::Reactor)
    sys.outflows
end

function exogenous_inputs(sys::Reactor)
    sys.exogenous_inputs
end

function subsystems(sys::Reactor)
    sys.systems
end

"""
    @reactor function NAME(ARGS; KWARGS)
        # Reactor implementation
        # Returns a Reactor, takes the `states` as arg
        # and `name` as kwarg
    end

Given the Base implementation of a reactor, needs to be based on the `states` in the reactor, given as argument called `states`,
automatically create methods to call the same reactor with one or multiple processes, which are then automatically connected.

Conditions for the provided function:
- A vector of states is provided to the reactor in an argument called `states`. It can be of type `Vector`, but cannot have a default value.
- The function must take the name of the resulting reactor as kwarg called `name`. Can be of type `Symbol`, but cannot have a default value.

Results in defining a function with the given name and two methods:
- The function as given
- A method for providing a `Process` or `Vector{Process}` instead of the `states`. Makes all processes internal to the reactor and automatically connected, adds the default processes as well. [See the connection implementation.](@ref Reactor)

Documents the added function as well. For this, the docstring of the original function is reused with the following modifications:
- Replace the function call with the appropriate arguments
- Replace everything beginning with:
    
    'The system defines the mass flow'

    by

    'This function constructs a REACTOR_NAME, directly with the supplied processes in it.
    If `use_default_processes = true`, the default processes are added, which enables easy addition
    of processes to the default ones as well as calling this function without any process.'

- Change of `states` parameter documentation by `processes`
- Addition of parameter documentation for `state_mapping` and `use_default_processes`
"""
macro reactor(expr::Expr)
    # Check if given a function
    @assert (expr.head == :function || (expr.head == :(=) &&
        expr.args[1] isa Expr &&
        expr.args[1].head == :call)) "Must provide a valid function declaration"
    # Get function name and args
    fsign = expr.args[1]
    fname = fsign.args[1]
    fargs = fsign.args[2:end]
    # Get the index of the kwargs in the arguments
    kwargs_ind = findfirst([typeof(arg) == Expr && arg.head == :parameters for arg in fargs])

    # Check if args contain `states` and kwargs `name`
    @assert any([occursin(r"^states(::Vector)?$", string(arg)) for arg in fargs]) "The function must take an argument `states`"
    @assert !isnothing(kwargs_ind) && any([occursin(r"^name(::Symbol)?$", string(kwarg)) for kwarg in fargs[kwargs_ind].args]) "The function must take a keyword argument `name`"

    # In the beginning, add the given base implementation
    exprs = [esc(:(@component $expr))]

    # For with a vector of processes
    fargs_process = deepcopy(fargs)
    map!(x -> startswith(string(x), r"states(::Vector)?") ? Expr(:kw, :(processes::Union{Process, AbstractVector{Process}}), :(Vector{Process}())) : x, fargs_process, fargs_process)
    # Add kwarg 'state_mapping' to account for different naming
    push!(fargs_process[kwargs_ind].args, Expr(:kw, :(state_mapping::AbstractDict{Symbol, @NamedTuple{names::Set{Symbol}, particulate::Bool, colloidal::Bool, soluble::Bool}}), :(default_state_mapping())))
    # Add kwarg 'use_default_processes' to add specify if defaults should be used
    push!(fargs_process[kwargs_ind].args, Expr(:kw, :(use_default_processes::Bool), :(true)))
    # Parse fargs_process to string
    fargs_string = []
    fkwargs_string = []
    for arg in fargs_process
        if arg isa Expr
            if arg.head == :parameters
                for arg in arg.args
                    if arg isa Expr
                        push!(fkwargs_string, "$(first(split(string(arg.args[1]), "::"))) = $(arg.args[2])")
                    else
                        push!(fkwargs_string, string(arg))
                    end                    
                end
                continue
            end
            push!(fargs_string, "$(first(split(string(arg.args[1]), "::"))) = $(arg.args[2])")
        else
            push!(fargs_string, string(arg))
        end
    end
    fargs_process_string = join(fargs_string, ", ") * "; " * join(fkwargs_string, ", ")

    # replace Args in call
    fargs_process_reactor_call = deepcopy(fargs)
    map!(x -> begin 
        occursin(r"^states(::Vector)?$", string(x)) && return :reactor_states
        typeof(x) == Expr && x.head == :kw && return x.args[1]
        x
    end, fargs_process_reactor_call, fargs_process_reactor_call)
    # replace keyword Args in call
    map!(x -> begin
        occursin(r"^name(::Symbol)?$", string(x)) && return Expr(:kw, :name, :(Symbol(name, "_reactor")))
        typeof(x) == Expr && x.head == :kw && return x.args[1]
        x
    end, fargs_process_reactor_call[kwargs_ind].args, fargs_process_reactor_call[kwargs_ind].args)
    # Actual function
    expr_process_vec = quote 
        docstr = (@doc $(esc(fname)))
        if docstr isa Docs.DocStr
            # For DocStr
            docstr = docstr.text[1]
        else
            # For Markdown (when in REPL)
            docstr = string(docstr.content[1])
        end
        docstr = replace(docstr, Regex("$($fname)\\(.*\\)") => "$($fname)($($fargs_process_string))")
        docstr = replace(docstr, Regex("This system defines the mass flow .*") => 
        """
        This function constructs a $($fname), directly with the supplied processes in it.
        If `use_default_processes = true`, the default processes are added, which enables easy addition
        of processes to the default ones as well as calling this function without any process.
        """)
        docstr = replace(docstr, Regex("(# Parameters.*?)`states`: [^\n]*", "s") => 
            s"\1`processes`: The processes in the reactor. Will be extended by the default processes (unless `use_default_processes=false`). Can be either a single `Process` or a `Vector{Process}`. Defaults to an empty `Vector{Process}`, and thus only the default processes.")
        docstr = replace(docstr, Regex("(# Parameters.*?)(\n\n#)", "s") => 
            s"\1
* `state_mapping`: Mapping to rename and set states equal. Defaults to `default_state_mapping()`.
* `use_default_processes`: If the default processes (from `default_processes()`) are added to the given processes.\2")
        @doc "$docstr"
        @component function $(esc(fname))($(esc.(fargs_process)...))
            if $(esc(:processes)) isa Process
                $(esc(:processes)) = [$(esc(:processes))]
            end
            # Add default processes if required
            $(esc(:use_default_processes)) && append!($(esc(:processes)), default_processes(name_prefix=$(esc(:name))))
            # Check if there are any processes
            if isempty($(esc(:processes)))
                throw(ArgumentError("Cannot make a $(String(nameof($fname))) without a process or the states it should have. \n\n\
                Either specify it directly in the call to the reactor (see corresponding docs) \
                or add a default process using `set_default_process` or `add_default_process`"))
            end
            # Construct reactor
            reactor_states = default_states(;$(esc(:processes)), $(esc(:state_mapping)))
            reactor = $(esc(fname))($([arg == :reactor_states ? arg : esc(arg) for arg in fargs_process_reactor_call]...))
            # If only one process and no renaming, forward to simpler function
            if length($(esc(:processes))) == 1 && all(in.(keys($(esc(:state_mapping))), Ref(Symbol.(symbolic_to_namestring.(unknowns(states(only($(esc(:processes))))))))))
                Reactor(reactor, only($(esc(:processes))); name=$(esc(:name)))
            else
                Reactor(reactor, $(esc(:processes)); name=$(esc(:name)), $(esc(:state_mapping)))
            end
        end
    end
    push!(exprs, expr_process_vec)

    Expr(:block, exprs...) # Return all functions in a block Expr
end

"""
    CSTR(V, states; initial_states = nothing, name)

A Continuously Stirred Tank Reactor (CSTR).

This system defines the mass flow of a CSTR without any reactions. The reactions are to be supplied from an external system.

The mass transfer of an given component ``C`` in a CSTR with volume ``V`` and flow rate ``q`` is defined as:

``\\frac{dC}{dt} = (C_{in} - C) * \\frac{q}{V} + r_C``

where ``r_C`` is the externally supplied reaction rate corresponding to the component.

# Parameters:
- `V`: The volume of the CSTR
- `states`: The components that the CSTR should have and for which the mass transfer is defined
- `initial_states`: The initial concentrations in the CSTR
- `name`: The name of the CSTR

# States:
- One state for each component defined by the `states` parameter

# Connectors:
- `inflow`: The inflow of the CSTR
- `outflow`: The outflow of the CSTR
- `states`: The current values of all states in the CSTR
- `rates`: Input for the reaction rates for each state
"""
@reactor function CSTR(V, states; initial_states = nothing, name)
    # Get all connectors
    @named inflow = InflowPort(states)
    rate = ReactionInputPort(states; name=:rates)
    state = StateOutputPort(states; initial_states, set_guess = false, name = :states)
    @named outflow = OutflowPort(states)
    # The parameter for the Volume
    ps = @parameters V = V

    # The CSTR equation (inflow rate = outflow rate)
    eqs = [inflow.q ~ outflow.q]
    # For each Component c add the following equations:
    #   dc/dt = c_in * q_in/V - c * q_out/V + r_c (Conservation of mass + reaction term (supplied from connector))
    #   c_out = c (The output is equal to the internal state)
    for sym = Symbol.(symbolic_to_namestring.(unknowns(state)))
        in = getproperty(inflow, sym)
        r = getproperty(rate, sym)
        s = getproperty(state, sym)
        out = getproperty(outflow, sym)
        push!(eqs, D(s) ~ in * inflow.q / V - s * outflow.q / V + r)
        push!(eqs, out ~ s)
    end

    # Compose and return the CSTR system
    Reactor("CSTR", eqs, t, ps, [inflow], [outflow], rate, state, name=name)
end

"""
    BatchReactor(states; initial_states = nothing, name)

A Batch Reactor.

This system defines the mass transfer of a batch reactor (which is in fact empty). The reactions are to be
supplied from an external system. 

The volume of the tank is not needed, as only concentration is considered and the equation thus independent of the volume.
The equations for component ``C`` in a Batch reactor is then defined as:

``\\frac{dC}{dt} = r_C``

where ``r_C`` is the externally supplied reaction rate corresponding to the component.

# Parameters:
- `states`: The components that the batch reactor should have and for which the mass transfer is defined
- `initial_states`: The initial concentrations in the batch reactor
- `name`: The name of the reactor

# States:
- One state for each component defined by the `states` parameter

# Connectors:
- `states`: The current values of all states in the batch reactor
- `rates`: Input for the reaction rates for each state
"""
@reactor function BatchReactor(states; initial_states = nothing, name)
    # Get all connectors
    rate = ReactionInputPort(states; name=:rates)
    state = StateOutputPort(states; initial_states, set_guess = false, name = :states)
    
    #  Placeholder for the equations
    eqs = Equation[]
    # For each Component c add the following equations:
    #   dc/dt = r_c (Reaction term (supplied from connector))
    for sym = Symbol.(symbolic_to_namestring.(unknowns(state)))
        r = getproperty(rate, sym)
        s = getproperty(state, sym)
        push!(eqs, D(s) ~ r)
    end

    # Compose and return the batch reactor system
    Reactor("Batch Reactor", eqs, t, [], [], [], rate, state, name=name)
end
