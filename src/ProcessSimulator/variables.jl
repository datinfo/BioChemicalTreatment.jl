# Variable Annotations

# If the component is particulate, colloidal or soluble
struct VariableParticulate <: Symbolics.AbstractVariableMetadata end
struct VariableColloidal <: Symbolics.AbstractVariableMetadata end
struct VariableSoluble <: Symbolics.AbstractVariableMetadata end

Symbolics.option_to_metadata_type(::Val{:particulate}) = VariableParticulate
Symbolics.option_to_metadata_type(::Val{:colloidal}) = VariableColloidal
Symbolics.option_to_metadata_type(::Val{:soluble}) = VariableSoluble

isparticulate(x::Symbolics.Num; default = nothing) = isparticulate(Symbolics.unwrap(x); default = default)
iscolloidal(x::Symbolics.Num; default = nothing) = iscolloidal(Symbolics.unwrap(x); default = default)
issoluble(x::Symbolics.Num; default = nothing) = issoluble(Symbolics.unwrap(x); default = default)

"""
    isparticulate(x; default = nothing)

Determine if the symbolic variable `x` is marked as particulate or not.

`default` indicates how unlabeled variables should be treated, by default it is `nothing` to be able to distinct between labeled and unlabeled variables.
"""
function isparticulate(x; default = nothing)
    p = Symbolics.getparent(x, nothing)
    p === nothing || (x = p)
    Symbolics.getmetadata(x, VariableParticulate, default)
end

"""
    iscolloidal(x; default = nothing)

Determine if the symbolic variable `x` is marked as colloidal or not.

`default` indicates how unlabeled variables should be treated, by default it is `nothing` to be able to distinct between labeled and unlabeled variables.
"""
function iscolloidal(x; default = nothing)
    p = Symbolics.getparent(x, nothing)
    p === nothing || (x = p)
    Symbolics.getmetadata(x, VariableColloidal, default)
end

"""
    issoluble(x; default = nothing)

Determine if the symbolic variable `x` is marked as soluble or not.

`default` indicates how unlabeled variables should be treated, by default it is `nothing` to be able to distinct between labeled and unlabeled variables.
"""
function issoluble(x; default = nothing)
    p = Symbolics.getparent(x, nothing)
    p === nothing || (x = p)
    Symbolics.getmetadata(x, VariableSoluble, default)
end

