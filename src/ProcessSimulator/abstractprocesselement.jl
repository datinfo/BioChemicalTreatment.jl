"""
    AbstractProcessElement

An abstract type for every flow element for implementing generic functionality.

This abstract type is used for implementing generic functionality needed for every
flow element, e.g. [`Reactor`](@ref), [`FlowElement`](@ref) or [`Process`](@ref).
It defines the default interface and some general functions on them.

As this library is designed to be fully compatible with 
[`ModelingToolkit`](@extref ModelingToolkit :doc:`index`) all flow elements should
be convertible to an [`ODESystem`](@extref :jl:type:`ModelingToolkit.ODESystem`). This abstract
type then implements several functions from `ModelingToolkit` by falling back to this 
conversion. Further it implements the most general interface consisting of:
- [`states`](@ref)
- [`rates`](@ref)
- [`inflows`](@ref)
- [`outflows`](@ref)
- [`exogenous_inputs`](@ref)
- [`exogenous_outputs`](@ref)
- [`subsystems`](@ref)

# Extended help

# Implementation

If implementing a type inheriting from this, the [`Base.convert(::Type{<:ODESystem}, x::AbstractProcessElement)`](@ref) 
should be implemented for the new type, and it should return a 
[`ModelingToolkit.ODESystem`](@extref :jl:type:`ModelingToolkit.ODESystem`). 

Further, where applicable, the following as well:
- [`states`](@ref)
- [`rates`](@ref)
- [`inflows`](@ref)
- [`outflows`](@ref)
- [`exogenous_inputs`](@ref)
- [`exogenous_outputs`](@ref)
- [`subsystems`](@ref)

"""
abstract type AbstractProcessElement end

"""
    convert(::Type{<:ODESystem}, x::AbstractProcessElement)

Convert the AbstractProcessElement x to an [`ModelingToolkit.ODESystem`](@extref :jl:type:`ModelingToolkit.ODESystem`).

# Extended Help

# Implementation

This function must be implemented for each type inheriting from [`AbstractProcessElement`](@ref).
It takes one instance of the new type and return a [`ModelingToolkit.ODESystem`](@extref :jl:type:`ModelingToolkit.ODESystem`).
"""
function Base.convert(::Type{<:ODESystem}, x::AbstractProcessElement)
    error("No conversion to ODESystem defined for type $(typeof(x))!")
end

"""
    typestr(sys::AbstractProcessElement)

Get a string representing the type of the processelement.
Added to the type to carry additional data.

Defaults to the `typestr` field of `sys` or the real type.
"""
function typestr(sys::AbstractProcessElement)
    hasproperty(sys, :typestr) ? sys.typestr : string(nameof(Base.typeof(sys)))
end

# Show in repl
function Base.show(io::IO, mime::MIME"text/plain", sys::AbstractProcessElement; hint=true, bold=true, extended=false)
    limit = get(io, :limit, false) # if output should be limited,
    rows = div(first(displaysize(io)), (1 + hasstates(sys) + extended*hasrates(sys) + hasinflows(sys) + hasoutflows(sys) + hasexogenous_inputs(sys) + hasexogenous_outputs(sys) + extended*hassubsystems(sys) + extended + 1))

    # Print name and description
    description(sys) = ModelingToolkit.has_description(sys) ? ModelingToolkit.get_description(sys) : ""
    desc = description(sys)
    name = nameof(sys)
    printstyled(io, typestr(sys), " '", name, "':"; bold)
    !isempty(desc) && print(io, " ", desc)

    # Print states and rates
    for varfunc in (extended ? [states, rates] : [states]) # rates only when extended
        vars = nothing
        try
            vars = unknowns(varfunc(sys))
        catch
            continue
        end
        nvars = length(vars)
        nvars == 0 && continue # skip
        header = titlecase(String(nameof(varfunc))) # e.g. "Unknowns"
        printstyled(io, "\n$header ($nvars):"; bold)
        hint && print(io, " see $(nameof(varfunc))($name)")
        nrows = min(nvars, limit ? rows : nvars)
        nrows += nrows+1 == nvars
        defs = ModelingToolkit.has_defaults(sys) ? defaults(sys) : nothing
        for i in 1:nrows
            s = vars[i]
            print(io, "\n  ", s)
            if !isnothing(defs)
                val = get(defs, s, nothing) 
                isnothing(val) &&  Symbolics.hasmetadata(s, Symbolics.VariableDefaultValue) && (val = Symbolics.getmetadata(s, Symbolics.VariableDefaultValue))
                if !isnothing(val)
                    print(io, " [defaults to ")
                    show(
                        IOContext(io, :compact => true, :limit => true,
                            :displaysize => (1, displaysize(io)[2])),
                        val)
                    print(io, "]")
                else
                    Symbolics.hasmetadata(s, ModelingToolkit.VariableGuess) && (val = Symbolics.getmetadata(s, ModelingToolkit.VariableGuess))
                    print(io, " [guess is ")
                    show(
                        IOContext(io, :compact => true, :limit => true,
                            :displaysize => (1, displaysize(io)[2])),
                        val)
                    print(io, "]")
                end
                desc = getdescription(s)
            end
            if !isnothing(desc) && desc != ""
                print(io, ": ", desc)
            end
        end
        limited = nrows < nvars
        limited && printstyled(io, "\n  ⋮") # too many variables to print
    end
    
    # Print Connectors
    for f in [inflows, outflows, exogenous_inputs, exogenous_outputs]
        conn = nothing
        try
            conn = f(sys)
        catch
            continue
        end
        nconn = length(conn)
        nrows = min(nconn, limit ? rows : nconn)
        nrows += nrows+1 == nconn
        nrows > 0 && printstyled(io, "\n$(titlecase(join(split(string(nameof(f)), '_'), ' '))) ($(nconn)):"; bold)
        nrows > 0 && hint && print(io, " see $(nameof(f))($name)")
        for i in 1:nrows
            sub = conn[i]
            local name = Symbol(lstrip_namespace(nameof(sub), nameof(sys)))
            print(io, "\n  :", name)
            desc = description(sub)
            if !isempty(desc)
                maxlen = displaysize(io)[2] - length(name) - 6 # remaining length of line
                if limit && length(desc) > maxlen
                    desc = chop(desc, tail = length(desc) - maxlen) * "…" # too long
                end
                print(io, ": ", desc)
            end
        end
        limited = nrows < nconn
        limited && print(io, "\n  ⋮") # too many to print
    end

    # Print subsystems
    if extended && hassubsystems(sys)
        subs = subsystems(sys)
        hasstates(sys) && filter!(x -> !(x == states(sys)), subs)
        hasrates(sys) && filter!(x -> !(x == rates(sys)), subs)
        hasinflows(sys) && filter!(x -> !(x in inflows(sys)), subs)
        hasoutflows(sys) && filter!(x -> !(x in outflows(sys)), subs)
        hasexogenous_inputs(sys) && filter!(x -> !(x in exogenous_inputs(sys)), subs)
        hasexogenous_outputs(sys) && filter!(x -> !(x in exogenous_outputs(sys)), subs)
        nsubs = length(subs)
        nrows = min(nsubs, limit ? rows : nsubs)
        nrows += nrows+1 == nsubs
        nrows > 0 && printstyled(io, "\nSubsystems ($(nsubs)):"; bold)
        nrows > 0 && hint && print(io, " see hierarchy($name)")
        for i in 1:nrows
            sub = subs[i]
            local name = Symbol(lstrip_namespace(nameof(sub), nameof(sys)))
            print(io, "\n  :", name)
            desc = description(sub)
            if !isempty(desc)
                maxlen = displaysize(io)[2] - length(name) - 6 # remaining length of line
                if limit && length(desc) > maxlen
                    desc = chop(desc, tail = length(desc) - maxlen) * "…" # too long
                end
                print(io, ": ", desc)
            end
        end
        limited = nrows < nsubs
        limited && print(io, "\n  ⋮") # too many to print
    end

    # Print equations
    eqs = equations(sys)
    if extended && eqs isa AbstractArray && eltype(eqs) <: Equation
        neqs = count(eq -> !(eq.lhs isa Connection), eqs)
        next = ModelingToolkit.n_expanded_connection_equations(convert(ODESystem, sys))
        ntot = neqs + next
        ntot > 0 && printstyled(io, "\nEquations ($ntot):"; bold)
        neqs > 0 && print(io, "\n  $neqs standard", hint ? ": see equations($name)" : "")
        next > 0 && print(io, "\n  $next connecting",
            hint ? ": see equations(expand_connections($name))" : "")
        #Base.print_matrix(io, eqs) # usually too long and not useful to print all equations
    end

    # Print variables
    for varfunc in (extended ? [ModelingToolkit.get_unknowns, parameters] : [parameters])
        vars = varfunc(sys)
        nvars = length(vars)
        nvars == 0 && continue # skip
        header = titlecase(String(nameof(varfunc))) # e.g. "Unknowns"
        printstyled(io, "\n$header ($nvars):"; bold)
        hint && print(io, " see $(nameof(varfunc))($name)")
        nrows = min(nvars, limit ? rows : nvars)
        nrows += nrows+1 == nvars
        defs = ModelingToolkit.has_defaults(sys) ? defaults(sys) : nothing
        for i in 1:nrows
            s = vars[i]
            print(io, "\n  ", s)
            if !isnothing(defs)
                val = get(defs, s, nothing)
                if !isnothing(val)
                    print(io, " [defaults to ")
                    show(
                        IOContext(io, :compact => true, :limit => true,
                            :displaysize => (1, displaysize(io)[2])),
                        val)
                    print(io, "]")
                end
                desc = getdescription(s)
            end
            if !isnothing(desc) && desc != ""
                print(io, ": ", desc)
            end
        end
        limited = nrows < nvars
        limited && printstyled(io, "\n  ⋮") # too many variables to print
    end

    # Print observed
    nobs = extended && ModelingToolkit.has_observed(sys) ? length(observed(sys)) : 0
    nobs > 0 && printstyled(io, "\nObserved ($nobs):"; bold)
    nobs > 0 && hint && print(io, " see observed($name)")

    return nothing
end


# Implement the AbstractSystem Interface
# Just Apply everything to the underlying ODESystem

ModelingToolkit.equations(sys::AbstractProcessElement) = ModelingToolkit.equations(convert(ODESystem, sys))
ModelingToolkit.unknowns(sys::AbstractProcessElement) = ModelingToolkit.unknowns(convert(ODESystem, sys))
ModelingToolkit.parameters(sys::AbstractProcessElement) = ModelingToolkit.parameters(convert(ODESystem, sys))
ModelingToolkit.independent_variable_symbols(sys::AbstractProcessElement) = ModelingToolkit.independent_variable_symbols(convert(ODESystem, sys))
ModelingToolkit.nameof(sys::AbstractProcessElement) = ModelingToolkit.nameof(convert(ODESystem, sys))
ModelingToolkit.hierarchy(sys::AbstractProcessElement) = ModelingToolkit.hierarchy(convert(ODESystem, sys))
ModelingToolkit.expand_connections(sys::AbstractProcessElement) = ModelingToolkit.expand_connections(convert(ODESystem, sys))

ModelingToolkit.has_eqs(sys::AbstractProcessElement) = ModelingToolkit.has_eqs(convert(ODESystem, sys))
ModelingToolkit.get_eqs(sys::AbstractProcessElement) = ModelingToolkit.get_eqs(convert(ODESystem, sys))
ModelingToolkit.has_unknowns(sys::AbstractProcessElement) = ModelingToolkit.has_unknowns(convert(ODESystem, sys))
ModelingToolkit.get_unknowns(sys::AbstractProcessElement) = ModelingToolkit.get_unknowns(convert(ODESystem, sys))
ModelingToolkit.has_ps(sys::AbstractProcessElement) = ModelingToolkit.has_ps(convert(ODESystem, sys))
ModelingToolkit.get_ps(sys::AbstractProcessElement) = ModelingToolkit.get_ps(convert(ODESystem, sys))
ModelingToolkit.has_systems(sys::AbstractProcessElement) = ModelingToolkit.has_systems(convert(ODESystem, sys))
ModelingToolkit.get_systems(sys::AbstractProcessElement) = ModelingToolkit.get_systems(convert(ODESystem, sys))

ModelingToolkit.is_dde(sys::AbstractProcessElement) = ModelingToolkit.is_dde(convert(ODESystem, sys))

ModelingToolkit.has_parameter_dependencies(sys::AbstractProcessElement) = ModelingToolkit.has_parameter_dependencies(convert(ODESystem, sys))
ModelingToolkit.get_parameter_dependencies(sys::AbstractProcessElement) = ModelingToolkit.get_parameter_dependencies(convert(ODESystem, sys))

ModelingToolkit.has_constraints(sys::AbstractProcessElement) = ModelingToolkit.has_constraints(convert(ODESystem, sys))
ModelingToolkit.get_constraints(sys::AbstractProcessElement) = ModelingToolkit.get_constraints(convert(ODESystem, sys))

ModelingToolkit.has_op(sys::AbstractProcessElement) = ModelingToolkit.has_op(convert(ODESystem, sys))
ModelingToolkit.get_op(sys::AbstractProcessElement) = ModelingToolkit.get_op(convert(ODESystem, sys))

ModelingToolkit.observed(sys::AbstractProcessElement) = ModelingToolkit.observed(convert(ODESystem, sys))
ModelingToolkit.get_iv(sys::AbstractProcessElement) = ModelingToolkit.get_iv(convert(ODESystem, sys))
ModelingToolkit.independent_variables(sys::AbstractProcessElement) = ModelingToolkit.independent_variables(convert(ODESystem, sys))
ModelingToolkit.defaults(sys::AbstractProcessElement) = ModelingToolkit.defaults(convert(ODESystem, sys))

ModelingToolkit.has_observed(sys::AbstractProcessElement) = ModelingToolkit.has_observed(convert(ODESystem, sys))
ModelingToolkit.get_observed(sys::AbstractProcessElement) = ModelingToolkit.get_observed(convert(ODESystem, sys))
ModelingToolkit.get_continuous_events(sys::AbstractProcessElement) = ModelingToolkit.get_continuous_events(convert(ODESystem, sys))
ModelingToolkit.has_defaults(sys::AbstractProcessElement) = ModelingToolkit.has_defaults(convert(ODESystem, sys))
ModelingToolkit.get_defaults(sys::AbstractProcessElement) = ModelingToolkit.get_defaults(convert(ODESystem, sys))
ModelingToolkit.has_noiseeqs(sys::AbstractProcessElement) = ModelingToolkit.has_noiseeqs(convert(ODESystem, sys))
ModelingToolkit.get_noiseeqs(sys::AbstractProcessElement) = ModelingToolkit.get_noiseeqs(convert(ODESystem, sys))
ModelingToolkit.has_metadata(sys::AbstractProcessElement) = ModelingToolkit.has_metadata(convert(ODESystem, sys))
ModelingToolkit.get_metadata(sys::AbstractProcessElement) = ModelingToolkit.get_metadata(convert(ODESystem, sys))

ModelingToolkit.has_jac(sys::AbstractProcessElement) = ModelingToolkit.has_jac(convert(ODESystem, sys))
ModelingToolkit.get_jac(sys::AbstractProcessElement) = ModelingToolkit.get_jac(convert(ODESystem, sys))
ModelingToolkit.has_tgrad(sys::AbstractProcessElement) = ModelingToolkit.has_tgrad(convert(ODESystem, sys))
ModelingToolkit.get_tgrad(sys::AbstractProcessElement) = ModelingToolkit.get_tgrad(convert(ODESystem, sys))

ModelingToolkit.has_description(sys::AbstractProcessElement) = ModelingToolkit.has_description(convert(ODESystem, sys))
ModelingToolkit.get_description(sys::AbstractProcessElement) = ModelingToolkit.get_description(convert(ODESystem, sys))

# States
"""
    sates(sys)

Get the states connector of the dynamical system represented by the process element `sys`.

This function returns the states of the dynamical system, i.e. all variables that 
are involved as derivatives in any of the differential equations describing the
model. As not all systems are dynamic (they can be static as well), this function is
not applicable to static systems. Additionally this connector is used for all process
elements which need the internal states of a dynamical systems for processing, i.e. the
[`Process`](@ref) need the states of the associated [`Reactor`](@ref) to compute the rates.

# Extended Help
# Implementation

This function should be overridden for each type inheriting from [`AbstractProcessElement`](@ref)
which has states or needs the states of another [`AbstractProcessElement`](@ref). Note that depending on
if it needs or provides states, different connectors are needed ([`StateInputPort`](@ref) vs. [`StateOutputPort`](@ref))
"""
function states(sys::AbstractProcessElement)::ODESystem
    error("No states defined for type $(typeof(sys))!")
end

"""
    num_states(sys)

Returns the number of states in the [`AbstractProcessElement`](@ref).
"""
function num_states(sys::AbstractProcessElement)
    try 
        !isnothing(states(sys)) && return 1
    catch e
        if e isa ErrorException && e.msg == "No states defined for type $(typeof(sys))!"
            return 0
        else
            rethrow(e)
        end
    end
end

"""
    hasstates(sys)

Returns `true` if the [`AbstractProcessElement`](@ref) has states (i.e. the [`states`](@ref) function does not throw an error).
"""
function hasstates(sys::AbstractProcessElement)::Bool
    num_states(sys) > 0
end

# Rates
"""
    rates(sys)

Get the rates connector of the system represented by the process element `sys`.

This function returns the rates of the system. Not all process elements have rates,
but this mainly used for [`Reactor`](@ref)s and [`Process`](@ref)es. The [`Reactor`](@ref)
uses the rates as reaction rates for the process happening in the [`Reactor`](@ref) and the
[`Process`](@ref) provides these rates to the reactor.

# Extended Help
# Implementation

This function should be overridden for each type inheriting from [`AbstractProcessElement`](@ref)
which needs or provides rates. Note that depending on if it needs or provides rates, different 
connectors are needed ([`ReactionInputPort`](@ref) vs. [`ReactionOutputPort`](@ref))
"""
function rates(sys::AbstractProcessElement)::ODESystem
    error("No rates defined for type $(typeof(sys))!")
end

"""
    num_rates(sys)

Returns the number of rates of the [`AbstractProcessElement`](@ref).
"""
function num_rates(sys::AbstractProcessElement)
    try 
        !isnothing(rates(sys)) && return 1 
    catch e
        if e isa ErrorException && e.msg == "No rates defined for type $(typeof(sys))!"
            return 0
        else
            rethrow(e)
        end
    end
end

"""
    hasrates(sys)

Returns `true` if the [`AbstractProcessElement`](@ref) has rates (i.e. the [`rates`](@ref) function does not throw an error).
"""
function hasrates(sys::AbstractProcessElement)::Bool
    num_rates(sys) > 0
end

# Inflows
"""
    inflows(sys)

Get the vector of inflow connectors of the system represented by the process element `sys`.

This function returns the inflows of the system. Inflows are all flows into the system.

# Extended Help

# Implementation

This function should be overridden for each type inheriting from [`AbstractProcessElement`](@ref)
which could have one or more inflows.
"""
function inflows(sys::AbstractProcessElement)::Vector{ODESystem}
    error("No inflows defined for type $(typeof(sys))!")
end

"""
    num_inflows(sys)

Returns the number of inflows of the [`AbstractProcessElement`](@ref). Zero if there are none
"""
function num_inflows(sys::AbstractProcessElement)
    try 
        return length(inflows(sys))
    catch e
        if e isa ErrorException && e.msg == "No inflows defined for type $(typeof(sys))!"
            return 0
        else
            rethrow(e)
        end
    end
end

"""
    hasinflows(sys)

Returns `true` if the [`AbstractProcessElement`](@ref) has inflows (i.e. the [`inflows`](@ref) function does not throw an error).
"""
function hasinflows(sys::AbstractProcessElement)::Bool
    num_inflows(sys) > 0
end

"""
    inflow_names(sys)

Return the names of each inflow of the process element `sys`.

# Extended Help

# Implementation

This function defaults to output the names assigned to each inflow connector.
It is recomended to set these names to the names that should be output here
and this function should only be overridden in special cases.
"""
function inflow_names(sys::AbstractProcessElement)::Vector{Symbol}
    Symbol.(lstrip_namespace.(nameof.(inflows(sys)), Ref(nameof(sys))))
end

"""
    inflows(sys, names::Vector{Symbol})

Get the vector of inflow connectors in `sys` called `names`.
"""
function inflows(sys::AbstractProcessElement, names::Vector{Symbol})::Vector{ODESystem}
    if isempty(inflows(sys))
        throw(ArgumentError("Cannot get inflows called $names in the flow element. There are no inflows defined for this system."))
    end
    idxs = indexin(names, inflow_names(sys))
    if any(isnothing.(idxs))
        throw(ArgumentError("There are no inflows called $(names[isnothing.(idxs)]) in the flow element. Available inflows are $(inflow_names(sys))."))
    end
    inflows(sys)[idxs]
end

"""
    inflows(sys, names::Symbol)

Get the inflow connector in `sys` called `names`.
"""
function inflows(sys::AbstractProcessElement, names::Symbol)::ODESystem
    inflows(sys, [names])[1]
end

"""
    inflows(sys, idxs::Union{Int, Vector{Int}})

Get the inflow connectors in `sys` at indices `idxs`.

If `idxs` is a single integer, the output will be the connector, if
it is a vector of integers, the output will be a vecotr of connectors.
"""
function inflows(sys::AbstractProcessElement, idxs::Union{Int, Vector{Int}})
    inflows(sys)[idxs]
end


# Outflows
"""
    outflows(sys)

Get the vector of outflow connectors of the system represented by the process element `sys`.

This function returns the outflows of the system. Outflows are all flows leaving the system.

# Extended Help

# Implementation

This function should be overridden for each type inheriting from [`AbstractProcessElement`](@ref)
which could have one or more outflows.
"""
function outflows(sys::AbstractProcessElement)
    error("No outflows defined for type $(typeof(sys))!")
end

"""
    num_outflows(sys)

Returns the number of outflows of the [`AbstractProcessElement`](@ref).
"""
function num_outflows(sys::AbstractProcessElement)
    try
        return length(outflows(sys))
    catch e 
        if e isa ErrorException && e.msg == "No outflows defined for type $(typeof(sys))!"
            return 0
        else
            rethrow(e)
        end
    end
end

"""
    hasoutflows(sys)

Returns `true` if the [`AbstractProcessElement`](@ref) has outflows (i.e. the [`outflows`](@ref) function does not throw an error).
"""
function hasoutflows(sys::AbstractProcessElement)::Bool
    num_outflows(sys) > 0
end

"""
    outflow_names(sys)

Return the names of each outflow of the process element `sys`.

# Extended Help

# Implementation

This function defaults to output the names assigned to each outflow connector.
It is recomended to set these names to the names that should be output here
and this function should only be overridden in special cases.
"""
function outflow_names(sys::AbstractProcessElement)::Vector{Symbol}
    Symbol.(lstrip_namespace.(nameof.(outflows(sys)), Ref(nameof(sys))))
end

"""
    outflows(sys, names::Vector{Symbol})

Get the vector of outflow connectors in `sys` called `names`.
"""
function outflows(sys::AbstractProcessElement, names::Vector{Symbol})
    if isempty(outflows(sys))
        throw(ArgumentError("Cannot get outflows called $names in the flow element. There are no outflows defined for this system."))
    end
    idxs = indexin(names, outflow_names(sys))
    if any(isnothing.(idxs))
        throw(ArgumentError("There are no outflows called $(names[isnothing.(idxs)]) in the flow element. Available outflows are $(outflow_names(sys))."))
    end
    outflows(sys)[idxs]
end

"""
    outflows(sys, names::Symbol)

Get the outflow connector in `sys` called `names`.
"""
function outflows(sys::AbstractProcessElement, names::Symbol)
    outflows(sys, [names])[1]
end

"""
    outflows(sys, idxs::Union{Int, Vector{Int}})

Get the outflow connectors in `sys` at indices `idxs`.

If `idxs` is a single integer, the output will be the connector, if
it is a vector of integers, the output will be a vecotr of connectors.
"""
function outflows(sys::AbstractProcessElement, idxs::Union{Int, Vector{Int}})
    outflows(sys)[idxs]
end

# Exogenous Inputs
"""
    exogenous_inputs(sys)

Get the vector of exogenous input connectors of the system represented by the process element `sys`.

This function returns the exogenous inputs of the system. Exogenous inputs are all inputs to the system
which are not connectors provided by this package, i.e. not flows, states nor rates.

# Extended Help

# Implementation

This function should be overridden for each type inheriting from [`AbstractProcessElement`](@ref)
which could have one or more exogenous inputs.
"""
function exogenous_inputs(sys::AbstractProcessElement)
    error("No exogenous_inputs defined for type $(typeof(sys))!")
end

"""
    num_exogenous_inputs(sys)

Returns the number of exogenous_inputs in the [`AbstractProcessElement`](@ref).
"""
function num_exogenous_inputs(sys::AbstractProcessElement)
    try 
        return length(exogenous_inputs(sys))
    catch e
        if e isa ErrorException && e.msg == "No exogenous_inputs defined for type $(typeof(sys))!"
            return 0
        else
            rethrow(e)
        end
    end
end

"""
    hasexogenous_inputs(sys)

Returns `true` if the [`AbstractProcessElement`](@ref) has exogenous_inputs (i.e. the [`exogenous_inputs`](@ref) function does not throw an error).
"""
function hasexogenous_inputs(sys::AbstractProcessElement)::Bool
    num_exogenous_inputs(sys) > 0
end


"""
    exogenous_input_names(sys)

Return the names of each exogenous input of the process element `sys`.

# Extended Help

# Implementation

This function defaults to output the names assigned to each exogenous input connector.
It is recomended to set these names to the names that should be output here
and this function should only be overridden in special cases.
"""
function exogenous_input_names(sys::AbstractProcessElement)::Vector{Symbol}
    Symbol.(lstrip_namespace.(nameof.(exogenous_inputs(sys)), Ref(nameof(sys))))
end

"""
    exogenous_inputs(sys, names::Vector{Symbol})

Get the vector of exogenous input connectors in `sys` called `names`.
"""
function exogenous_inputs(sys::AbstractProcessElement, names::Vector{Symbol})
    if isempty(exogenous_input_names(sys))
        throw(ArgumentError("Cannot get exogenous_inputs called $names in the flow element. There are no exogenous_inputs defined for this system."))
    end
    idxs = indexin(names, exogenous_input_names(sys))
    if any(isnothing.(idxs))
        throw(ArgumentError("There are no exogenous_inputs called $(names[isnothing.(idxs)]) in the flow element. Available exogenous_inputs are $(exogenous_input_names(sys))."))
    end
    exogenous_inputs(sys)[idxs]
end

"""
    exogenous_inputs(sys, names::Symbol)

Get the exogenous input connector in `sys` called `names`.
"""
function exogenous_inputs(sys::AbstractProcessElement, names::Symbol)
    exogenous_inputs(sys, [names])[1]
end

"""
    exogenous_inputs(sys, idxs::Union{Int, Vector{Int}})

Get the exogenous input connectors in `sys` at indices `idxs`.

If `idxs` is a single integer, the output will be the connector, if
it is a vector of integers, the output will be a vecotr of connectors.
"""
function exogenous_inputs(sys::AbstractProcessElement, idxs::Union{Int, Vector{Int}})
    exogenous_inputs(sys)[idxs]
end

# Exogenous Outputs
"""
    exogenous_outputs(sys)

Get the vector of exogenous output connectors of the system represented by the process element `sys`.

This function returns the exogenous outputs of the system. Exogenous outputs are all outputs of the system
which are not connectors provided by this package, i.e. not flows, states nor rates.

# Extended Help

# Implementation

This function should be overridden for each type inheriting from [`AbstractProcessElement`](@ref)
which could have one or more exogenous outputs.
"""
function exogenous_outputs(sys::AbstractProcessElement)
    error("No exogenous_outputs defined for type $(typeof(sys))!")
end

"""
    num_exogenous_outputs(sys)

Returns the number of exogenous_outputs in the [`AbstractProcessElement`](@ref).
"""
function num_exogenous_outputs(sys::AbstractProcessElement)
    try 
        return length(exogenous_outputs(sys))
    catch e
        if e isa ErrorException && e.msg == "No exogenous_outputs defined for type $(typeof(sys))!"
            return 0
        else
            rethrow(e)
        end
    end
end

"""
    hasexogenous_outputs(sys)

Returns `true` if the [`AbstractProcessElement`](@ref) has exogenous_outputs (i.e. the [`exogenous_outputs`](@ref) function does not throw an error).
"""
function hasexogenous_outputs(sys::AbstractProcessElement)::Bool
    num_exogenous_outputs(sys) > 0
end

"""
    exogenous_output_names(sys)

Return the names of each exogenous output of the process element `sys`.

# Extended Help

# Implementation

This function defaults to output the names assigned to each exogenous output connector.
It is recomended to set these names to the names that should be output here
and this function should only be overridden in special cases.
"""
function exogenous_output_names(sys::AbstractProcessElement)::Vector{Symbol}
    Symbol.(lstrip_namespace.(nameof.(exogenous_outputs(sys)), Ref(nameof(sys))))
end

"""
    exogenous_outputs(sys, names::Vector{Symbol})

Get the vector of exogenous output connectors in `sys` called `names`.
"""
function exogenous_outputs(sys::AbstractProcessElement, names::Vector{Symbol})
    if isempty(exogenous_output_names(sys))
        throw(ArgumentError("Cannot get exogenous_outputs called $names in the flow element. There are no exogenous_outputs defined for this system."))
    end
    idxs = indexin(names, exogenous_output_names(sys))
    if any(isnothing.(idxs))
        throw(ArgumentError("There are no exogenous_outputs called $(names[isnothing.(idxs)]) in the flow element. Available exogenous_outputs are $(exogenous_output_names(sys))."))
    end
    exogenous_outputs(sys)[idxs]
end

"""
    exogenous_outputs(sys, names::Symbol)

Get the exogenous output connector in `sys` called `names`.
"""
function exogenous_outputs(sys::AbstractProcessElement, names::Symbol)
    exogenous_outputs(sys, [names])[1]
end

"""
    exogenous_outputs(sys, idxs::Union{Int, Vector{Int}})

Get the exogenous output connectors in `sys` at indices `idxs`.

If `idxs` is a single integer, the output will be the connector, if
it is a vector of integers, the output will be a vecotr of connectors.
"""
function exogenous_outputs(sys::AbstractProcessElement, idxs::Union{Int, Vector{Int}})
    exogenous_outputs(sys)[idxs]
end

# Subsystems
"""
    subsystems(sys)

Get the vector of subsystems of the process element `sys`.

# Extended Help

# Implementation

This function should be overridden for each type inheriting from [`AbstractProcessElement`](@ref)
which could have one or more subsystems.
"""
function subsystems(sys::AbstractProcessElement)
    error("No subsystems defined for type $(typeof(sys))!")
end

"""
    num_subsystems(sys)

Returns the number of subsystems in the [`AbstractProcessElement`](@ref).
"""
function num_subsystems(sys::AbstractProcessElement)
    try 
        return length(subsystems(sys))
    catch e
        if e isa ErrorException && e.msg == "No subsystems defined for type $(typeof(sys))!"
            return 0
        else
            rethrow(e)
        end
    end
end

"""
    hassubsystems(sys)

Returns `true` if the [`AbstractProcessElement`](@ref) has subsystems (i.e. the [`subsystems`](@ref) function does not throw an error).
"""
function hassubsystems(sys::AbstractProcessElement)::Bool
    num_subsystems(sys) > 0
end

"""
    subsystem_names(sys)

Return the names of each subsystem of the process element `sys`.

# Extended Help

# Implementation

This function defaults to output the names assigned to each subsystem.
It is recomended to set these names to the names that should be output here
and this function should only be overridden in special cases.
"""
function subsystem_names(sys::AbstractProcessElement)::Vector{Symbol}
    Symbol.(lstrip_namespace.(nameof.(subsystems(sys)), Ref(nameof(sys))))
end

"""
    subsystems(sys, names::Vector{Symbol})

Get the vector of subsystems in `sys` called `names`.
"""
function subsystems(sys::AbstractProcessElement, names::Vector{Symbol})
    if isempty(subsystem_names(sys))
        throw(ArgumentError("Cannot get subsystems called $names in the flow element. There are no subsystems defined for this system."))
    end
    idxs = indexin(names, subsystem_names(sys))
    if any(isnothing.(idxs))
        throw(ArgumentError("There are no subsystems called $(names[isnothing.(idxs)]) in the flow element. Available subsystems are $(subsystem_names(sys))."))
    end
    subsystems(sys)[idxs]
end

"""
    subsystems(sys, names::Symbol)

Get the subsystem in `sys` called `names`.
"""
function subsystems(sys::AbstractProcessElement, names::Symbol)
    subsystems(sys, [names])[1]
end

"""
    subsystems(sys, idxs::Union{Int, Vector{Int}})

Get the subsystems in `sys` at indices `idxs`.

If `idxs` is a single integer, the output will be the subsystem, if
it is a vector of integers, the output will be a vecotr of systems.
"""
function subsystems(sys::AbstractProcessElement, idxs::Union{Int, Vector{Int}})
    subsystems(sys)[idxs]
end

