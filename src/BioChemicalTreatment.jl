# Copyright (C) 2023 Juan Pablo Carbajal
# Copyright (C) 2023 Florian Wenk
# Copyright (C) 2023 Andreas Froemelt
# Copyright (C) 2023 Mariane Yvonne Schneider
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Author: Juan Pablo Carbajal <ajuanpi+dev@gmail.com>
# Author: Florian Wenk <florian.wenk@eawag.ch>
# Author: Andreas Froemelt <andreas.froemelt@eawag.ch>
# Author: Mariane Yvonne Schneider <myschneider@meiru.ch>
# Date: 07.07.2023



module BioChemicalTreatment
using Reexport

welcome() = println("Welcome to BioChemicalTreatment")

export Reactions
include("Reactions/Reactions.jl")

export ProcessSimulator
include("ProcessSimulator/ProcessSimulator.jl")
@reexport using BioChemicalTreatment.ProcessSimulator

end
