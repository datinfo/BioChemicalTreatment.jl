"""
    BioChemicalReactionSystem

A type for a biochemical reaction. Currently only preliminary until a final structure has been found.

Fields:
- `states`: The states of the system
- `state_properties`: Properties associated with the states
- `parameters`: The parameters of the system
- `parameter_properties`: Properties associated with the parameters
- `processrates`: Symbols for the processrates of the system
- `processrate_eqns`: Dict to get the equations for the processrates
- `processrate_properties`: Properties associated with the processrates
- `stoichmat`: The stoichiometric matrix
- `compositionmat`: The composition matrix
- `composition_names`: The names of the composition

# Constructors
    BioChemicalReactionSystem(modelname::AbstractString; name=Symbol(modelname))

Get the model called `modelname` from the library.

    BioChemicalReactionSystem(
            dir::AbstractString, modelname::AbstractString;
            states_path = joinpath(dir, modelname *"_states.csv"),
            parameters_path = joinpath(dir, modelname *"_parameters.csv"),
            processrates_path = joinpath(dir, modelname *"_processrates.csv"),
            matrix_path = joinpath(dir, modelname *"_matrix.csv"),
            compositionmat_path = joinpath(dir, modelname *"_compositionmatrix.csv");
            delimiter=';',
            name=:MatrixDefinedReaction
            )

Get the model named `modelname` found in the folder `dir`. Overwrite single files to be from a different location by providing the path in the corresponding keyword argument.
The files should be equivalent to `csv`, but delimited by the given delimiter (default `';'`)

    BioChemicalReactionSystem(states_path, parameters_path, processrates_path, matrix_path, compositionmatrix_path; delimiter=';', name=:MatrixDefinedReaction)

Get the model specified by the files under the given paths. Files should be `csv`-files which are delimited by the given delimiter (default `';'`).

    BioChemicalReactionSystem(file_path::AbstractString;
        states_sheet="states", parameters_sheet="parameters", 
        processrates_sheet="processrates", matrix_sheet="matrix", 
        compositionmatrix_sheet="compositionmatrix", 
        name::Symbol=:MatrixDefinedReaction)

Get the model specified in the xlsx-file under `file_path`. The file needs to have sheets for the different matrices with the given names.

!!! note
        To load models defined in XLSX-files, the [XLSX.jl](@extref XLSX :doc:`index`) package needs to be loaded as it enables reading the files.
"""
struct BioChemicalReactionSystem
    name::Symbol

    states::Vector
    state_properties::Dict

    parameters::Vector
    parameter_properties::Dict

    processrates::Vector
    processrate_eqns::Dict
    processrates_properties::Dict

    stoichmat::Matrix

    compositionmat::Matrix
    composition_names::Vector

    function BioChemicalReactionSystem(
        states::AbstractVector, parameters::AbstractVector, processrates::AbstractVector, processrate_eqns::AbstractDict, 
        stoichmat::AbstractMatrix, compositionmat::AbstractMatrix=zeros(0, length(states)); 
        state_properties::AbstractDict = Dict(states .=> repeat([NamedTuple()], length(states))), 
        parameter_properties::AbstractDict = Dict(parameters .=> repeat([NamedTuple()], length(parameters))), 
        processrate_properties::AbstractDict = Dict(processrates .=> repeat([NamedTuple()], length(processrates))), 
        composition_names::AbstractVector = "Composition ".*string(1:size(compositionmat, 1)),
        name::Symbol=:MatrixDefinedReaction
        )

        # Check sizes of all required(non-keyword) arguments
        @assert length(states) == size(stoichmat, 2) "The stoichiometric matrix must have the same number of columns as states"
        @assert length(processrates) == size(stoichmat, 1) "The stoichiometric matrix must have the same number of rows as processes"
        @assert length(states) == size(compositionmat, 2) "The composition matrix must have the same number of columns as states"
        @assert isempty(setdiff(Set(processrates), Set(keys(processrate_eqns)))) "There must be an equation for each process rate"

        # Check if properties match (no properties for non-existing values)
        @assert isempty(setdiff(Set(keys(state_properties)), Set(states))) "Unknown states $(setdiff(Set(keys(state_properties)), Set(states))) in state properties"
        @assert isempty(setdiff(Set(keys(parameter_properties)), Set(parameters))) "Unknown parameters $(setdiff(Set(keys(parameter_properties)), Set(parameters))) in parameter properties"
        @assert isempty(setdiff(Set(keys(processrate_properties)), Set(processrates))) "Unknown processrates $(setdiff(Set(keys(processrate_properties)), Set(processrates))) in processrate properties"
        @assert length(composition_names) == size(compositionmat, 1) "Must have equal number of composition names and columns in the composition matrix"

        new(name,
            states, state_properties, 
            parameters, parameter_properties, 
            processrates, processrate_eqns, processrate_properties, 
            stoichmat, 
            compositionmat, composition_names
        )
    end

    function BioChemicalReactionSystem(states_file::AbstractMatrix, parameters_file::AbstractMatrix, rates_file::AbstractMatrix, stoichmat_file::AbstractMatrix, compositionmat_file::AbstractMatrix; name::Symbol=:MatrixDefinedReaction)
        # Check sizes of states and rates and compare to matrices to avoid wrong matrix sizes
        n_states = size(states_file, 1) - 1 # one line of header
        n_rates = size(rates_file, 1) - 1 # one line of header
        @assert size(stoichmat_file, 1) - 1 == n_rates "Number of rows in the stoichiometric matrix does not match number of rates in processrate file.\nRows in stoichiometry: $(size(stoichmat_file, 1) - 1)\nNumber of rates: $n_rates"
        @assert size(stoichmat_file, 2) - 1 == n_states "Number of columns in the stoichiometric matrix does not match number of states in state file.\nColumns in stoichiometry: $(size(stoichmat_file, 2) - 1)\nNumber of states: $n_states"
        @assert size(compositionmat_file, 2) - 1 == n_states "Number of columns in the composition matrix does not match number of states in state file.\nColumns in composition matrix: $(size(compositionmat_file, 2) - 1)\nNumber of states: $n_states"
    
        # Extract symbols, equations and properties
        states, state_props = parse_states(states_file[2:end, :], states_file[1, :])
        params, params_props = parse_params(parameters_file[2:end, :], parameters_file[1, :])
        rates, rate_eqns, rate_props = parse_processrates(rates_file[2:end, :], rates_file[1, :], states, params)
        stoichmat, stoichmat_states, stoichmat_rates = parse_stoichmat(stoichmat_file[2:end, 2:end], stoichmat_file[1, 2:end], stoichmat_file[2:end, 1], params)
        compmat, compmat_states, compnames = parse_compmat(compositionmat_file[2:end, 2:end], compositionmat_file[1, 2:end], compositionmat_file[2:end, 1], params)

        # Check if states and processrates in stoichiometric and composition matrix match the ones in specific files
        @assert isempty(setdiff(Set(states), Set(stoichmat_states))) "States in states file and stoichiometric matrix do not match. Non-matching states:\nStates file: $(collect(setdiff(Set(states), Set(stoichmat_states))))\nStoichiometry: $(collect(setdiff(Set(stoichmat_states), Set(states))))"
        @assert isempty(setdiff(Set(rates), Set(stoichmat_rates))) "Rates in processrates file and stoichiometric matrix do not match. Non-matching rates:\nProcessrates file: $(collect(setdiff(Set(rates), Set(stoichmat_rates))))\nStoichiometry: $(collect(setdiff(Set(stoichmat_rates), Set(rates))))"
        @assert isempty(setdiff(Set(states), Set(compmat_states))) "States in states file and composition matrix do not match. Non-matching states:\nStates file: $(collect(setdiff(Set(states), Set(compmat_states))))\nComposition: $(collect(setdiff(Set(compmat_states), Set(states))))"
    
        # Ensure proper ordering of matrices
        Base.permutecols!!(stoichmat, sortperm(string.(stoichmat_states))[invperm(sortperm(string.(states)))]) # Columns of stoichmat according to states
        Base.permutecols!!(stoichmat', sortperm(string.(stoichmat_rates))[invperm(sortperm(string.(rates)))]) # Rows of stoichmat according to rates
        Base.permutecols!!(compmat, sortperm(string.(compmat_states))[invperm(sortperm(string.(states)))]) # Columns of compmat according to states
    
        # Construct and return the BioChemicalReactionSystem
        BioChemicalReactionSystem(
            states, params, rates, rate_eqns, stoichmat, compmat; 
            state_properties = state_props, parameter_properties = params_props,
            processrate_properties = rate_props, composition_names=compnames,
            name
            )
    end

    function BioChemicalReactionSystem(states_path, parameters_path, processrates_path, matrix_path, compositionmatrix_path; 
            delimiter::Char=';', name::Symbol=:MatrixDefinedReaction)
        # Read in all files
        states_file = readdlm(states_path, delimiter, skipblanks=true)
        parameters_file = readdlm(parameters_path, delimiter, skipblanks=true)
        rates_file = readdlm(processrates_path, delimiter, skipblanks=true)
        stoichmat_file = readdlm(matrix_path, delimiter, skipblanks=true)
        compositionmat_file = readdlm(compositionmatrix_path, delimiter, skipblanks=true)

        return BioChemicalReactionSystem(states_file, parameters_file, rates_file, stoichmat_file, compositionmat_file; name)
    end
    
    function BioChemicalReactionSystem(
        dir::AbstractString, modelname::AbstractString;
        states_path = joinpath(dir, modelname *"_states.csv"),
        parameters_path = joinpath(dir, modelname *"_parameters.csv"),
        processrates_path = joinpath(dir, modelname *"_processrates.csv"),
        matrix_path = joinpath(dir, modelname *"_matrix.csv"),
        compositionmat_path = joinpath(dir, modelname *"_compositionmatrix.csv"),
        delimiter=';',
        name=Symbol(modelname),
        )
        BioChemicalReactionSystem(states_path, parameters_path, processrates_path, matrix_path, compositionmat_path; delimiter, name)
    end

    function BioChemicalReactionSystem(::Val{:model}, modelname::AbstractString; name=Symbol(modelname))
        BioChemicalReactionSystem(@artifact_str("model_"*modelname), modelname; name)
    end

    function BioChemicalReactionSystem(stringarg::AbstractString; kwargs...)
        if endswith(stringarg, ".xlsx")
            ext = Base.get_extension(@__MODULE__, :XLSXExt)
            if isnothing(ext)
                throw(ErrorException("Needs XLSX.jl to read models from excel files. Load it first using\n\n'using XLSX'"))
            else
                BioChemicalReactionSystem(Val(:xlsx), stringarg; kwargs...)
            end
        else
            BioChemicalReactionSystem(Val(:model), stringarg; kwargs...)
        end
    end
end

# Check the composition matrix if it matches with the stoichiometric matrix
"""
    check_composition(stoichmat, compositionmat; atol = sqrt(eps()))

Check if the stoichiometric matrix `stoichmat` fulfills mass conservation specified by the composition matrix `compositionmat`. 
Assumes that the column orderings (corresponding to states) are matching.

# Keyword arguments
- `atol`: Absolute tolerance for numeric comparison. If symbolic inputs are provided (even if they represent numbers), the values have to be exactly zero. 
"""
function check_composition(stoichmat, compositionmat; atol = sqrt(eps()))
    result = simplify.(stoichmat * compositionmat')
    all(broadcast(x -> x === Num(0) || (!isa(x, Num) && abs(x) <= atol), result)) # Return true if Symbolic and every value exactly 0. If not symbolic, lower than atol.
end
"""
    check_composition(sys::BioChemicalReactionSystem; params_substitution = :expressions, kwargs...)

Check if `sys` fulfills the mass conservation requirement.

Optional keyword argument `params_substitution` specifies how the parameters are treated:
- `:expressions`: All parameters specified as expressions are replaced. Otherwise left symbolic. If true it is fulfilled for every parameter values of non-expression parameters.
- `:values`: All parameters are replaced with their default values. If true, it is fulfilled with the default parameters.
- `nothing` (of type `Nothing`, not a symbol): The parameters are left symbolic as they are. If true, it is fulfilled with all possible values for parameters.

Using additional `kwargs`, individual parameters can be replaced. Replacing by values has only an effect when `params_substitution == :values` and 
replacing by expressions when `params_substitution == :values` or `params_substitution == :expressions`.
"""
function check_composition(sys::BioChemicalReactionSystem; params_substitution = :expressions, kwargs...)
    stoichmat = sys.stoichmat
    compositionmat = sys.compositionmat
    if params_substitution == :expressions
        stoichmat = simplify.(substitute.(stoichmat, Ref(parameter_expressions(sys; kwargs...))))
        compositionmat = simplify.(substitute.(compositionmat, Ref(parameter_expressions(sys; kwargs...))))
    elseif params_substitution == :values
        stoichmat = Symbolics.unwrap.(substitute.(stoichmat, Ref(parameter_values(sys; kwargs...))))
        compositionmat = Symbolics.unwrap.(substitute.(compositionmat, Ref(parameter_values(sys; kwargs...))))
    elseif !isnothing(params_substitution)
        throw(ArgumentError("Substitute argument 'subs' was passed invalid value '$params_substitution'. Valid values are the symbols ':expressions' and ':values' or 'nothing'."))
    end
    check_composition(stoichmat, compositionmat)
end

# Assemble the equations
"""
    equations(stoichmat, rates, states)

Assemble and return the rate equations specified by the stoichiometric matrix `stoichmat` with rows associated to `rates` and columns to `states`.
Order is assumed to be matching between stoichiometric matrix and rates/states.
"""
function assemble_equations(stoichmat, rates, states)
    Dict(states .=> stoichmat' * rates)
end
"""
    equations(sys::BioChemicalReactionSystem)

Assemble and return the rate equations specified of `sys`.
"""
assemble_equations(sys::BioChemicalReactionSystem) = assemble_equations(sys.stoichmat, sys.processrates, sys.states)

# Get the parameter values or expression (defaults or values provided in kwargs, value default superseeds expression default, kwargs)
"""
    parameter_value_or_expression(sys::BioChemicalReactionSystem, default=0; kwargs...)

Get the value or expression for each parameter of the given `sys`. Defaults can be overridden by providing corresponding `kwargs`.

Returns a dict with keys being the parameters and values the corresponding value or expression.
For values, the following values are taken (always the first in the list that exists):  
1. Value or expression provided in `kwargs`
2. Default value provided in system
3. Default expression provided in system
4. `default` parameter provided to this function
"""
function parameter_value_or_expression(sys::BioChemicalReactionSystem, default=0; kwargs...)
    # Assert if invalid values were given
    @assert issubset(symbolic_to_namestring.(keys(kwargs)), symbolic_to_namestring.(sys.parameters)) "The parameters $(setdiff(symbolic_to_namestring.(keys(kwargs)), symbolic_to_namestring.(sys.parameters))) for which values were given do not exist in the system."
    
    value_map = Dict([k => isempty(v[:value]) ? (haskey(v, :expression) ? v[:expression] : default) : v[:value] for (k, v) in sys.parameter_properties])
    for k in keys(value_map)
        if haskey(kwargs, Symbol(symbolic_to_namestring(k)))
            setval = kwargs[Symbol(symbolic_to_namestring(k))]
            if setval isa AbstractString
                setval = parse_equation(setval, sys.parameters)
            end
            value_map[k] = setval
        end
    end
    value_map
end

"""
    parameter_expressions(sys::BioChemicalReactionSystem, default=0; kwargs...)

Return a dict mapping parameters associated with expressions to the corresponding expression.
Defaults for each parameter can be overridden in `kwargs`, overall default (if nothing else provided) is set by `default`.
"""
function parameter_expressions(sys::BioChemicalReactionSystem, default=0; kwargs...)
    val_map = parameter_value_or_expression(sys, default; kwargs...)
    #TODO: Properly solve the system to allow for a more broad range of expressions
    Dict([k => v for (k, v) in val_map if v isa Num])
end

"""
    parameter_values(sys::BioChemicalReactionSystem, default=0; kwargs...)

Return a dict mapping parameters to the corresponding values. For parameters associated with expressions, the expression is evaluated.
Currently only works if there are no dependencies of expressions on other non-numeric parameters.
Defaults for each parameter can be overridden in `kwargs`, overall default (if nothing else provided) is set by `default`.
"""
function parameter_values(sys::BioChemicalReactionSystem, default=0; kwargs...)
    val_map = parameter_value_or_expression(sys, default; kwargs...)
    #TODO: Properly solve the system to allow for a more broad range of expressions
    for (k, v) in val_map
        try
            val_map[k] = Symbolics.value.(substitute(v, val_map))
        catch e
            throw(ArgumentError("Cannot provide values for all parameters. Parameter '$k' evaluates to '$(val_map[k])' which is not numeric."))
        end
    end
    val_map
end
