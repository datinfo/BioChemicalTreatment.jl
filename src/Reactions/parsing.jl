# Custom equation parser to restrict available functionalities.
# Should mitigate potential RCE problems
"""
    DEFAULT_PARSING_CONTEXT

The default context used for parsing expressions.
Contains only mathematical functions.

This is done to restrict to mathematical functions which hopefully should 
prevent remote code execution problems. (Generally anyways no problem as only
trusted models should be included. But add additional layer of security)
"""
const DEFAULT_PARSING_CONTEXT::AbstractDict{Symbol, Any} = Dict([
    :- => Base.:-
    :+ => Base.:(+)
    :* => Base.:*
    :/ => Base.:(/)
    :\ => Base.:\
    :^ => Base.:^
    :inv => Base.inv
    :mod => Base.mod
    :rem => Base.rem
    :mod1 => Base.mod1
    :sin => Base.sin
    :cos => Base.cos
    :tan => Base.tan
    :sind => Base.Math.sind
    :cosd => Base.Math.cosd
    :tand => Base.Math.tand
    :sincosd => Base.Math.sincosd
    :sinpi => Base.Math.sinpi
    :cospi => Base.Math.cospi
    :sinh => Base.sinh
    :cosh => Base.cosh
    :tanh => Base.tanh
    :asin => Base.asin
    :acos => Base.acos
    :atan => Base.atan
    :asind => Base.Math.asind
    :acosd => Base.Math.acosd
    :atand => Base.Math.atand
    :sec => Base.Math.sec
    :csc => Base.Math.csc
    :cot => Base.Math.cot
    :secd => Base.Math.secd
    :cscd => Base.Math.cscd
    :cotd => Base.Math.cotd
    :asec => Base.Math.asec
    :acsc => Base.Math.acsc
    :acot => Base.Math.acot
    :asecd => Base.Math.asecd
    :acscd => Base.Math.acscd
    :acotd => Base.Math.acotd
    :sech => Base.Math.sech
    :csch => Base.Math.csch
    :coth => Base.Math.coth
    :asinh => Base.asinh
    :acosh => Base.acosh
    :atanh => Base.atanh
    :asech => Base.Math.asech
    :acsch => Base.Math.acsch
    :acoth => Base.Math.acoth
    :sinc => Base.Math.sinc
    :deg2rad => Base.Math.deg2rad
    :rad2deg => Base.Math.rad2deg
    :hypot => Base.Math.hypot
    :log => Base.log
    :log => Base.log
    :log2 => Base.log2
    :log10 => Base.log10
    :log1p => Base.log1p
    :exp => Base.exp
    :exp2 => Base.exp2
    :exp10 => Base.exp10
    :expm1 => Base.expm1
    :ceil => Base.ceil
    :floor => Base.floor
    :min => Base.min
    :max => Base.max
    :abs => Base.abs
    :abs2 => Base.abs2
    :copysign => Base.copysign
    :sign => Base.sign
    :sqrt => Base.sqrt
    :√ => Base.sqrt
    :binomial => Base.binomial
    :e => Base.MathConstants.e
    :ℯ => Base.MathConstants.ℯ
    :pi => Base.MathConstants.pi
    :π => Base.MathConstants.π
    :eulergamma => Base.MathConstants.eulergamma
    :catalan => Base.MathConstants.catalan
    :golden => Base.MathConstants.golden
    :φ => Base.MathConstants.φ
])

"""
    parse_equation(eqn, symbols; context::AbstractDict = DEFAULT_PARSING_CONTEXT)

Parse an equation given as Expr or String.
Only Numbers, Variables given in `symbols`, and functions from `context` are parsed. If a symbol not found in any of those, an error is thrown.

Purpose of this is to restrict available functionality when parsing strings. Should mitigate problematic remote code execution if `symbols` and
`context` is chosen appropriately. 
"""
function parse_equation(eqn, symbols::AbstractVector{<:Union{Num, SymbolicUtils.BasicSymbolic}} = Num[]; context::AbstractDict{Symbol, Any} = DEFAULT_PARSING_CONTEXT) # Make states and params dicts if given as vectors
    parse_equation(
        eqn,
        Dict(Symbol.(symbolic_to_namestring.(symbols)) .=> symbols);
        context
    )
end
parse_equation(eqn::AbstractString, symbols::AbstractDict{Symbol, <:Union{Num, SymbolicUtils.BasicSymbolic}}; context::AbstractDict{Symbol, Any} = DEFAULT_PARSING_CONTEXT) = parse_equation(Meta.parse(eqn), symbols; context) # If geven as string, parse to Expr first
parse_equation(eqn::Number, symbols::AbstractDict{Symbol, <:Union{Num, SymbolicUtils.BasicSymbolic}}; context::AbstractDict{Symbol, Any} = DEFAULT_PARSING_CONTEXT) = eqn # Numbers are just themselves
parse_equation(eqn::Nothing, symbols::AbstractDict{Symbol, <:Union{Num, SymbolicUtils.BasicSymbolic}}; context::AbstractDict{Symbol, Any} = DEFAULT_PARSING_CONTEXT) = 0 # Nothing evaluates to 0
function parse_equation(eqn::Symbol, symbols::AbstractDict{Symbol, <:Union{Num, SymbolicUtils.BasicSymbolic}} = Dict(); context::AbstractDict{Symbol, Any} = DEFAULT_PARSING_CONTEXT)
    if haskey(symbols, eqn)
        symbols[eqn]
    elseif haskey(context, eqn)
        context[eqn]
    else
        throw(ErrorException("Cannot parse $eqn. Not defined in symbols nor context."))
    end
end
function parse_equation(eqn, symbols; context::AbstractDict{Symbol, Any} = DEFAULT_PARSING_CONTEXT)
    if eqn.head == :call
        if haskey(context, eqn.args[1])
            return context[eqn.args[1]](parse_equation.(eqn.args[2:end], Ref(symbols); context)...)
        else
            throw(ErrorException("Cannot parse expression $eqn. Misspelled function name or calling a user-defined symbol.\n\
            Calling user-defined symbols (e.g. rates) is currently not supported. As a workaround, write out the expression directly."))
        end
    end
end

"""
    parse_variable(var; 
                   dependency::AbstractArray{<:Union{Symbolics.BasicSymbolic, Symbolics.Num}} = Symbolics.Num[], 
                   description::AbstractString = "", particulate::Union{Bool, Nothing} = nothing, 
                   colloidal::Union{Bool, Nothing} = nothing, soluble::Union{Bool, Nothing} = nothing)

Parse the variable `var` and return a Symbolics variable with this name. Keyword arguments to specify properties of it.

Available keyworks/properties:
- description: Description of the variable
- particulate: If the represented compound is particulate
- colloidal: If the represented compound is colloidal
- soluble: If the represented compound is soluble
"""
function parse_variable(var; dependency::AbstractArray{<:Union{Symbolics.BasicSymbolic, Symbolics.Num}} = Symbolics.Num[], description::AbstractString = "", particulate::Union{Bool, Nothing} = nothing, colloidal::Union{Bool, Nothing} = nothing, soluble::Union{Bool, Nothing} = nothing)
    @assert !any(contains.(string(var), [".", "(", ")", "₊"])) "Invalid variable to parse. Cannot contain any of [$(join([".", "(", ")", "₊"], ", "))]."
    genvar_str = "@variables $var"
    if !isempty(dependency)
        genvar_str *= "(" * join(dependency, ", ") * ")"
    end
    genvar_str *= " ["
    if !isempty(description)
        genvar_str *= "description = \"$description\","
    end
    if !isnothing(particulate)
        genvar_str *= "particulate = $particulate,"
    end
    if !isnothing(colloidal)
        genvar_str *= "colloidal = $colloidal,"
    end
    if !isnothing(soluble)
        genvar_str *= "soluble = $soluble,"
    end
    genvar_str *= "]"
    genvar_expr = Meta.parse(genvar_str)
    eval(genvar_expr)[1]
end

# Parsing functions for specific matrices
"""
    parse_states(statemat, header)

Parse the states given in matrix `statemat` with header `header`.
Required fields are:
- `name`: The name of the state
- `description`: The description of the state
- `particle_size`: The particle size; `particulate`, `colloidal` or `soluble`. Multiple can be specified (just need to contain all of them)
"""
function parse_states(statemat, header)
    header_dict = Dict([Symbol(x) => i for (i, x) in enumerate(header)])

    sts = broadcast(
        (var, desc, part_size) -> parse_variable(var; dependency=[t], description=desc, 
            particulate=contains(part_size, "particulate"), colloidal=contains(part_size, "colloidal"), soluble=contains(part_size, "soluble")), 
        statemat[:, header_dict[:name]], statemat[:, header_dict[:description]], statemat[:, header_dict[:particle_size]])
    notname_idxs = vec(header .!= "name")
    state_dict = Dict(sts .=> NamedTuple{Tuple(Symbol.(header[notname_idxs]))}.(eachrow(statemat[:, notname_idxs])))

    return sts, state_dict
end

"""
    parse_params(paramsmat, header)

Parse the parameters given in matrix `paramsmat` with header `header`.
Required fields are:
- `name`: The name of the parameter
- `description`: The description of the parameter

Optional fields are:
- `expression`: An expression to set the parameter if no value is given. Can consist of the parameters and the functions in [`DEFAULT_PARSING_CONTEXT`](@ref).
"""
function parse_params(paramsmat, header)
    header_dict = Dict([Symbol(x) => i for (i, x) in enumerate(header)])

    # Get the parameters
    pars = broadcast((var, desc) -> parse_variable(var; description=desc), paramsmat[:, header_dict[:name]], paramsmat[:, header_dict[:description]])

    # Parse expressions if there are some
    if haskey(header_dict, :expression)
        paramsmat[:, header_dict[:expression]] = parse_equation.(paramsmat[:, header_dict[:expression]], Ref(pars))
    end

    notname_idxs = vec(header .!= "name")
    params_dict = Dict(pars .=> NamedTuple{Tuple(Symbol.(header[notname_idxs]))}.(eachrow(paramsmat[:, notname_idxs])))

    return pars, params_dict
end

"""
    parse_processrates(statemat, header, states, params)

Parse the process rates given in matrix `ratesmat` with header `header`.
Required fields are:
- `name`: The name of the parameter
- `description`: The description of the parameter
- `equation`: The rate equation. Allowed symbols are the variables defined in `states` and `params` and the functions in [`DEFAULT_PARSING_CONTEXT`](@ref).
"""
function parse_processrates(ratesmat, header, states, params)
    header_dict = Dict([Symbol(x) => i for (i, x) in enumerate(header)])

    # Parse the names
    ratenames = broadcast((var, desc) -> parse_variable(var; dependency=[t], description=desc), ratesmat[:, header_dict[:name]], ratesmat[:, header_dict[:description]])

    # Parse the equations
    eqn_dict = Dict(ratenames .=> parse_equation.(ratesmat[:, header_dict[:equation]], Ref([states; params; ratenames])))

    notname_idxs = vec(header .!= "name")
    rates_dict = Dict(ratenames .=> NamedTuple{Tuple(Symbol.(header[notname_idxs]))}.(eachrow(ratesmat[:, notname_idxs])))

    return ratenames, eqn_dict, rates_dict
end

"""
    parse_stoichmat(stoichmat, mat_states, mat_rates, params)

Parse the stoichiometric matrix given in `stoichmat` with columns specified by state variables `mat_states`
and rows by rate variables `mat_rates`. It can contain numbers, the parameters in `params` and mathematical functions
in [`DEFAULT_PARSING_CONTEXT`](@ref).
"""
function parse_stoichmat(stoichmat, mat_states, mat_rates, params)
    # Parse the matrix
    matrix = parse_equation.(stoichmat, Ref(params))

    return matrix, parse_variable.(mat_states; dependency=[t]), parse_variable.(mat_rates; dependency=[t])
end

"""
    parse_compmat(compmat, mat_states, composition_names, params)

Parse the composition matrix given in `compmat` with columns specified by state variables `mat_states`
and the composition in the rows are named `composition_names`. It can contain numbers, the parameters in `params` 
and mathematical functions in [`DEFAULT_PARSING_CONTEXT`](@ref).
"""
function parse_compmat(compmat, mat_states, composition_names, params)
    # Parse the matrix
    matrix = parse_equation.(compmat, Ref(params))

    return matrix, parse_variable.(mat_states; dependency=[t]), Symbol.(composition_names)
end
