# Activated Sludge Model nr. 3 (ASM3)
The original source of ASM3 is [Gujer:1999](@cite). This model has been tested and the terms standardised since, so that we decided to further include the information from [Hauduc:2010](@cite) and [Corominas:2010](@cite). The goal was to provide ASM3 in a machine readable form. Therefore, we also included some of our own changes in the notion to make it consistent and shorten the terms in order to be able to display full equations. 

!!! note "Using this Model"
    Create a process with this model using

    ```julia
    Process("ASM3")
    ```
    
    Add keyword arguments to overwrite the default values of the parameters. E.g.
    
    ```julia
    Process("ASM3"; f_SUXCBhyd=1)
    ```

    This works for all parameters below and just add multiples for every to overwrite.

!!! warning "Initial State"
    When using this Model, one needs to set the initial state/initial condition **in the corresponding reactor**. This is because the default values are all `0`, and the some of the process equations have a division by `0` if all states are `0`!

## States
```@eval
Main.state_descriptions[:ASM3]
```

## Process Rates
```@eval
Main.processrate_descriptions[:ASM3]
```

## Stoichiometric Matrix
```@eval
Main.stoichmat[:ASM3]
```

## Composition Matrix
```@eval
Main.compositionmat[:ASM3]
```
## Parameters
```@eval
Main.parameter_descriptions[:ASM3]
```

## References
```@bibliography
Pages = []
Canonical = false

Hauduc:2010
Gujer:1999
Corominas:2010
```
