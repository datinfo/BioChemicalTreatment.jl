# Activated Sludge Model nr. 1 (ASM1)
The original source of the activated sludge model is [Henze:1987](@cite). This model has been tested and extended since, so that we decided to further include the information from [Hauduc:2010](@cite) and [Corominas:2010](@cite). The goal was to provide these models in a machine readable form with a mass balance that sums up to zero.

!!! note "Using this Model"
    Create a process with this model using

    ```julia
    Process("ASM1")
    ```
    
    Add keyword arguments to overwrite the default values of the parameters. E.g.
    
    ```julia
    Process("ASM1"; Y_OHO=1)
    ```

    This works for all parameters below and just add multiples for every to overwrite.

!!! warning "Initial State"
    When using this Model, one needs to set the initial state/initial condition **in the corresponding reactor**. This is because the default values are all `0`, and the some of the process equations have a division by `0` if all states are `0`!

## States
```@eval
Main.state_descriptions[:ASM1]
```

## Process Rates
```@eval
Main.processrate_descriptions[:ASM1]
```

## Stoichiometric Matrix
```@eval
Main.stoichmat[:ASM1]
```

## Composition Matrix
```@eval
Main.compositionmat[:ASM1]
```

## Parameters
```@eval
Main.parameter_descriptions[:ASM1]
```

## References
```@bibliography
Pages = []
Canonical = false

Hauduc:2010
Henze:1987
Corominas:2010
```

