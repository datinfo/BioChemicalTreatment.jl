"""
    symbolic_to_namestring(sym)

A helper function to get the name of a symbol as string cutting the dependencies.
"""
function symbolic_to_namestring(sym)::String
    # Remove dependencies
    split(string(sym), '(')[1]
end
function symbolic_to_namestring(sym::Symbolics.Num)::String
    symbolic_to_namestring(Symbolics.unwrap(sym))
end