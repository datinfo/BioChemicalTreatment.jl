module Reactions
    using Symbolics
    using LazyArtifacts
    using DelimitedFiles

    # Define symbols for independent variable (time) t, and the derivative operator w.r.t. it
    @variables t
    D = Differential(t)

    export BioChemicalReactionSystem, check_composition, assemble_equations
    include("biochemicalreactionsystem.jl")

    include("parsing.jl")

    include("utils.jl")
end