# Disinfection with Ozone

As a small example, we consider the following code, which implements a simple connection of two continuously stirred tank reactors (CSTR) as commonly used in water treatment. It serves as a quick example to start with. For more detailed explanations, we referred to the [API of the ProcessSimulator](@ref ProcessSimulator) and the more advanced examples.

This example implements two reactors used for ozonation of water. In the first reactor, the concentration of ozone is kept constant, to simulate a supply of ozone in the beginning. The stoichiometric matrix for the ozonation process is defined in the library and accessible under the name [`OzoneDisinfection`](@ref). This system is depicted as follows:

```@example
using TikzPictures # hide
TikzPicture(L""" %# hide
       \tikzstyle{block} = [draw, rectangle, inner sep=20pt, align=center] %# hide
       \tikzstyle{clarifier} = [draw, regular polygon, regular polygon sides=3, shape border rotate=180, align=center] %# hide
       \node (inf) {Influent}; %# hide
       \node[block, right=2cm of inf] (cstr1) {CSTR 1 with Ozonation\\Constant ozone concentration}; %# hide
       \node[block, right=2cm of cstr1] (cstr2) {CSTR 2 with Ozonation}; %# hide
       \node[right=of cstr2] (eff) {Effluent}; %# hide
 %# hide
       \draw[->] (inf) -- (cstr1) node[pos=0.5]{$q_{in},C_{in}$}; %# hide
       \draw[->] (cstr1) -- (cstr2) node[pos=0.5]{$q_{r},C_{r}$}; %# hide
       \draw[->] (cstr2) -- (eff) node[pos=0.5]{$q_{eff},C_{eff}$}; %# hide
       """; options=">=stealth, auto, node distance=2cm, scale=2, background rectangle/.style={fill=white}, show background rectangle", preamble=raw"\usetikzlibrary{arrows,arrows.meta,bending,positioning,patterns,decorations.pathmorphing,calc,shapes.geometric,backgrounds}") # hide
```

For building up this system, it is proceeded as follows:

## Plug and Play Example

```@example
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting

# Define the needed constants
v = 1000.0/2    # Reactor volume
q = 10000.0/24  # Flow rate through the reactor
kO3 = 10.0/24  # Reaction rate for the decay of ozone
kd = 1500.0/24  # Reaction rate for the decay of the bacteria

# Set default process
@set_process disinfection = OzoneDisinfection(;kO3, kd)

# The reactors
@named CSTR1 = CSTR(v, OzoneDisinfection(;kO3=0, kd, name=:disin_supply); initial_states = [0.5, 0]) # Override the default process!
@named CSTR2 = CSTR(v; initial_states = [0.5, 0])

# The influent
@named influent = Influent([0.5, 1], flowrate=q)

# The connections
connection_eqns = [
	# Connect the flows
	connect(outflows(influent)[1], inflows(CSTR1)[1]), # Influent -> CSTR1
	connect(outflows(CSTR1)[1], inflows(CSTR2)[1]), # CSTR1 -> CSTR2
]

# Building the overall system
@named system = ODESystem(connection_eqns, t, systems = [
	influent,
	CSTR1,
	CSTR2,
])
system_simplified = structural_simplify(system)

## Simulate the system (for 1 day)
# First create a ODEProblem for the time span (0, 1) and then solve it
prob = ODEProblem(system_simplified, [], (0, 1), [])
sol = solve(prob)

## Plot the output
plot(sol,
	idxs = [states(CSTR1).X_B ,states(CSTR2).X_B], # Plot bacteria in tank
	legend = :right,
	title = "Remaining fraction of bacteria in each tank")
```

## Step-by-Step Explanations
### Setup

First, the needed packages are imported and constants defined:

```jldoctest ozone_test; output=false
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting

# Define the needed constants
v = 1000.0/2    # Reactor volume
q = 10000.0/24  # Flow rate through the reactor
kO3 = 10.0/24  # Reaction rate for the decay of ozone
kd = 1500.0/24  # Reaction rate for the decay of the bacteria

# output

62.5
```
```@setup ozone
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting

# Define the needed constants
v = 1000.0/2    # Reactor volume
q = 10000.0/24  # Flow rate through the reactor
kO3 = 10.0/24  # Reaction rate for the decay of ozone
kd = 1500.0/24  # Reaction rate for the decay of the bacteria
```
### System Creation

Then, all needed systems (here inflow and the reactors) are created.

We start with setting the default reaction rates system for the disinfection dynamics. This system defines all compounds needed 
for this specific reaction, which are needed afterwards for defining the CSTRs.

```jldoctest ozone_test; output=false
@set_process disinfection = OzoneDisinfection(;kO3, kd)

# output

Process OzoneDisinfection 'disinfection':
States (2): see states(disinfection)
  S_O3(t) [guess is 0.0]: S_O3
  X_B(t) [guess is 0.0]: X_B
Parameters (2): see parameters(disinfection)
  kO3 [defaults to 0.416667]
  kd [defaults to 62.5]
```
```@setup ozone
@set_process disinfection = OzoneDisinfection(;kO3, kd)
```
We continue with the two CSTRs. But wait: The first reactor is supposed to have zero ozone decomposition rate (to simulate the supply, as explained above), but the default ozonation process set above has it fixed to `10/24`! Thus this process must be overridden. This can be done by supplying a new process directly to the reactor and setting `use_default_processes=false`:

```jldoctest ozone_test; output=false
@named CSTR1 = CSTR(v, OzoneDisinfection(;kO3=0, kd, name=:disin_supply); initial_states = [0.5, 0], use_default_processes=false)
@named CSTR2 = CSTR(v; initial_states = [0.5, 0])

# output

CSTR with OzoneDisinfection 'CSTR2':
States (2): see states(CSTR2)
  S_O3(t) [defaults to 0.5]: S_O3
  X_B(t) [defaults to 0.0]: X_B
Inflows (1): see inflows(CSTR2)
  :CSTR2_reactor₊inflow
Outflows (1): see outflows(CSTR2)
  :CSTR2_reactor₊outflow
Parameters (3): see parameters(CSTR2)
  CSTR2_reactor₊V [defaults to 500.0]
  CSTR2_disinfection₊kO3 [defaults to 0.416667]
  CSTR2_disinfection₊kd [defaults to 62.5]
```
```@setup ozone
@named CSTR1 = CSTR(v, OzoneDisinfection(;kO3=0, kd, name=:disin_supply); initial_states = [0.5, 0], use_default_processes=false)
@named CSTR2 = CSTR(v; initial_states = [0.5, 0])
```
Finally, the influent is to be created:
It needs the variables to provide an input for, and the corresponding values. We take the values directly from the
default state, which are:
```jldoctest ozone_test; output=false
default_states()

# output

2-element Vector{Num}:
 S_O3(t)
  X_B(t)
```
```@example ozone
default_states() # hide
```
To this, the value vector has to be aligned and additionally the flowrate supplied. 
The ozone concentration is set to 0.5 to keep this level in the first reactor (Consider the ozone to be supplied before the first reactor)
and the bacteria concentration is set to 1, such that one can interpret the bacteria in the effluent
as the percentage of bacteria that are remaining.

```jldoctest ozone_test; output=false
@named influent = Influent([0.5, 1], flowrate=q)

# output

Influent 'influent':
Outflows (1): see outflows(influent)
  :influent_to_outflowport₊outflow
Parameters (3): see parameters(influent)
  q_const₊k [defaults to 416.667]: Constant output value of block
  S_O3_const₊k [defaults to 0.5]: Constant output value of block
  X_B_const₊k [defaults to 1.0]: Constant output value of block
```
```@setup ozone
@named influent = Influent([0.5, 1], flowrate=q)
```

### Connection

Then, the connection equations for the systems are created. For this, simply the inflows and outflows can be connected as desired:

```jldoctest ozone_test; output=false
connection_eqns = [
	# Connect the flows
	connect(outflows(influent)[1], inflows(CSTR1)[1]), # Influent -> CSTR1
	connect(outflows(CSTR1)[1], inflows(CSTR2)[1]), # CSTR1 -> CSTR2
]

# output

2-element Vector{Equation}:
 connect()
 connect()
```
```@setup ozone
connection_eqns = [
	# Connect the flows
	connect(outflows(influent)[1], inflows(CSTR1)[1]), # Influent -> CSTR1
	connect(outflows(CSTR1)[1], inflows(CSTR2)[1]), # CSTR1 -> CSTR2
]
```

With the connections, we can then build the overall model:

```jldoctest ozone_test; output=false
@named system = ODESystem(connection_eqns, t, systems = [
	influent,
	CSTR1,
	CSTR2,
])

# output

Model system:
Subsystems (3): see hierarchy(system)
  influent
  CSTR1
  CSTR2
Equations (69):
  40 standard: see equations(system)
  29 connecting: see equations(expand_connections(system))
Unknowns (69): see unknowns(system)
  influent₊influent_to_outflowport₊outflow₊q(t): Flow Rate
  influent₊influent_to_outflowport₊outflow₊S_O3(t): S_O3 concentration
  influent₊influent_to_outflowport₊outflow₊X_B(t): X_B concentration
  influent₊influent_to_outflowport₊q₊u(t): Inner variable in RealInput q
  ⋮
Parameters (9): see parameters(system)
  influent₊q_const₊k [defaults to 416.667]: Constant output value of block
  influent₊S_O3_const₊k [defaults to 0.5]: Constant output value of block
  influent₊X_B_const₊k [defaults to 1.0]: Constant output value of block
  CSTR1₊CSTR1_reactor₊V [defaults to 500.0]
  ⋮
```
```@setup ozone
@named system = ODESystem(connection_eqns, t, systems = [
	influent,
	CSTR1,
	CSTR2,
])
```

And simplify it to get the minimum set of equations:

```jldoctest ozone_test; output=false
system_simplified = structural_simplify(system)
full_equations(system_simplified) # Display the equations

# output

4-element Vector{Equation}:
 Differential(t)(CSTR1₊CSTR1_reactor₊states₊S_O3(t)) ~ (-influent₊q_const₊k*CSTR1₊CSTR1_reactor₊states₊S_O3(t)) / CSTR1₊CSTR1_reactor₊V + (influent₊S_O3_const₊k*influent₊q_const₊k) / CSTR1₊CSTR1_reactor₊V - CSTR1₊disin_supply₊kO3*CSTR1₊CSTR1_reactor₊states₊S_O3(t)
 Differential(t)(CSTR1₊CSTR1_reactor₊states₊X_B(t)) ~ (influent₊X_B_const₊k*influent₊q_const₊k) / CSTR1₊CSTR1_reactor₊V + (-influent₊q_const₊k*CSTR1₊CSTR1_reactor₊states₊X_B(t)) / CSTR1₊CSTR1_reactor₊V - CSTR1₊disin_supply₊kd*CSTR1₊CSTR1_reactor₊states₊S_O3(t)*CSTR1₊CSTR1_reactor₊states₊X_B(t)
 Differential(t)(CSTR2₊CSTR2_reactor₊states₊S_O3(t)) ~ (influent₊q_const₊k*CSTR1₊CSTR1_reactor₊states₊S_O3(t)) / CSTR2₊CSTR2_reactor₊V + (-influent₊q_const₊k*CSTR2₊CSTR2_reactor₊states₊S_O3(t)) / CSTR2₊CSTR2_reactor₊V - CSTR2₊CSTR2_disinfection₊kO3*CSTR2₊CSTR2_reactor₊states₊S_O3(t)
 Differential(t)(CSTR2₊CSTR2_reactor₊states₊X_B(t)) ~ (-influent₊q_const₊k*CSTR2₊CSTR2_reactor₊states₊X_B(t)) / CSTR2₊CSTR2_reactor₊V + (influent₊q_const₊k*CSTR1₊CSTR1_reactor₊states₊X_B(t)) / CSTR2₊CSTR2_reactor₊V - CSTR2₊CSTR2_disinfection₊kd*CSTR2₊CSTR2_reactor₊states₊S_O3(t)*CSTR2₊CSTR2_reactor₊states₊X_B(t)
```
```@example ozone
system_simplified = structural_simplify(system) # hide
full_equations(system_simplified) # Display the equations # hide
```

### Simulation and Plotting

Finally, we simulate and plot the result.

```jldoctest ozone_test; output=false
## Simulate the system (for 1 day)
# First create a ODEProblem for the time span (0, 1) and then solve it
prob = ODEProblem(system_simplified, [], (0, 1), [])
sol = solve(prob)

## Plot the output
plot(sol,
	idxs = [CSTR1.states.X_B ,CSTR2.states.X_B], # Plot bacteria in tank
	legend = :right,
	title = "Remaining fraction of bacteria in each tank")

# output

Plot{Plots.GRBackend() n=2}
```
```@example ozone
## Simulate the system (for 1 day) # hide
# First create a ODEProblem and then solve it # hide
prob = ODEProblem(system_simplified, [], (0, 1), []) # hide
sol = solve(prob) # hide
 # hide
## Plot the ouput # hide
plot(sol, # hide
	idxs = [CSTR1.states.X_B ,CSTR2.states.X_B], # Plot bacteria in tank # hide
	legend = :right, # hide
	title = "Remaining fraction of bacteria in each tank") # hide
```

