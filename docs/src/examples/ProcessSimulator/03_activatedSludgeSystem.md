# Activated Sludge System with ASM1

In this example, a small activated sludge system with the activated sludge system nr. 1 (ASM1) will be built. An activated sludge system is a simple setup with a single CSTR:

```@example
using TikzPictures # hide
TikzPicture(L""" %# hide
       \tikzstyle{block} = [draw, rectangle, inner sep=20pt, align=center] %# hide
       \tikzstyle{clarifier} = [draw, regular polygon, regular polygon sides=3, shape border rotate=180, align=center] %# hide
       \node (inf) {Influent}; %# hide
       \node[block, right=2cm of inf] (cstr) {CSTR with ASM1}; %# hide
       \node[clarifier, right=of cstr] (clar) {Ideal Settler}; %# hide
       \node[right=of clar] (eff) {Effluent}; %# hide
       \draw [->] %# hide
       let %# hide
           \p1 = (clar.south), %# hide
           \p2 = ($(inf.east)!0.5!(cstr.west)!0.5!(clar.south)$), %# hide
       in %# hide
           (\x2, \y1 - .5cm) -- node [pos=1, anchor=north] (was) {Wastage} (\x2, \y1 -1cm); %# hide
 %# hide
       \draw[->] (inf) -- (cstr) node[pos=0.5]{$q_{in},C_{in}$}; %# hide
       \draw[->] (cstr) -- (clar) node[pos=0.5]{$q_r,C_r$}; %# hide
       \draw[->] (clar) -- (eff) node[pos=0.5]{$q_{e},C_{e}$}; %# hide
       \draw %# hide
       let %# hide
           \p1 = (clar.south), %# hide
           \p2 = ($(inf.east)!0.5!(cstr.west)$), %# hide
       in %# hide
	   (\p1) -- node[pos=0, anchor=north west]{$q_{s}, C_{s}$} (\x1, \y1 - .5cm) -| (\p2); %# hide
       """; options=">=stealth, auto, node distance=2cm, scale=2, background rectangle/.style={fill=white}, show background rectangle", preamble=raw"\usetikzlibrary{arrows,arrows.meta,bending,positioning,patterns,decorations.pathmorphing,calc,shapes.geometric, backgrounds}") # hide
```

For building and simulating this in `BioChemicalTreatment.jl` it has to be noted first, that this actually needs a series of systems, some of which are invisible in the plot above.
For example the unifying of the inflow and return sludge in the beginning needs an own system, and the splitting of the wastage stream needs even two: A [`FlowSeparator`](@ref) to split the flow and then a [`FlowPump`](@ref) to specify the flow rate for the split. Introducing all of those yields a new diagram:

```@example
using TikzPictures # hide
TikzPicture(L""" %# hide
       \tikzstyle{block} = [draw, rectangle, inner sep=20pt, align=center] %# hide
       \tikzstyle{clarifier} = [draw, regular polygon, regular polygon sides=3, shape border rotate=180, align=center] %# hide
       \tikzstyle{pump} = [draw, dashed, circle, align=center] %# hide
       \node[block, dashed] (inf) {Influent}; %# hide
       \node[block, dashed, right=2cm of inf] (unif) {Unifier}; %# hide
       \node[block, right=2cm of unif] (cstr) {CSTR with ASM1}; %# hide
       \node[clarifier, right=of cstr] (clar) {Ideal Settler}; %# hide
       \node[right=of clar] (eff) {Effluent}; %# hide
       \node[pump, below=.5cm of clar] (clarpump) {Settler Pump}; %# hide
       \node[block, dashed, left=of clarpump] (wastsplit) {Wastage\\Split}; %# hide
       \node[pump, below=.5cm of wastsplit] (wastpump) {Wastage\\Pump}; %# hide
 %# hide
       \draw[->] (inf) -- (unif) node[pos=0.5]{$q_{in},C_{in}$}; %# hide
       \draw[->] (unif) -- (cstr) node[pos=0.5]{$q_{in},C_{in}$}; %# hide
       \draw[->] (cstr) -- (clar) node[pos=0.5]{$q_r,C_r$}; %# hide
       \draw[->] (clar) -- (eff) node[pos=0.5]{$q_{e},C_{e}$}; %# hide
       \draw[->] (clar) -- (clarpump); %# hide
       \draw[->] (clarpump) -- (wastsplit) node[pos=0, anchor=south]{$q_{s}, C_{s}$}; %# hide
       \draw[->] (wastsplit) -| (unif); %# hide
       \draw[->] (wastsplit) -- (wastpump); %# hide
       \draw %# hide
       let %# hide
           \p1 = (wastpump.south), %# hide
       in %# hide
	   (\p1) --  (\x1, \y1 - .5cm) node[pos=1, anchor=north]{Wastage}; %# hide
       """; options=">=stealth, auto, node distance=2cm, scale=2, background rectangle/.style={fill=white}, show background rectangle", preamble=raw"\usetikzlibrary{arrows,arrows.meta,bending,positioning,patterns,decorations.pathmorphing,calc,shapes.geometric, backgrounds}") # hide
```

Now with all these required systems, the building of the model can be started.

## Plug and Play Example

```@example
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting

# Keeping track
sys = []
eqns = Equation[]

# Default Process
@set_process asm1 = Process("ASM1")

# The reactor
@named cstr = CSTR(1000; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # 1000 m^3 is the assumed volume, we set the initial states to all 1 as the equations do not work with the default (all 0)
push!(sys, cstr)

# The other systems
@named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], flowrate = 18446) # This is the stabilization influent from BSM1
push!(sys, influent)

# The unifier with two inflows
@named unifier = FlowUnifier()
push!(sys, unifier)

# The ideal settler
@named settler = IdealClarifier()
push!(sys, settler)

# The settler pump
@named settler_pump = FlowPump(; flowrate=1000) # Flowrate 1000m^3/d
push!(sys, settler_pump)

# The wastage split into 2 flows
@named wastage_split = FlowSeparator()
push!(sys, wastage_split)

# The wastage pump
@named wastage_pump = FlowPump(; flowrate=250) # Flowrate 250 m^3/d wasted
push!(sys, wastage_pump)

# Connect the flows
append!(eqns, [
	# Influent -> unifier
	connect(outflows(influent, 1), inflows(unifier, 1)),
	# Unifier -> cstr
	connect(outflows(unifier, 1), inflows(cstr, 1)),
	# CSTR -> settler
	connect(outflows(cstr, 1), inflows(settler, 1)),
	# Settler -> settler pump
	connect(outflows(settler, :sludge), inflows(settler_pump, 1)), # Use the named access to get the sludge outflow of the settler, is more clear
	# Settler pump -> wastage split
	connect(outflows(settler_pump, 1), inflows(wastage_split, 1)),
	# wastage split back -> unifier
	connect(outflows(wastage_split, 1), inflows(unifier, 2)), # Note the 2 for the unifier: We do not want to connect the same inflow twice!
	# Wastage split -> wastage pump
	connect(outflows(wastage_split, 2), inflows(wastage_pump, 1)), # Also here the 2 for the split: We take the other outflow not going to the unifier!
])

# Assembly of the full system
@named model = ODESystem(eqns, t, systems = sys)
model_simplified = structural_simplify(model)

# Simulation and plotting
## Simulate the system (for 1 day)
# First create a ODEProblem and then solve it
prob = ODEProblem(model_simplified, [], (0, 1); initializealg=NoInit()) # No initialization scheme
sol = solve(prob)

## Plot the ouput, only the effluent of the settler
plot(
	sol, 
	idxs=getproperty.(Ref(outflows(settler, :effluent)), propertynames(outflows(settler, :effluent))[2:end-1]), # settler effluent
	legend=:topright,
	title = "Activated Sludge System Effluent"
	)
```

## Step-by-Step Explanations
### Setup

First, the needed packages are imported and constants defined:

```jldoctest activated_sludge_test; output=false
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting

# output

```
```@setup activated_sludge
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting
```

Further, to allow for easy bookkeeping, empty vectors for the systems and all equations are created. Those are not neccessary to make here, but in the end for building the model those lists are required. Thus it is easier to keep these vectors growing as the systems and equations are defined which helps not to forget about some of them.

```jldoctest activated_sludge_test; output=false
sys = ODESystem[]
eqns = Equation[]

# output

Equation[]
```
```@setup activated_sludge
sys = ODESystem[]
eqns = Equation[]
```

### System Creation

Then, all needed systems are created, including the "hidden" systems, which are usually not shown in a flow diagram.
The second flow diagram above shows all of them, the "hidden" ones using a dashed line.

As usual, first the main process for the model is set as default, as this defines the tracked components in the flow, which can then be used automatically.
In this example, the `ASM1` model defined as matrix is used to show how one of the matrix-defined models can be loaded. This is done using:

```jldoctest activated_sludge_test; output=false
@set_process asm1 = Process("ASM1")

# output

Process ASM1 'asm1':
States (14): see states(asm1)
  S_Alk(t) [guess is 0.0]: S_Alk
  S_B(t) [guess is 0.0]: S_B
  S_BN(t) [guess is 0.0]: S_BN
  S_N2(t) [guess is 0.0]: S_N2
  S_NHx(t) [guess is 0.0]: S_NHx
  S_NOx(t) [guess is 0.0]: S_NOx
  S_O2(t) [guess is 0.0]: S_O2
  S_U(t) [guess is 0.0]: S_U
  ⋮
Parameters (35): see parameters(asm1)
  K_NHxOHO [defaults to 0.05]
  m_ANOMax [defaults to 0.8]
  m_OHOMax [defaults to 6]
  K_O2ANO [defaults to 0.4]
  K_XCBhyd [defaults to 0.03]
  COD_P [defaults to 40]
  COD_C [defaults to 32]
  Y_ANO [defaults to 0.24]
  ⋮
```
```@setup activated_sludge
@set_process asm1 = Process("ASM1")
```

Hereby, the default parameters are used. If others are desired, one could provide them directly as keyword arguments to this function or they could as well be overwritten later (for overriding the parameters, see [this section](@ref "Adaptation of parameters")). 

!!! note
	This way of loading matrix-defined models is equal for all included in the library.

	Try it out by using `ASM3` instead of `ASM1` to work with the `ASM3` instead. 
	In this case, it is as well needed to adapt the influent to have values for the correct states.

Continuing, the reactor is created. As in the other examples, this one needs to know its volume and the states. It is then directly added to the system vector.

```jldoctest activated_sludge_test
@named cstr = CSTR(1000; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # 1000 m^3 is the assumed volume, we set the initial states to all 1 as the equations do not work with the default (all 0)
push!(sys, cstr)
nameof.(sys) # Show all added systems

# output

1-element Vector{Symbol}:
 :cstr
```
```@setup activated_sludge
@named cstr = CSTR(1000; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # 1000 m^3 is the assumed volume, we set the initial states to all 1 as the equations do not work with the default (all 0)
push!(sys, cstr)
```

Afterwards, all other systems can be created and added to the vector of systems. A constant influent is assumed (see the [`BSM1`](@ref) example for a time varying influent) as well as some default values for the pumped flows.

```jldoctest activated_sludge_test
# First the inflow
@named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], flowrate = 18446) # This is the stabilization influent from BSM1
push!(sys, influent)

# The unifier with two inflows
@named unifier = FlowUnifier() # defaults to two inflows, otherwise add more using `n_in`
push!(sys, unifier)

# The ideal settler
@named settler = IdealClarifier()
push!(sys, settler)

# The settler pump
@named settler_pump = FlowPump(; flowrate=1000) # Flowrate 1000m^3/d
push!(sys, settler_pump)

# The wastage split into 2 flows
@named wastage_split = FlowSeparator() # Defaults to two outflows
push!(sys, wastage_split)

# The wastage pump
@named wastage_pump = FlowPump(; flowrate=250) # Flowrate 250 m^3/d wasted
push!(sys, wastage_pump)
nameof.(sys) # Show all added systems

# output

7-element Vector{Symbol}:
 :cstr
 :influent
 :unifier
 :settler
 :settler_pump
 :wastage_split
 :wastage_pump
```
```@setup activated_sludge
# First the inflow
@named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], flowrate = 18446) # This is the stabilization influent from BSM1
push!(sys, influent)

# The unifier with two inflows
@named unifier = FlowUnifier()
push!(sys, unifier)

# The ideal settler
@named settler = IdealClarifier()
push!(sys, settler)

# The settler pump
@named settler_pump = FlowPump(; flowrate=1000) # Flowrate 1000m^3/d
push!(sys, settler_pump)

# The wastage split into 2 flows
@named wastage_split = FlowSeparator()
push!(sys, wastage_split)

# The wastage pump
@named wastage_pump = FlowPump(; flowrate=250) # Flowrate 250 m^3/d wasted
push!(sys, wastage_pump)
```

Now, all systems are created, and the flows can be connected (The other connections for the process to the reactor have already been added)

### Connection of the Flows

Then, the connection equations for the systems are created and added to the vector.

```jldoctest activated_sludge_test; output=false
append!(eqns, [
	# Influent to the unifier
	connect(outflows(influent, 1), inflows(unifier, 1)),
	# Unifier to the cstr
	connect(outflows(unifier, 1), inflows(cstr, 1)),
	# CSTR to the settler
	connect(outflows(cstr, 1), inflows(settler, 1)),
	# Settler to the settler pump
	connect(outflows(settler, :sludge), inflows(settler_pump, 1)), # Use the named access to get the sludge outflow of the settler, is more clear
	# Settler pump to wastage split
	connect(outflows(settler_pump, 1), inflows(wastage_split, 1)),
	# wastage split back to the unifier
	connect(outflows(wastage_split, 1), inflows(unifier, 2)), # Note the 2 for the unifier: We do not want to connect the same inflow twice!
	# Wastage split to wastage pump
	connect(outflows(wastage_split, 2), inflows(wastage_pump, 1)), # Also here the 2 for the split: We take the other outflow not going to the unifier!
])

# output

7-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
```
```@setup activated_sludge
append!(eqns, [
	# Influent to the unifier
	connect(outflows(influent, 1), inflows(unifier, 1)),
	# Unifier to the cstr
	connect(outflows(unifier, 1), inflows(cstr, 1)),
	# CSTR to the settler
	connect(outflows(cstr, 1), inflows(settler, 1)),
	# Settler to the settler pump
	connect(outflows(settler, :sludge), inflows(settler_pump, 1)), # Use the named access to get the sludge outflow of the settler, is more clear
	# Settler pump to wastage split
	connect(outflows(settler_pump, 1), inflows(wastage_split, 1)),
	# wastage split back to the unifier
	connect(outflows(wastage_split, 1), inflows(unifier, 2)), # Note the 2 for the unifier: We do not want to connect the same inflow twice!
	# Wastage split to wastage pump
	connect(outflows(wastage_split, 2), inflows(wastage_pump, 1)), # Also here the 2 for the split: We take the other outflow not going to the unifier!
])
```

!!! details "The `inflows`and `outflows` functions"
	The `inflows` and `outflows` functions, when applied without any other argument, return a vector of the inflow and outflow connectors. 
	However, also a second argument can be given:
	- **`Integer`**: Only the port at this position is returned. This is used e.g. for the first connection of the influent to the unifier
	- **`Symbol`**(starting with `:`): Only the port with the name according to the symbol is returned. This is used for the connection from the settler to the pump

	Depending on the situation, one or the other is more convenient, but both can be used for every case. For some, the name is just not too helpful and then the numeric variant is easier to read, but for other systems the name might be helpful.
	For example, for getting the second outflow from the wastage separator, the following expressions are equivalent:

	```jldoctest activated_sludge_test
	julia> outflows(wastage_split)[2]
	Model wastage_split₊outflow_2:
	Equations (15):
	  15 connecting: see equations(expand_connections(wastage_split₊outflow_2))
	Unknowns (15): see unknowns(wastage_split₊outflow_2)
	  q(t): Flow Rate
	  S_Alk(t): S_Alk concentration
	  S_B(t): S_B concentration
	  S_BN(t): S_BN concentration
	  ⋮

	julia> outflows(wastage_split, 2)
	Model wastage_split₊outflow_2:
	Equations (15):
	  15 connecting: see equations(expand_connections(wastage_split₊outflow_2))
	Unknowns (15): see unknowns(wastage_split₊outflow_2)
	  q(t): Flow Rate
	  S_Alk(t): S_Alk concentration
	  S_B(t): S_B concentration
	  S_BN(t): S_BN concentration
	  ⋮

	julia> outflows(wastage_split, :outflow_2)
	Model wastage_split₊outflow_2:
	Equations (15):
	  15 connecting: see equations(expand_connections(wastage_split₊outflow_2))
	Unknowns (15): see unknowns(wastage_split₊outflow_2)
	  q(t): Flow Rate
	  S_Alk(t): S_Alk concentration
	  S_B(t): S_B concentration
	  S_BN(t): S_BN concentration
	  ⋮
	```

	Here, the named access seems overly verbose. However, for getting the sludge outflow from the settler, the same three variants are:

	```jldoctest activated_sludge_test
	julia> outflows(settler)[2]
	Model settler₊sludge:
	Equations (15):
	  15 connecting: see equations(expand_connections(settler₊sludge))
	Unknowns (15): see unknowns(settler₊sludge)
	  q(t): Flow Rate
	  S_Alk(t): S_Alk concentration
	  S_B(t): S_B concentration
	  S_BN(t): S_BN concentration
	  ⋮

	julia> outflows(settler, 2)
	Model settler₊sludge:
	Equations (15):
	  15 connecting: see equations(expand_connections(settler₊sludge))
	Unknowns (15): see unknowns(settler₊sludge)
	  q(t): Flow Rate
	  S_Alk(t): S_Alk concentration
	  S_B(t): S_B concentration
	  S_BN(t): S_BN concentration
	  ⋮

	julia> outflows(settler, :sludge)
	Model settler₊sludge:
	Equations (15):
	  15 connecting: see equations(expand_connections(settler₊sludge))
	Unknowns (15): see unknowns(settler₊sludge)
	  q(t): Flow Rate
	  S_Alk(t): S_Alk concentration
	  S_B(t): S_B concentration
	  S_BN(t): S_BN concentration
	  ⋮

	```

	Here instead, the variant with the name seems more clear than the number.

Now, with all systems and connection equations ready, the overall model can be build:

```jldoctest activated_sludge_test; output=false
@named model = ODESystem(eqns, t, systems = sys)

# output

Model model:
Subsystems (7): see hierarchy(model)
  cstr
  influent
  unifier
  settler
  ⋮
Equations (334):
  156 standard: see equations(model)
  178 connecting: see equations(expand_connections(model))
Unknowns (334): see unknowns(model)
  cstr₊cstr_asm1₊g_hO2(t): Aerobic growth of heterotrophs
  cstr₊cstr_asm1₊g_hAn(t): Anoxic growth of heterotrophs
  cstr₊cstr_asm1₊g_aO2(t): Aerobic growth of autotrophs
  cstr₊cstr_asm1₊d_h(t): Decay of heterotrophs
  ⋮
Parameters (54): see parameters(model)
  cstr₊cstr_asm1₊K_NHxOHO [defaults to 0.05]
  cstr₊cstr_asm1₊m_ANOMax [defaults to 0.8]
  cstr₊cstr_asm1₊m_OHOMax [defaults to 6]
  cstr₊cstr_asm1₊K_O2ANO [defaults to 0.4]
  ⋮
```
```@setup activated_sludge
@named model = ODESystem(eqns, t, systems = sys)
```

And simplify it to get the minimum set of equations:

```jldoctest activated_sludge_test; output=false
model_simplified = structural_simplify(model)

# output

Model model:
Equations (56):
  56 standard: see equations(model)
Unknowns (56): see unknowns(model)
  cstr₊cstr_reactor₊states₊S_Alk(t) [defaults to 1.0]: S_Alk
  cstr₊cstr_reactor₊states₊S_B(t) [defaults to 1.0]: S_B
  cstr₊cstr_reactor₊states₊S_BN(t) [defaults to 1.0]: S_BN
  cstr₊cstr_reactor₊states₊S_N2(t) [defaults to 1.0]: S_N2
  ⋮
Parameters (54): see parameters(model)
  cstr₊cstr_asm1₊q_am [defaults to 0.08]
  cstr₊cstr_asm1₊m_ANOMax [defaults to 0.8]
  influent₊X_OHO_const₊k [defaults to 28.17]: Constant output value of block
  cstr₊cstr_asm1₊f_XUBiolys [defaults to 0.08]
  ⋮
Observed (278): see observed(model)
```
```@setup activated_sludge
model_simplified = structural_simplify(model)
```

### Simulation and Plotting

Finally, we simulate and plot the result.

```@example activated_sludge
## Simulate the system (for 1 day)
# First create a ODEProblem and then solve it
prob = ODEProblem(model_simplified, [], (0, 1); initializealg=NoInit()) # No initialization scheme for the DAE. See note below
sol = solve(prob)

## Plot the ouput, only the effluent of the settler
plot(
	sol, 
	idxs=getproperty.(Ref(outflows(settler, :effluent)), propertynames(outflows(settler, :effluent))[2:end-1]),
	legend=:topright,
	title = "Activated Sludge System Effluent"
	)
```

!!! note "About the `initializealg` argument to the problem"
	For the solution of the system, the algorithm generally needs initial conditions for all states. Due to the model transformations (the call to `structural_simplify`), the initial conditions needed might be different from the ones of the original problem. That's where the initialization algorithm jumps in and computes these newly needed initial conditions.  
	
	This initialization algorithm is set using the `initializealg` keyword argument when building the `ODEProblem`. Sometimes, this argument might not be neccessary (mostly for systems without any recirculation), but for others it will be needed. There are multiple options for this algorithm, see [https://docs.sciml.ai/DiffEqDocs/stable/solvers/dae_solve/#Initialization-Schemes] for reference.

