# Reactor with multiple Processes: ASM1 with Aeration

This example shows the current procedure of how multiple processes are combined in a single reactor. For this, we use an examplary system of a single aerated reactor with `ASM1` and constant input:

```@example
using TikzPictures # hide
TikzPicture(L""" %# hide
       \tikzstyle{block} = [draw, rectangle, inner sep=20pt, align=center] %# hide
       \tikzstyle{clarifier} = [draw, regular polygon, regular polygon sides=3, shape border rotate=180, align=center] %# hide
       \node (inf) {Influent}; %# hide
       \node[block, right=2cm of inf] (cstr) {Aerated CSTR with ASM1}; %# hide
       \node[right=of cstr] (eff) {Effluent}; %# hide
  %# hide
       \draw[->] (inf) -- (cstr) node[pos=0.5]{$q_{in},C_{in}$}; %# hide
       \draw[->] (cstr) -- (eff) node[pos=0.5]{$q_{eff},C_{eff}$}; %# hide
       """; options=">=stealth, auto, node distance=2cm, scale=2, background rectangle/.style={fill=white}, show background rectangle", preamble=raw"\usetikzlibrary{arrows,arrows.meta,bending,positioning,patterns,decorations.pathmorphing,calc,shapes.geometric,backgrounds}") # hide
```

Now, let's see how this can be accomplished in three different cases using the high level interface. If you are interested in the inner workings, refer to [the same example using the low-level interface](@ref "Low-Level Reactor with multiple Processes: ASM1 with Aeration").

The three presented cases are:

1. [Combining Variables of the Same Name](@ref)
2. [Adding a Process to the Default One](@ref)
3. [Combining Variables with Different Names](@ref)

## Combining Variables of the Same Name

The first example of multiple processes, is concerning the case where two variables have the same name.
To do so, two processes are given to the reactor and below it is shown how this is done.

### Plug and Play Example

```@example
using BioChemicalTreatment # This Package :)
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more
using DifferentialEquations # Solve the equations
using Plots # Plotting
clear_default_processes() # hide
clear_default_state_mapping() # hide

# Processes
@named asm = ASM1() # Use the default parameters
@named aer = Aeration() # The aeration process

# Reactor
@named react = CSTR(1000, [asm, aer]; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)

# Influent and aeration input
@named influent = Influent([7.0, 30.0, 6.95, 31.56, 0.0, 0.0, 69.5, 0.0, 28.17, 51.2, 10.59, 0.0, 202.32], unknowns(states(asm)), flowrate = 18446) # BSM1 Stabilization
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)

# Connection
eqs = [
  connect(outflows(influent, 1), inflows(react, 1))
  connect(exogenous_inputs(react, :aer₊k_La), aeration_ctrl.output)
]

# Model generation
@named model = ODESystem(eqs, t, systems=[influent, react, aeration_ctrl])
model_simplified = structural_simplify(model)

# Simulation
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)

# Plotting, only oxygen
plot(sol, idxs=[inflows(react, 1).S_O, states(react).S_O], label=["Inflow" "Reactor"], title="Oxygen concentration in CSTR with ASM1 and aeration", legend=:topright)

# Plotting the rates
plot(sol, idxs=[rates(react).S_O, subsystems(react, :asm).rates.S_O, subsystems(react, :aer).rates.S_O], label=["Combined" "ASM" "Aeration"], title="Oxygen rate in CSTR with ASM1 and aeration", legend=:topright)
```

### Step-by-Step Explanations

As always, the first step is the setup in loading the packages:

```jldoctest same_name_test; output=false
using BioChemicalTreatment # This Package :)
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more
using DifferentialEquations # Solve the equations
using Plots # Plotting

# output

```
```@setup same_name
using BioChemicalTreatment # This Package :) # hide
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations # hide
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more # hide
using DifferentialEquations # Solve the equations # hide
using Plots # Plotting # hide
clear_default_processes() # hide
clear_default_state_mapping() # hide
```

And then, the processes (in this case two of them) are created:

```jldoctest same_name_test; output = false, setup = :(clear_default_processes() && clear_default_state_mapping())
@named asm = ASM1() # Use the default parameters
@named aer = Aeration() # The aeration process

# output

Process Aeration 'aer':
States (1): see states(aer)
  S_O(t) [guess is 0.0]: S_O
Exogenous Inputs (1): see exogenous_inputs(aer)
  :k_La
Parameters (1): see parameters(aer)
  S_O_max [defaults to 8]
```
```@setup same_name
@named asm = ASM1() # Use the matrix-defined version with default parameters # hide
@named aer = Aeration() # The aeration process # hide
```

Looking at the states of the two processes, it is visible that both have a state `S_O`, which is the one to be combined:

```@example same_name
unknowns(states(asm))' # Transpose for better visibility
```
```@example same_name
unknowns(states(aer))' # Transpose for better visibility
```

(For confirmation, that these are both oxygen and not only equally named variables,see the respective documentation). In this case, generating the reactor with both processes is very easy: 

```jldoctest same_name_test; output = false
@named react = CSTR(1000, [asm, aer]; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)

# output

CSTR with ASM1 and Aeration 'react':
States (13): see states(react)
  S_ALK(t) [defaults to 1.0]: S_ALK
  S_I(t) [defaults to 1.0]: S_I
  S_ND(t) [defaults to 1.0]: S_ND
  S_NH(t) [defaults to 1.0]: S_NH
  ⋮
Inflows (1): see inflows(react)
  :react_reactor₊inflow
Outflows (1): see outflows(react)
  :react_reactor₊outflow
Exogenous Inputs (1): see exogenous_inputs(react)
  :aer₊k_La
Parameters (21): see parameters(react)
  react_reactor₊V [defaults to 1000]
  asm₊Y_H [defaults to 0.67]
  asm₊Y_A [defaults to 0.24]
  asm₊i_XB [defaults to 0.086]
  ⋮
```
```@example same_name
@named react = CSTR(1000, [asm, aer]; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself) # hide
```

Hereby, the equally named states are automatically connected and the rates of both processes added. 

Then, the influent and the input for the aeration (the strength of aeration) are created:

```jldoctest same_name_test; output = false
@named influent = Influent([7.0, 30.0, 6.95, 31.56, 0.0, 0.0, 69.5, 0.0, 28.17, 51.2, 10.59, 0.0, 202.32], unknowns(states(asm)), flowrate = 18446) # BSM1 Stabilization
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)

# output

Model aeration_ctrl:
Subsystems (1): see hierarchy(aeration_ctrl)
  output
Equations (1):
  1 standard: see equations(aeration_ctrl)
Unknowns (1): see unknowns(aeration_ctrl)
  output₊u(t): Inner variable in RealOutput output
Parameters (1): see parameters(aeration_ctrl)
  k [defaults to 240]: Constant output value of block
```
```@setup same_name
@named influent = Influent([7.0, 30.0, 6.95, 31.56, 0.0, 0.0, 69.5, 0.0, 28.17, 51.2, 10.59, 0.0, 202.32], unknowns(states(asm)), flowrate = 18446) # hide
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240) # hide
```

And everything is connected:

```jldoctest same_name_test; output = false
eqs = [
  connect(outflows(influent, 1), inflows(react, 1))
  connect(exogenous_inputs(react, :aer₊k_La), aeration_ctrl.output)
]

# output

2-element Vector{Equation}:
 connect(influent.influent_to_outflowport.outflow, react.react_reactor.inflow)
 connect(react.aer.k_La, aeration_ctrl.output)
```
```@example same_name
eqs = [ # hide
  connect(outflows(influent, 1), inflows(react, 1)) # hide
  connect(exogenous_inputs(react, :aer₊k_La), aeration_ctrl.output) # hide
] # hide
```

And finally, the system can be generated, simulated and plotted:

```@example same_name
# Model generation
@named model = ODESystem(eqs, t, systems=[influent, react, aeration_ctrl])
model_simplified = structural_simplify(model)

# Simulation
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)

# Plotting, only oxygen
plot(sol, idxs=[inflows(react, 1).S_O, states(react).S_O], label=["Inflow" "Reactor"], title="Oxygen concentration in CSTR with ASM1 and aeration", legend=:topright)
```

And indeed, the oxygen concentration is not only consumed, but also added by the aeration. Thus both processes are active and it is even possible to look at the individual rates:

```@example same_name
plot(sol, idxs=[rates(react).S_O, subsystems(react, :asm).rates.S_O, subsystems(react, :aer).rates.S_O], label=["Combined" "ASM" "Aeration"], title="Oxygen rate in CSTR with ASM1 and aeration", legend=:topright)
```

## Adding a Process to the Default One

The second example of multiple processes, is concerning the case, where a process is to be added to the default ones.
This is useful e.g. if a system is built where some of the reactors are aerated, but the others not. Then, the base process
can be set as default and then the aeration process can be added to the individual reactors.

### Plug and Play Example

```@example
using BioChemicalTreatment # This Package :)
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more
using DifferentialEquations # Solve the equations
using Plots # Plotting
clear_default_processes() # hide
clear_default_state_mapping() # hide

# Default process: asm
@set_process asm = ASM1() # Use the default parameters

# Additional process
@named aer = Aeration() # The aeration process

# Reactor with the additional process
@named react = CSTR(1000, aer; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)

# Influent and aeration input
@named influent = Influent([7.0, 30.0, 6.95, 31.56, 0.0, 0.0, 69.5, 0.0, 28.17, 51.2, 10.59, 0.0, 202.32], flowrate = 18446) # BSM1 Stabilization
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)

# Connection
eqs = [
  connect(outflows(influent, 1), inflows(react, 1))
  connect(exogenous_inputs(react, :aer₊k_La), aeration_ctrl.output)
]

# Model generation
@named model = ODESystem(eqs, t, systems=[influent, react, aeration_ctrl])
model_simplified = structural_simplify(model)

# Simulation
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)

# Plotting, only oxygen
plot(sol, idxs=[inflows(react, 1).S_O, states(react).S_O], label=["Inflow" "Reactor"], title="Oxygen concentration in CSTR with ASM1 and aeration", legend=:topright)

# Plotting the rates
plot(sol, idxs=[rates(react).S_O, subsystems(react, :react_asm).rates.S_O, subsystems(react, :aer).rates.S_O], label=["Combined" "ASM" "Aeration"], title="Oxygen rate in CSTR with ASM1 and aeration", legend=:topright)
```

### Step-by-Step Explanations

As always, the first step is the setup in loading the packages:

```jldoctest add_to_default_test; output=false
using BioChemicalTreatment # This Package :)
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more
using DifferentialEquations # Solve the equations
using Plots # Plotting

# output

```
```@setup add_to_default
using BioChemicalTreatment # This Package :) # hide
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations # hide
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more # hide
using DifferentialEquations # Solve the equations # hide
using Plots # Plotting # hide
clear_default_processes() # hide
clear_default_state_mapping() # hide
```

And then, the asm is set as default process:

```jldoctest add_to_default_test; output = false, setup = :(clear_default_processes() && clear_default_state_mapping())
@set_process asm = ASM1() # Use the default parameters

# output

Process ASM1 'asm':
States (13): see states(asm)
  S_ALK(t) [guess is 0.0]: S_ALK
  S_I(t) [guess is 0.0]: S_I
  S_ND(t) [guess is 0.0]: S_ND
  S_NH(t) [guess is 0.0]: S_NH
  S_NO(t) [guess is 0.0]: S_NO
  S_O(t) [guess is 0.0]: S_O
  S_S(t) [guess is 0.0]: S_S
  X_BA(t) [guess is 0.0]: X_BA
  ⋮
Parameters (19): see parameters(asm)
  Y_H [defaults to 0.67]
  Y_A [defaults to 0.24]
  i_XB [defaults to 0.086]
  i_XP [defaults to 0.01]
  f_P [defaults to 0.08]
  mu_H [defaults to 6.0]
  K_S [defaults to 20.0]
  K_OH [defaults to 0.2]
  ⋮
```
```@setup add_to_default
@set_process asm = ASM1() # Use the default parameters
```

Then, the reactor with the additional aeration process is created. For this, just the aeration process 
is created and given to the reactor upon creation.

```jldoctest add_to_default_test; output = false
@named aer = Aeration() # The aeration process

# Reactor with additional aeration
@named react = CSTR(1000, aer; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)

# output

CSTR with Aeration and ASM1 'react':
States (13): see states(react)
  S_ALK(t) [defaults to 1.0]: S_ALK
  S_I(t) [defaults to 1.0]: S_I
  S_ND(t) [defaults to 1.0]: S_ND
  S_NH(t) [defaults to 1.0]: S_NH
  ⋮
Inflows (1): see inflows(react)
  :react_reactor₊inflow
Outflows (1): see outflows(react)
  :react_reactor₊outflow
Exogenous Inputs (1): see exogenous_inputs(react)
  :aer₊k_La
Parameters (21): see parameters(react)
  react_reactor₊V [defaults to 1000]
  aer₊S_O_max [defaults to 8]
  react_asm₊Y_H [defaults to 0.67]
  react_asm₊Y_A [defaults to 0.24]
  ⋮
```
```@setup add_to_default
@named aer = Aeration() # The aeration process # hide
# hide
# Reactor with additional aeration # hide
@named react = CSTR(1000, aer; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself) # hide
```

Hereby, the default process automatically added, the equally named states are connected and the rates of both processes added. 

!!! note
    If the addition of the default process is not wanted, supply `use_default_processes=false` to the reactor call.

    If the new process is to be added to all subsequent reactors, it can be added to the defaults using

    ```julia
    @add_process aer = Aeration()
    ```

    Note the difference between `@add_process`, which adds a process and `@set_process` which removes all other processes.

    Further note that it is not easily possible to remove default processes, thus it is recommended to use the defaults only
    for the processes that are really the base processes occurring in every reactor. These should as well define all the components
    in the flow.

Then, it is proceeded exactly as in [the example above](@ref "Combining Variables of the Same Name"):

```@example add_to_default
# Influent and aeration input
@named influent = Influent([7.0, 30.0, 6.95, 31.56, 0.0, 0.0, 69.5, 0.0, 28.17, 51.2, 10.59, 0.0, 202.32], flowrate = 18446) # BSM1 Stabilization
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)

# Connection
eqs = [
  connect(outflows(influent, 1), inflows(react, 1))
  connect(exogenous_inputs(react, :aer₊k_La), aeration_ctrl.output)
]

# Model generation
@named model = ODESystem(eqs, t, systems=[influent, react, aeration_ctrl])
model_simplified = structural_simplify(model)

# Simulation
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)

# Plotting, only oxygen
plot(sol, idxs=[inflows(react, 1).S_O, states(react).S_O], label=["Inflow" "Reactor"], title="Oxygen concentration in CSTR with ASM1 and aeration", legend=:topright)
```

And the plot of the rates, note that now the `asm` subsystem of the reactor has a different name, i.e. is prepended by the name of the reactor:

```@example add_to_default
# Plotting the rates
plot(sol, idxs=[rates(react).S_O, subsystems(react, :react_asm).rates.S_O, subsystems(react, :aer).rates.S_O], label=["Combined" "ASM" "Aeration"], title="Oxygen rate in CSTR with ASM1 and aeration", legend=:topright)
```

## Combining Variables with Different Names

The third example of multiple processes, is concerning the case where the variables to be combined do not have the same names.
This can occur if different model naming conventions are used, e.g. `S_O` and `S_O2` are both commonly used for the dissolved
oxygen concentration.

As seen in the two examples above, the aeration model uses `S_O` and the directly defined ASM1 (called using `ASM1()`) as well.
However, the matrix-defined version of the ASM1 (called using `Process("ASM1")`) is using `S_O2` instead (as the ASM3 does).
In the example below it is shown how this case can be handled using the default processes. If it is rather desired without the defaults,
the same as in the first example could be done.

### Plug and Play Example

```@example
using BioChemicalTreatment # This Package :)
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more
using DifferentialEquations # Solve the equations
using Plots # Plotting
clear_default_processes() # hide
clear_default_state_mapping() # hide

# Default process: asm
@set_process asm = Process("ASM1") # Use the default parameters

# Additional process
@named aer = Aeration() # The aeration process

# Set the state mapping to map both to S_O2
@set_state_mapping S_O2 = states(asm).S_O2, states(aer).S_O

# Reactor with the additional process
@named react = CSTR(1000, aer; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)

# Influent and aeration input
@named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], flowrate = 18446) # BSM1 Stabilization
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)

# Connection
eqs = [
  connect(outflows(influent, 1), inflows(react, 1))
  connect(exogenous_inputs(react, :aer₊k_La), aeration_ctrl.output)
]

# Model generation
@named model = ODESystem(eqs, t, systems=[influent, react, aeration_ctrl])
model_simplified = structural_simplify(model)

# Simulation
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)

# Plotting, only oxygen
plot(sol, idxs=[inflows(react, 1).S_O2, states(react).S_O2], label=["Inflow" "Reactor"], title="Oxygen concentration in CSTR with ASM1 and aeration", legend=:topright)

# Plotting the rates
plot(sol, idxs=[rates(react).S_O2, subsystems(react, :react_asm).rates.S_O2, subsystems(react, :aer).rates.S_O], label=["Combined" "ASM" "Aeration"], title="Oxygen rate in CSTR with ASM1 and aeration", legend=:topright)
```

### Step-by-Step Explanations

As always, the first step is the setup in loading the packages:

```jldoctest variable_renaming_test; output=false
using BioChemicalTreatment # This Package :)
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more
using DifferentialEquations # Solve the equations
using Plots # Plotting

# output

```
```@setup variable_renaming
using BioChemicalTreatment # This Package :) # hide
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations # hide
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more # hide
using DifferentialEquations # Solve the equations # hide
using Plots # Plotting # hide
clear_default_processes() # hide
clear_default_state_mapping() # hide
```

And then, the matrix-defined asm is set as default process and the additional aeration process is created:

```jldoctest variable_renaming_test; output = false, setup = :(clear_default_processes() && clear_default_state_mapping())
@set_process asm = Process("ASM1") # Use the default parameters
@named aer = Aeration() # The aeration process

# output

Process Aeration 'aer':
States (1): see states(aer)
  S_O(t) [guess is 0.0]: S_O
Exogenous Inputs (1): see exogenous_inputs(aer)
  :k_La
Parameters (1): see parameters(aer)
  S_O_max [defaults to 8]
```
```@setup variable_renaming
@set_process asm = Process("ASM1") # Use the default parameters
@named aer = Aeration() # The aeration process
```

Then, a mapping between the states `S_O2` of the asm and `S_O` of the aeration is added, to let the program know that they are
in fact the same states. To do so `@set_state_mapping` is used (alternatively `@add_state_mapping` is possible, with the only difference
that the `set`- version clears the existing mappings).

```jldoctest variable_renaming_test; output = false
# Set the state mapping to map both to S_O2
# Left of the equal sign is the name to use, on the right hand side the states that are mapped to it. Note that for the name also any other name not yet existing could have been used, and like this variables could be renamed.
@set_state_mapping S_O2 = states(asm).S_O2, states(aer).S_O

# output

true
```
```@setup variable_renaming
@set_state_mapping S_O2 = states(asm).S_O2, states(aer).S_O
```

Then, the rest is exactly equal to the example above (possible for both, with and without default processes). The only difference is the name of the states and their order in the influent (it is different for the two ASM1 implementations, as the state names are different and they are ordered alphabetically to have it consistent):

```@example variable_renaming
# Reactor with the additional process
@named react = CSTR(1000, aer; initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)

# Influent and aeration input
@named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], flowrate = 18446) # BSM1 Stabilization
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)

# Connection
eqs = [
  connect(outflows(influent, 1), inflows(react, 1))
  connect(exogenous_inputs(react, :aer₊k_La), aeration_ctrl.output)
]

# Model generation
@named model = ODESystem(eqs, t, systems=[influent, react, aeration_ctrl])
model_simplified = structural_simplify(model)

# Simulation
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)

# Plotting, only oxygen
plot(sol, idxs=[inflows(react, 1).S_O2, states(react).S_O2], label=["Inflow" "Reactor"], title="Oxygen concentration in CSTR with ASM1 and aeration", legend=:topright)
```

And the plot of the rates, note that now the `asm` subsystem of the reactor has a different name, i.e. is prepended by the name of the reactor:

```@example variable_renaming
# Plotting the rates
plot(sol, idxs=[rates(react).S_O2, subsystems(react, :react_asm).rates.S_O2, subsystems(react, :aer).rates.S_O], label=["Combined" "ASM" "Aeration"], title="Oxygen rate in CSTR with ASM1 and aeration", legend=:topright)
```
