# About the Reactions Submodule

!!! warning
    The reactions submodule is still in the beginnings of its development. Thus it is not yet usable and this section exists for future examples of it.

The intention of the `Reactions` submodule is to allow for building new models, modifying existing ones and also to investigate the mathematical structure and properties of the models.

To simulate the models in the context of a larger Bio-Chemical system, such as a Water Resource Recovery Facility, the models can be translated to the `ProcessSimulator` and used there.
