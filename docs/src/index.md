# BioChemicalTreatment.jl

*A tool for modelling biochemical treatment processes such as activated sludge processes for wastewater treatment in Julia.*

This package is designed to be a flexible tool for modelling biochemical treatment processes.
It allows users to simulate such processes and components by defining and connecting them through subsystems.

## Repository
The code for the module is hosted at [BioChemicalTreatment](https://gitlab.com/datinfo/BioChemicalTreatment.jl).

## Feature summary
The idea behind BioChemicalTreatment.jl is to create a package that can be used to model any biochemical treatment process. Several reactor types and biochemical models are included. It is compatible with the [Modeling Toolkit Standard Library](@extref ModelingToolkitStandardLibrary :doc:`index`) in the sense that any signal can be connected to the ModelingToolkitStandardlibrary.Blocks (after suitable conversion). This enables inclusion of control loops (e.g. PID for aeration), other signal processing tasks (e.g. low-pass for signal smoothing) and more in the models of the biochemical treatment processes.

### Feature List
- Reactors
    - [Continuously Stirred Tank Reactor (CSTR)](@ref BioChemicalTreatment.ProcessSimulator.CSTR): Completely mixed reactor with equal volumetric inflow and outflow.
    - [Batch Reactor](@ref BioChemicalTreatment.ProcessSimulator.BatchReactor): Completely mixed reactor without any inflow or outflow.

- Biochemical Models:  Equation-defined Models
    - [Activated Sludge Model No.1 (ASM1)](@ref BioChemicalTreatment.ProcessSimulator.ASM1): The ASM1 used in wastewater treatment [Henze:1987](@cite).
    - [Ozone Disinfection](@ref BioChemicalTreatment.ProcessSimulator.OzoneDisinfection): A simple model for disinfection of bacteria with ozone.
    - [Aeration](@ref BioChemicalTreatment.ProcessSimulator.Aeration): A simple aeration model using the oxygen transfer coefficient as input.

```@eval
using Markdown
Markdown.parse("""
- Biochemical Models: Matrix-defined Models
"""	* join("    - [" .* Main.model_headings .* "](" .* replace.(Main.available_model_links, ".md" => "") .* ")", "\n")
)
```

!!! note "Matrix-defined models"
    Note that all models marked as *Matrix-defined* are included in this package as so-called artifacts. This means that they are **not** downloaded by default, but lazily on demand for the first use and **for the first usage, an active internet connection is required**.

    The reason behind this is to reduce memory usage as there may be a large number of models in the package which are not used for every user. By downloading it only on demand, it is ensured that only the needed models are on the system.

    To manually download the models (e.g, for later offline usage), the following snippets can be used:
    First load the TOML file associated with the package

    ```julia
    import Pkg
    using BioChemicalTreatment
    
    toml = Pkg.Artifacts.find_artifacts_toml(pathof(BioChemicalTreatment))
    ```
    Then either download a model of your choice by replacing  _"MODELNAME"_  with the model name to download:

    ```julia
    Pkg.Artifacts.ensure_artifact_installed("model_MODELNAME", toml)
    ```

    or download all models with:
        
    ```julia
    Pkg.Artifacts.ensure_all_artifacts_installed(toml)
    ```
    
    
- **Clarifiers**: state separators
    - [Ideal Clarifier](@ref BioChemicalTreatment.ProcessSimulator.IdealClarifier): An ideal state separator based on particle size (settling all particulate states).
- **Flow Elements**: elements without dynamics i.e. they contain no differential equations
    - [Influent](@ref BioChemicalTreatment.ProcessSimulator.Influent): A system for providing an influent.
    - [Flow Connector](@ref BioChemicalTreatment.ProcessSimulator.FlowConnector): Connect multiple flows and mix them before splitting in multiple
    - [Flow Unifier](@ref BioChemicalTreatment.ProcessSimulator.FlowUnifier): A subcategory of flow connector that unifies multiple flows into one.
    - [Flow Separator](@ref BioChemicalTreatment.ProcessSimulator.FlowSeparator): A subcategory of flow connector that separates a flow into multiple flows.
    - [Flow Pump](@ref BioChemicalTreatment.ProcessSimulator.FlowPump): Pump a flow, by specifying its flow rate.
    - [Sensor](@ref BioChemicalTreatment.ProcessSimulator.Sensor): Sense any component in the flow, including qualitative and quantitative components. A sensor can for example be used for control.
- **Modeling Toolkit Standard Library Compatibility**
    -  [RealInputToOutflowPort](@ref BioChemicalTreatment.ProcessSimulator.RealInputToOutflowPort): Transform a series of [RealInputs](@extref ModelingToolkitStandardLibrary.Blocks.RealInput) into an outflow to be used with BioChemicalTreatment.jl library.
    -  [InflowPortToRealOutput](@ref BioChemicalTreatment.ProcessSimulator.InflowPortToRealOutput): Transform a flow of this library into a series of [RealOutputs](@extref ModelingToolkitStandardLibrary.Blocks.RealOutput).
    -  [RealInputToReactionOutput](@ref BioChemicalTreatment.ProcessSimulator.RealInputToReactionOutput): Transform a series of [RealInputs](@extref ModelingToolkitStandardLibrary.Blocks.RealInput) into a reaction port to be used with this library.
    -  [ReactionInputToRealOutput](@ref BioChemicalTreatment.ProcessSimulator.ReactionInputToRealOutput): Transform a reaction port of this library into a series of [RealOutputs](@extref ModelingToolkitStandardLibrary.Blocks.RealOutput).
    -  [RealInputToStateOutput](@ref BioChemicalTreatment.ProcessSimulator.RealInputToStateOutput): Transform a series of [RealInputs](@extref ModelingToolkitStandardLibrary.Blocks.RealInput) into a state port to be used with this library.
    -  [StateInputToRealOutput](@ref BioChemicalTreatment.ProcessSimulator.StateInputToRealOutput): Transform a state port of this library into a series of [RealOutputs](@extref ModelingToolkitStandardLibrary.Blocks.RealOutput).

!!! note "Modeling Toolkit Standard Library"
    The systems under "Modeling Toolkit Standard Library Compatibility" provide interfaces to connect the models for biochemical treatment processes in this library with the systems from the [Modeling Toolkit Standard Library](@extref ModelingToolkitStandardLibrary :doc:`index`). This enables the connection of any signal in this library to the ModelingToolkitStandardlibrary.Blocks (after suitable conversion) and the functionality provided there can be used.

    This includes various signal processing utils, e.g.
    - [Constants](@extref ModelingToolkitStandardLibrary.Blocks.Constant)
    - [Sums (``+``)](@extref ModelingToolkitStandardLibrary.Blocks.Sum)
    - [Products (``*``)](@extref ModelingToolkitStandardLibrary.Blocks.Product)
    - [Integrators](@extref ModelingToolkitStandardLibrary.Blocks.Integrator)
    - [PID Controllers](@extref ModelingToolkitStandardLibrary.Blocks.PID)
    - [First Order Systems (can be used e.g. as lowpass filters)](@extref ModelingToolkitStandardLibrary.Blocks.FirstOrder)
    - [State Space Models](@extref ModelingToolkitStandardLibrary.Blocks.StateSpace)
    - [Transfer Functions](@extref ModelingToolkitStandardLibrary.Blocks.TransferFunction)
    - and more...

## Install and remove
To install the package and test if it runs:

```julia
julia --project=.
using Pkg
] # to open the package manager prompt
add BioChemicalTreatment
# go back with backspace and use it
using BioChemicalTreatment
BioChemicalTreatment.welcome() # returns "Welcome to BioChemicalTreatment"
```

You can remove the package with

```julia
]
rm BioChemicalTreatment
```

## User Manual

### Examples for the ProcessSimulator Submodule
```@contents
Pages = Main.examples_ProcessSimulator
Depth = 1
```

### Examples for the Reactions Submodule
```@contents
Pages = Main.examples_Reactions
Depth = 1
```

## Reference Manual
```@contents
Pages = ["api_ProcessSimulator.md", "api_Reactions.md"]
Depth = 1
```

## Contributing
The preferred way to contribute is to raise an issue using the [issue tracker](https://gitlab.com/datinfo/BioChemicalTreatment.jl/-/issues) to raise awareness.
In addition, merge requests are always welcome. In particular, anyone adding a new model or system is strongly encouraged to open a merge request to include it in the BioChemicalTreatment.jl library.

For major changes, please open an issue first to discuss what you want to change, in order to avoid unnecessary work.

And please always make sure to update the tests as appropriate.

## Citation

If you use BioChemicalTreatment.jl in your work, please cite the following, but note that the order of the authors can arbitrarily be chosen:

```
@misc{2023biochemicaltreatment,
	title = {{BioChemicalTreatment.jl} to model biochemical processes in water and wastewater treatment},
	url = {https://gitlab.com/datinfo/BioChemicalTreatment.jl},
	author = {Florian Wenk and Juan Pablo Carbajal and Andreas Froemelt and Mariane Yvonne Schneider},
	year = {2023}
}
```

## Development

### Install development version

Install the latest development version of the code using

```julia
julia --project=.
using Pkg
] # to open the package manager prompt
add https://gitlab.com/datinfo/BioChemicalTreatment.jl
# go back with backspace and use it
using BioChemicalTreatment
BioChemicalTreatment.welcome() # returns "Welcome to BioChemicalTreatment"
```

If you want to work with a specific branch instead, change the link to `https://gitlab.com/datinfo/BioChemicalTreatment.jl#BRANCHNAME` (set `BRANCHNAME` to the name of the branch you want to install).

### Documentation
The documentation uses `LaTeX` with (`TikzPictures.jl`)[https://github.com/JuliaTeX/TikzPictures.jl] for generate some of the graphics. Compiling those, and thus building the documentation, requires `lualatex` and `dvisvgm` installed.

To build the documentation locally, these dependencies have to be installed first:
`Lualatex` can be installed through [`texlive`](https://www.tug.org/texlive/) or [`MiKTeX`](https://miktex.org/). You can [get `dvisvgm` here](https://dvisvgm.de/).
If you are using Debian/Ubuntu or similar, all dependencies can be installed using `sudo apt-get install pdf2svg texlive-latex-base texlive-binaries texlive-pictures texlive-latex-extra texlive-luatex`.

Once these dependencies are installed, the documentation can be built locally. For this, you need to first instantiate the `docs` project.
To do this run the following command from the root folder of the repository:

```
julia --project=docs -e 'using Pkg; Pkg.develop(PackageSpec(path=pwd())); Pkg.instantiate();'
```
!!! warning
    in some consoles you might need to replace `'` with `"`

You need to do this only once, or when you change the dependencies for the documentation.
Once this is done, you can generate the documentation using the command:

```
julia --project=docs -e 'include("docs/make.jl")'
```

!!! warning
    in some consoles you might need to replace `'` with `"` and use `\"` for the internal quotes.

The generated HTML will be in the `docs/build` folder.

To run the doctests you can run:

```
julia --project=docs -e '
    using Documenter: DocMeta, doctest; using BioChemicalTreatment, ModelingToolkit, DifferentialEquations, Plots; 
    DocMeta.setdocmeta!(BioChemicalTreatment, :DocTestSetup, :(using BioChemicalTreatment; using ModelingToolkit); recursive=true)
    doctestfilters = [
            r"(?<=[\.0-9]{6})[\.0-9]+", # Compare numbers only until the fifth significant digit
            r"(?<=\d)\.\d+(?=e-[5-9])", # Compare only leading digits for numbers smaller than 1e-4
            r"(?<=\d)\.\d+(?=e-\d+\d)", # Compare only leading digits for numbers smaller than 1e-10
            r"(?<=ODESystem\(0x)[\da-f]+(?=,)", # Ignore ODESystem tags/ids
            r"(?<=Plot{)[^}]*n=\d*(?=})", # Ignore Plot number
            r"(?<=connect\()[^\)]*(?=\))", # Do not check connection contents (sometimes they are printed, sometimes not)
    ]
    doctest(BioChemicalTreatment; manual = true, doctestfilters)'
```

This command can be joined with the previous one to run doctests and build the docs.

### Testing

For testing the project, use the following command:

```
using Pkg, BioChemicalTreatment
Pkg.test("BioChemicalTreatment")
```

This executes all tests for the package as well as the doctests.



