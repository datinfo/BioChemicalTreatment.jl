```@meta
CurrentModule = BioChemicalTreatment
```

# ProcessSimulator

This module is for simulating complex processes with different reactor combinations.
It provides a series of reactors, settlers and connection elements to be used for building process configurations.

## Index

```@index
Pages = ["api_ProcessSimulator.md"]
```

## Documentation

### System Types
```@docs
Reactor
Process
FlowElement
MTKStandardLibraryInterface
```

### Defaults
```@docs
default_processes
@set_process
set_default_process
@add_process
add_default_process
clear_default_processes
default_state_mapping
@set_state_mapping
set_default_state_mapping
@add_state_mapping
add_default_state_mapping
clear_default_state_mapping
get_default_state
default_states
```

### Helpers and Accessor functions
```@docs
t
inflows
inflow_names
hasinflows
num_inflows
outflows
outflow_names
hasoutflows
num_outflows
rates
hasrates
num_rates
states
hasstates
num_states
exogenous_inputs
exogenous_input_names
hasexogenous_inputs
num_exogenous_inputs
exogenous_outputs
exogenous_output_names
hasexogenous_outputs
num_exogenous_outputs
subsystems
subsystem_names
hassubsystems
num_subsystems
iscolloidal
isparticulate
issoluble
```

### ProcessDiagram
```@docs
    ProcessDiagram
```
#### Printing options
```@docs
    set_linewidth!
    set_nodedistance!
    set_blockwidth!
    set_blockheight!
    set_innersep!
    set_background!
    print_connector_names!
```
#### Element Ordering
```@docs
    switch_chains!
    move_chain_vertically!
    move_chain_horizontally!
    merge_chains!
    insert_empty_columns!
    switch_columns!
    move_column!
    get_elem
    move_elem!
    switch_elem!
    cleanup!
```
#### Output options
```@docs
    get_latex_preamble
    get_tikzpicture
    get_latex
```