```@meta
CurrentModule = BioChemicalTreatment
```

# Internal API

Here, all internal API is documented. Note that this is, as the name says, **internal**. Thus it is only intended for internal use and might change unexpectedly.

## Index

```@index
Pages = ["api_internal.md"]
```

## ProcessSimulator Internal API

### ProcessElement Interface
```@docs
ProcessSimulator.AbstractProcessElement
ProcessSimulator.typestr
Base.convert
```

### Reactor
```@docs
ProcessSimulator.@reactor
```

### Defaults
```@docs
ProcessSimulator.DEFAULT_PROCESSES
ProcessSimulator.DEFAULT_PROCESSES_NAMES
ProcessSimulator.DEFAULT_PROCESSES_KWARGS
ProcessSimulator.DEFAULT_PROCESSES_ARGS
ProcessSimulator.DEFAULT_STATE_MAPPING
```

### Ports
```@docs
ProcessSimulator.InflowPort
ProcessSimulator.OutflowPort
ProcessSimulator.ReactionInputPort
ProcessSimulator.ReactionOutputPort
ProcessSimulator.StateInputPort
ProcessSimulator.StateOutputPort
```

### PortMetadata
```@docs
ProcessSimulator.PortMetadata
ProcessSimulator.init_port_metadata
ProcessSimulator.set_port_metadata!
ProcessSimulator.assemble_port_metadata_fields
```

### Utilities
```@docs
ProcessSimulator.get_default_or_guess
ProcessSimulator.get_influent_source
ProcessSimulator.to_ordered_array
ProcessSimulator.realports_from_combinedport
ProcessSimulator.symbolic_to_namestring
ProcessSimulator.strip_dependencies
ProcessSimulator.lstrip_namespace
ProcessSimulator.rstrip_namespace
ProcessSimulator.split_namespace
ProcessSimulator.get_namespaced_property
```

### ProcessDiagram
```@docs
ProcessSimulator.metadatachain_to_elementchain
ProcessSimulator.remove_elem!
ProcessSimulator.isempty_column
ProcessSimulator.strip_namespace
ProcessSimulator.get_label
ProcessSimulator.remove_column!
ProcessSimulator.remove_empty_columns!
ProcessSimulator.inchain
ProcessSimulator.strip_chain!
ProcessSimulator.get_tikzpicture_body
ProcessSimulator.get_portmetadata
ProcessSimulator.find_reference_chains
ProcessSimulator.strip_chains!
ProcessSimulator.get_blocking
ProcessSimulator.insert_elem!
ProcessSimulator.order_connections
ProcessSimulator.get_name
ProcessSimulator.get_node_style
ProcessSimulator.remove_empty_chains!
ProcessSimulator.get_element_positions
ProcessSimulator.assemble_chains
ProcessSimulator.get_element_chains
ProcessSimulator.insert_column!
ProcessSimulator.metadata_connections
ProcessSimulator.order_chains_horizontally
ProcessSimulator.get_port_label
ProcessSimulator.squeeze_vertically!
ProcessSimulator.get_tikzpicture_options
ProcessSimulator.straightify_verticals!
ProcessSimulator.get_connected
```

## Reactions Internal API

### BioChemicalReactionSystem
```@docs
Reactions.parameter_expressions
Reactions.parameter_values
Reactions.parameter_value_or_expression
```

### Parsing
```@docs
Reactions.DEFAULT_PARSING_CONTEXT
Reactions.parse_equation
Reactions.parse_variable
Reactions.parse_states
Reactions.parse_params
Reactions.parse_stoichmat
Reactions.parse_processrates
Reactions.parse_compmat
```

### Utilities
```@docs
Reactions.symbolic_to_namestring
```

