```@meta
CurrentModule = BioChemicalTreatment
```

# Reactions

This module is for defining, investigating and modifying biochemical reactions.


## Index
```@index
Pages = ["api_Reactions.md"]
```

## Documentation
```@docs
Reactions.BioChemicalReactionSystem
Reactions.assemble_equations
Reactions.check_composition
```

