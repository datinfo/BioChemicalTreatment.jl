```@meta
CurrentModule = BioChemicalTreatment
```

# Process Models

A list of the available Process Models.

There are two different kinds of defining processes in this library, the [`equation-defined models`](@ref "Models") and the [ `matrix-defined models`](@ref "Matrix-defined Models"). The list below is thus split into the two parts.

## Index

```@index
Pages = ["processes.md"]
```

## Models
```@docs
ASM1
OzoneDisinfection
Aeration
```

## Matrix-defined Models

Matrix-Defined models are loaded using the default `Process` constructor, which supports loading models directly from files as well as using the included models:

```@docs
Process()
```

Currently, the following matrix-defined process models are part of this library:

```@eval
using Markdown
Markdown.parse(
    join("- \"" .* Main.available_models .* "\": [" .* Main.model_headings .* "](" .* "../../" .*  replace.(Main.available_model_links, ".md" => "") .* ")", "\n")
)
```

They are called by passing the first string in the list above (before the colon) as argument to the `Process`:

```julia
@named asm = Process("ASM1")
```

To set model parameters, use keyword arguments:

```julia
@named asm = Process("ASM1"; Y_H = 12)
```
