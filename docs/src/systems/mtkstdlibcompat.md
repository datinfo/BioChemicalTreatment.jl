```@meta
CurrentModule = BioChemicalTreatment
```

# ModelingToolkitStandardLibrary Compatibility system

A list of the available systems for compatibility with the .

## Index

```@index
Pages = ["mtkstdlibcompat.md"]
```

## Models
```@docs
InflowPortToRealOutput
ReactionInputToRealOutput
RealInputToOutflowPort
RealInputToReactionOutput
RealInputToStateOutput
StateInputToRealOutput
```