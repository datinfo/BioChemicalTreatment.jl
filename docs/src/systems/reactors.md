```@meta
CurrentModule = BioChemicalTreatment
```

# Reactors

A list of the available Reactors.

## Index

```@index
Pages = ["reactors.md"]
```

## Models
```@docs
BatchReactor
CSTR
```