# Low-Level Reactor with multiple Processes: ASM1 with Aeration

!!! note
	This example is exactly the same as the [corresponding the high level example](@ref "Reactor with multiple Processes: ASM1 with Aeration"). Thus the two interfaces can be compared.

  For this system it can already be seen how the high-level interface simplifies the definition and one can imagine how it goes on for more complex systems.

This example shows the current procedure of how multiple processes are combined in a single reactor. For this, we use an examplary system of a single aerated reactor with `ASM1` and constant input:

```@example
using TikzPictures # hide
TikzPicture(L""" %# hide
       \tikzstyle{block} = [draw, rectangle, inner sep=20pt, align=center] %# hide
       \tikzstyle{clarifier} = [draw, regular polygon, regular polygon sides=3, shape border rotate=180, align=center] %# hide
       \node (inf) {Influent}; %# hide
       \node[block, right=2cm of inf] (cstr) {Aerated CSTR with ASM1}; %# hide
       \node[right=of cstr] (eff) {Effluent}; %# hide
  %# hide
       \draw[->] (inf) -- (cstr) node[pos=0.5]{$q_{in},C_{in}$}; %# hide
       \draw[->] (cstr) -- (eff) node[pos=0.5]{$q_{eff},C_{eff}$}; %# hide
       """; options=">=stealth, auto, node distance=2cm, scale=2, background rectangle/.style={fill=white}, show background rectangle", preamble=raw"\usetikzlibrary{arrows,arrows.meta,bending,positioning,patterns,decorations.pathmorphing,calc,shapes.geometric,backgrounds}") # hide
```

Now, combining two processes in a single reactor is exactly the point where the split between the `Reactor` and the `Process` such that the reaction rate is provided as an external signal to the `Reactor` comes in handy (see [Low Level: Reactors vs. Processes](@ref) for details). This is, because the reaction rates of the different processes can simply be added, and thus this can be done using external connections. Now this can be represented as:

```@example
using TikzPictures # hide
TikzPicture(L""" %# hide
    \tikzstyle{block} = [draw, rectangle, inner sep=20pt, align=center] %# hide
    \tikzstyle{clarifier} = [draw, regular polygon, regular polygon sides=3, shape border rotate=180, align=center] %# hide
    \node[block] (cstr) {CSTR}; %# hide
    \node[block, inner sep=10pt, above=.5cm of cstr] (asm) {ASM1}; %# hide
    \node[block, inner sep=10pt, above=.5cm of asm] (aer) {Aeration}; %# hide
    \node[draw, circle, left= 1cm of asm] (sum) {+}; %# hide
    \node at ($(cstr.south west)!0.33!(cstr.north west) + (-1.5cm, 0)$) (inf) {Influent}; %# hide
    \node at ($(cstr.south east)!0.33!(cstr.north east) + (1.5cm, 0)$) (eff) {Effluent}; %# hide
 %# hide
    \draw[->] (inf) -- ($(cstr.south west)!0.33!(cstr.north west)$) node[pos=0.5, below]{$q_{in},C_{in}$}; %# hide
    \draw[->] ($(cstr.south east)!0.33!(cstr.north east)$) -- (eff) node[pos=0.5, below]{$q_{eff},C_{eff}$}; %# hide
    \draw[->] (aer) -| (sum); %# hide
    \draw[->] (asm) -- (sum); %# hide
    \draw[->] (sum) |- ($(cstr.south west)!0.66!(cstr.north west)$) node[pos=0.75]{rates}; %# hide
    \draw[->] ($(cstr.south east)!0.66!(cstr.north east)$) -- node[pos=0.5]{states}($(cstr.south east)!0.66!(cstr.north east) + (0.5cm, 0)$) |- (asm); %# hide
    \draw[->] ($(cstr.south east)!0.66!(cstr.north east)$) -- ($(cstr.south east)!0.66!(cstr.north east) + (0.5cm, 0)$) |- (aer); %# hide
    """; options=">=stealth, auto, scale=2, background rectangle/.style={fill=white}, show background rectangle", preamble=raw"\usetikzlibrary{arrows,arrows.meta,bending,positioning,patterns,decorations.pathmorphing,calc,shapes.geometric,backgrounds}") # hide
```

For setting this up however, an adder block is needed. Luckily, this can be taken from the `ModelingToolkitStandardLibrary` as [`ModelingToolkitStandardLibrary.Blocks.Add`](@extref) where the compatibility systems provided in this library come in handy.

There is only one caveat left: The two processes do not need the same states and also do not provide the same rates. Instead, the `Aeration` needs only the dissolved oxygen state as input and also provides only a rate for the dissolved oxygen. This makes the combination more complex, but also this can be solved using the standard library compatibility systems, as they act as kind of a multiplexer: they split the ports into their individual signals and they can also assemble individual signals into the corresponding port.

Now, let's start the example of how to actually do this.

## Setup

As always, the first step is the setup. Note that now also the `ModelingToolkitStandardLibrary` is loaded for the adder system and the bookkeeping variables are also introduced.

```jldoctest multiple_process_test; output=false
using BioChemicalTreatment # This Package :)
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more
using DifferentialEquations # Solve the equations
using Plots # Plotting

sys = ODESystem[]
eqs = Equation[]

# output

Equation[]
```
```@setup multiple_process
using BioChemicalTreatment # This Package :) # hide
using ModelingToolkit # Basic Modeling Framework. Takes e.g. care of simplification of the equations # hide
using ModelingToolkitStandardLibrary # Standard library for MTK. Provides e.g. simple math operations (adder, product...), controllers (PID) and more # hide
using DifferentialEquations # Solve the equations # hide
using Plots # Plotting # hide
 # hide
sys = ODESystem[] # hide
eqs = Equation[] # hide
```

## Creating the Processes and Reactor

Then, as usual, first the processes (in this case two of them) are created.

```jldoctest multiple_process_test
@named asm = Process("ASM1") # Use the matrix-defined version with default parameters
@named aer = Aeration() # The aeration process
append!(sys, [asm, aer]) # Bookkeeping

@named react = CSTR(1000, unknowns(states(asm)); initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself)
push!(sys, react) # Bookkeeping
nameof.(sys) # Show all added systems

# output

3-element Vector{Symbol}:
 :asm
 :aer
 :react
```
```@setup multiple_process
@named asm = Process("ASM1") # Use the matrix-defined version with default parameters # hide
@named aer = Aeration() # The aeration process # hide
append!(sys, [asm, aer]) # Bookkeeping # hide
 # hide
@named react = CSTR(1000, unknowns(states(asm)); initial_states=[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) # Volume of 1000, initial_states of all 1 (the default of all 0 does yield infinity, check the model for yourself) # hide
push!(sys, react) # Bookkeeping # hide
```

For the reactor, only the states of the `ASM1` are used, as those superset the states of the aeration.

## Creating the Systems for the combination

For the combination of the processes, a series of other helper systems is needed. First the state of the reactor needs to be split into single ports

```jldoctest multiple_process_test
@named reactor_state_to_single_ports = StateInputToRealOutput(unknowns(states(react))) # note the use of the states of the reactor here as the reactor state port is to be split
push!(sys, reactor_state_to_single_ports)
nameof.(sys) # Show all added systems

# output

4-element Vector{Symbol}:
 :asm
 :aer
 :react
 :reactor_state_to_single_ports
```
```@setup multiple_process
@named reactor_state_to_single_ports = StateInputToRealOutput(unknowns(states(react))) # note the use of the states of the reactor here as the reactor state port is to be split # hide
push!(sys, reactor_state_to_single_ports) # hide
```

And then the aeration state needs to be reassembled from it:

```jldoctest multiple_process_test
@named single_ports_to_aeration_states = RealInputToStateOutput(unknowns(states(aer))) # Similarly here, aeration state is to be built -> use unknowns(states(aer))
push!(sys, single_ports_to_aeration_states)
nameof.(sys) # Show all added systems

# output

5-element Vector{Symbol}:
 :asm
 :aer
 :react
 :reactor_state_to_single_ports
 :single_ports_to_aeration_states
```
```@setup multiple_process
@named single_ports_to_aeration_states = RealInputToStateOutput(unknowns(states(aer))) # Similarly here, aeration state is to be built -> use unknowns(states(aer)) # hide
push!(sys, single_ports_to_aeration_states) # hide
```

Now the state inputs for both processes are available (for the `ASM1` directly the state from the reactor can be taken). So it's time to look at the other side of the rates. There, first the rate outputs of both processes have to be split apart

```jldoctest multiple_process_test
@named asm_rates_to_single_ports = ReactionInputToRealOutput(unknowns(rates(asm))) # asm rates is to be split -> unknowns(rates(asm))
@named aeration_rates_to_single_ports = ReactionInputToRealOutput(unknowns(rates(aer))) # aeration rates is to be split -> unknowns(rates(aer))
append!(sys, [asm_rates_to_single_ports, aeration_rates_to_single_ports])
nameof.(sys) # Show all added systems

# output

7-element Vector{Symbol}:
 :asm
 :aer
 :react
 :reactor_state_to_single_ports
 :single_ports_to_aeration_states
 :asm_rates_to_single_ports
 :aeration_rates_to_single_ports
```
```@setup multiple_process
@named asm_rates_to_single_ports = ReactionInputToRealOutput(unknowns(rates(asm))) # asm rates is to be split -> unknowns(rates(asm)) # hide
@named aeration_rates_to_single_ports = ReactionInputToRealOutput(unknowns(rates(aer))) # aeration rates is to be split -> unknowns(rates(aer)) # hide
append!(sys, [asm_rates_to_single_ports, aeration_rates_to_single_ports]) # hide
```

then, an adder is needed to add the rates of the dissolved oxigen

```jldoctest multiple_process_test
@named add_aeration_rate = ModelingToolkitStandardLibrary.Blocks.Add()
push!(sys, add_aeration_rate)
nameof.(sys) # Show all added systems

# output

8-element Vector{Symbol}:
 :asm
 :aer
 :react
 :reactor_state_to_single_ports
 :single_ports_to_aeration_states
 :asm_rates_to_single_ports
 :aeration_rates_to_single_ports
 :add_aeration_rate
```
```@setup multiple_process
@named add_aeration_rate = ModelingToolkitStandardLibrary.Blocks.Add() # hide
push!(sys, add_aeration_rate) # hide
```

and finally the rates port has to be reassembled to be able to provide it to the reactor

```jldoctest multiple_process_test
@named single_ports_to_reaction_output = RealInputToReactionOutput(unknowns(rates(react))) # reactor rates is to be built -> unknowns(rates(react))
push!(sys, single_ports_to_reaction_output)
nameof.(sys) # Show all added systems

# output

9-element Vector{Symbol}:
 :asm
 :aer
 :react
 :reactor_state_to_single_ports
 :single_ports_to_aeration_states
 :asm_rates_to_single_ports
 :aeration_rates_to_single_ports
 :add_aeration_rate
 :single_ports_to_reaction_output
```
```@setup multiple_process
@named single_ports_to_reaction_output = RealInputToReactionOutput(unknowns(rates(react))) # reactor rates is to be built -> unknowns(rates(react)) # hide
push!(sys, single_ports_to_reaction_output) # hide
```

## Creating the other needed systems

Now the most of the needed systems have been created, but two more are missing. **Do you figure out which?**

They are **the influent** and a **aeration strength input for the aeration**. The aeration process models the aeration, but it needs an input of the oxygen transfer coefficient, which allows it to be controlled (e.g. to hold a constant level of dissolved oxygen). See the [docs of the aeration for details](@ref Aeration).

As influent and this control input, constant values are assumed for simplicity. For the influent the stabilization from the BSM1 is taken:

```jldoctest multiple_process_test
@named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], unknowns(states(asm)), flowrate = 18446) # This is the stabilization influent from BSM1
push!(sys, influent)
nameof.(sys) # Show all added systems

# output

10-element Vector{Symbol}:
 :asm
 :aer
 :react
 :reactor_state_to_single_ports
 :single_ports_to_aeration_states
 :asm_rates_to_single_ports
 :aeration_rates_to_single_ports
 :add_aeration_rate
 :single_ports_to_reaction_output
 :influent
```
```@setup multiple_process
@named influent = Influent([7.0, 69.5, 6.95, 0.0, 31.56, 0.0, 0.0, 30.0, 202.32, 10.59, 0.0, 28.17, 0.0, 51.2], unknowns(states(asm)), flowrate = 18446) # This is the stabilization influent from BSM1 # hide
push!(sys, influent) # hide
```

And for the control input, a value of `240 1/d` is assumed. This port is a [`ModelingToolkitStandardLibrary.Blocks.RealInput`](@extref) as this port can directly be connected to the systems from there, e.g. the PID controller. Luckily, the library there also features a constant, which is used to specify this constant input:

```jldoctest multiple_process_test
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240)
push!(sys, aeration_ctrl)
nameof.(sys) # Show all added systems

# output

11-element Vector{Symbol}:
 :asm
 :aer
 :react
 :reactor_state_to_single_ports
 :single_ports_to_aeration_states
 :asm_rates_to_single_ports
 :aeration_rates_to_single_ports
 :add_aeration_rate
 :single_ports_to_reaction_output
 :influent
 :aeration_ctrl
```
```@setup multiple_process
@named aeration_ctrl = ModelingToolkitStandardLibrary.Blocks.Constant(;k = 240) # hide
push!(sys, aeration_ctrl) # hide
```

## Connecting

It is started by connecting the current state of the reactor to the processes. As the `ASM1` has the same states as the reactor this works as usual:

```jldoctest multiple_process_test; output=false
push!(eqs, connect(states(react), states(asm))) 

# output

1-element Vector{Equation}:
 connect()
```
```@setup multiple_process
push!(eqs, connect(states(react), states(asm)))  # hide
```

For the `Aeration`, which only needs the dissolved oxygen concentration, first the port is split into its individual values to extract the dissolved oxygen, which is then fed to the system assembling the state port for the `Aeration` process and then to the process itself:

```jldoctest multiple_process_test; output=false
push!(eqs, connect(states(react), states(reactor_state_to_single_ports))) # First the reactor states to the splitting system
push!(eqs, connect(exogenous_outputs(reactor_state_to_single_ports, :S_O2), exogenous_inputs(single_ports_to_aeration_states, :S_O))) # Then the split up dissolved oxygen to the reassembly system
push!(eqs, connect(states(single_ports_to_aeration_states), states(aer))) # Then the reassembly system to the aeration states

# output

4-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
```
```@setup multiple_process
push!(eqs, connect(states(react), states(reactor_state_to_single_ports))) # First the reactor states to the splitting system # hide
push!(eqs, connect(exogenous_outputs(reactor_state_to_single_ports, :S_O2), exogenous_inputs(single_ports_to_aeration_states, :S_O))) # Then the split up dissolved oxygen to the reassembly system # hide
push!(eqs, connect(states(single_ports_to_aeration_states), states(aer))) # Then the reassembly system to the aeration states # hide
```

!!! warning
    Note the different names of the dissolved oxygen in the two processes, `S_O` vs `S_O2`. This is unfortunate, but comes from the fact that there are multiple naming conventions out there, the one from the original ASM [Henze:2006](@cite) (used by the `Aeration` system) and the revised one from [Corominas:2010](@cite) (Used by the matrix-defined `ASM1`). If the original ASM1 (using `ASM1()`) would have been used, both variables would be called `S_O`. 
    The variable naming in this package is still work in progress and it might be unified at some point in the future.
    **However, this does not hinder the combination of two processes.** 
    
    This implies, however, that arbitrary process rates can be added and combined like this. Thus the user must pay attention that the correct rates are connected!

Then, the other side of adding and connecting the rates is tackled. For this, first the rate outputs of both systems have to be split up:

```jldoctest multiple_process_test; output=false
push!(eqs, connect(rates(asm), rates(asm_rates_to_single_ports)))
push!(eqs, connect(rates(aer), rates(aeration_rates_to_single_ports))) 

# output

6-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
```
```@setup multiple_process
push!(eqs, connect(rates(asm), rates(asm_rates_to_single_ports))) # hide
push!(eqs, connect(rates(aer), rates(aeration_rates_to_single_ports)))  # hide
```

And then, the dissolved oxygen rates can be summed up by connection to the addition system:

```jldoctest multiple_process_test; output=false
push!(eqs, connect(exogenous_outputs(asm_rates_to_single_ports, :S_O2), add_aeration_rate.input1))
push!(eqs, connect(exogenous_outputs(aeration_rates_to_single_ports, :S_O), add_aeration_rate.input2)) 

# output

8-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
```
```@setup multiple_process
push!(eqs, connect(exogenous_outputs(asm_rates_to_single_ports, :S_O2), add_aeration_rate.input1)) # hide
push!(eqs, connect(exogenous_outputs(aeration_rates_to_single_ports, :S_O), add_aeration_rate.input2))  # hide
```

It is continued by assembling the combined rates port by connecting all single signals from the split `ASM1` rates port and the summed up rate for dissolved oxygen to the reassembly system:

```jldoctest multiple_process_test; output=false
append!(eqs, [
    # First the summed up aeration rate
    connect(add_aeration_rate.output, exogenous_inputs(single_ports_to_reaction_output, :S_O2))
    # Then all others directly from the splitted asm rates 
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_Alk), exogenous_inputs(single_ports_to_reaction_output, :S_Alk))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_B), exogenous_inputs(single_ports_to_reaction_output, :S_B))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_BN), exogenous_inputs(single_ports_to_reaction_output, :S_BN))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_N2), exogenous_inputs(single_ports_to_reaction_output, :S_N2))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_NHx), exogenous_inputs(single_ports_to_reaction_output, :S_NHx))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_NOx), exogenous_inputs(single_ports_to_reaction_output, :S_NOx))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_U), exogenous_inputs(single_ports_to_reaction_output, :S_U))
    connect(exogenous_outputs(asm_rates_to_single_ports, :XC_B), exogenous_inputs(single_ports_to_reaction_output, :XC_B))
    connect(exogenous_outputs(asm_rates_to_single_ports, :XC_BN), exogenous_inputs(single_ports_to_reaction_output, :XC_BN))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_ANO), exogenous_inputs(single_ports_to_reaction_output, :X_ANO))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_OHO), exogenous_inputs(single_ports_to_reaction_output, :X_OHO))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_UE), exogenous_inputs(single_ports_to_reaction_output, :X_UE))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_UInf), exogenous_inputs(single_ports_to_reaction_output, :X_UInf))
    ])

# output

22-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 ⋮
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
```
```@setup multiple_process
append!(eqs, [
    # First the summed up aeration rate
    connect(add_aeration_rate.output, exogenous_inputs(single_ports_to_reaction_output, :S_O2))
    # Then all others directly from the splitted asm rates 
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_Alk), exogenous_inputs(single_ports_to_reaction_output, :S_Alk))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_B), exogenous_inputs(single_ports_to_reaction_output, :S_B))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_BN), exogenous_inputs(single_ports_to_reaction_output, :S_BN))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_N2), exogenous_inputs(single_ports_to_reaction_output, :S_N2))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_NHx), exogenous_inputs(single_ports_to_reaction_output, :S_NHx))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_NOx), exogenous_inputs(single_ports_to_reaction_output, :S_NOx))
    connect(exogenous_outputs(asm_rates_to_single_ports, :S_U), exogenous_inputs(single_ports_to_reaction_output, :S_U))
    connect(exogenous_outputs(asm_rates_to_single_ports, :XC_B), exogenous_inputs(single_ports_to_reaction_output, :XC_B))
    connect(exogenous_outputs(asm_rates_to_single_ports, :XC_BN), exogenous_inputs(single_ports_to_reaction_output, :XC_BN))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_ANO), exogenous_inputs(single_ports_to_reaction_output, :X_ANO))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_OHO), exogenous_inputs(single_ports_to_reaction_output, :X_OHO))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_UE), exogenous_inputs(single_ports_to_reaction_output, :X_UE))
    connect(exogenous_outputs(asm_rates_to_single_ports, :X_UInf), exogenous_inputs(single_ports_to_reaction_output, :X_UInf))
    ])
```

And finally, the assembled rates to the reactor:

```jldoctest multiple_process_test; output=false
push!(eqs, connect(rates(single_ports_to_reaction_output), rates(react)))

# output

23-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 ⋮
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
```
```@setup multiple_process
push!(eqs, connect(rates(single_ports_to_reaction_output), rates(react)))
```

With this, the reactor and process are connected and ready. Now adding the influent and the control input, and then the system is ready to be simulated:

```jldoctest multiple_process_test; output=false
push!(eqs, connect(outflows(influent, 1), inflows(react, 1)))
push!(eqs, connect(exogenous_inputs(aer, :k_La), aeration_ctrl.output))

# output

25-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 ⋮
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
```
```@setup multiple_process
push!(eqs, connect(outflows(influent, 1), inflows(react, 1)))
push!(eqs, connect(exogenous_inputs(aer, :k_La), aeration_ctrl.output))
```


## Simulation and plotting

Before simulating, the model must be built and simplified, as usual: 

```jldoctest multiple_process_test; output=false
@named model = ODESystem(eqs, t, systems=sys)
model_simplified = structural_simplify(model)

# output

Model model:
Equations (14):
  14 standard: see equations(model)
Unknowns (14): see unknowns(model)
  react₊states₊S_Alk(t) [defaults to 1.0]: S_Alk
  react₊states₊S_B(t) [defaults to 1.0]: S_B
  react₊states₊S_BN(t) [defaults to 1.0]: S_BN
  react₊states₊S_N2(t) [defaults to 1.0]: S_N2
  ⋮
Parameters (55): see parameters(model)
  asm₊K_NHxOHO [defaults to 0.05]
  asm₊b_OHO [defaults to 0.62]
  influent₊X_OHO_const₊k [defaults to 28.17]: Constant output value of block
  asm₊COD_S [defaults to 48]
  ⋮
Observed (220): see observed(model)
```
```@setup multiple_process
@named model = ODESystem(eqs, t, systems=sys)
model_simplified = structural_simplify(model)
```

!!! note
    Incredibly, the simplification was able to simplify all of this to `14` differential equation, one for each state in the `ASM1`. And the dissolved oxygen has indeed the rates terms of the ASM *and* the aeration! 

    Check it for yourself:

    Number of equations of the unsimplified model:
    ```jldoctest multiple_process_test
    julia> length(equations(model))
    168
    ```
    Number of equations of the simplified model:
    ```jldoctest multiple_process_test
    julia> length(equations(model_simplified))
    14
    ```
    Check the equations of the simplified model for yourself using 
    ```@example multiple_process
    full_equations(model_simplified)
    ```

Then, the problem can be built and solved:

```jldoctest multiple_process_test; output=false
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)

# output

retcode: Success
Interpolation: 3rd order Hermite
t: 46-element Vector{Float64}:
 0.0
 0.0008389808187078418
 0.0019798868733749675
 0.003565127456521551
 0.005523096160508261
 0.007985024781323895
 0.010958279809112521
 0.01456695768842423
 0.018910348479116948
 0.02419859270667441
 ⋮
 0.5057939260440187
 0.5366823982186049
 0.5720388707628972
 0.6133627414157641
 0.6636705845724261
 0.7276191828931585
 0.8149623804974663
 0.9480693910252629
 1.0
u: 46-element Vector{Vector{Float64}}:
 [1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]
 [1.0919689723032522, 2.054073712871958, 1.0925467801774102, 0.9846473826265947, 1.4681126634660109, 0.9858526474037235, 2.248278265472379, 1.4453444372485518, 4.089565895713854, 1.146072642407445, 0.9848105966984151, 1.4170158892713878, 0.984703155990524, 1.770906577581976]
 [1.2147128954623778, 3.4620993297015863, 1.2154649674517262, 0.964149720754447, 2.0927439792769658, 0.9673744167379842, 3.5628602389322777, 2.04000225484544, 8.213575085072579, 1.3417418873343807, 0.9646310798638335, 1.9742962543703175, 0.9643047307493278, 2.800279765284176]
 [1.380971401387137, 5.370011298849019, 1.3817644774694529, 0.9363808077520215, 2.9386828694977876, 0.9427124959703824, 4.849196363412564, 2.8457522526865535, 13.79902119305279, 1.6070353065183816, 0.937385172601008, 2.730789578710906, 0.9367158358922609, 4.195060796029828]
 [1.5796770985900128, 7.649515101075152, 1.5803893478706499, 0.9031937868529422, 3.9495758669805108, 0.9134980162546964, 5.855452844908887, 3.808974911451117, 20.472214173419978, 1.9241751589262874, 0.9048893555602677, 3.6381823833017317, 0.9038138903034116, 5.8624324329257265]
 [1.819522795437191, 10.397419823047308, 1.8199471593364627, 0.863145837344234, 5.169607838838957, 0.8783592290971195, 6.5713198987021535, 4.9717781324368095, 28.52231390036231, 2.3069036455784313, 0.8657080097927586, 4.739501547823585, 0.8642138049344672, 7.875284905114753]
 [2.0950173730352293, 13.545865183953154, 2.094746206450986, 0.8171679952051139, 6.570809470860467, 0.8379536158894507, 6.998644086051968, 6.3074796654871035, 37.76119848033081, 2.746298735969408, 0.8207149545496918, 6.014626793930268, 0.8188950144794623, 10.187430317498364]
 [2.409722954319815, 17.128123414835844, 2.407953716954925, 0.7646862781397048, 8.171286637160929, 0.7915538817170178, 7.217010440024998, 7.833243126353385, 48.30331749618302, 3.247836535566253, 0.7692955151170375, 7.487181121366211, 0.7673642308168308, 12.828579480791033]
 [2.7617885636772637, 21.112417083600434, 2.7570623376757193, 0.7060379971277069, 9.961671841267606, 0.7391679083712419, 7.304960658806518, 9.539922631559389, 60.07989568366215, 3.808292667617666, 0.7117105811567094, 9.15841042067588, 0.7100469991994061, 15.782900555320047]
 [3.1541534634215598, 25.51689108820476, 3.143882122056421, 0.6407720985408613, 11.957076219091055, 0.6800171948459937, 7.326569287054129, 11.44148730827701, 73.17993336417744, 4.431967409467445, 0.6474274856456175, 11.05588077336362, 0.6466288208352462, 19.074574581914]
 ⋮
 [7.016289728047927, 62.42847770259644, 6.278615303078927, 0.0001536418059506767, 31.792702268301042, 0.00017105999649398896, 7.130930099582145, 29.997525258326355, 197.8305531491418, 10.393289730884819, 0.00011468851448149233, 35.44204657159558, 0.09519791548568719, 51.195716136828686]
 [7.016533306377551, 62.42474908411451, 6.27827856730554, 8.999602136817514e-5, 31.794075793743456, 9.859474442493113e-5, 7.130870128572444, 29.998611434370364, 197.83403327042464, 10.39347630296312, 6.557386164275045e-5, 35.4495785297783, 0.09524807647484862, 51.1975963450149]
 [7.016685115309079, 62.42191051006901, 6.278032690179223, 4.863299731252676e-5, 31.794935818285293, 5.230184299740197e-5, 7.130828522890957, 29.999285561379303, 197.83581150308333, 10.393575364196808, 3.4483971823039266e-5, 35.454820614730814, 0.09528744804019927, 51.19876328211292]
 [7.016772850190289, 62.41988516363, 6.2778641464556255, 2.355450980973275e-5, 31.795435451621604, 2.4785541088980344e-5, 7.130801289108368, 29.999673300168585, 197.8365450609738, 10.393619621967094, 1.6186070046616247e-5, 35.458262562059176, 0.09531633427559705, 51.199434471327024]
 [7.016818899003985, 62.418546500105336, 6.2777570601638795, 9.626865710758382e-6, 31.79569929365873, 9.859756115426885e-6, 7.130784691914713, 29.999875656225086, 197.83671789376055, 10.393633455485823, 6.368320466921315e-6, 35.46036650035765, 0.09533593855769353, 51.199784756637946]
 [7.0168393491464025, 62.41777005903537, 6.277697398136357, 2.993207182036821e-6, 31.795817360141964, 2.956409855006743e-6, 7.130775787694269, 29.99996487500363, 197.83665351141772, 10.393633333197073, 1.883924440824298e-6, 35.46149851913887, 0.09534761219732626, 51.19993919742001]
 [7.016846234192816, 62.41740439027031, 6.27767046984603, 5.403841840016402e-7, 31.795857533801655, 5.026451805611887e-7, 7.130771903334548, 29.999994606765295, 197.83655029246484, 10.393629662139814, 3.139976500101584e-7, 35.46199371549477, 0.09535326355597684, 51.19999066412484]
 [7.016847514612589, 62.417291120047054, 6.277662526034054, 8.444383289851289e-9, 31.79586515213312, 3.70431173110983e-9, 7.130770792497451, 30.000000031050234, 197.83649537846165, 10.39362738482665, 1.659244378718366e-9, 35.4621356977338, 0.09535507121614775, 51.20000005374907]
 [7.016847512137644, 62.41728618647166, 6.277662209911883, 3.204647035763099e-9, 31.79586515064977, 1.3766302574610952e-9, 7.1307707500508934, 30.000000011421857, 197.83649138475982, 10.393627205695866, 6.318149164159596e-10, 35.462141140097884, 0.09535515483910138, 51.20000001977164]
```
```@setup multiple_process
ode_prob = ODEProblem(model_simplified, [], (0, 1))
sol = solve(ode_prob)
```

And the solution plotted:

```jldoctest multiple_process_test; output=false
plot(sol, title="CSTR with ASM1 and aeration", legend=:topright)

# output

Plot{Plots.GRBackend() n=14}
```
```@example multiple_process
plot(sol, title="CSTR with ASM1 and aeration", legend=:topright) # hide
```

Indeed it is visible that there is some oxygen in the tank and as well some heterotrophs (which need oxygen to grow) seem to be present. But, let's have a look at the oxygen concentrations:

```jldoctest multiple_process_test; output=false
plot(sol, idxs=[inflows(react, 1).S_O2, outflows(react, 1).S_O2], label=["Reactor Inflow" "Reactor Outflow"], title="Dissolved oxygen concentration", legend=:right)

# output

Plot{Plots.GRBackend() n=2}
```
```@example multiple_process
plot(sol, idxs=[inflows(react, 1).S_O2, outflows(react, 1).S_O2], label=["Reactor Inflow" "Reactor Outflow"], title="Dissolved oxygen concentration", legend=:right) # hide
```

This makes it evident, that the aeration works and the oxygen is supplied. However, it might be interesting to have a look at the rate of the dissolved oxygen, the total one in the reactor and as well the contributions of the two processes. It is also possible to plot those:

```jldoctest multiple_process_test; output=false
plot(sol, idxs=[rates(react).S_O2, rates(asm).S_O2, rates(aer).S_O], label=["Total Rate" "Rate from ASM1" "Rate from Aeration"], title="Dissolved oxygen reaction rates", legend=:topright)

# output

Plot{Plots.GRBackend() n=3}
```
```@example multiple_process
plot(sol, idxs=[rates(react).S_O2, rates(asm).S_O2, rates(aer).S_O], label=["Total Rate" "Rate from ASM1" "Rate from Aeration"], title="Dissolved oxygen reaction rates", legend=:topright) # hide
```

And as expected, the two individual rates sum up to the total rate in the reactor. 

