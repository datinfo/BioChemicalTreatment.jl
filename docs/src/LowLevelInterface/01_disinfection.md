# Low-Level Disinfection with Ozone

!!! note
	This example is exactly the same as the [corresponding the high level example](@ref "Disinfection with Ozone"). Thus the two interfaces can be compared.

	While it does not make a huge difference for this simple example, the advantages grow already for the [next one](@ref "Low-Level Reactor with multiple Processes: ASM1 with Aeration") and especially for more complex systems.

As a first small example, we consider the following code, which implements a simple connection of two continuously stirred tank reactors (CSTR) as commonly used in water treatment. It serves as a quick example to start with. For more detailed explanations, we referred to the [API of the ProcessSimulator](@ref ProcessSimulator).

This example implements two reactors used for ozonation of water. In the first reactor, the concentration of ozone is kept constant, to simulate a supply of ozone in the beginning. The stoichiometric matrix for the ozonation process is defined in the library and accessible under the name ozonation. This system is depicted as follows:

```@example
using TikzPictures # hide
TikzPicture(L""" %# hide
       \tikzstyle{block} = [draw, rectangle, inner sep=20pt, align=center] %# hide
       \tikzstyle{clarifier} = [draw, regular polygon, regular polygon sides=3, shape border rotate=180, align=center] %# hide
       \node (inf) {Influent}; %# hide
       \node[block, right=2cm of inf] (cstr1) {CSTR 1 with Ozonation\\Constant ozone concentration}; %# hide
       \node[block, right=2cm of cstr1] (cstr2) {CSTR 2 with Ozonation}; %# hide
       \node[right=of cstr2] (eff) {Effluent}; %# hide
 %# hide
       \draw[->] (inf) -- (cstr1) node[pos=0.5]{$q_{in},C_{in}$}; %# hide
       \draw[->] (cstr1) -- (cstr2) node[pos=0.5]{$q_{r},C_{r}$}; %# hide
       \draw[->] (cstr2) -- (eff) node[pos=0.5]{$q_{eff},C_{eff}$}; %# hide
       """; options=">=stealth, auto, node distance=2cm, scale=2, background rectangle/.style={fill=white}, show background rectangle", preamble=raw"\usetikzlibrary{arrows,arrows.meta,bending,positioning,patterns,decorations.pathmorphing,calc,shapes.geometric,backgrounds}") # hide
```

For building up this system, it is proceeded as follows:

## Setup

First, the needed packages are imported and constants defined:

```jldoctest ozone_test; output=false
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting

# Define the needed constants
v = 1000.0/2    # Reactor volume
q = 10000.0/24  # Flow rate through the reactor
kO3 = 10.0/24  # Reaction rate for the decay of ozone
kd = 1500.0/24  # Reaction rate for the decay of the bacteria

# output

62.5
```
```@setup ozone
using BioChemicalTreatment # Reactors etc.
using ModelingToolkit # Modeling Framework
using DifferentialEquations # Solve the equations
using Plots # Plotting

# Define the needed constants
v = 1000.0/2    # Reactor volume
q = 10000.0/24  # Flow rate through the reactor
kO3 = 10.0/24  # Reaction rate for the decay of ozone
kd = 1500.0/24  # Reaction rate for the decay of the bacteria
```
## System Creation

Then, all needed systems (here inflow and the reactors) are created.

We start with the reaction rates system for the disinfection dynamics. This system defines all compounds needed 
for this specific reaction, which are needed afterwards for defining the CSTRs. Thus it is convenient to start with 
this system. We need separate rates systems for each reactor, thus we create two identical ones, one for each CSTR.
For the parameters, we set the decay of ozone in the first reactor to zero, to model that it is supplied in this tank.

```jldoctest ozone_test; output=false
@named disinfection_r1 = OzoneDisinfection(;kO3 = 0, kd)
@named disinfection_r2 = OzoneDisinfection(;kO3, kd)
nameof(disinfection_r2)

# output

:disinfection_r2
```
```@setup ozone
@named disinfection_r1 = OzoneDisinfection(;kO3 = 0, kd)
@named disinfection_r2 = OzoneDisinfection(;kO3, kd)
```
We continue with the two CSTRs. 
Each one is a system with an inflow and outflow port, and further an input with the provided rates
(from the reaction rate system just created) and an output for the states, which are needed by the disinfection
reaction rates system for computation.
To create it we provide it with first the volume of the tank, and second with the compounds to have in the flow.
The compounds in the flow can be extracted as states from the OzoneDisinfection systems above. Here we use the states
of the corresponding rates, which is convenient. Note however, that this is not necessary and the states could as well be constructed independently of the rates.

```jldoctest ozone_test; output=false
@named CSTR1 = CSTR(v, unknowns(states(disinfection_r1)); initial_states = [0.5, 0])
@named CSTR2 = CSTR(v, unknowns(states(disinfection_r2)); initial_states = [0.5, 0])
nameof(CSTR2)

# output

:CSTR2
```
```@setup ozone
@named CSTR1 = CSTR(v, unknowns(states(disinfection_r1)); initial_states = [0.5, 0])
@named CSTR2 = CSTR(v, unknowns(states(disinfection_r2)); initial_states = [0.5, 0])
```
Finally, the influent is to be created:
It needs the variables to provide an input for, and the corresponding values. We take the values directly from the
variables in the inflow of the first CSTR, which are:
```jldoctest ozone_test; output=false
unknowns(inflows(CSTR1)[1])

# output

3-element Vector{SymbolicUtils.BasicSymbolic{Real}}:
 q(t)
 S_O3(t)
 X_B(t)
```
```@example ozone
unknowns(inflows(CSTR1)[1]) # hide
```
To this, the value vector has to be aligned. The ozone concentration is set to 0.5 to
keep this level in the first reactor (Consider the ozone to be supplied before the first reactor)
and the bacteria concentration is set to 1, such that one can interpret the bacteria in the effluent
as the percentage of bacteria that are remaining.

```jldoctest ozone_test; output=false
@named influent = Influent([q, 0.5, 1], unknowns(inflows(CSTR1)[1]))
nameof(influent)

# output

:influent
```
```@setup ozone
@named influent = Influent([q, 0.5, 1], unknowns(inflows(CSTR1)[1]))
```

## Connection

Then, the connection equations for the systems are created. We need on one hand to connect the reaction rate
systems to their reactors, and on the other the flows. For the reaction rates, the states of the reactor
must be connected to the corresponding reaction rates systems, and the rates supplied by the reaction rate system
to the reactor. For the flows, simply inflows and outflows can be connected as desired:

```jldoctest ozone_test; output=false
connection_eqns = [
	# Connect CSTR1 with its rates
	connect(states(CSTR1), states(disinfection_r1)),
	connect(rates(disinfection_r1), rates(CSTR1)),
	# Connect CSTR2 with its rates
	connect(states(CSTR2), states(disinfection_r2)),
	connect(rates(disinfection_r2), rates(CSTR2)),
	# Connect the flows
	connect(outflows(influent)[1], inflows(CSTR1)[1]),
	connect(outflows(CSTR1)[1], inflows(CSTR2)[1]),
]

# output

6-element Vector{Equation}:
 connect()
 connect()
 connect()
 connect()
 connect()
 connect()
```
```@setup ozone
connection_eqns = [
	# Connect CSTR1 with its rates
	connect(states(CSTR1), states(disinfection_r1)),
	connect(rates(disinfection_r1), rates(CSTR1)),
	# Connect CSTR2 with its rates
	connect(states(CSTR2), states(disinfection_r2)),
	connect(rates(disinfection_r2), rates(CSTR2)),
	# Connect the flows
	connect(outflows(influent)[1], inflows(CSTR1)[1]),
	connect(outflows(CSTR1)[1], inflows(CSTR2)[1]),
]
```

With the connections, we can then build the overall model:

```jldoctest ozone_test; output=false
@named system = ODESystem(connection_eqns, t, systems = [
	influent,
	CSTR1, disinfection_r1,
	CSTR2, disinfection_r2
])

# output

Model system:
Subsystems (5): see hierarchy(system)
  influent
  CSTR1
  disinfection_r1
  CSTR2
  ⋮
Equations (37):
  20 standard: see equations(system)
  17 connecting: see equations(expand_connections(system))
Unknowns (37): see unknowns(system)
  influent₊influent_to_outflowport₊outflow₊q(t): Flow Rate
  influent₊influent_to_outflowport₊outflow₊S_O3(t): S_O3 concentration
  influent₊influent_to_outflowport₊outflow₊X_B(t): X_B concentration
  influent₊influent_to_outflowport₊q₊u(t): Inner variable in RealInput q
  ⋮
Parameters (9): see parameters(system)
  influent₊q_const₊k [defaults to 416.667]: Constant output value of block
  influent₊S_O3_const₊k [defaults to 0.5]: Constant output value of block
  influent₊X_B_const₊k [defaults to 1]: Constant output value of block
  CSTR1₊V [defaults to 500.0]
  ⋮
```
```@setup ozone
@named system = ODESystem(connection_eqns, t, systems = [
	influent,
	CSTR1, disinfection_r1,
	CSTR2, disinfection_r2
])
```

And simplify it to get the minimum set of equations:

```jldoctest ozone_test; output=false
system_simplified = structural_simplify(system)
full_equations(system_simplified) # Display the equations

# output

4-element Vector{Equation}:
 Differential(t)(CSTR1₊states₊S_O3(t)) ~ (-influent₊q_const₊k*CSTR1₊states₊S_O3(t)) / CSTR1₊V + (influent₊S_O3_const₊k*influent₊q_const₊k) / CSTR1₊V - disinfection_r1₊kO3*CSTR1₊states₊S_O3(t)
 Differential(t)(CSTR1₊states₊X_B(t)) ~ (influent₊X_B_const₊k*influent₊q_const₊k) / CSTR1₊V + (-influent₊q_const₊k*CSTR1₊states₊X_B(t)) / CSTR1₊V - disinfection_r1₊kd*CSTR1₊states₊X_B(t)*CSTR1₊states₊S_O3(t)
 Differential(t)(CSTR2₊states₊S_O3(t)) ~ (influent₊q_const₊k*CSTR1₊states₊S_O3(t)) / CSTR2₊V + (-influent₊q_const₊k*CSTR2₊states₊S_O3(t)) / CSTR2₊V - disinfection_r2₊kO3*CSTR2₊states₊S_O3(t)
 Differential(t)(CSTR2₊states₊X_B(t)) ~ (-influent₊q_const₊k*CSTR2₊states₊X_B(t)) / CSTR2₊V + (influent₊q_const₊k*CSTR1₊states₊X_B(t)) / CSTR2₊V - disinfection_r2₊kd*CSTR2₊states₊X_B(t)*CSTR2₊states₊S_O3(t)
```
```@example ozone
system_simplified = structural_simplify(system) # hide
full_equations(system_simplified) # Display the equations # hide
```

## Simulation and Plotting

Finally, we simulate and plot the result.

```jldoctest ozone_test; output=false
## Simulate the system (for 1 day)
# First create a ODEProblem for the time span (0, 1) and then solve it
prob = ODEProblem(system_simplified, [], (0, 1), [])
sol = solve(prob)

## Plot the output
plot(sol,
	idxs = [CSTR1.states.X_B ,CSTR2.states.X_B], # Plot bacteria in tank
	legend = :right,
	title = "Remaining fraction of bacteria in each tank")

# output

Plot{Plots.GRBackend() n=2}
```
```@example ozone
## Simulate the system (for 1 day) # hide
# First create a ODEProblem and then solve it # hide
prob = ODEProblem(system_simplified, [], (0, 1), []) # hide
sol = solve(prob) # hide
 # hide
## Plot the ouput # hide
plot(sol, # hide
	idxs = [CSTR1.states.X_B ,CSTR2.states.X_B], # Plot bacteria in tank # hide
	legend = :right, # hide
	title = "Remaining fraction of bacteria in each tank") # hide
```

