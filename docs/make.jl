using BioChemicalTreatment
using Documenter
using DocumenterInterLinks
using DocumenterCitations
using Markdown
using Latexify

DocMeta.setdocmeta!(BioChemicalTreatment, :DocTestSetup, :(using BioChemicalTreatment); recursive=true)

# Bibliography
bib = CitationBibliography(
    joinpath(@__DIR__, "src", "refs.bib");
    style=:authoryear
)

# Get links for external references
links = InterLinks(
    "ModelingToolkit" => "https://docs.sciml.ai/ModelingToolkit/stable/objects.inv",
    "ModelingToolkitStandardLibrary" => "https://docs.sciml.ai/ModelingToolkitStandardLibrary/stable/objects.inv",
    "XLSX" => "https://felipenoris.github.io/XLSX.jl/stable/objects.inv"
    )

# Get filenames of examples, ignore hidden files (starting with .)
examples_ProcessSimulator = "examples/ProcessSimulator/" .* filter(!startswith("."), readdir(joinpath(@__DIR__, "src/examples/ProcessSimulator")))
examples_Reactions = "examples/Reactions/" .* filter(!startswith("."), readdir(joinpath(@__DIR__, "src/examples/Reactions")))


# Get filenames of Low Level interface examples, ignore hidden files (starting with .)
lowLevelInterface = "LowLevelInterface/" .* filter(!startswith("."), readdir(joinpath(@__DIR__, "src/LowLevelInterface")))

# Clean up local model description files if they are there, but warn the user about
if ispath(joinpath(@__DIR__, "src/models/")) && !isempty(readdir(joinpath(@__DIR__, "src/models/")))
    @warn "There are model descriptions generated for the matrix-defined models already in the documentation folder. 
        This usually happens when the previous build of the documentation failed. If there are files with the names of the models, 
        the following build of the documentation will fail. If you haven't made any manual changes there, it should be fine to remove 
        them."
    print("Do you want to remove all files in $(joinpath(@__DIR__, "src/models/"))? [Y/n] ")
    confirm = chomp(readline())
    if confirm == "Y" || isempty(confirm)
        @info "Removing the model description files."
        rm(joinpath(@__DIR__, "src/models/"), recursive=true)
    else
        @info "Keeping all files."
    end
end

# Get all model descriptions from models defined as tables. Each model defined such must have a description
# Model descriptions are markdown files located in the model folder and must have the same name as model folder followed by .md
available_models = readdir(joinpath(@__DIR__, "..", "src/Reactions/models/"))
# Temporarily copy all model description files to docs for reading in
mkpath(joinpath(@__DIR__, "src/models"))
hardlink.(joinpath.(@__DIR__, "../src/Reactions/models/" .* available_models, available_models .* ".md"), joinpath.(@__DIR__, "src/models/", available_models .* ".md"))
# Add full path to available_models
available_model_links = "models/" .* available_models .* ".md"
# Extract all headings for the models
markdown_contents = [Markdown.parse(join(readlines(joinpath(@__DIR__, "src", file)), "\n")) for file in available_model_links]
model_headings = [md[findfirst(x -> isa(x, Markdown.Header), md.content)].text[1] for md in markdown_contents]

# Assemble Markdown tables for model properties to print in associated files
state_descriptions = Dict()
parameter_descriptions = Dict()
processrate_descriptions = Dict()
stoichmat = Dict()
compositionmat = Dict()
for model in available_models
    # Read in model
    m = Reactions.BioChemicalReactionSystem(joinpath(@__DIR__, "..", "src/Reactions/models/", model), model)
    # Set up state descriptions
    stateprops = getindex.(Ref(m.state_properties), m.states)
    tab = Markdown.Table(
        eachrow(hcat(
            ["Name"; "``" .* latexify.(m.states; env=:raw) .* "``"], 
            ["Description"; getproperty.(stateprops, :description)], 
            ["Particle Size"; getproperty.(stateprops, :particle_size)]
        )), 
        repeat([:l], length(m.states)+1)
        )
    state_descriptions[Symbol(model)] = Markdown.parse(string(Markdown.MD(tab)))
    # Set up parameter descriptions
    paramprops = getindex.(Ref(m.parameter_properties), m.parameters)
    tab = Markdown.Table(
        eachrow(hcat(
            ["Name"; "``" .* latexify.(m.parameters; env=:raw) .* "``"], 
            ["Description"; getproperty.(paramprops, :description)], 
            ["Default Value"; "``" .* latexify.(broadcast(x -> isempty(x[:value]) ? x[:expression] : x[:value], paramprops); env=:raw) .* "``"], 
            ["Unit"; getproperty.(paramprops, :unit_description)], 
            ["Temperature"; getproperty.(paramprops, :temperature)]
        )), 
        repeat([:l], length(m.parameters)+1)
        )
    parameter_descriptions[Symbol(model)] = Markdown.parse(string(Markdown.MD(tab)))
    # Set up processrate descriptions
    processrateprops = getindex.(Ref(m.processrates_properties), m.processrates)
    tab = Markdown.Table(
        eachrow(hcat(
            ["Name"; "``" .* latexify.(m.processrates; env=:raw) .* "``"], 
            ["Description"; getproperty.(processrateprops, :description)], 
            ["Equation"; "``" .* latexify.(getindex.(Ref(m.processrate_eqns), m.processrates); env=:raw) .* "``"]
        )), 
        repeat([:l], length(m.processrates)+1)
        )
    processrate_descriptions[Symbol(model)] = Markdown.parse(string(Markdown.MD(tab)))
    # Set up Stoichiometric matrix
    tab = Markdown.Table(
        eachrow(
            hcat(vcat("``" .* latexify.(m.states'; env=:raw) .* "``", "``" .* latexify.(m.stoichmat; env=:raw) .* "``"), [""; "``" .* latexify.(m.processrates; env=:raw) .* " = " .* latexify.(getindex.(Ref(m.processrate_eqns), m.processrates); env=:raw) .* "``"])
        ), 
        [repeat([:c], length(m.states)); :l]
        )
    stoichmat[Symbol(model)] = Markdown.parse(string(Markdown.MD(tab)))
    # Set up Composition matrix
    tab = Markdown.Table(
        eachrow(
            hcat([""; string.(m.composition_names)], vcat("``" .* latexify.(m.states'; env=:raw) .* "``", "``" .* latexify.(m.compositionmat; env=:raw) .* "``"))
        ), 
        [:l; repeat([:c], length(m.states))]
        )
    compositionmat[Symbol(model)] = Markdown.parse(string(Markdown.MD(tab)))
end

makedocs(;
    modules=[BioChemicalTreatment],
    authors="Florian Wenk and Juan Pablo Carbajal and Andreas Froemelt and Mariane Schneider",
    repo="https://gitlab.com/datinfo/BioChemicalTreatment.jl/blob/{commit}{path}#{line}",
    sitename="BioChemicalTreatment.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        canonical="https://datinfo.gitlab.io/BioChemicalTreatment.jl",
        edit_link="main",
        assets=String[],
        repolink="https://gitlab.com/datinfo/BioChemicalTreatment.jl",
        size_threshold_ignore= ["examples/*"],  # see https://github.com/JuliaDocs/Documenter.jl/blob/master/CHANGELOG.md#added-1
        #example_size_threshold = 10000000
    ),
    doctest = false,
    pages=[
        "Home" => "index.md",
        "Usage" => "usage.md",
        "Examples" => [
            "ProcessSimulator" => examples_ProcessSimulator,
            "Reactions" => examples_Reactions
            ],
        "Systems" => [
            "Reactors" => "systems/reactors.md",
            "Process Models" => "systems/processes.md",
            "Matrix-Defined Process Models" => available_model_links,
            "Flowelements" => "systems/flowelements.md",
            "ModelingToolkitStandardLibrary Compatibility" => "systems/mtkstdlibcompat.md",
        ],
        "Low Level Interface" => [
            "LowLevelInterface/.00_lowlevelIntro.md",
            "Examples" => lowLevelInterface
        ],
        "Extension Reference" => [
            "Reactors" => "extension/reactors_ext.md",
            "Processes" => "extension/processes_ext.md"
        ],
        "API" => [
            "api_ProcessSimulator.md",
            "api_Reactions.md"
        ],
        "Internals" => [
            "Internal API" => "api_internal.md"
        ],
        "Bibliography" => "bibliography.md"
    ],
    pagesonly = true,
    warnonly = true,  # warn only about failing docs https://github.com/JuliaDocs/Documenter.jl/blob/master/CHANGELOG.md#breaking
    plugins=[
        links,
        bib
    ]
)

deploydocs(
    repo = "gitlab.com/datinfo/BioChemicalTreatment.jl",
    branch = "gl-pages",
    devbranch = "main"
)

# Clean up local model description files
rm.(joinpath.(@__DIR__, "src/models/", available_models .* ".md"))

nothing #prevent output from rm

