module XLSXExt
    using BioChemicalTreatment
    using XLSX

    function BioChemicalTreatment.Reactions.BioChemicalReactionSystem(::Val{:xlsx}, file_path::AbstractString;
        states_sheet::AbstractString="states", parameters_sheet::AbstractString="parameters", 
        processrates_sheet::AbstractString="processrates", matrix_sheet::AbstractString="matrix", 
        compositionmatrix_sheet::AbstractString="compositionmatrix", 
        name::Symbol=:MatrixDefinedReaction)
    
        xf = XLSX.readxlsx(file_path)
        
        # Read in all sheets

        !XLSX.hassheet(xf, states_sheet) && throw(ArgumentError("Error reading model from $file_path. The file does not have the sheet for the 'states' (specified to be called '$states'). Either add the proper sheet or check the sheet naming."))
        states_file = map(x -> ismissing(x) ? "" : x, xf[states_sheet][:])
        !XLSX.hassheet(xf, parameters_sheet) && throw(ArgumentError("Error reading model from $file_path. The file does not have the sheet for the 'parameters' (specified to be called '$parameters'). Either add the proper sheet or check the sheet naming."))
        parameters_file = map(x -> ismissing(x) ? "" : x, xf[parameters_sheet][:])
        !XLSX.hassheet(xf, processrates_sheet) && throw(ArgumentError("Error reading model from $file_path. The file does not have the sheet for the 'rates' (specified to be called '$rates'). Either add the proper sheet or check the sheet naming."))
        rates_file = map(x -> ismissing(x) ? "" : x, xf[processrates_sheet][:])
        !XLSX.hassheet(xf, matrix_sheet) && throw(ArgumentError("Error reading model from $file_path. The file does not have the sheet for the 'stoichmat' (specified to be called '$stoichmat'). Either add the proper sheet or check the sheet naming."))
        stoichmat_file = map(x -> ismissing(x) ? "" : x, xf[matrix_sheet][:])
        !XLSX.hassheet(xf, compositionmatrix_sheet) && throw(ArgumentError("Error reading model from $file_path. The file does not have the sheet for the 'compositionmat' (specified to be called '$compositionmat'). Either add the proper sheet or check the sheet naming."))
        compositionmat_file = map(x -> ismissing(x) ? "" : x, xf[compositionmatrix_sheet][:])

        return BioChemicalTreatment.Reactions.BioChemicalReactionSystem(states_file, parameters_file, rates_file, stoichmat_file, compositionmat_file; name)
    end
end