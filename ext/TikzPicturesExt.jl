"""
Extension to allow printing the process diagrams to svg using latex, if TikzPictures is loaded.
"""
module TikzPicturesExt

    using BioChemicalTreatment
    using TikzPictures

    showable(::MIME"image/svg+xml", pd::ProcessDiagram) = true

    function assemble_tikzpicture(pd::ProcessDiagram)
        TikzPicture(ProcessSimulator.get_tikzpicture_body(pd); 
            options=ProcessSimulator.get_tikzpicture_options(pd), 
            preamble=ProcessSimulator.get_latex_preamble(pd; include_tikz = false)
            )
    end

    function Base.show(f::IO, ::MIME"image/svg+xml", pd::ProcessDiagram)
        # Print it
        Base.show(f, MIME"image/svg+xml"(), assemble_tikzpicture(pd))
    end

    TikzPictures.save(f::Union{TEX,TIKZ}, pd::ProcessDiagram) = save(f, assemble_tikzpicture(pd))
    TikzPictures.save(f::SVG, pd::ProcessDiagram) = save(f, assemble_tikzpicture(pd))
    TikzPictures.save(f::PDF, pd::ProcessDiagram) = save(f, assemble_tikzpicture(pd))
end